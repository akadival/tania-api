<?php

namespace AppBundle\APIResponse;

class Success extends MainResponse
{
    /**
     * @param string $message
     */
    public function __construct($message = 'Success.')
    {
        $this->message = $message;
    }

}