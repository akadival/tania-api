<?php

namespace AppBundle\APIResponse\Reason;

use Symfony\Component\Validator\Constraints as Assert;

class Reason
{
    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $title;

}
