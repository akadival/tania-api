<?php

namespace AppBundle\APIResponse\Reason;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\APIResponse\Success;

class ReasonSuccess extends Success
{
    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Reason")
     * })
     */
    public $reasons = array();
}
