<?php

namespace AppBundle\APIResponse;

class SuccessWithData extends MainResponse
{
    /**
     * @param object|null $data
     * @param string $message
     */
    public function __construct($data = null, $message = "Success.")
    {
        $this->message = $message;
        $this->data = $data;
    }

}