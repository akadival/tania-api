<?php

namespace AppBundle\APIResponse\DeviceNotification;

use Symfony\Component\Validator\Constraints as Assert;

class DeviceNotificationList extends \AppBundle\APIResponse\Success
{

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\DeviceNotification\DeviceNotification")
     * })
     */
    public $notifications = array();

}
