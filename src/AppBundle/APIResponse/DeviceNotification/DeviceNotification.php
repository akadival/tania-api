<?php

namespace AppBundle\APIResponse\DeviceNotification;

use Symfony\Component\Validator\Constraints as Assert;

class DeviceNotification
{

    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $type;

    /**
     * @Assert\Type(type="string")
     */
    public $titleAr;

    /**
     * @Assert\Type(type="string")
     */
    public $titleEn;

    /**
     * @Assert\Type(type="string")
     */
    public $bodyAr;

    /**
     * @Assert\Type(type="string")
     */
    public $bodyEn;

    /**
     * @Assert\Type(type="array")
     */
    public $data;

    /**
     * @Assert\Type(type="numeric")
     */
    public $createdDate;

}
