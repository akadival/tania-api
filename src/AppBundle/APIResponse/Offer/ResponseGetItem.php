<?php

namespace AppBundle\APIResponse\Offer;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseGetItem
{
    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $name;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $count;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $price;

    /**
     * @Assert\Type(type="string")
     */
    public $image;

}
