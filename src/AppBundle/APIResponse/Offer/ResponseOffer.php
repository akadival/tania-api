<?php

namespace AppBundle\APIResponse\Offer;
use Symfony\Component\Validator\Constraints as Assert;

class ResponseOffer
{

    /**
     * @Assert\Type(type = "integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $title;

    /**
     * @Assert\Type(type="string")
     */
    public $titleAr;

    /**
     * @Assert\Type(type="string")
     */
    public $titleEn;

    /**
     * @Assert\Type(type="string")
     */
    public $description;

    /**
     * @Assert\Type(type="string")
     */
    public $descriptionAr;

    /**
     * @Assert\Type(type="string")
     */
    public $descriptionEn;

    /**
     * @Assert\Type(type="string")
     */
    public $type;

    /**
     * @Assert\Type(type="string")
     */
    public $image;

    /**
     * @Assert\Type(type="numeric")
     */
    public $startTime;

    /**
     * @Assert\Type(type="numeric")
     */
    public $expiryTime;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Offer\ResponseBuyItem")
     * })
     */
    public $buyItems;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Offer\ResponseGetItem")
     * })
     */
    public $getItems;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $getAmount;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $buyItemsCost = 0;

    /**
     * @Assert\Type("integer")
     */
    public $count = 1;
    /**
     * @Assert\Type("integer")
     */
    public $numberOfUsedTimes;

}
