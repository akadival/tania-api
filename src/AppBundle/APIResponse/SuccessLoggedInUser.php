<?php

namespace AppBundle\APIResponse;

use AppBundle\APIResponse\Success as ToolsSuccessResponse;
use Symfony\Component\Validator\Constraints as Assert;

class SuccessLoggedInUser extends ToolsSuccessResponse
{

    /**
     * @Assert\Type(type="LoggedInUser")
     */
    public $user;

}
