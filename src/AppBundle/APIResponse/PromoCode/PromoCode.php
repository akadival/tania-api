<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\APIResponse\PromoCode;

/**
 * Description of PromoCode
 *
 * @author root
 */
class PromoCode {
    public $id;
    public $code;
    public $type;
    public $title;
    public $discountAmount;
}
