<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\APIResponse\PromoCode;


use Symfony\Component\Validator\Constraints as Assert;


/**
 * Description of PromoCodeResponse
 *
 * @author root
 */
class PromoCodeResponse {

   /**
     * @Assert\Type(type="string")
     */
    public $promoCode;
}
