<?php


namespace AppBundle\APIResponse\PromoCode;


use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of PromoCodeResponse
 *
 * @author abdelrahman.allam
 */
class ValidatePromoCodeResponse {

    /**
     * @Assert\Type(type="boolean")
     */
    public $valid = true;

    /**
     * @Assert\Type(type="string")
     */
    public $message = 'Success';

    /**
     * @Assert\Type(type="AppBundle\APIResponse\PromoCode\PromoCode")
     */
    public $promoCode;
}
