<?php

namespace AppBundle\APIResponse\Shift;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseShiftDaysList extends \AppBundle\APIResponse\Success
{

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Shift\ResponseShiftDay")
     * })
     */
    public $days = array();

}
