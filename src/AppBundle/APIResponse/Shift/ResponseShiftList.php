<?php

namespace AppBundle\APIResponse\Shift;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseShiftList extends \AppBundle\APIResponse\Success
{

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Shift\ResponseShift")
     * })
     */
    public $shifts = array();

}
