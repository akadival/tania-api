<?php

namespace AppBundle\APIResponse\Shift;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseShiftDay
{
    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $day;

    /**
     * @Assert\Type(type="boolean")
     */
    public $active=true;

}
