<?php

namespace AppBundle\APIResponse\Shift;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseShift
{
    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $shift;
    
    /**
     * @Assert\Type(type="string")
     */
    public $day=null;
    
    /**
     * @Assert\Type(type="integer")
     */
    public $dayId=null;
    
    /**
     * @Assert\Type(type="boolean")
     */
    public $dayActive = true;
    
    /**
     * @Assert\Type(type="string")
     */
    public $from;

    /**
     * @Assert\Type(type="string")
     */
    public $to;

    /**
     * @Assert\Type(type="boolean")
     */
    public $disabled = false;

    /**
     * @Assert\Type(type="integer")
     */
    public $maximumAllowedOrdersPerDay=null;

    /**
     * @Assert\Type(type="integer")
     */
    public $ordersFound=0;

}
