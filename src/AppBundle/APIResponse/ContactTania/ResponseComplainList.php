<?php

namespace AppBundle\APIResponse\ContactTania;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseComplainList extends \AppBundle\APIResponse\Success
{

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\ContactTania\ResponseComplain")
     * })
     */
    public $complains = array();

}
