<?php

namespace AppBundle\APIResponse\ContactTania;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseSuggestionList extends \AppBundle\APIResponse\Success
{

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\ContactTania\ResponseSuggestion")
     * })
     */
    public $suggestions = array();

}
