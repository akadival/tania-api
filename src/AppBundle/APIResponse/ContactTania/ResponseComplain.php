<?php

namespace AppBundle\APIResponse\ContactTania;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseComplain
{
    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $title;

    /**
     * @Assert\Type(type="string")
     */
    public $description;

    /**
     * @Assert\Type(type="string")
     */
    public $order;

}
