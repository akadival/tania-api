<?php

namespace AppBundle\APIResponse\ContactTania;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseSuggestion
{
    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $title;

    /**
     * @Assert\Type(type="string")
     */
    public $description;

}
