<?php

namespace AppBundle\APIResponse\Package;

use Symfony\Component\Validator\Constraints as Assert;

class RequestRedeem
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     */
    public $packageId;
}
