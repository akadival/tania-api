<?php

namespace AppBundle\APIResponse\Package;
use Symfony\Component\Validator\Constraints as Assert;

class ResponsePackage
{

    /**
     * @Assert\Type(type = "integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $title;

    /**
     * @Assert\Type(type="string")
     */
    public $description;

    /**
     * @Assert\Type(type="string")
     */
    public $image;

    /**
     * @Assert\Type(type="numeric")
     */
    public $startTime;

    /**
     * @Assert\Type(type="numeric")
     */
    public $expiryTime;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Package\ResponseBuyItem")
     * })
     */
    public $buyItems;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $getAmount;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $buyItemsCost = 0;

    /**
     * @Assert\Type("integer")
     */
    public $count = 1;
    /**
     * @Assert\Type("integer")
     */
    public $numberOfUsedTimes;

}
