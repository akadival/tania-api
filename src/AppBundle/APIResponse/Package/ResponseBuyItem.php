<?php

namespace AppBundle\APIResponse\Package;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseBuyItem
{
    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $name;

    /**
     * @Assert\Type(type="string")
     */
    public $nameAr;

    /**
     * @Assert\Type(type="string")
     */
    public $nameEn;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $count;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $price;

    /**
     * @Assert\Type(type="string")
     */
    public $image;

}
