<?php

namespace AppBundle\APIResponse\RatingTag;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseRatingTag {

    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $name;

}
