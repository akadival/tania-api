<?php

namespace AppBundle\APIResponse\RatingTag;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseRatingTagList extends \AppBundle\APIResponse\Success
{

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\RatingTag\ResponseRatingTag")
     * })
     */
    public $tags = array();

}
