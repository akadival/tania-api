<?php

namespace AppBundle\APIResponse\Payment;

use Symfony\Component\Validator\Constraints as Assert;

class RequestPayOneNonRecurring
{

    /**
     * @Assert\Type(type="string")
     */
    public $token_name;

    /**
     * @Assert\Type(type="string")
     */
    public $merchant_reference;

    /**
     * @Assert\Type(type="string")
     */
    public $return_url;

}
