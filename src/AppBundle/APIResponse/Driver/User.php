<?php

namespace AppBundle\APIResponse\Driver;

use Symfony\Component\Validator\Constraints as Assert;

class User
{

    /**
     * @Assert\Type(type="string")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $fullName;

    /**
     * @Assert\Type(type="string")
     */
    public $phone;


}
