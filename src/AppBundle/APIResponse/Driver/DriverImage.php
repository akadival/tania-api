<?php

namespace AppBundle\APIResponse\Driver;

use Ibtikar\ShareEconomyToolsBundle\APIResponse\Success;
use Symfony\Component\Validator\Constraints as Assert;

class DriverImage extends Success
{

    /**
     * @Assert\Type(type="string")
     */
    public $image;

}
