<?php

namespace AppBundle\APIResponse\Driver;

use Symfony\Component\Validator\Constraints as Assert;

class Driver
{

    /**
     * @Assert\Type(type="integer")
     */
    public $driverId;

    /**
     * @Assert\Type(type="string")
     */
    public $fullName;

    /**
     * @Assert\Type(type="string")
     */
    public $fullNameAr;

    /**
     * @Assert\Type(type="string")
     */
    public $phone;

    /**
     * @Assert\Type(type="string")
     */
    public $username;

    /**
     * @Assert\Type(type="string")
     */
    public $email;

    /**
     * @Assert\Type(type="string")
     */
    public $image;

    /**
     * @Assert\Type(type="numeric")
     */
    public $cityAreaId;

    /**
     * @Assert\Type(type="numeric")
     */
    public $cityId;

    /**
     * @Assert\Type(type="numeric")
     */
    public $rate;

    /**
     * @Assert\Type(type="string")
     */
    public $vanNumber;

    /**
     * @Assert\Type(type="numeric")
     */
    public $isActive;

    /**
     * @Assert\Type(type="string")
     */
    public $locale;
}
