<?php

namespace AppBundle\APIResponse\Masjed;

use Ibtikar\TaniaModelBundle\Entity\UserAddress;
use Symfony\Component\Validator\Constraints as Assert;

class ResponseMasjed
{
    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $title;

    /**
     * @Assert\Type(type="string")
     */
    public $address;

    /**
     * @Assert\Type(type="numeric")
     */
    public $longitude;

    /**
     * @Assert\Type(type="numeric")
     */
    public $latitude;

    /**
     * @Assert\Type(type="string")
     */
    public $type = UserAddress::TYPE_MASAJED;

    /**
     * @Assert\Type(type="numeric")
     */
    public $capacity;


}
