<?php

namespace AppBundle\APIResponse\Masjed;

use AppBundle\APIResponse\MetaData;
use Symfony\Component\Validator\Constraints as Assert;

class ResponseMasjedList
{

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Masjed\ResponseMasjed")
     * })
     */
    public $masajed = array();

    /**
     * @Assert\Type(type="AppBundle\APIResponse\MetaData")
     */
    public $metadata;

    public function __construct($currentPage = 1, $isNextPage = false)
    {
        $this->metadata = new MetaData();
        $this->metadata->currentPage = $currentPage;
        $this->metadata->isNextPage = $isNextPage;
    }

}
