<?php

namespace AppBundle\APIResponse\Masjed;

use Symfony\Component\Validator\Constraints as Assert;

class RequestMasjed
{

    /**
     * @Assert\Type("numeric")
     * @Assert\Length(max=20)
     */
    public $longitude;

    /**
     * @Assert\Type("numeric")
     * @Assert\Length(max=20)
     */
    public $latitude;

}
