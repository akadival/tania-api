<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseOrder extends \AppBundle\APIResponse\Success
{

    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="numeric")
     */
    public $totalPrice;

}
