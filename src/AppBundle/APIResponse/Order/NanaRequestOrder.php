<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class NanaRequestOrder
{

    /**
     * @Assert\Type("string")
     */
    public $notes;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Order\NanaRequestOrderItem")
     * })
     */
    public $items;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\Customer")
     */
    public $customer;

    /**
     * Assert\Type(type = "integer")
     */
//    public $addressId;

    /**
     * Assert\Type(type="numeric")
     */
//    public $lat;

    /**
     * Assert\Type(type="numeric")
     */
//    public $long;

    /**
     * Assert\Type("numeric")
     * Assert\NotBlank
     */
//    public $totalPrice;

    /**
     * Assert\Type("integer")
     */
//    public $shiftId;

    /**
     * Assert\Type(type="AppBundle\APIResponse\Order\PaymentMethod")
     */
//    public $paymentMethod;

    /**
     * Assert\Type("integer")
     */
//    public $receivingDate;

    /**
     * Assert\Type(type="AppBundle\APIResponse\Order\Receipt")
     * Assert\NotBlank
     */
//    public $receipt;

}
