<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class NanaRequestOrderItem
{
    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $sku;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $quantity;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $price;

}
