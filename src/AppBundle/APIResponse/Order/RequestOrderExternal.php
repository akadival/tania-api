<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class RequestOrderExternal
{

    /**
     * @Assert\Type(type = "integer")
     */
    public $id;

    /**
     * @Assert\Type(type="numeric")
     */
    public $latitude;

    /**
     * @Assert\Type(type="numeric")
     */
    public $longitude;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\User\User")
     */
    public $user;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Order\RequestOrderItem")
     * })
     */
    public $items;

    /**
     * @Assert\Type("integer")
     */
    public $shiftId;

    /**
     * @Assert\Type("string")
     */
    public $note;

    /**
     * @Assert\Type("integer")
     */
    public $receivingDate;

}
