<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseOrderListData
{

    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="numeric")
     */
    public $totalPrice;

    /**
     * @Assert\Type(type="numeric")
     */
    public $createdDate;

    /**
     * @Assert\Type(type="string")
     */
    public $status;

    /**
     * @Assert\Type(type="numeric")
     */
    public $receivingDate;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\Shift")
     */
    public $shift;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\Shift")
     */
    public $paymentMethod;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\ResponseOrderDriver")
     */
    public $driver;

    /**
     * @Assert\Type(type="integer")
     */
    public $addressId;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\Address")
     */
    public $address;

    /**
     * @Assert\Type(type="numeric")
     */
    public $rate;

    /**
     * @Assert\Type(type="string")
     */
    public $rateComment;

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\RatingTag\ResponseRatingTag")
     * })
     */
    public $ratingTags;
    
    /**
     * @Assert\Type(type="string")
     */
    public $note;

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Order\ResponseOrderListItem")
     * })
     */
    public $items = array();

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Offer\ResponseOffer")
     * })
     */
    public $offers = array();

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\Receipt")
     */
    public $receipt;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\PromoCode\PromoCode")
     */
    public $promoCode;
}
