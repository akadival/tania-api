<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class BalanceOrder
{

    /**
     * @Assert\Type(type = "integer")
     */
    public $addressId;

    /**
     * @Assert\Type(type="numeric")
     */
    public $lat;

    /**
     * @Assert\Type(type="numeric")
     */
    public $long;

    /**
     * @Assert\Type("integer")
     * @Assert\NotBlank
     */
    public $shiftId;

    /**
     * @Assert\Valid
     * @Assert\Type(type="AppBundle\APIResponse\Order\PaymentMethod")
     */
    public $paymentMethod;

    /**
     * @Assert\Type("string")
     */
    public $note;

    /**
     * @Assert\Type("integer")
     */
    public $receivingDate;

    /**
     * @Assert\Type("integer")
     * @Assert\NotBlank
     */
    public $balanceId;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\Receipt")
     * @Assert\NotBlank
     */
    public $receipt;

}
