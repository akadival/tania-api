<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class PaymentMethod
{

    /**
     * @Assert\Type(type="string")
     * @Assert\Choice({"CASH", "SADAD"}, groups={"balance"})
     * @Assert\NotBlank
     */
    public $type;

    /**
     * @Assert\Type(type="numeric")
     */
    public $id;


    /**
     * @Assert\Type(type="string")
     */
    public $cardNumber;

    /**
     * @Assert\Type(type="numeric")
     */
    public $value;

    /**
     * @Assert\Type(type="numeric")
     */
    public $expiryDate;

    /**
     * @Assert\Type(type="bool")
     */
    public $isDefault;

    /**
     * @Assert\Type(type="string")
     */
    public $merchantReference;

    /**
     * @Assert\Type(type="string")
     */
    public $paymentOption;

    /**
     * @Assert\Type(type="numeric")
     */
    public $fortId;

    /**
     * @Assert\Type(type="string")
     */
    public $tokenName;

       /**
     * @Assert\Type(type="string")
     */
    public $cardSecurityCode;
}
