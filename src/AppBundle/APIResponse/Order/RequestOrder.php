<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class RequestOrder
{
    /**
     * @Assert\Type(type = "integer")
     */
    public $orderId;

    /**
     * @Assert\Type(type = "integer")
     */
    public $addressId;

    /**
     * @Assert\Type(type="numeric")
     */
    public $lat;

    /**
     * @Assert\Type(type="numeric")
     */
    public $long;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Order\RequestOrderItem")
     * })
     */
    public $items;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Offer\ResponseOffer")
     * })
     */
    public $offers;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank
     */
    public $totalPrice;

    /**
     * @Assert\Type("integer")
     */
    public $shiftId;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\PaymentMethod")
     */
    public $paymentMethod;

    /**
     * @Assert\Type("string")
     */
    public $note;

    /**
     * @Assert\Type("integer")
     */
    public $receivingDate;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\Receipt")
     * @Assert\NotBlank
     */
    public $receipt;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\PromoCode\PromoCode")
     */
    public $promoCode;

    /**
     * @Assert\Type(type="boolean")
     */
    public $isPaymentChanged;

         /**
     * @Assert\Type("string")
     */
    public $source;

    /**
     * @Assert\Type(type = "integer")
     */
    public $created_by;

     /**
     * @Assert\Type(type = "integer")
     */
    public $createdBy;

         /**
     * @Assert\Type("string")
     */
    public $created_by_fullName;


    /**
     * @Assert\Type("integer")
     */
    public $address_verified;
}
