<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class Address
{

    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $title;

    /**
     * @Assert\Type(type="text")
     */
    public $address;

    /**
     * @Assert\Type(type="numeric")
     */
    public $lat;

    /**
     * @Assert\Type(type="numeric")
     */
    public $long;

    /**
     * @Assert\Type(type="numeric")
     */
    public $type;

    /**
     * @Assert\Type(type="numeric")
     */
    public $capacity;

    /**
     * @Assert\Type(type="numeric")
     */
    public $distance;

}
