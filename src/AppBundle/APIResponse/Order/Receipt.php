<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class Receipt
{
    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $totalPrice;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $finalPrice;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $taxFees;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $discount = 0;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $offerDiscount = 0;
}
