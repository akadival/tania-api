<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class RequestEditOrder
{

    /**
     * @Assert\Type(type = "integer")
     */
    public $addressId;

    /**
     * @Assert\Type("string")
     */
    public $lat;

    /**
     * @Assert\Type("string")
     */
    public $long;
    
    /**
     * @Assert\Type(type = "integer")
     */
    public $orderId;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Order\RequestOrderItem")
     * })
     */
    public $items;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Offer\ResponseOffer")
     * })
     */
    public $offers;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank
     */
    public $totalPrice;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\PaymentMethod")
     */
    public $paymentMethod;

    /**
     * @Assert\Type("string")
     */
    public $note;

    /**
     * @Assert\Type("integer")
     */
    public $receivingDate;

    /**
     * @Assert\Type("integer")
     * @Assert\NotBlank
     */
    public $shiftId;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Order\Receipt")
     */
    public $receipt;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\PromoCode\PromoCode")
     */
    public $promoCode;

        /**
     * @Assert\Type("string")
     */
    public $source;


     /**
     * @Assert\Type(type = "integer")
     */
    public $created_by;


}
