<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class RequestOrderItem
{
    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $name;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $count;

    /**
     * @Assert\Type(type="numeric")
     * @Assert\NotBlank
     */
    public $price;

    /**
     * @Assert\Type(type="string")
     */
    public $image;

}
