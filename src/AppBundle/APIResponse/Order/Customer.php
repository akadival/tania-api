<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class Customer
{

    /**
     * @Assert\Type(type="string")
     */
    public $name;

    /**
     * @Assert\Type(type="string")
     */
    public $phone;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\User\CustomerAddress")
     */
    public $address;

}
