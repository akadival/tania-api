<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class Shift
{

    /**
     * @Assert\Type(type="integer")
     */
    public $id;


    /**
     * @Assert\Type(type="string")
     */
    public $shift;

    /**
     * @Assert\Type(type="numeric")
     */
    public $from;

    /**
     * @Assert\Type(type="numeric")
     */
    public $to;
}
