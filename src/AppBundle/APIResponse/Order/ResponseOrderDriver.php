<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseOrderDriver
{

    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $fullName;

    /**
     * @Assert\Type(type="string")
     */
    public $fullNameAr;

    /**
     * @Assert\Type(type="string")
     */
    public $phone;

    /**
     * @Assert\Type(type="string")
     */
    public $username;

    /**
     * @Assert\Type(type="string")
     */
    public $image;

    /**
     * @Assert\Type(type="numeric")
     */
    public $rate;

    /**
     * @Assert\Type(type="string")
     */
    public $vanNumber;

}
