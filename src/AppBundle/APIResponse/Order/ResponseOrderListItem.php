<?php

namespace AppBundle\APIResponse\Order;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseOrderListItem
{
    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $name;

    /**
     * @Assert\Type(type="numeric")
     */
    public $count;

    /**
     * @Assert\Type(type="numeric")
     */
    public $price;

    /**
     * @Assert\Type(type="string")
     */
    public $image;


    /**
     * @Assert\Type(type="numeric")
     */
    public $minimumAmountToOrder;

}
