<?php

namespace AppBundle\APIResponse;

use Symfony\Component\Validator\Constraints as Assert;

class MetaData
{

    /**
     * @Assert\Type(type="numeric")
     */
    public $currentPage;

    /**
     * @Assert\Type(type="boolean")
     */
    public $isNextPage;

}