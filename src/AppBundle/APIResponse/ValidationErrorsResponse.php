<?php

namespace AppBundle\APIResponse;

use Symfony\Component\Validator\Constraints as Assert;

class ValidationErrorsResponse extends MainResponse
{
    /**
     * @Assert\Type("integer")
     */
    public $code = 400;

    /**
     * @Assert\Type("boolean")
     */
    public $status = false;

    /**
     * @Assert\Type("array")
     */
    public $errors = [];

}