<?php

namespace AppBundle\APIResponse;

use Symfony\Component\Validator\Constraints as Assert;

class User
{

    /**
     * @Assert\Type(type="string")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $email;

    /**
     * @Assert\Type(type="string")
     */
    public $fullName;

    /**
     * @Assert\Type(type="string")
     */
    public $phone;

    /**
     * @Assert\Type(type="string")
     */
    public $image;

    /**
     * @Assert\Type(type="bool")
     */
    public $emailVerified;

    /**
     * @Assert\Type(type="bool")
     */
    public $isPhoneVerified;

    /**
     * @Assert\Type(type="integer")
     */
    public $cityId;

    /**
     * @Assert\NotBlank
     */
    public $token;

}
