<?php

namespace AppBundle\APIResponse\Item;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseItem
{
    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $name;

    /**
     * @Assert\Type(type="string")
     */
    public $nameEn;

    /**
     * @Assert\Type(type="string")
     */
    public $nameAr;

    /**
     * @Assert\Type(type="numeric")
     */
    public $price;

    /**
     * @Assert\Type(type="numeric")
     */
    public $oldPrice;

    /**
     * @Assert\Type(type="string")
     */
    public $image;

    /**
     * @Assert\Type(type="numeric")
     */
    public $cityId;

    /**
     * @Assert\Type(type="integer")
     */
    public $minimumAmountToOrder;

    /**
     * @Assert\Type(type="integer")
     */
    public $currentCapacity;

    /**
     * @Assert\Type(type="integer")
     */
    public $totalCapacity;
    
    public $attribute;
    
    public $brand;
    
    public $package;
    
    public $packageSize;
    
    public $type;
    
}
