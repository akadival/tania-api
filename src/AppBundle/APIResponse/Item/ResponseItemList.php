<?php

namespace AppBundle\APIResponse\Item;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseItemList extends \AppBundle\APIResponse\Success
{

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Item\ResponseItem")
     * })
     */
    public $items = array();

}
