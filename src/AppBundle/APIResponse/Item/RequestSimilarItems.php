<?php

namespace AppBundle\APIResponse\Item;

use Symfony\Component\Validator\Constraints as Assert;

class RequestSimilarItems
{

    /**
     * Assert\NotBlank(groups={"no-user"})
     * @Assert\Type("numeric")
     * @Assert\Length(max=20)
     */
    public $longitude;

    /**
     * Assert\NotBlank(groups={"no-user"})
     * @Assert\Type("numeric")
     * @Assert\Length(max=20)
     */
    public $latitude;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     * @Assert\Length(max=20)
     */
    public $itemId;
    
}
