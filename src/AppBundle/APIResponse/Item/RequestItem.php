<?php

namespace AppBundle\APIResponse\Item;

use Symfony\Component\Validator\Constraints as Assert;

class RequestItem
{

    /**
     * Assert\NotBlank(groups={"no-user"})
     * @Assert\Type("numeric")
     * @Assert\Length(max=20)
     */
    public $longitude;

    /**
     * Assert\NotBlank(groups={"no-user"})
     * @Assert\Type("numeric")
     * @Assert\Length(max=20)
     */
    public $latitude;

}
