<?php

namespace AppBundle\APIResponse\User;

use Symfony\Component\Validator\Constraints as Assert;

class RequestLoginPhone
{

    /**
     * @Assert\Type(type="string")
     * @Assert\NotBlank
     */
    public $phone;

}
