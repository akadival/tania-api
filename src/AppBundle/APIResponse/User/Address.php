<?php

namespace AppBundle\APIResponse\User;

use Symfony\Component\Validator\Constraints as Assert;

class Address
{
    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     * @Assert\NotBlank(message="fill_mandatory_field")
     * @Assert\Length(min = 3, max = 20, maxMessage="addressTitle_length_not_valid", minMessage="addressTitle_length_not_valid")
     */
    public $title;

    /**
     * @Assert\NotBlank(message="fill_mandatory_field")
     * @Assert\Length(min = 4, max = 300, maxMessage="address_length_not_valid", minMessage="address_length_not_valid")
     */
    public $address;

    /**
     * @Assert\NotBlank(message="fill_mandatory_field")
     */
    public $longitude;

    /**
     * @Assert\NotBlank(message="fill_mandatory_field")
     */
    public $latitude;

    /**
     * Assert\NotBlank(message="fill_mandatory_field")
     * Assert\Length(min = 3, max = 300, maxMessage="address_length_not_valid", minMessage="address_length_not_valid")
     */
    public $customerGroup;

}
