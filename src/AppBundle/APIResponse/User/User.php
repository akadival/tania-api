<?php

namespace AppBundle\APIResponse\User;

use Symfony\Component\Validator\Constraints as Assert;

class User
{

    /**
     * @Assert\Type(type="string")
     */
    public $fullName;

    /**
     * @Assert\Type(type="string")
     */
    public $phone;

    /**
     * @Assert\Type(type="string")
     */
    public $latitude;

    /**
     * @Assert\Type(type="bool")
     */
    public $longitude;

}
