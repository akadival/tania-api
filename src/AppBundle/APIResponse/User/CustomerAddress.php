<?php

namespace AppBundle\APIResponse\User;

use Symfony\Component\Validator\Constraints as Assert;

class CustomerAddress
{

    /**
     * @Assert\NotBlank(message="fill_mandatory_field")
     * @Assert\Length(min = 4, max = 300, maxMessage="address_length_not_valid", minMessage="address_length_not_valid")
     */
    public $description;

    /**
     * @Assert\Type(type="AppBundle\APIResponse\Address\GPS")
     */
    public $gps;

}
