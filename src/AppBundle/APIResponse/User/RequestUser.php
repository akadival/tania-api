<?php

namespace AppBundle\APIResponse\User;

use Symfony\Component\Validator\Constraints as Assert;

class RequestUser
{

    /**
     * @Assert\Type(type="string")
     * @Assert\NotBlank
     */
    public $fullName;

    /**
     * @Assert\Type(type="string")
     * @Assert\NotBlank
     */
    public $phone;

    /**
     * @Assert\Type(type="string")
     * @Assert\NotBlank
     */
    public $password;

    /**
     * @Assert\Type(type="string")
     */
    public $email;

    /**
     * @Assert\Type(type="string")
     */
    public $neighborhood;

    /**
     * @Assert\Type(type="string")
     */
    public $address;

    /**
     * @Assert\Type(type="numeric")
     */
    public $latitude = 0;

    /**
     * @Assert\Type(type="numeric")
     */
    public $longitude = 0;

    /**
     * @Assert\Type(type="integer")
     */
    public $city;

    /**
     * @Assert\Type(type="string")
     */
    public $locale;
}
