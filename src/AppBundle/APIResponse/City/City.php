<?php

namespace AppBundle\APIResponse\City;

use Symfony\Component\Validator\Constraints as Assert;

class City {

    /**
     * @Assert\Type(type="integer")
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     */
    public $name;

    /**
     * @Assert\Type(type="string")
     */
    public $nameAr;

}
