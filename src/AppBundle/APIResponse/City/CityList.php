<?php

namespace AppBundle\APIResponse\City;

use Symfony\Component\Validator\Constraints as Assert;

class CityList extends \AppBundle\APIResponse\Success {

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\City\City")
     * })
     */
    public $cities = array();

}
