<?php

namespace AppBundle\APIResponse\Address;

use Symfony\Component\Validator\Constraints as Assert;

class GPS
{

    /**
     * @Assert\NotBlank(message="fill_mandatory_field")
     */
    public $longitude;

    /**
     * @Assert\NotBlank(message="fill_mandatory_field")
     */
    public $latitude;

}
