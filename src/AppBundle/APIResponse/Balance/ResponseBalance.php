<?php

namespace AppBundle\APIResponse\Balance;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseBalance
{
    public $id;

    public $balanceName;

    public $price;

    public $points;

}
