<?php

namespace AppBundle\APIResponse\Balance;

use Symfony\Component\Validator\Constraints as Assert;

class ResponseBalanceList extends \AppBundle\APIResponse\Success
{

    /**
     * @Assert\Type(type="array")
     * @Assert\All({
     *      @Assert\Type(type="AppBundle\APIResponse\Balance\ResponseBalance")
     * })
     */
    public $balances = array();

}
