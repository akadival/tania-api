<?php

namespace AppBundle\Service;

use Ibtikar\TaniaModelBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Ibtikar\TaniaModelBundle\Entity\Order;
use Ibtikar\TaniaModelBundle\Service\NotificationCenter;
use Ibtikar\ShareEconomyPayFortBundle\Service\PaymentOperations;
use AppBundle\APIResponse\Balance\ResponseBalance;

class OrderOperations extends APIOperations
{
    private $container;
    private $payfort;

    public function __construct($container, PaymentOperations $payfort)
    {
        $this->container = $container;
        $this->payfort = $payfort;
        parent::__construct($container->get('validator'), $container->get('translator'));
    }

    /**
     * Gets a container service by its id.
     *
     * @param string $id The service id
     *
     * @return object The service
     */
    public function get($id)
    {
        return $this->container->get($id);
    }

    public function finishOrder(Order $order)
    {
        $em = $this->get('doctrine')->getManager();

        //update status
        $order->setStatus(Order::$statuses['delivered']);
        $order->setEndDate(new \DateTime());

        //update order van capacity
        $orderItems = $order->getOrderItems();
        $orderItemsArray = array();

        if(!($order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest)){
            foreach($orderItems as $orderItem)
                $orderItemsArray[$orderItem->getItem()->getId()] = $orderItem->getCount();

            $vanItems = $order->getVan() ? $order->getVan()->getVanItems() : null;

            foreach($vanItems as $vanItem)
            {
                $itemId = $vanItem->getItem()->getId();
                if(isset($orderItemsArray[$itemId]))
                    $vanItem->setCurrentCapacity(max($vanItem->getCurrentCapacity() - $orderItemsArray[$itemId],0));
            }
        }

        $user = $order->getUser();
        if($user && $order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest && $order->getPaymentMethod() != Order::CASH){
            $user->setBalance(($user->getBalance() + $order->getPoints()));
        } elseif ($order instanceof \Ibtikar\TaniaModelBundle\Entity\Order && $user) {
        $em->getRepository('IbtikarTaniaModelBundle:UserItemPackage')->
                    insertOrIncreaseItemsCountsIfExist($order);
        }
        $em->flush();
        
        if ($order->getPaymentMethod() == Order::CREDIT) {
            $this->payfort->payInvoice($order);
        } elseif (!$order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest) { 
            // no notification in balance request since the points will be transfered after dashboard confirmation
            $this->notifySuccessDelivery($order);
        }

        //NANACALL2
        if($order->getSource() == 'nana') {
            $syncResult = $this->nanaApiUpdateOrderStatus($order->getId(), Order::$statuses['delivered']);
            if($syncResult && isset($syncResult->status) && $syncResult->status){
                $order->setIsNanaSynced(TRUE);
            }
            if($syncResult) {
                $order->setNanaSyncData($syncResult);
            }else{
                $order->setNanaSyncData(array());
            }
            $em->flush();
        }
    }

    public function finishOrderV2(Order $order)
    {
        $em = $this->get('doctrine')->getManager();

        //update status
        $order->setStatus(Order::$statuses['delivered']);
        $order->setEndDate(new \DateTime());

        //update order van capacity
        $orderItems = $order->getOrderItems();
        $orderItemsArray = array();

        if(!($order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest)){
            foreach($orderItems as $orderItem)
                $orderItemsArray[$orderItem->getItem()->getId()] = $orderItem->getCount();

            $vanItems = $order->getVan() ? $order->getVan()->getVanItems() : null;

            foreach($vanItems as $vanItem)
            {
                $itemId = $vanItem->getItem()->getId();
                if(isset($orderItemsArray[$itemId]))
                    $vanItem->setCurrentCapacity(max($vanItem->getCurrentCapacity() - $orderItemsArray[$itemId],0));
            }
        }

        $user = $order->getUser();
        if($user && $order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest && $order->getPaymentMethod() != Order::CASH){
            $user->setBalance(($user->getBalance() + $order->getPoints()));
        } elseif ($order instanceof \Ibtikar\TaniaModelBundle\Entity\Order && $user) {
        $em->getRepository('IbtikarTaniaModelBundle:UserItemPackage')->
                    insertOrIncreaseItemsCountsIfExist($order);
        }
        $em->flush();
        
        if ($order->getPaymentMethod() == Order::CREDIT) {
            //$this->payfort->payInvoice($order); // NEW-ISPL comment
            $this->notifySuccessDelivery($order); // NEW-ISPL 
        } elseif (!$order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest) { 
            // no notification in balance request since the points will be transfered after dashboard confirmation
            $this->notifySuccessDelivery($order);
        }

        //NANACALL2
        if($order->getSource() == 'nana') {
            $syncResult = $this->nanaApiUpdateOrderStatus($order->getId(), Order::$statuses['delivered']);
            if($syncResult && isset($syncResult->status) && $syncResult->status){
                $order->setIsNanaSynced(TRUE);
            }
            if($syncResult) {
                $order->setNanaSyncData($syncResult);
            }else{
                $order->setNanaSyncData(array());
            }
            $em->flush();
        }
    }

    public function payCard(Order $order)
    {
         
         $this->payfort->payInvoice($order); // NEW-ISPL
    }

    public function getBalanceRequest(Order $balance)
    {
        $balanceData = new ResponseBalance();
        $balanceData->balanceName = $balance->getBalanceName();
        $balanceData->points = $balance->getPoints();
        $balanceData->price = $balance->getPrice();
        unset($balanceData->id);

        return $balanceData;
    }

    public function notifyFailedPayment(Order $order)
    {

        $user = $order->getUser();

        if($user) {
            $title = $this->translator->trans('Payment failed', array(), 'notifications', $user->getLocale());
            $body = $this->translator->trans('Payment has failed for your order %orderId%', array(
                '%orderId%' => $order->getId(),
            ), 'notifications', $user->getLocale());
            $titleAr = $this->translator->trans('Payment failed', array(), 'notifications', 'ar');
            $bodyAr = $this->translator->trans('Payment has failed for your order %orderId%', array('%orderId%' => $order->getId()), 'notifications', 'ar');
            $titleEn = $this->translator->trans('Payment failed', array(), 'notifications', 'en');
            $bodyEn = $this->translator->trans('Payment has failed for your order %orderId%', array('%orderId%' => $order->getId()), 'notifications', 'en');

            $notifData = array(
                'id' => $order->getId(),
                'oldStatus' => Order::$statuses['delivering'],
                'newStatus' => $order->getStatus(),
                'title' => $title,
                'body' => $body,
            );
            $this->get('notification_center')->sendNotificationToUser($user, NotificationCenter::USER_PAYMENT_FAILED, $titleAr, $titleEn, $bodyAr, $bodyEn, $user->getLocale(), $notifData);
        }
    }

    public function notifySuccessDelivery(Order $order)
    {

        $user = $order->getUser();

        if($user) {
            $title = $this->translator->trans('Order delivered', array(), 'notifications', $user->getLocale());
            $body = $this->translator->trans('Your order %orderId% is delivered', array(
                '%orderId%' => $order->getId(),
            ), 'notifications', $user->getLocale());
            $titleAr = $this->translator->trans('Order delivered', array(), 'notifications', 'ar');
            $bodyAr = $this->translator->trans('Your order %orderId% is delivered', array('%orderId%' => $order->getId()), 'notifications', 'ar');
            $titleEn = $this->translator->trans('Order delivered', array(), 'notifications', 'en');
            $bodyEn = $this->translator->trans('Your order %orderId% is delivered', array('%orderId%' => $order->getId()), 'notifications', 'en');
            $notifData = array(
                'id' => $order->getId(),
                'oldStatus' => Order::$statuses['delivering'],
                'newStatus' => $order->getStatus(),
                'title' => $title,
                'body' => $body,
            );
            $this->get('notification_center')->sendNotificationToUser($user, NotificationCenter::USER_ORDER_DELIVERED, $titleAr, $titleEn, $bodyAr, $bodyEn, $user->getLocale(), $notifData);
        }
    }

    /**
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     * @param string $promoCodeString
     * @param User $validateForUser
     * @return PromoCode|JsonResponse
     */
    public function getValidationErrorResponseForPromoCodeOrPromoCode($promoCodeString, User $validateForUser = null, $orderId = null, $city = null)
    {
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $em = $this->container->get('doctrine')->getManager();
        /* @var $promoCode \Ibtikar\TaniaModelBundle\Entity\PromoCode */
        $promoCode = $em->getRepository('IbtikarTaniaModelBundle:PromoCode')->findOneBy(['code' => $promoCodeString]);
        $order = null;
        if($orderId){
            /* @var Order $order */
            $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(array('id'=> $orderId));
        }
        
        if ($city && !$promoCode->isCitySupported($city)) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("The promo code is not applied on the order's area"));
        }

        if($order && $order->getPromoCodeName() == $promoCodeString){
            return $promoCode;
        }

        if (!$promoCode) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('This promocode is invalid', array(), 'order'));
        }
        if ($promoCode->getExpired()) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('Expired promocode', array(), 'order'));
        }
        if (!$promoCode->isUsable()) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('This promocode is invalid', array(), 'order'));
        }
        if ($validateForUser && $promoCode->getMaximumNumberOfAllowedTimesPerUser()) {
            $countPromoCodeTimesByUser = (integer) $em->getRepository('IbtikarTaniaModelBundle:Order')
                    ->countPromoCodeUsedTimesByUser($validateForUser->getId(), $promoCode->getId());

            if ($promoCode->getMaximumNumberOfAllowedTimesPerUser() <= $countPromoCodeTimesByUser) {
                return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('This promocode is used before', array(), 'order'));
            }
        }
        return $promoCode;
    }
}
