<?php

namespace AppBundle\Service;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\APIOperations;
use Ibtikar\TaniaModelBundle\Entity\BaseUser;
use Ibtikar\TaniaModelBundle\Entity\User;
use Ibtikar\TaniaModelBundle\Entity\PhoneVerificationCode;
use AppBundle\APIResponse;
use Ibtikar\TaniaModelBundle\Entity\Driver;
use AppBundle\APIResponse\Driver\Driver as ResponseDriver;
use Ibtikar\TaniaModelBundle\Entity\Order;

class UserOperations extends APIOperations
{

    /* @var $container \Symfony\Component\DependencyInjection\ContainerAwareInterface */
    private $container;

    private $router;

    /**
     * @param ContainerAwareInterface $container
     */
    public function __construct($container, $router)
    {
        $this->container = $container;
        $this->router = $router;
        parent::__construct($container->get('validator'), $container->get('translator'));
    }

    /**
     * Gets a container service by its id.
     *
     * @param string $id The service id
     *
     * @return object The service
     */
    public function get($id)
    {
        return $this->container->get($id);
    }

    /**
     * Gets a container configuration parameter by its name.
     *
     * @param string $name The parameter name
     *
     * @return mixed
     */
    public function getParameter($name)
    {
        return $this->container->getParameter($name);
    }

    /**
     * @return BaseUser|null
     */
    public function getLoggedInUser()
    {
        $token = $this->container->get('security.token_storage')->getToken();
        if ($token && is_object($token)) {
            $user = $token->getUser();
            if (is_object($user) && $user instanceof BaseUser) {
                return $user;
            }
        }
    }

    /**
     * @param string|null $message
     * @return JsonResponse
     */
    public function getInvalidCredentialsJsonResponse($message = null)
    {
        $errorResponse = new APIResponse\InvalidCredentials();
        if ($message) {
            $errorResponse->message = $message;
        }
        return $this->getJsonResponseForObject($errorResponse);
    }

    /**
     * @param BaseUser $user
     * @return array
     */
    public function getUserData(BaseUser $user)
    {

        $responseUser = $this->getUserObjectResponse($user);
        return $this->getObjectDataAsArray($responseUser);
    }

    /**
     * @param BaseUser $user
     * @return array
     */
    public function getDriverData(Driver $user)
    {

        $responseUser = $this->getDriverObjectResponse($user);
        return $this->getObjectDataAsArray($responseUser);
    }

    /**
     *
     * @param BaseUser $user
     * @return \Ibtikar\ShareEconomyUMSBundle\APIResponse\User
     */
    public function getUserObjectResponse(BaseUser $user)
    {
        $responseUser = new APIResponse\User();
        $responseUser->id = $user->getId();
        $responseUser->fullName = $user->getFullName();
        $responseUser->email = $user->getEmail();
        $responseUser->phone = $user->getPhone();
        $responseUser->emailVerified = $user->getEmailVerified();
        $responseUser->isPhoneVerified = $user->getIsPhoneVerified();
        $responseUser->locale = $user->getLocale();
        $responseUser->neighborhood = $user->getNeighborhood();
        $responseUser->addresses = $user->getUserAddresses();
        $responseUser->defaultPaymentMethod = $user->getPaymentMethod();

        //cash, credit, balance
        $responseUser->paymentMethods = $this->getUserPaymentMethods($user);

        if($user->getCountry())
            $responseUser->countryCode = $user->getCountry()->getCode();
        if ($user->getImage()) {
            $responseUser->image = $this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost() . '/' . $user->getWebPath();
        }
        if ($user instanceof User) {
            $userCity = $user->getCity();
            if ($userCity) {
                $responseUser->cityId = $userCity->getId();
                if(strtolower($user->getLocale()) == 'ar')
                    $responseUser->cityName = $userCity->getNameAr();
                else
                    $responseUser->cityName = $userCity->getNameEn();
            }
        }

        return $responseUser;
    }

    function getUserPaymentMethods($user)
    {
        $requestFrom = $this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom');

        $userPaymentMethods[] = ['type' => Order:: CASH, 'cardNumber' => Order::$paymentMethodList[Order:: CASH], 'isDefault' => ($user->getPaymentMethod() == Order::CASH)? TRUE: FALSE, 'paymentOption' =>''];
        $userPaymentMethods[] = ['type' => Order:: BALANCE, 'cardNumber' => Order::$paymentMethodList[Order:: BALANCE], 'value' =>  $user->getBalance(), 'isDefault' => ($user->getPaymentMethod() == Order::BALANCE)? TRUE: FALSE, 'paymentOption' =>''];
        /*$userPaymentMethods[] = ['type' => Order:: SADAD, 'cardNumber' => Order::$paymentMethodList[Order:: SADAD], 'isDefault' => ($user->getPaymentMethod() == Order::SADAD)? TRUE: FALSE, 'paymentOption' =>''];*/
        $userPaymentMethods[] = ['type' => Order:: SADAD, 'cardNumber' => Order::$paymentMethodList[Order:: SADAD], 'isDefault' => ($user->getPaymentMethod() == Order::SADAD)? TRUE: FALSE, 'paymentOption' =>''];

        $em = $this->get('doctrine')->getManager();
        $paymentMethods = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->findBy(['holder' => $user->getId()]);
        //$temp = ($user->getId()=='76524' || $user->getId()=='28903')?true:false;
        $temp = true;
        if ($temp  && count($paymentMethods)) {
            foreach ($paymentMethods as $paymentMethod) {
            if($paymentMethod->getPaymentOption()!='MADA') { 

                $credit                    = new \Ibtikar\ShareEconomyPayFortBundle\APIResponse\PaymentMethod();
                $credit->id                = $paymentMethod->getId();
                $credit->fortId            = $paymentMethod->getFortId();
                $credit->cardNumber        = $paymentMethod->getCardNumber();
                $credit->expiryDate        = $paymentMethod->getExpiryDate();
                $credit->merchantReference = $paymentMethod->getMerchantReference();
                $credit->tokenName         = $paymentMethod->getTokenName();
                $credit->paymentOption     = $paymentMethod->getPaymentOption();
                $credit->isDefault         = $paymentMethod->getIsDefault();
                $credit->paymentOption2    = $paymentMethod->getPaymentOption();
                $credit->type              = Order::CREDIT;
                $userPaymentMethods[] = $credit;
                }
            }
        }

        return $userPaymentMethods;
    }


    /*function getUserPaymentMethodsV1($user)
    {
        $requestFrom = $this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom');

        $userPaymentMethods[] = ['type' => Order:: CASH, 'cardNumber' => Order::$paymentMethodList[Order:: CASH], 'isDefault' => ($user->getPaymentMethod() == Order::CASH)? TRUE: FALSE, 'paymentOption' =>''];
        $userPaymentMethods[] = ['type' => Order:: BALANCE, 'cardNumber' => Order::$paymentMethodList[Order:: BALANCE], 'value' =>  $user->getBalance(), 'isDefault' => ($user->getPaymentMethod() == Order::BALANCE)? TRUE: FALSE, 'paymentOption' =>''];
       
        $userPaymentMethods[] = ['type' => Order:: SADAD, 'cardNumber' => Order::$paymentMethodList[Order:: SADAD], 'isDefault' => ($user->getPaymentMethod() == Order::SADAD)? TRUE: FALSE, 'paymentOption' =>''];

        $em = $this->get('doctrine')->getManager();
        $paymentMethods = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->findBy(['holder' => $user->getId()]);
        if (count($paymentMethods)) {
        foreach ($paymentMethods as $paymentMethod) {
            if($paymentMethod->getPaymentOption()!='MADA') { 

                $credit                    = new \Ibtikar\ShareEconomyPayFortBundle\APIResponse\PaymentMethod();
                $credit->id                = $paymentMethod->getId();
                $credit->fortId            = $paymentMethod->getFortId();
                $credit->cardNumber        = $paymentMethod->getCardNumber();
                $credit->expiryDate        = $paymentMethod->getExpiryDate();
                $credit->merchantReference = $paymentMethod->getMerchantReference();
                $credit->tokenName         = $paymentMethod->getTokenName();
                $credit->paymentOption     = $paymentMethod->getPaymentOption();
                $credit->isDefault         = $paymentMethod->getIsDefault();
                $credit->paymentOption2    = $paymentMethod->getPaymentOption();
                //if($requestFrom=='ios')
                //$credit->type              = $paymentMethod->getPaymentOption();   
                //else 
                $credit->type              = Order::CREDIT;
                $userPaymentMethods[] = $credit;
                }
            }
        }

        return $userPaymentMethods;
    }*/

    /**
     *
     * @param BaseUser $user
     * @return \Ibtikar\ShareEconomyUMSBundle\APIResponse\User
     */
    public function getDriverObjectResponse(Driver $user)
    {
        $responseUser = new ResponseDriver();
        $responseUser->driverId = $user->getId();
        $responseUser->fullName = $user->getFullName();
        $responseUser->fullNameAr = $user->getFullNameAr();
        $responseUser->username = $user->getUsername();
        $responseUser->phone = $user->getPhone();
        $responseUser->locale = $user->getLocale();
        $responseUser->vanNumber = count($user->getVanDrivers())>0?(string)$user->getVanDrivers()[0]->getVan()->getVanNumber():null;
        $responseUser->cityAreaId = count($user->getDriverCityAreas())>0?$user->getDriverCityAreas()[0]->getCityArea()->getId():null;
        $responseUser->cityId = count($user->getDriverCityAreas())>0?$user->getDriverCityAreas()[0]->getCityArea()->getCity()->getId():null;
        $responseUser->rate = $user->getDriverRate()?(double)$user->getDriverRate():0;
        $responseUser->image = $user->getImage()?$this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost() . '/' . $user->getWebPath():null;
        $responseUser->isActive = $user->getStatus();
        $responseUser->email = $user->getEmail();
        $responseUser->area = count($user->getDriverCityAreas())>0? json_decode($user->getDriverCityAreas()[0]->getCityArea()->getPolygon()):null;

        return $responseUser;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getLoggedInUserDataJsonResponse(Request $request)
    {
        $loggedInUser = $this->getLoggedInUser();
        if ($loggedInUser) {
            $userData = $this->getUserData($loggedInUser);
            $authorizationHeader = $request->headers->get('Authorization');
            if ($authorizationHeader) {
                $userData['token'] = str_replace('Bearer ', '', $authorizationHeader);
            }
            $loggedInUserResponse = new APIResponse\SuccessLoggedInUser();
            $loggedInUserResponse->user = $userData;
            return $this->getJsonResponseForObject($loggedInUserResponse);
        }
        return $this->getInvalidCredentialsJsonResponse();
    }

    /**
     * send phone verification code SMS
     *
     * @param BaseUser $user
     * @param type $code
     * @return boolean
     */
    public function sendVerificationCodeMessage(BaseUser $user, $code)
    {
        try {
            $message = str_replace(array('%project%', '%code%', '%validationTimeInMinutes%'), array($this->getParameter('nexmo_from_name'), $code->getCode(), $this->getParameter('verification_code_expiry_minutes')), 'Verification code for %project% is (%code%), valid for %validationTimeInMinutes% minutes');
//            $this->get('jhg_nexmo_sms')->sendText($user->getPhone(), $message, null, 0, $user->getLocale() === 'ar' ? 'unicode' : 'text');
            $this->get('jawaly_sms')->sendText($user->getPhone(), $message, $user->getLocale() === 'ar' ? true : false);
            $return = true;
        } catch (\Exception $ex) {
            $this->get('logger')->critical($ex->getMessage());
            $return = false;
        }

        return $return;
    }

    /**
     * send password SMS
     *
     * @param BaseUser $user
     * @param string $password
     * @return boolean
     */
    public function sendPasswordMessage(BaseUser $user, $password)
    {
        try {
            $message = str_replace(array('%project%', '%code%', '%validationTimeInMinutes%'), array($this->getParameter('nexmo_from_name'), $password, $this->getParameter('verification_code_expiry_minutes')), 'Verification code for %project% is (%code%), valid for %validationTimeInMinutes% minutes');
//            $this->get('jhg_nexmo_sms')->sendText($user->getPhone(), $message, null, 0, $user->getLocale() === 'ar' ? 'unicode' : 'text');
            $this->get('jawaly_sms')->sendText($user->getPhone(), $message, $user->getLocale() === 'ar' ? true : false);
            $return = true;
        } catch (\Exception $ex) {
            $this->get('logger')->critical($ex->getMessage());
            $return = false;
        }

        return $return;
    }

    /**
     * send phone verification code SMS
     *
     * @param BaseUser $user
     * @param type $code
     * @return boolean
     */
    public function sendVerificationCodeMessageToCustomer(BaseUser $user, $code)
    {
        try {
            $message = str_replace(array('%project%', '%code%', '%validationTimeInMinutes%'), array($this->getParameter('nexmo_from_name'), $code->getCode(), $this->getParameter('verification_code_expiry_minutes')), 'Verification code for %project% is (%code%), valid for %validationTimeInMinutes% minutes');
//            $this->get('jhg_nexmo_sms')->sendText($phone, $message, null, 0, $user->getLocale() === 'ar' ? 'unicode' : 'text');
            $this->get('jawaly_sms')->sendText($phone, $message, $user->getLocale() === 'ar' ? true : false);
            $return = true;
        } catch (\Exception $ex) {
            $this->get('logger')->critical($ex->getMessage());
            $return = false;
        }

        return $return;
    }

    /**
     * add new verification code to the user
     *
     * @param BaseUser $user
     * @return PhoneVerificationCode
     */
    public function addNewVerificationCode(BaseUser $user)
    {
        $phoneVerificationCode = new PhoneVerificationCode();
        $user->addPhoneVerificationCode($phoneVerificationCode);

        return $phoneVerificationCode;
    }

    /**
     * generate random email verification token
     *
     * @param BaseUser $user
     */
    public function generateNewEmailVerificationToken(BaseUser $user)
    {
        $now = new \DateTime();

        if (null !== $user->getLastEmailVerificationRequestDate() && $user->getLastEmailVerificationRequestDate()->format('Ymd') == $now->format('Ymd')) {
            $user->setVerificationEmailRequests($user->getVerificationEmailRequests() + 1);
        } else {
            $user->setVerificationEmailRequests(1);
            $user->setLastEmailVerificationRequestDate($now);
        }

        $user->setEmailVerificationTokenExpiryTime(new \DateTime('+1 day'));
        $user->setEmailVerificationToken(bin2hex(random_bytes(32)));
    }


    /**
     * @param BaseUser $user
     * @return boolean
     */
    public function verifyUserEmail(BaseUser $user)
    {
        $user->setEmailVerified(true);
        $user->setEmailVerificationToken(null);
        $user->setEmailVerificationTokenExpiryTime(null);
        $this->get('doctrine')->getManager()->flush($user);
        return true;
    }


    /**
     * @param string $userEmail
     * @return string
     */
    public function sendResetPasswordEmail($userEmail)
    {
        $translator = $this->get('translator');
        if (!$userEmail) {
            return $translator->trans('fill_mandatory_field', array(), 'validators');
        }
        $em = $this->get('doctrine')->getManager();
        $user = $em->getRepository('IbtikarTaniaModelBundle:User')->findOneBy(['email' => $userEmail, 'systemUser' => false]);
        if (!$user || $user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            return $translator->trans('email_not_registered');
        }
        if (!$this->canRequestForgetPassword($user)) {
            return $translator->trans('reach_max_forget_password_requests_error');
        }
        $this->generateNewForgetPasswordToken($user);
        $em->flush($user);
        $this->get('tania.email_sender')->sendResetPasswordEmail($user, $this->getParameter('project_name'));
        return 'success';
    }

    /**
     * @param string $userEmail
     * @return string
     */
    public function sendResetDriverPasswordEmail($userEmail)
    {
        $translator = $this->get('translator');
        if (!$userEmail) {
            return $translator->trans('fill_mandatory_field', array(), 'validators');
        }
        $em = $this->get('doctrine')->getManager();
        $user = $em->getRepository('IbtikarTaniaModelBundle:User')->findOneBy(['email' => $userEmail, 'systemUser' => false]);
        if (!$user || !$user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            return $translator->trans('email_not_registered');
        }
        if (!$this->canRequestForgetPassword($user)) {
            return $translator->trans('reach_max_forget_password_requests_error');
        }
        $this->generateNewForgetPasswordToken($user);
        $em->flush($user);
        $this->get('tania.email_sender')->sendResetPasswordEmail($user, $this->getParameter('project_name'));
        return 'success';
    }

    /**
     * check the ability of requesting new forget password email
     *
     * @param BaseUser $user
     * @return boolean
     */
    public function canRequestForgetPassword(BaseUser $user)
    {
        $now    = new \DateTime();
        $return = true;

        if (null !== $user->getLastForgetPasswordRequestDate()) {
            if (($user->getLastForgetPasswordRequestDate()->format('Ymd') == $now->format('Ymd')) && $user->getForgetPasswordRequests() >= $this->getParameter('max_daily_forget_passwords_requests')) {
                $return = false;
            }
        }

        return $return;
    }

    /**
     * check the ability of requesting new one time password sms
     *
     * @param BaseUser $user
     * @return boolean
     */
    public function canRequestOneTimePassword(BaseUser $user)
    {
        $now    = new \DateTime();
        $return = true;

        if (null !== $user->getlastLoginPasswordRequestDate()) {
            if (($user->getlastLoginPasswordRequestDate()->format('Ymd') == $now->format('Ymd')) && $user->getLoginPasswordRequests() >= $this->getParameter('max_daily_forget_passwords_requests')) {
                $return = false;
            }
        }

        return $return;
    }

    /**
     * generate random forget password token
     *
     * @param BaseUser $user
     */
    public function generateNewForgetPasswordToken(BaseUser $user)
    {
        $now = new \DateTime();

        if (null !== $user->getLastForgetPasswordRequestDate() && $user->getLastForgetPasswordRequestDate()->format('Ymd') == $now->format('Ymd')) {
            $user->setForgetPasswordRequests($user->getForgetPasswordRequests() + 1);
        } else {
            $user->setForgetPasswordRequests(1);
            $user->setLastForgetPasswordRequestDate($now);
        }

        $user->setChangePasswordTokenExpiryTime(new \DateTime('+3 day'));
        $user->setChangePasswordToken(bin2hex(random_bytes(32)));
    }

    /**
     * save the user information to the database
     *
     * @param BaseUser $user
     * @param string $oldEmail
     * @param string $oldPhone
     * @return boolean
     */
    public function updateUserInformation(BaseUser $user, $oldEmail, $oldPhone)
    {
        if ($user->getEmail() !== $oldEmail) {
            $this->generateNewEmailVerificationToken($user);
            $user->setEmailVerified(false);

            // send verification email
            $this->get('tania.email_sender')->sendEmailVerification($user, $this->getParameter('project_name'));
        }

        if ($user->getPhone() !== $oldPhone) {
            $user->setIsPhoneVerified(false);
            $phoneVerificationCode = $this->addNewVerificationCode($user);

            // send phone verification code
            $this->sendVerificationCodeMessage($user, $phoneVerificationCode);
        }

        $this->get('doctrine')->getManager()->flush();
        return true;
    }

    /**
     * check if the user reached the max todays requests
     *
     * @param BaseUser $user
     * @return boolean
     */
    public function canRequestPhoneVerificationCode(BaseUser $user)
    {
        $em         = $this->get('doctrine')->getManager();
        $codesCount = $em->getRepository('IbtikarTaniaModelBundle:User')->countTodaysCodes($user);

        return $codesCount < $this->getParameter('max_daily_verification_code_requests');
    }

    /**
     * send reset password url
     *
     * @param BaseUser $user
     * @param type $code
     * @return boolean
     */
    public function sendDriverResetPasswordUrl($userPhone)
    {

        $translator = $this->get('translator');

        $em = $this->get('doctrine')->getManager();
        $driver = $em->getRepository('IbtikarTaniaModelBundle:Driver')->findOneBy(['phone' => $userPhone, 'systemUser' => false]);
        if (!$driver)
            return $translator->trans('phone_not_registered');

        if (!$this->canRequestForgetPassword($driver)) {
            return $translator->trans('reach_max_forget_password_requests_error');
        }
        $this->generateNewForgetPasswordToken($driver);
        $em->flush($driver);
        $resetPasswordUrl = $this->router->generate('driver_reset_password', ['phone' => $driver->getPhone(), 'token' => $driver->getChangePasswordToken()], UrlGeneratorInterface::ABSOLUTE_URL);
        /* @var $googleShortenUrlService \Ibtikar\GoogleServicesBundle\Service\ShortenUrl */
        $googleShortenUrlService = $this->container->get('google_shorten_url');
        $resetPasswordShortUrl = $googleShortenUrlService->shorten($resetPasswordUrl);
        if (!$resetPasswordShortUrl) {
            $resetPasswordShortUrl = $resetPasswordUrl;
        }
        $driverLocale = $driver->getLocale() === 'ar' ? 'ar' : 'en';
        $message = $translator->trans('Click the following link to reset tania password', array(), $driverLocale) . ' ' . $resetPasswordShortUrl;

        try {
//            $this->get('jhg_nexmo_sms')->sendText($driver->getPhone(), $message, null, 0, $driverLocale === 'ar' ? 'unicode' : 'text');
            $this->get('jawaly_sms')->sendText($driver->getPhone(), $message, $driverLocale === 'ar' ? true : false);
            $return = 'success';
        } catch (\Exception $ex) {
            $this->get('logger')->critical($ex->getMessage());
            $return = false;
        }

        return $return;
    }

    public function generateRandomString($length = 4){
        $length1 = max($length - 3, 0);
        $length2 = $length - $length1;

        $str1 = substr(str_shuffle(str_repeat("0123456789", $length1)), 0, $length1);
        $str2 = substr(str_shuffle(str_repeat("0123456789", $length2)), 0, $length2);
        //$str2 = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", $length2)), 0, $length2);
        return $str1.$str2;
    }

    public function generateRandomStringRegister($length = 4){
        $length1 = max($length - 3, 0);
        $length2 = $length - $length1;

        $str1 = substr(str_shuffle(str_repeat("0123456789", $length1)), 0, $length1);
        //$str2 = substr(str_shuffle(str_repeat("0123456789", $length2)), 0, $length2);
        $str2 = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", $length2)), 0, $length2);
        return $str1.$str2;
    }

}
