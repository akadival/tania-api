<?php

namespace AppBundle\Service;

use Swift_Message;
use Swift_Attachment;
use Ibtikar\TaniaModelBundle\Entity\BaseUser;
use Psr\Log\LoggerInterface;

class EmailSender
{

    protected $em;
    protected $mailer;
    protected $templating;
    protected $senderEmail;
    protected $logger;
    protected $translator;

    public function __construct($em, $mailer, $templating, $senderEmail, LoggerInterface $logger, $translator)
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->senderEmail = $senderEmail;
        $this->logger = $logger;
        $this->translator = $translator;
    }

    public function send($to = "", $subject = "", $content = "", $type = "text/html", $files = array())
    {
        $message = Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->senderEmail, $this->senderEmail)
                ->setSender($this->senderEmail, $this->senderEmail)
                ->setTo($to)
                ->setContentType($type)
                ->setBody($content);

        if (!empty($files)) {
            foreach ($files as $name => $path) {
                $message->attach(Swift_Attachment::fromPath($path)->setFilename($name));
            }
        }

        try {
            $this->mailer->send($message);
        } catch (\Exception $exc) {
            $this->logger->critical($exc->getTraceAsString());
        }
    }

    public function sendToMany($to = array(), $subject = "", $content = "", $type = "text/html", $files = array())
    {
        $message = Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->senderEmail, $this->senderEmail)
                ->setSender($this->senderEmail, $this->senderEmail)
                ->setTo($this->senderEmail, $this->senderEmail)
                ->setBcc($to)
                ->setContentType($type)
                ->setBody($content);

        if (!empty($files)) {
            foreach ($files as $name => $path) {
                $message->attach(Swift_Attachment::fromPath($path)->setFilename($name));
            }
        }

        try {
            $this->mailer->send($message);
        } catch (\Exception $exc) {
            $this->logger->critical($exc->getTraceAsString());
        }
    }

    /**
     * send verification email
     *
     * @param BaseUser $user
     * @return type
     */
    public function sendEmailVerification(BaseUser $user, $projectName)
    {

        $template = $this->templating->render('IbtikarShareEconomyDashboardDesignBundle:Email:layout.html.twig', [
            'name' => $user->getFullName(),
            'headText' => false,
            'longText' => $this->templating->render('AppBundle:Emails:emailVerification.html.twig', array('user' => $user)),
            'boldText' => false,
            'rows' => [
            ],
            'thanks' => $this->translator->trans('Thanks', array(), 'email'),
            'websiteteam' => $this->translator->trans($projectName.' Team', array(), 'email')
        ]);
        return $this->send($user->getEmail(), $this->translator->trans('Please verify your email', array(), 'email'), $template);
    }

    /**
     * send verification code email
     *
     * @param BaseUser $user
     * @return type
     */
    public function sendVerificationCodeEmail(BaseUser $user, $projectName, $message)
    {
        $template = $this->templating->render('IbtikarShareEconomyDashboardDesignBundle:Email:layout.html.twig', [
            'name' => $user->getFullName(),
            'headText' => false,
            'longText' => $this->templating->render('AppBundle:Emails:sendVerificationCode.html.twig', array('user' => $user, 'message' => $message)),
            'boldText' => false,
            'rows' => [
            ],
            'thanks' => $this->translator->trans('Thanks', array(), 'email'),
            'websiteteam' => $this->translator->trans($projectName.' Team', array(), 'email')
        ]);
        return $this->send($user->getEmail(), $this->translator->trans('Please verify your email', array(), 'email'), $template);
    }

    /**
     * send forget password email
     *
     * @param BaseUser $user
     * @return type
     */
    public function sendResetPasswordEmail(BaseUser $user, $projectName)
    {
        $template = $this->templating->render('IbtikarShareEconomyDashboardDesignBundle:Email:layout.html.twig', [
            'name' => $user->getFullName(),
            'headText' => false,
            'longText' => $this->templating->render('AppBundle:Emails:sendResetPasswordEmail.html.twig', array('user' => $user)),
            'boldText' => false,
            'rows' => [
            ],
            'thanks' => $this->translator->trans('Thanks', array(), 'email'),
            'websiteteam' => $this->translator->trans($projectName.' Team', array(), 'email')
        ]);
        return $this->send($user->getEmail(), $this->translator->trans('Reset your password in Tania', array(), 'email'), $template);
    }
}
