<?php

namespace AppBundle\Service;

use Ibtikar\TaniaModelBundle\Entity\PhoneVerificationCode;


class PhoneVerificationCodeBusiness
{

    /* @var $container \Symfony\Component\DependencyInjection\ContainerAwareInterface */
    private $container;

    /**
     * @param ContainerAwareInterface $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * check code validity
     *
     * @param PhoneVerificationCode $verificationCode
     * @return boolean
     */
    public function isValidCode(PhoneVerificationCode $verificationCode)
    {
        $minCreationTime = new \DateTime('-' . $this->getParameter('verification_code_expiry_minutes') . ' minutes');

        return $minCreationTime < $verificationCode->getCreatedAt();
    }

    /**
     * get validity remaining seconds
     *
     * @param PhoneVerificationCode $verificationCode
     * @return integer
     */
    public function getValidityRemainingSeconds(PhoneVerificationCode $verificationCode)
    {
        $now  = new \DateTime();
        $diff = $now->format('U') - $verificationCode->getCreatedAt()->format('U');

        return $diff > ( $this->getParameter('verification_code_expiry_minutes') * 60 ) ? 0 : ( $this->getParameter('verification_code_expiry_minutes') * 60 ) - $diff;
    }

    /**
     * Gets a container configuration parameter by its name.
     *
     * @param string $name The parameter name
     *
     * @return mixed
     */
    public function getParameter($name)
    {
        return $this->container->getParameter($name);
    }
}