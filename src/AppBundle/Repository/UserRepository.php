<?php

namespace AppBundle\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Ibtikar\TaniaModelBundle\Repository\BaseUserRepository;

class UserRepository extends BaseUserRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
                ->where('u.phone = :phone')
                ->andWhere('u.systemUser = false')
                ->andWhere('u INSTANCE OF Ibtikar\TaniaModelBundle\Entity\User')
                ->setParameter('phone', $username)
                ->getQuery()
                ->getOneOrNullResult();
    }
}