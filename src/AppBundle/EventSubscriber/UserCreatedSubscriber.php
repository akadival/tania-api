<?php
namespace AppBundle\EventSubscriber;


use AppBundle\Event\UserCreatedEvent;
use Psr\Log\LoggerInterface;

/**
 * Description of UserCreatedSubscriber
 *
 * @author abdelrahman.allanm
 */
class UserCreatedSubscriber {


    protected  $logger;
    function __construct(LoggerInterface $logger){
        $this->logger = $logger;
        $this->logger->info('Helloz');
        file_put_contents("/tmp/usercreated", "Lozzz");
    }


    public function onUserCreated(UserCreatedEvent $event){

        // send created user Tania's XML SOAP API
        $client = new \SoapClient('https://185.10.112.195/taniawebservice/TaniaDataService.asmx?wsdl');
        // SoapClient->Insert_NewRegistration



        //    and the progress, set the status of the User



        // log the event

        $this->logger->info('Yes!!, User Is Created!!');

    }
}
