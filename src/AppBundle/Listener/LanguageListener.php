<?php

namespace AppBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class LanguageListener
{
    protected $acceptedLocales;

    public function __construct($acceptedLocales)
    {
        $this->acceptedLocales = $acceptedLocales;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onRequest(GetResponseEvent $event)
    {
        $request         = $event->getRequest();
        $requestedLocale = $request->headers->get('accept-language');

        if ($requestedLocale && in_array($requestedLocale, $this->acceptedLocales)) {
            $request->setLocale($requestedLocale);
        }
    }
}