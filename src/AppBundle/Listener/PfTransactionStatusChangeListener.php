<?php

namespace AppBundle\Listener;

use Ibtikar\ShareEconomyPayFortBundle\Events\PfTransactionStatusChangeEvent;
use Ibtikar\TaniaModelBundle\Entity\Order;

class PfTransactionStatusChangeListener
{
    private $em;
    private $orderOperations;

    /**
     * @param $em
     */
    public function __construct($em, $orderOperations)
    {
        $this->em = $em;
        $this->orderOperations = $orderOperations;
    }


    /**
     * @param PfTransactionStatusChangeEvent $event
     */
    public function successTransaction(PfTransactionStatusChangeEvent $event)
    {
        $transaction = $event->getTransaction();
        $order   = $transaction->getInvoice();

        // When credit card charged at the time of delivery
        //$order->setStatus(Order::$statuses['delivered']); // NEW-ISPL commented

        // Credit card charged at the time of placing order
        $order->setStatus(Order::$statuses['new']);  // NEW-ISPL
        $order->setCancelReason(NULL);
        $this->em->flush();

        if($order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest){
            $user = $order->getUser();
            if($user) {
                $user->setBalance(($user->getBalance() + $order->getPoints()));
                $this->em->flush();
            }
        }
        else if($order instanceof \Ibtikar\TaniaModelBundle\Entity\Order){
            $order->setEndDate(new \DateTime());
            //$this->orderOperations->notifySuccessDelivery($order); // NEW-ISPL commented
        }

    }


    /**
     *
     * @param PfTransactionStatusChangeEvent $event
     */
    public function failTransaction($event)
    {
        $transaction = $event->getTransaction();

        $order = $transaction->getInvoice();

        $order->setStatus(Order::$statuses['transaction-pending']);
        $this->em->flush();

        $this->orderOperations->notifyFailedPayment($order);
    }
}