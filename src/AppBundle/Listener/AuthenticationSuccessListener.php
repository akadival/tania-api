<?php

namespace AppBundle\Listener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use AppBundle\APIResponse\SuccessLoggedInUser;
use Ibtikar\TaniaModelBundle\Entity\BaseUser;
use Symfony\Component\HttpFoundation\RequestStack;
use AppBundle\APIResponse\InvalidCredentials;
use Symfony\Component\Translation\TranslatorInterface;

class AuthenticationSuccessListener
{

    /* @var $userOperations \Ibtikar\ShareEconomyUMSBundle\Service\UserOperations */
    private $userOperations;
    private $requestStack;
    private $container;
    protected $em;
    private $translator;

    /**
     * @param ContainerAwareInterface $container
     */
    public function __construct($container, RequestStack $requestStack, $em, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->container = $container;
        $this->translator = $translator;
        $this->userOperations = $container->get('user_operations');
        $this->requestStack = $requestStack;
    }

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $eventData = $event->getData();
        $user      = $event->getUser();
        $request = $this->requestStack->getCurrentRequest();

        if($request->get('username') && $request->get('username')!= $user->getUsername())
        {
            $output = new InvalidCredentials();
            $output->message = $this->translator->trans('The entered username is not registered, please enter again.', array(), 'security');
            $event->setData((array)$output);
            return;
        }

        if (!$user instanceof BaseUser)
            return;

        $apiKey = $request->headers->get('X-Api-Key');

        if($apiKey == $this->container->getParameter('ios_api_key'))
            $user->setDeviceType(BaseUser::IOS_USER_TYPE);
        else if($apiKey == $this->container->getParameter('android_api_key'))
            $user->setDeviceType(BaseUser::ANDROID_USER_TYPE);

        if($user->getlastLoginPasswordRequestDate()) {
            $user->setIsPhoneVerified(true); // Added To Check for Login with one time password
        }

        $user->setLoginPasswordRequests(0); //Resetting The failed login attempts counter
        $this->em->flush();

        $loggedInUserResponse = new SuccessLoggedInUser();
        $responseData         = $this->userOperations->getObjectDataAsArray($loggedInUserResponse);
        $userData = array();
        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver)
            $userData             = $this->userOperations->getDriverData($user);
        elseif ($user instanceof \Ibtikar\TaniaModelBundle\Entity\User)
            $userData             = $this->userOperations->getUserData($user);


        foreach ($userData as $key => $value) {
            $responseData['user'][$key] = $value;
        }

        $responseData['user']['token'] = $eventData['token'];
        $event->setData($responseData);
    }
}
