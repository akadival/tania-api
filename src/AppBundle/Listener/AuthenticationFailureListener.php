<?php

namespace AppBundle\Listener;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Translation\TranslatorInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use AppBundle\Service\UserOperations;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class AuthenticationFailureListener
{

    /* @var $tranlator \Symfony\Component\Translation\TranslatorInterface */
    private $translator;

    /* @var $userOperations  */
    private $userOperations;

    private $request;

    /**
     * @param TranslatorInterface $translator
     * @param UserOperations $userOperations
     */
    public function __construct(RequestStack $requestStack, TranslatorInterface $translator, UserOperations $userOperations)
    {
        $this->translator = $translator;
        $this->userOperations = $userOperations;
        $this->requestStack = $requestStack;
    }

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {

        $request = $this->requestStack->getCurrentRequest();
        $exception = $event->getException();
        $errorMessage = $exception->getMessage();

        $password = isset(json_decode($request->getContent())->password) ? json_decode($request->getContent())->password : null;
        if(isset(json_decode($request->getContent())->phone)){
            $request->request->set('password', json_decode($request->getContent())->phone);
        }

        if (!$request->get('password') && !$password) {
            $errorMessage = $this->translator->trans('Please fill the mandatory field first.', array(), 'security');
        } else {
//            var_dump($exception->getMessage());die;
            if ($exception instanceof UsernameNotFoundException) {
                $errorMessage = $this->translator->trans('The entered phone is not registered, please enter again.', array(), 'security');
                if ($exception->getUsername() === AuthenticationProviderInterface::USERNAME_NONE_PROVIDED) {
                    $errorMessage = $this->translator->trans('Please fill the mandatory field first.', array(), 'security');
                }
            }

            if($exception instanceof BadCredentialsException)
                $errorMessage = $this->translator->trans('The presented password is invalid.', array(), 'security');
        }
        $event->setResponse($this->userOperations->getInvalidCredentialsJsonResponse($errorMessage));
    }
}
