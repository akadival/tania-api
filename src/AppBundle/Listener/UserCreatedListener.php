<?php


namespace AppBundle\Listener;

use AppBundle\Event\UserCreatedEvent;
use Psr\Log\LoggerInterface;

/**
 * Description of UserCreatedListener
 *
 * @author abdelrahman.allam
 */
class UserCreatedListener {

    //put your code here

    protected $logger;

    function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
        $this->logger->info('Helloz');
        file_put_contents("/tmp/usercreated", "Lozzz");
    }

    /**
     * @var SaveDataEvent
     */
    public function onUserCreated(UserCreatedEvent $event) {


        $em->flush();

        $output = new RegisterUserSuccess();
        $output->user = $userOperations->getUserData($user);
        // TODO: execute this block of code with async pattern or queue
        try {
            // TODO: move wsdl url to config/paramters
            $wdsl = "http://185.10.112.195/taniawebservice/TaniaDataService.asmx?wsdl";
            $client = new \SoapClient($wdsl);
            $newRegister = [];
            $newRegister['RegDate'] = date('c'); // ISO 8601
            $newRegister['RegNo'] = $user->getId();
            $newRegister['FullName'] = $user->getFullName();
            $newRegister['TelephoneNo'] = str_replace("+", "00", $user->getPhone());
            $newRegister['Address'] = $user->getAddress();
            $newRegister['Email'] = $user->getEmail();
            $newRegister['Password'] = $user->getUserPassword();
            $newRegister['City'] = $user->getCity();
            $newRegister['Neighborhood'] = $user->getNeighborhood();
            $newRegister['Latitude'] = $user->getLatitude();
            $newRegister['Longitude'] = $user->getLongitude();
            $taniaResponce = $client->Insert_NewRegistration($newRegister);
            $this->get('logger')->info('Tania Soap: insert new registeration response:' . $taniaResponce->Insert_NewRegistrationResult);
        } catch (Exception $ex) {
            $this->get('logger')->critical($exc->getMessage());
        }
    }

}
