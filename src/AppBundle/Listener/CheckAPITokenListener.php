<?php

namespace AppBundle\Listener;

use AppBundle\Service\APIOperations;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class CheckAPITokenListener
{

    /* @var $apiKeys array */
    private $apiKeys = array(
        'nana'            => 'rxy8klmz2nj8fq1p',
        'tania-system'    => 'nb478ecgsvh83mxw',
        'tania-order-now' => '7owts65tss9lxsvr',
        'tania-website'   => 'po9n6h4q9lnvow',
        'oxidane'         => '1IxYbWCYOm13PmDp',
    );

    /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
    private $apiOperations;

    /**
     * @param APIOperations $apiOperations
     */
    public function __construct(APIOperations $apiOperations, $androidAPIKey, $iosAPIKey)
    {
        $this->apiOperations = $apiOperations;
        $this->apiKeys['android'] = $androidAPIKey;
        $this->apiKeys['ios'] = $iosAPIKey;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (strpos($request->getRequestUri(), '/api/doc') === false && strpos($request->getRequestUri(), '/api/configs/') === false && strpos($request->getRequestUri(), '/api') !== false) {
            $apiKeyIndex = array_search($request->headers->get('X-Api-Key'), $this->apiKeys, true);
            if ($apiKeyIndex === false) {
                $event->setResponse($this->apiOperations->getInvalidAPIKeyJsonResponse());
                return;
            }
            $request->attributes->set('requestFrom', $apiKeyIndex);
        }
    }
}
