<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppSyncTaniaSystemCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:sync-tania-system')
            ->setDescription('Sync database with Tania SOAP service')
            ->addArgument('entity', InputArgument::REQUIRED, 'entity name will be sync')
//            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('entity');
        $em   = $this->getContainer()->get('doctrine')->getManager();
        $this->getContainer()->get('logger')->info("app:sync-tania-system started");
        if ($argument == "user") {
            $users = $em->getRepository('IbtikarTaniaModelBundle:User')->findBy(['isSynced' => false]);
            foreach ($users as $user) {
                try {
                    $wdsl = $this->getContainer()->getParameter('tania_sync_soap_service_domain');
                    $client = new \SoapClient($wdsl);
                    $newRegister = [];
                    $newRegister['RegDate'] = $user->getCreatedAt()->format('c'); // ISO 8601
                    $newRegister['RegNo'] = $user->getId();
                    $newRegister['FullName'] = $user->getFullName();
                    $newRegister['TelephoneNo'] = str_replace("+", "00", $user->getPhone());
                    $newRegister['Address'] = $user->getAddress();
                    $newRegister['Email'] = $user->getEmail();
                    $newRegister['Password'] = $user->getUserPassword();
                    $newRegister['City'] = $user->getCity() ? $user->getCity()->getId() : '';
                    $newRegister['Neighborhood'] = $user->getNeighborhood();
                    $newRegister['Latitude'] = $user->getLatitude();
                    $newRegister['Longitude'] = $user->getLongitude();
                    $output->writeln(var_dump($newRegister));
                    $taniaResponce = $client->Insert_NewRegistration($newRegister);
                    $this->getContainer()->get('logger')->info('Tania Soap: insert new registeration response:' . $taniaResponce->Insert_NewRegistrationResult);
                    $user->setIsSynced(true);
                    $em->persist($user);
                    $em->flush();
                } catch (Exception $ex) {
                    $this->get('logger')->critical($exc->getMessage());
                }
            }
        }
    }

}
