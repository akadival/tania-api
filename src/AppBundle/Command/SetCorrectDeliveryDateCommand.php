<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

class SetCorrectDeliveryDateCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('order:migrate-delivery-date')
            ->setDescription('Set delivery date from string to date')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $batchSize = 50;
        $i = 0;
        $q = $em->createQuery('SELECT o FROM Ibtikar\TaniaModelBundle\Entity\Order o');
        $iterableResult = $q->iterate();
        foreach ($iterableResult as $row) {
            /* @var $order \Ibtikar\TaniaModelBundle\Entity\Order */
            $order = $row[0];
            $orderReceivingDateTimestamp = $order->getReceivingDate();
            if ($orderReceivingDateTimestamp) {
                try {
                    $orderRequiredDeliveryDate = new \DateTime('@' . $orderReceivingDateTimestamp);
                    $orderRequiredDeliveryDate->setTimezone(new \DateTimeZone(date_default_timezone_get()));
                    if (strlen($orderReceivingDateTimestamp) !== 10) {
                        if ($output->isVerbose()) {
                            $output->writeln('<error>Invalid date for timestamp: ' . $orderReceivingDateTimestamp . ' orderId: ' . $order->getId() . '</error>');
                        }
                        $order->setRequiredDeliveryDate(null);
                    } else {
                        $order->setRequiredDeliveryDate($orderRequiredDeliveryDate);
                    }
                } catch (\Exception $e) {
                    if ($output->isVerbose()) {
                        $output->writeln('<error>Exception: ' . $e->getMessage() . ' orderId: ' . $order->getId() . '</error>');
                    }
                }
            }
            if (($i % $batchSize) === 0) {
                $em->flush();
                if ($output->isVerbose()) {
                    $output->writeln('<info>Saving to database</info>');
                }
                $em->clear();
            }
            ++$i;
        }
        if ($output->isVerbose()) {
            $output->writeln('<info>Saving to database</info>');
        }
        $em->flush();
    }
}
