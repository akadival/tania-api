<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\DefaultController;
use AppBundle\APIResponse\Item\RequestItem;
use AppBundle\APIResponse\Item\RequestSimilarItems;

class ItemController extends DefaultController
{

    /**
     * Get Items
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Item\RequestItem",
     *  section="Item",
     *  tags={
     *     "user application"="DarkCyan",
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getItemListAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $user = $this->getUser();
        if ($user && $user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver)
        {
            if(!count($user->getVanDrivers())) //if driver is not assigned to van
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("You are not assigned to any van"));

            $items = $em->getRepository("IbtikarTaniaModelBundle:Item")->getVanItems($user->getVanDrivers()[0]->getVan()->getId());
            if(!count($items)){
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
            }

            $responseItem = $this->getDriverItemObject($request, $items);
            return $apiOperations->getJsonResponseForObject($responseItem);
        }
        $validationGroups = array('Default');
        if (!$user) {
            $validationGroups[] = 'no-user';
        }

        $requestItem = new RequestItem();
        $validationErrorsResponse = $apiOperations->bindAndValidateObjectDataFromRequest($requestItem, $request, $validationGroups);
        if ($validationErrorsResponse) {
            return $validationErrorsResponse;
        }
        if(!$requestItem->latitude || !$requestItem->longitude || $requestItem->latitude == '' || $requestItem->longitude == ''){
            $city = '';
            if ($user) {
                $cityArea = $this->getCityAreaPerLatLong($user->getLatitude(), $user->getLongitude());
                if ($cityArea)
                    $city = $cityArea->getCity();
            }
        }
        else{
            $city = '';
            $cityArea = $this->getCityAreaPerLatLong($requestItem->latitude, $requestItem->longitude);
            if($cityArea)
                $city = $cityArea->getCity();
        }
        
        /*if(!$city){
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
        }*/
        
        // send this to repository
        $itemRepository = $em->getRepository('IbtikarTaniaModelBundle:Item');
        $items = $itemRepository->getAllItems();
        if(count($items)<=0){
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $responseItem = $this->getItemObjectPerCity($request, $items, $city);
        return $apiOperations->getJsonResponseForObject($responseItem);
    }

    /**
     * check capacity
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Item",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkCapacityAction(Request $request){
        return $this->getItemListAction($request);
    }

    /**
     * Get All Items
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Item\RequestItem",
     *  section="Item",
     *  tags={
     *     "user application"="DarkCyan",
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getNanaItemListAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $validationGroups = array('Default');

        $requestItem = new RequestItem();
        $validationErrorsResponse = $apiOperations->bindAndValidateObjectDataFromRequest($requestItem, $request, $validationGroups);
        if ($validationErrorsResponse) {
            return $validationErrorsResponse;
        }

        $itemRepository = $em->getRepository('IbtikarTaniaModelBundle:Item');
        $items = $itemRepository->getAllItems();
        if(count($items)<=0){
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }

        $city = null;

        if($requestItem->longitude && $requestItem->latitude) {
            $cityArea = $this->getCityAreaPerLatLong($requestItem->latitude, $requestItem->longitude);
            if ($cityArea)
                $city = $cityArea->getCity();
        }

        $responseItem = $this->getItemObjectPerCity($request, $items, $city);
        return $apiOperations->getJsonResponseForObject($responseItem);
    }
    
    /**
     * Get Similar Items
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Item\RequestSimilarItems",
     *  section="Item",
     *  tags={
     *     "user application"="DarkCyan",
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getSimilarItemsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $apiOperations = $this->get('api_operations');
        $validationGroups = array('Default');
        $requestSimilarItems = new RequestSimilarItems();
        $validationErrorsResponse = $apiOperations->bindAndValidateObjectDataFromRequest($requestSimilarItems, $request, $validationGroups);
        if ($validationErrorsResponse) {
            return $validationErrorsResponse;
        }
        $city = null;
        if ($requestSimilarItems->longitude != null && $requestSimilarItems->latitude != null) {
            $cityArea = $this->getCityAreaPerLatLong($requestSimilarItems->latitude, $requestSimilarItems->longitude);
            if(isset($cityArea) && $cityArea){
                $city = $cityArea->getCity();
            }else{
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
            if(!$city){
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        }
        $itemRepository = $em->getRepository('IbtikarTaniaModelBundle:Item');
        $givenItem = $itemRepository->findOneBy(array('id' => $requestSimilarItems->itemId));
        if (!$givenItem) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $items = $itemRepository->getSimilarItems($givenItem);
        if(count($items)<=0){
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $responseItem = $this->getItemObjectPerCity($request, $items, $city);
        return $apiOperations->getJsonResponseForObject($responseItem);
    }

    /**
     * Get Home Items
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Item\RequestItem",
     *  section="Item",
     *  tags={
     *     "user application"="DarkCyan",
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function homeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $validationGroups = array('Default');

        $requestItem = new RequestItem();
        $validationErrorsResponse = $apiOperations->bindAndValidateObjectDataFromRequest($requestItem, $request, $validationGroups);
        if ($validationErrorsResponse) {
            return $validationErrorsResponse;
        }

        $itemRepository = $em->getRepository('IbtikarTaniaModelBundle:Item');
        $items = $itemRepository->getHomeItems();
        if(count($items)<=0){
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }

        $city = null;

        if($requestItem->longitude && $requestItem->latitude) {
            $cityArea = $this->getCityAreaPerLatLong($requestItem->latitude, $requestItem->longitude);
            if ($cityArea)
                $city = $cityArea->getCity();
        }

        $responseItem = $this->getItemObjectPerCity($request, $items, $city);
        return $apiOperations->getJsonResponseForObject($responseItem);
    }    
    
    public function getAction(Request $request, $itemId)
    {
        
    }


    /**
     * Get Items
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Item",
     *  tags={
     *     "user application"="DarkCyan",
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getByIdAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        // send this to repository
        $item = $em->getRepository('IbtikarTaniaModelBundle:Item')->findOneById($id);

        if(!$item){
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $responseItem = $this->getItemObject($request, $item);
        return $apiOperations->getSuccessDataJsonResponse($responseItem);
    }
    
}
