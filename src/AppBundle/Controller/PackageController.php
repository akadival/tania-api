<?php

namespace AppBundle\Controller;

use AppBundle\APIResponse\Package\ResponsePackageList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\DefaultController;
use AppBundle\APIResponse\Item\RequestItem;

class PackageController extends DefaultController {

    /**
     * Get Packages
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Package",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=true}
     *  },
     *  tags={
     *     "user application"="DarkCyan"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $page = ($request->get('page') && $request->get('page') > 0 ) ? $request->get('page') : 1;
        $limit = 25;
        $em->getFilters()->disable('softdeleteable');
        $packages = $em->createQueryBuilder()
                ->select('i')
                ->from('IbtikarTaniaModelBundle:Package', 'i')
                ->where('i.enabled = :isEnabled')
                ->andWhere('i.expiryTime >= :now')
                ->andWhere('i.deletedAt is NULL')
                ->orderBy('i.createdAt', 'DESC')
                ->setParameter('isEnabled', true)
                ->setParameter('now', date('Y-m-d H:i:s', time()))
                ->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit)
                ->getQuery()
                ->getResult();
        if (isset($packages[$limit - 1])) {
            $isNextPage = true;
        } else {
            $isNextPage = false;
        }
        $packagesResult = array();
        foreach ($packages as $package) {
            $packagesResult[] = $this->buildPackageObject($request, $package);
        }
        $responsePackageList = new ResponsePackageList($page, $isNextPage);
        $responsePackageList->packages = $packagesResult;
        return $apiOperations->getJsonResponseForObject($responsePackageList);
    }
    
    /**
     * Package Collected Items
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Package",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function itemsAction() 
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $userItemPackages = $em->getRepository('IbtikarTaniaModelBundle:UserItemPackage')->findBy(['user' => $this->getUser()]);
        $response = $this->prepareUserItemResponseObject($userItemPackages);
        return $apiOperations->getSuccessDataJsonResponse($response);
        
    }

    private function prepareUserItemResponseObject($userItemPackages) 
    {
        $response = new \stdClass();
        $response->items = [];
        foreach ($userItemPackages as $userItemPackage) {
            $item = new \stdClass();
            $item->id = $userItemPackage->getItem()->getId();
            $item->purchasedCount = $userItemPackage->getPurchasedCount();
            $item->redeemedCount = $userItemPackage->getRedeemedCount();
            $item->redeemableCount = max($userItemPackage->getPurchasedCount() - $userItemPackage->getRedeemedCount(), 0);
            $response->items[] = $item;
        }
        return $response;
    }

    /**
     * redeem package
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Package",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="packageId", "dataType"="string", "required"=true},
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function redeemAction(Request $request) {
        $packageId = $request->get('packageId');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestRedeem = new \AppBundle\APIResponse\Package\RequestRedeem();
        $validationErrorsResponse = $apiOperations->bindAndValidateObjectDataFromRequest($requestRedeem, $request);
        if ($validationErrorsResponse) {
            return $validationErrorsResponse;
        }
        /* @var $package \Ibtikar\TaniaModelBundle\Entity\Package */
        $package = $em->getRepository('IbtikarTaniaModelBundle:Package')
                ->findOneBy(['id' => $packageId]);

        $packageAssertionResult = $this->assertPackageIsRedeemable($package);
        if ($packageAssertionResult !== true) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans($packageAssertionResult));
        }
        $packageBuyItemIds = $package->getBuyItemsIds();
        /* @var $userItemPackage \Ibtikar\TaniaModelBundle\Entity\UserItemPackage */
        $userItemPackages = $em->getRepository('IbtikarTaniaModelBundle:UserItemPackage')
                ->findBy(['user' => $user, 'item' => $packageBuyItemIds]);
        $packageBuyItems = $package->getPackageBuyItems();
        if (count($userItemPackages) < count($packageBuyItems)) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans("Not all Items collected"));
        }
        foreach ($userItemPackages as $userItemPackage) {
            $itemId = $userItemPackage->getItem()->getId();
            $currentPackageBuyItem = $package->findPackageBuyItemByItemId($itemId);
            if ($userItemPackage->getRedeemableCount() < $currentPackageBuyItem->getCount()) {
                return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans("Not enough items collected"));
            }
            $userItemPackage->setRedeemedCount($userItemPackage->getRedeemedCount() + $currentPackageBuyItem->getCount());
            $em->persist($userItemPackage);
        }
        //add UserPackage record
        $userPackage = new \Ibtikar\TaniaModelBundle\Entity\UserPackage();
        $userPackage->setPackage($package);
        $userPackage->setPointsEarned($package->getGetAmount());
        $userPackage->setRedeemedAt(new \DateTime("now"));
        $userPackage->setUser($user);
        $em->persist($userPackage);
        //update user points
        $user->setPoints($user->getPoints() + $package->getGetAmount());
        $em->persist($user);
        $em->flush();
        return $apiOperations->getSuccessJsonResponse();
    }

    /**
     * 
     * @param $package
     * @return true|string true if redeemable or string error message if not
     */
    private function assertPackageIsRedeemable($package) {
        if (!$package) {
            return "Package is not found";
        }
        if (!$package->getEnabled()) {
            return "Package is not enabled";
        }
        if ($package->getExpiryTime()->getTimestamp() < time()) {
            return "Package is expired";
        }
        $packageBuyItems = $package->getPackageBuyItems();
        if (!$packageBuyItems) {
            return "Package has no items";
        }
        return true;
    } 

    /**
     * Transfer Points to Balance
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *  },
     *  section="Package",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function transferPointsToBalanceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $currentPoints = $user->getPoints();
        if ($currentPoints == 0) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('No points to transfer'));
        }
        $oldBalance = $user->getBalance();
        $user->setPoints(0);
        $user->setUsedPoints($user->getUsedPoints() + $currentPoints);
        $user->setBalance($oldBalance + $currentPoints);
        $response = new \stdClass();
        $response->newBalance = $user->getBalance();
        $response->oldBalance = $oldBalance;
        $response->redeemedPoints = $currentPoints;
        $em->persist($user);
        $em->flush();
        return $apiOperations->getSuccessDataJsonResponse($response);
    }

}
