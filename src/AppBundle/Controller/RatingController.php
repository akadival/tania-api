<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\DefaultController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

//use AppBundle\APIResponse\RatingTag\;

class RatingController extends DefaultController {

    /**
     * Get Order List
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Rating",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function tagsByRatingAmountAction(Request $request, $rating) {
        $em = $this->getDoctrine()->getManager();
        $apiOperations = $this->get('api_operations');
        $ratingTagRepository = $em->getRepository('IbtikarTaniaModelBundle:RatingTag');
        $tags = $ratingTagRepository->getRatingTagsByRatingValue($rating);
        if (count($tags) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No tags was found"));
        }
        $ratingTagListResponse = new \AppBundle\APIResponse\RatingTag\ResponseRatingTagList();
        $ratingTagResponse = new \AppBundle\APIResponse\RatingTag\ResponseRatingTag();
        $apiOperations->bindDoctrineResultArray($ratingTagListResponse, $ratingTagResponse, $tags);
        return $apiOperations->getJsonResponseForObject($ratingTagListResponse);
    }

    /**
     * Get Order List
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Rating",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function tagsAction(Request $request) {
        $locale = ucfirst($request->headers->get('accept-language'));
        if (strlen($locale) > 2) {
            $locale = "En";
        }
        $em = $this->getDoctrine()->getManager();
        $apiOperations = $this->get('api_operations');
        $ratingTagRepository = $em->getRepository('IbtikarTaniaModelBundle:RatingRange');
        $joinedResult = $ratingTagRepository->getRatingTagJoinRatingRange();
        // this function can be rewritten in a better way :)

        $ratingsTags = [
                "1" => [],
                "2" => [],
                "3" => [],
                "4" => [],
                "5" => [],
        ];
        foreach ($joinedResult as $row) {
            foreach (range($row['start'], $row['end']) as $num) {
                foreach ($row['ratingTagRatingRanges'] as $rrrr) {
                    unset($rrrr['ratingTag']['deletedAt']);
                    if ($locale == 'En') {
                        $rrrr['ratingTag']['name'] = $rrrr['ratingTag']['nameEn'];
                    }
                    unset($rrrr['ratingTag']['nameEn']);
                    if (!in_array($rrrr['ratingTag'], $ratingsTags[$num])) {
                        $rtobj = $rrrr['ratingTag'];
                        $ratingsTags[$num][] = $rtobj;
                    }
                }
            }
        }

        $res = ['ratings' => [
                    ['value' => 1, 'tags' => $ratingsTags["1"]],
                    ['value' => 2, 'tags' => $ratingsTags["2"]],
                    ['value' => 3, 'tags' => $ratingsTags["3"]],
                    ['value' => 4, 'tags' => $ratingsTags["4"]],
                    ['value' => 5, 'tags' => $ratingsTags["5"]]
        ]];

        return $apiOperations->getSuccessDataJsonResponse($res);
    }

}
