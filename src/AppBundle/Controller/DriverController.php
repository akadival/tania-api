<?php

namespace AppBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type as formInputsTypes;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\APIResponse\Driver\DriverImage;
use AppBundle\APIResponse\Driver\Driver as ResponseDriver;
use AppBundle\APIResponse\RegisterUserFail;
use AppBundle\APIResponse\RegisterUserSuccess;

class DriverController extends DefaultController
{

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
    }

    /**
     * Login with an existing user
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Driver",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true},
     *      {"name"="password", "dataType"="string", "required"=true}
     *  },
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the login information was not correct",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200="AppBundle\APIResponse\SuccessLoggedInUser",
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @return JsonResponse
     */
    public function loginAction()
    {
        // The security layer will intercept this request it should never reach here
        return new JsonResponse(array('code' => 401, 'message' => 'Bad credentials'));
    }

    /**
     * Update driver image
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Driver",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="Image64", "dataType"="string", "required"=true, "format"="{base64 encoded string}"}
     *  },
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the login information was not correct",
     *      403="Returned if the api key is not valid",
     *      422="Returned if there is a validation error in the sent data",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200="AppBundle\APIResponse\Driver\DriverImage",
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      422="Ibtikar\ShareEconomyToolsBundle\APIResponse\ValidationErrors",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function changeProfileImageAction(Request $request)
    {
        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        /* @var $driver \Ibtikar\TaniaModelBundle\Entity\Driver */
        $driver = $this->getUser();
        $tempUrlPath = null;
        $fileSystem = new Filesystem();
        $image = $request->get('Image64');
        if ($image) {
            $imageString = base64_decode($image);
            if ($imageString) {
                $imageRandomName = uniqid();
                $uploadDirectory = $driver->getUploadRootDir() . '/api-temp/';
                $fileSystem->mkdir($uploadDirectory, 0755);
                $uploadPath = $uploadDirectory . $imageRandomName;
                if (@file_put_contents($uploadPath, $imageString)) {
                    $fileWithoutExtension = new File($uploadPath, false);
                    $imageExtension = $fileWithoutExtension->guessExtension();
                    if ($imageExtension) {
                        $tempUrlPath = "$uploadPath.$imageExtension";
                        $fileSystem->rename($uploadPath, $tempUrlPath);
                        $file = new File($tempUrlPath, false);
                        $driver->setFile($file);
                    }
                }
            }
        }
        /* @var $validator \Symfony\Component\Validator\Validator\RecursiveValidator */
        $validator = $this->get('validator');
        $errorsObjects = $validator->validate($driver, null, array('image-required', 'image'));
        if (count($errorsObjects) > 0) {
            if ($tempUrlPath) {
                $fileSystem->remove($tempUrlPath);
            }
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        if ($tempUrlPath) {
            @unlink($tempUrlPath);
        }
        $response = new DriverImage();
        $response->image = $driver->getImage() ? $request->getSchemeAndHttpHost() . '/' . $driver->getWebPath() : null;
        return $apiOperations->getJsonResponseForObject($response);
    }

    /**
     * Update driver image
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Driver",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="isActive", "dataType"="bool", "required"=true}
     *  },
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the login information was not correct",
     *      403="Returned if the api key is not valid",
     *      422="Returned if there is a validation error in the sent data",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200="AppBundle\APIResponse\SuccessLoggedInUser",
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      422="Ibtikar\ShareEconomyToolsBundle\APIResponse\ValidationErrors",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function setStatusAction(Request $request)
    {
        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        /* @var $driver \Ibtikar\TaniaModelBundle\Entity\Driver */
        $driver = $this->getUser();

        $isActive = trim($request->get('isActive'));
        $driver->setStatus($isActive);

        $em = $this->getDoctrine()->getManager();
        $em->persist($driver);
        $em->flush();
        $output = array();
        $output['status'] = true;
        $output['message'] = 'Success.';
        $output['code'] = '200';
        $output['currentStatus'] = $isActive;
        return new JsonResponse($output);        
        // return $apiOperations->getSuccessJsonResponse();
    }


    /**
     * Update driver information
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  input="AppBundle\APIResponse\Driver\Driver",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  description="Update driver information",
     *  section="Driver",
     *  statusCodes = {
     *      200 = "Returned on success",
     *      400 = "Validation failed.",
     *      500 = "Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200 = "AppBundle\APIResponse\RegisterUserSuccess",
     *      400 = "AppBundle\APIResponse\RegisterUserFail",
     *      500 = "AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function editProfileAction(Request $request)
    {
        $userOperations = $this->get('user_operations');

        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $apiOperations = $this->get('api_operations');

        $driverObj = new ResponseDriver();
        $driver           = $this->getUser();
        $apiOperations->bindObjectDataFromJsonRequest($driverObj, $request);

        $driver->setFullName(trim($driverObj->fullName));
        $driver->setFullNameAr(trim($driverObj->fullNameAr));
        $driver->setEmail(trim($driverObj->email));
        $driver->setPhone(trim($driverObj->phone));
        $driver->setLocale(trim($driverObj->locale));

        $validationMessages = $this->get('user_operations')->validateObject($driver, ['driver_edit_profile']);





        if (!$this->get('google.phone_number')->validatePhoneNumber($driver->getPhone())) {
            $errorMsg = $translator->trans('Please enter a valid phone number', array(), 'validators');
            $validationMessages['phone'] = $errorMsg;
        }

        if (count($validationMessages)) {
            $output         = new RegisterUserFail();
            $output->errors = $validationMessages;
        } else {
            $em->flush();
            $output         = new RegisterUserSuccess();
            $output->user = $userOperations->getDriverObjectResponse($driver);
            $output->user->token = $this->get('lexik_jwt_authentication.encoder')->encode(['phone' => $driver->getPhone()]);
        }

        return new JsonResponse($output);
    }

    /**
     * reset password
     *
     * @ApiDoc(
     *  resource=true,
     *  description="send forgot password sms",
     *  section="Driver",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="phone", "dataType"="string", "required"=true}
     *  },
     *  statusCodes={
     *      200="Returned on success",
     *      403="Returned if the api key is not valid",
     *      422="Returned if there is a validation error in the sent data",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200="AppBundle\APIResponse\Success",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      422="AppBundle\APIResponse\Fail",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendResetPasswordEmailAction(Request $request)
    {
        $userOperations = $this->get('user_operations');
        $message = $userOperations->sendDriverResetPasswordUrl($request->get('phone'));

        if ($message === 'success') {
            return $userOperations->getSuccessJsonResponse();
        }
        return $userOperations->getSingleErrorJsonResponse($message);
    }

    /**
     * @param Request $request
     * @param string $phone
     * @param string $token
     * @return Response
     */
    public function resetPasswordAction(Request $request, $phone, $token)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('IbtikarTaniaModelBundle:Driver')->findOneBy(array('phone' => $phone, 'changePasswordToken' => $token));
        if (!$user) {
            throw $this->createNotFoundException();
        }
        $currentTime = new \DateTime();
        $translator = $this->get('translator');
        if ($user->getChangePasswordTokenExpiryTime() < $currentTime) {
            $this->addFlash('error', $translator->trans('The change password link expired please request a new one.'));
            return $this->render('IbtikarShareEconomyDashboardDesignBundle:Layout:not_loggedin.html.twig');
        }

        $formBuilder = $this->createFormBuilder($user, array(
                'validation_groups' => 'resetPassword',
            ))
            ->setMethod('POST')
            ->add('userPassword', formInputsTypes\RepeatedType::class, array(
                'type' => formInputsTypes\PasswordType::class,
                'invalid_message' => 'The password fields must match',
                'required' => true,
                'first_options' => array('label' => 'Password', 'attr' => array('autocomplete' => 'off')),
                'second_options' => array('label' => 'Repeat Password', 'attr' => array('autocomplete' => 'off')),
            ))
            ->add('Reset your password', formInputsTypes\SubmitType::class);
        $form = $formBuilder->getForm();
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $user->setValidPassword();
                $user->setChangePasswordToken(null);
                $user->setForgetPasswordRequests(0);
                $user->setChangePasswordTokenExpiryTime(null);
                $em->flush($user);
                $this->addFlash('success', $translator->trans('Password changed sucessfully'));
                return $this->render('IbtikarShareEconomyDashboardDesignBundle:Layout:not_loggedin.html.twig');
            }
        }
        return $this->render('IbtikarShareEconomyDashboardDesignBundle:Layout:not_loggedin_form.html.twig', array(
                'form' => $form->createView(),
                'title' => $translator->trans('Reset your password'),
        ));
    }
}
