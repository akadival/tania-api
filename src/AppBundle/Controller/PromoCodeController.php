<?php

namespace AppBundle\Controller;

use AppBundle\APIResponse\PromoCode\ValidatePromoCodeResponse;
use Ibtikar\TaniaModelBundle\Entity\Order;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Ibtikar\TaniaModelBundle\Entity\PromoCode;

class PromoCodeController extends DefaultController
{

    /**
     * Validate Promo Code
     *
     * @ApiDoc(
     *  resource=true,
     *  parameters={
     *      {"name"="promocode", "dataType"="string", "required"=true},
     *      {"name"="orderId", "dataType"="string", "required"=false},
     *      {"name"="long", "dataType"="string", "required"=false},
     *      {"name"="lat", "dataType"="string", "required"=false},
     *  },
     *  output="AppBundle\APIResponse\PromoCode\ValidatePromoCodeResponse",
     *  tags={
     *     "user application"="DarkCyan"
     *  },
     *  section="PromoCode",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      422="Returned if there is a validation error in the sent data",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function validateAction(Request $request){
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $em = $this->getDoctrine()->getManager();
        $requestData = json_decode($request->getContent(), true);

        if ( isset($requestData["promocode"]) == false
                ||  $requestData["promocode"] == ''
                || empty($requestData["promocode"])) {
            // return not found promocode
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('This promocode is invalid', array(), 'order'));
        }

        /* @var $promocode PromoCode */
        $promocode = $em->getRepository('IbtikarTaniaModelBundle:PromoCode')->findOneBy(['code' => $requestData["promocode"]]);
        if (!$promocode) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('This promocode is invalid', array(), 'order'));
        }
        // lat & long should be mandatory after applying that on mobile
        if (isset($requestData['lat']) && isset($requestData['long'])) {
            $cityArea = $this->getCityAreaPerLatLong($requestData['lat'], $requestData['long']);
            if(!$cityArea){
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("The promo code is not applied on the order's area"));
            }
            $city = $cityArea->getCity();
            if(!$city){
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("The promo code is not applied on the order's area"));
            }
            if (!$promocode->isCitySupported($city)) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("The promo code is not applied on the order's area"));
            }
        }
        
        $order = null;

        if ( isset($requestData["orderId"])){
            $orderId = $requestData['orderId'];
            /* @var $order Order */
            $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(array('id'=> $orderId));
        }

        $orderPromo = null;

        if($order){
            $orderPromo = $order->getPromoCodeName();
        }

        if($orderPromo != $requestData["promocode"]) {
            $validationResponse = $this->get('order_operations')->getValidationErrorResponseForPromoCodeOrPromoCode($requestData["promocode"], $this->getUser());
            if ($validationResponse instanceof JsonResponse) {
                return $validationResponse;
            }
        }

        /* @var ValidatePromoCodeResponse $response */
        $response = new ValidatePromoCodeResponse();
        $response->promoCode = new \AppBundle\APIResponse\PromoCode\PromoCode();
        $response->promoCode->id = $promocode->getId();
        $response->promoCode->type = $promocode->getType();
        $response->promoCode->code = $promocode->getCode();
        $response->promoCode->title = $promocode->getTitle();
        $response->promoCode->discountAmount = (double)$promocode->getDiscountAmount();
        $response->message = $this->get('translator')->trans('This promocode is valid', array(), 'order');

        return new JsonResponse($response);
    }
}
