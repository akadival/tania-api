<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\DefaultController;
use AppBundle\APIResponse\Shift\ResponseShift;
use AppBundle\APIResponse\Shift\ResponseShiftDay;
use AppBundle\APIResponse\Shift\ResponseShiftList;
use AppBundle\APIResponse\Shift\ResponseShiftDaysList;

class ShiftController extends DefaultController
{

    /**
     * List shifts
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Shift",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findBy(array(
            'shiftDay' => null,
            'isDeleted' => 0
        ));
        $shiftsOrdersCountToday = $em->getRepository('IbtikarTaniaModelBundle:Order')->getShiftsOrdersCountToday();

        $responseShiftList = new ResponseShiftList();

        foreach ($shifts as $shift) {
            /* @var $shift \Ibtikar\TaniaModelBundle\Entity\Shift */

            $responseShift = new ResponseShift();
            $responseShift->id = $shift->getId();
            if (strtolower($request->headers->get('accept-language')) == 'en') {
                $responseShift->shift = $shift->getShift();
            } else {
                $responseShift->shift = $shift->getShiftAr();
            }
            $responseShift->from = strtotime($shift->getFrom()->format('H:i'));
            $responseShift->to = strtotime($shift->getTo()->format('H:i'));
            if ($shift->getMaximumAllowedOrdersPerDay()) {
                foreach ($shiftsOrdersCountToday as $shiftOrdersCountToday) {
                    if (isset($shiftOrdersCountToday['shiftId']) && $shiftOrdersCountToday['shiftId'] == $shift->getId()) {
                        if ($shiftOrdersCountToday['ordersCount'] >= $shift->getMaximumAllowedOrdersPerDay()) {
                            $responseShift->disabled = true;
                        }
                        break;
                    }
                }
            }
            if ($responseShift->disabled === false) {
                $firstTimeToday = new \DateTime('midnight');
                $firstTimeToday->modify($shift->getTo()
                    ->format('H:i'));
                if (time() > $firstTimeToday->getTimestamp()) {
                    $responseShift->disabled = true;
                }
            }

            $responseShiftList->shifts[] = $responseShift;
        }
        return $apiOperations->getJsonResponseForObject($responseShiftList);
    }

    /**
     * List shift Days
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Shift",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=false,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function listDayAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $shiftDays = $em->getRepository('IbtikarTaniaModelBundle:ShiftDays')->findAll();

        $responseShiftList = new ResponseShiftDaysList();

        foreach ($shiftDays as $day) {
            /* @var $shift \Ibtikar\TaniaModelBundle\Entity\Shift */

            $responseShiftDay = new ResponseShiftDay();
            $responseShiftDay->id = $day->getId();
            if (strtolower($request->headers->get('accept-language')) == 'en') {
                $responseShiftDay->day = $day->getDayEn();
            } else {
                $responseShiftDay->day = $day->getDayAr();
            }
            $dayShifts = $day->getShifts();
            $countShifts = 0;
            foreach ($dayShifts as $dayShift) {
                if ($dayShift->getIsDeleted() == 0)
                    $countShifts ++;
            }

            if ($countShifts <= 0 || $day->getActive() == 0)
                $responseShiftDay->active = false;
            else
                $responseShiftDay->active = true;

            $responseShiftList->days[] = $responseShiftDay;
        }
        return $apiOperations->getJsonResponseForObject($responseShiftList);
    }

    /**
     * List shifts in day
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Shift",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function listShiftAction(Request $request, $dayId)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $dayId = trim($dayId);
        $dayEntity = $em->getRepository('IbtikarTaniaModelBundle:ShiftDays')->findOneBy(array(
            'id' => $dayId
        ));
        if ($dayEntity == null) {
            $errorMsg = $translator->trans("day_not_found", array(), 'api_error_message');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        $shifts = $em->getRepository("IbtikarTaniaModelBundle:Shift")->findBy(array(
            'shiftDay' => $dayEntity,
            'isDeleted' => 0
        ));

        $shiftsOrdersCountToday = $em->getRepository('IbtikarTaniaModelBundle:Order')->getShiftsOrdersCountToday();

        $responseShiftList = new ResponseShiftList();

        foreach ($shifts as $shift) {
            /* @var $shift \Ibtikar\TaniaModelBundle\Entity\Shift */

            $responseShift = new ResponseShift();
            $responseShift->id = $shift->getId();
            if (strtolower($request->headers->get('accept-language')) == 'en') {
                $responseShift->shift = $shift->getShift();
            } else {
                $responseShift->shift = $shift->getShiftAr();
            }
            $responseShift->from = strtotime($shift->getFrom()->format('H:i')) + 3 * 60 * 60;
            $responseShift->to = strtotime($shift->getTo()->format('H:i')) + 3 * 60 * 60;
            if ($shift->getMaximumAllowedOrdersPerDay()) {
                foreach ($shiftsOrdersCountToday as $shiftOrdersCountToday) {
                    if (isset($shiftOrdersCountToday['shiftId']) && $shiftOrdersCountToday['shiftId'] == $shift->getId()) {
                        if ($shiftOrdersCountToday['ordersCount'] >= $shift->getMaximumAllowedOrdersPerDay()) {
                            $responseShift->disabled = true;
                        }
                        break;
                    }
                }
            }
            if ($responseShift->disabled === false) {
                $firstTimeToday = new \DateTime('midnight');
                $firstTimeToday->modify($shift->getTo()
                    ->format('H:i'));
                if (time() > $firstTimeToday->getTimestamp()) {
                    $responseShift->disabled = true;
                }
            }

            $responseShift->dayId = $dayEntity->getId();
            if (strtolower($request->headers->get('accept-language')) == 'en') {
                $responseShift->day = $dayEntity->getDayEn();
            } else {
                $responseShift->day = $dayEntity->getDayAr();
            }
            $dayShifts = $dayEntity->getShifts();
            $countShifts = 0;
            foreach ($dayShifts as $dayShift) {
                if ($dayShift->getIsDeleted() == 0)
                    $countShifts ++;
            }

            if ($countShifts <= 0 || $dayEntity->getActive() == 0)
                $responseShift->dayActive = false;
            else
                $responseShift->dayActive = true;

            $responseShiftList->shifts[] = $responseShift;
        }
        return $apiOperations->getJsonResponseForObject($responseShiftList);
    }

    /**
     * List shifts in day
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Shift",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="time", "dataType"="string", "required"=true}
     *  },
     *  authentication=false,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function listShiftDateTimeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $dateTime = trim($request->query->get("time")); //timestamp
        if(strlen($dateTime)<=0){
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('time_stamp_required', array(), 'api_error_message'));
        }
        $actualDate=new \DateTime();
        $actualDate->setTimestamp($dateTime);
        $dayString = date('l', $dateTime);
        $actualDateMidnight=new \DateTime();
        $actualDateMidnight->setTimestamp($dateTime);
        $actualDateMidnight->modify("midnight");


        $dayEntity = $em->getRepository('IbtikarTaniaModelBundle:ShiftDays')->findOneBy(array(
            'dayEn' => $dayString
        ));
        if ($dayEntity == null) {
            $errorMsg = $translator->trans("day_not_found", array(), 'api_error_message');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        $shifts = $em->getRepository("IbtikarTaniaModelBundle:Shift")->findBy(array(
            'shiftDay' => $dayEntity,
            'isDeleted' => 0
        ));

        $shiftsOrdersCountToday = $em->getRepository('IbtikarTaniaModelBundle:Order')->getShiftsOrdersCountToday();

        $responseShiftList = new ResponseShiftList();

        foreach ($shifts as $shift) {
            $timeQuery=clone $actualDate;
            /* @var $shift \Ibtikar\TaniaModelBundle\Entity\Shift */
            $shiftsOrdersCountForShift = $em->getRepository('IbtikarTaniaModelBundle:Order')->getShiftsOrdersCountForShift($shift,$timeQuery);

            $responseShift = new ResponseShift();
            $responseShift->id = $shift->getId();
            if (strtolower($request->headers->get('accept-language')) == 'en') {
                $responseShift->shift = $shift->getShift();
            } else {
                $responseShift->shift = $shift->getShiftAr();
            }
            $responseShift->from = strtotime($shift->getFrom()->format('H:i')) ;//+ 3 * 60 * 60;
            $responseShift->to = strtotime($shift->getTo()->format('H:i')) ;//+ 3 * 60 * 60;
            if ($shift->getMaximumAllowedOrdersPerDay()) {
                if (isset($shiftsOrdersCountForShift[0]) &&isset($shiftsOrdersCountForShift[0]['shiftId']) && $shiftsOrdersCountForShift[0]['shiftId'] == $shift->getId()) {

                    if ($shiftsOrdersCountForShift[0]['ordersCount'] >= $shift->getMaximumAllowedOrdersPerDay()) {
                        $responseShift->disabled = true;
                    }
                    if (isset($shiftsOrdersCountForShift[0]) &&isset($shiftsOrdersCountForShift[0]['ordersCount'])){
                        $responseShift->ordersFound=$shiftsOrdersCountForShift[0]['ordersCount'];
                    }
                }
            }
            $responseShift->maximumAllowedOrdersPerDay=$shift->getMaximumAllowedOrdersPerDay();
            if ($responseShift->disabled === false) {
                $todayTime=new \DateTime("midnight");
                $firstTimeToday = new \DateTime();
                $firstTimeToday->setTimestamp($dateTime);
                $firstTimeToday->modify($shift->getTo()
                    ->format('H:i'));
                if($todayTime->getTimestamp()>$actualDateMidnight->getTimestamp()){
                    $responseShift->disabled = true;
                }else if($todayTime->getTimestamp()<$actualDateMidnight->getTimestamp()){
                    $responseShift->disabled = false;
                }else if ($todayTime->getTimestamp()==$actualDateMidnight->getTimestamp() && $actualDate->getTimestamp() > $firstTimeToday->getTimestamp()) {
                    $responseShift->disabled = true;
                }
            }

            $responseShift->dayId = $dayEntity->getId();
            if (strtolower($request->headers->get('accept-language')) == 'en') {
                $responseShift->day = $dayEntity->getDayEn();
            } else {
                $responseShift->day = $dayEntity->getDayAr();
            }
            $dayShifts = $dayEntity->getShifts();
            $countShifts = 0;
            foreach ($dayShifts as $dayShift) {
                if ($dayShift->getIsDeleted() == 0)
                    $countShifts ++;
            }

            if ($countShifts <= 0 || $dayEntity->getActive() == 0)
                $responseShift->dayActive = false;
            else
                $responseShift->dayActive = true;

            $responseShiftList->shifts[] = $responseShift;
        }
        return $apiOperations->getJsonResponseForObject($responseShiftList);
    }
}
