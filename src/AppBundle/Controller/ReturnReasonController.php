<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\DefaultController;
use Ibtikar\TaniaModelBundle\Entity\Reason;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ReturnReasonController extends DefaultController
{

    /**
     * Get Return Reasons
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "user application"="DarkCyan",
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function listAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');


        $reasons = $em->getRepository("IbtikarTaniaModelBundle:Reason")->findBy(['type' => Reason::TYPE_RETURN]);

        $reasonsResponse = new \AppBundle\APIResponse\Reason\ReasonSuccess();
        foreach ($reasons as $reason) {
            $reasonResponse = new \AppBundle\APIResponse\Reason\Reason();
            //$apiOperations->bindObjectDataFromObject($reasonResponse, $reason);
            $reasonResponse->id = $reason->getId();
            if($request->headers->get('accept-language') == 'en')
                $reasonResponse->title = $reason->getReasonEn();
            else
                $reasonResponse->title = $reason->getReasonAr();

            $reasonsResponse->reasons [] = $reasonResponse;
        }
        return new JsonResponse($reasonsResponse);

    }
}
