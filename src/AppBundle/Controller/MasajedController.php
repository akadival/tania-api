<?php

namespace AppBundle\Controller;

use AppBundle\APIResponse\Masjed\ResponseMasjedList;
use AppBundle\APIResponse\Masjed\RequestMasjed;
use Doctrine\Common\Util\Debug;
use Ibtikar\TaniaModelBundle\Repository\UserAddressRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Ibtikar\TaniaModelBundle\Entity\UserAddress;

class MasajedController extends Controller
{

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
    }

    /**
     * Get Masajed
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Masjed\RequestMasjed",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=true}
     *  },
     *  section="Masajed",
     *  tags={
     *     "user application"="DarkCyan",
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $requestMasjed = new RequestMasjed();
        $validationErrorsResponse = $apiOperations->bindAndValidateObjectDataFromRequest($requestMasjed, $request);
        if ($validationErrorsResponse) {
            return $validationErrorsResponse;
        }

        $page = ($request->get('page') && $request->get('page') > 0 ) ? $request->get('page') : 1;
//        $limit = ($request->get('limit') && $request->get('limit') > 0) ? $request->get('limit') : 10;
        $limit = 15;
//        if($limit > 50) {
//            $limit = 50;
//        }

        /* @var UserAddressRepository $masajedRepo */
        $masajedRepo = $em->getRepository("IbtikarTaniaModelBundle:UserAddress");
        $masajed = $masajedRepo->getAllMasajed($page, $requestMasjed->latitude, $requestMasjed->longitude, $limit);

        $masajedList = array();

        if(isset($masajed[$limit - 1]))
        {
            $isNextPage = true;
        } else {
            $isNextPage = false;
        }

        /* @var $masjed UserAddress */
        foreach($masajed as $masjed) {
            $distanceFromUser =(($requestMasjed->latitude && $requestMasjed->longitude)) ? $this->getDistance($requestMasjed->latitude, $requestMasjed->longitude, $masjed->getLatitude(), $masjed->getLongitude()) : false;
            if(!$distanceFromUser){
                $distanceFromUser = -1;
            }
            $masajedList[] = array('id' => $masjed->getId(), 'title' => $masjed->getTitle(), 'address' => $masjed->getAddress(), 'longitude' => (DOUBLE)$masjed->getLongitude(), 'latitude' => (DOUBLE)$masjed->getLatitude(), 'type' => UserAddress::TYPE_MASAJED, 'capacity' => $masjed->getCapacity(), 'distance' => $distanceFromUser);
        }

        usort($masajedList, array($this, 'cmp'));

        $responseItemList = new ResponseMasjedList($page, $isNextPage);
        $responseItemList->masajed = $masajedList;

        return $apiOperations->getJsonResponseForObject($responseItemList);
    }

    protected function getDistance($lat1, $lon1, $lat2, $lon2, $unit = "K") {

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $lon1 . "&destinations=" . $lat2 . "," . $lon2 . "&mode=driving&language=en-US&key=AIzaSyD0mokVIIqXYgFBEWcrgRwWT4QoBB7qp94";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $status = $response_a['rows'][0]['elements'][0]['status'];
        if (in_array($status, ["NOT_FOUND", "ZERO_RESULTS"])) {
            return FALSE;
        }
        $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
//        $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

        return $dist;

    }

    private function cmp($a, $b)
    {
        if($a['distance'] !== null) {
            return $a['distance'] >= $b['distance'];
        }
        return TRUE;
    }


}