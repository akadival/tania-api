<?php

namespace AppBundle\Controller;

use AppBundle\APIResponse\Offer\ResponseOfferList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\DefaultController;
use AppBundle\APIResponse\Item\RequestItem;

class OfferController extends DefaultController
{

    /**
     * Get Offers
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Offer",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=true}
     *  },
     *  tags={
     *     "user application"="DarkCyan"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

//        $user = $this->getUser();

        $page = ($request->get('page') && $request->get('page') > 0 ) ? $request->get('page') : 1;
        $limit = 25;

        $em->getFilters()->disable('softdeleteable');
        $offers = $em->createQueryBuilder()
                        ->select('i')
                        ->from('IbtikarTaniaModelBundle:Offer', 'i')
                        ->where('i.enabled = :isEnabled')
                        ->andWhere('i.expiryTime >= :now')
                        ->andWhere('i.deletedAt is NULL')
                        ->orderBy('i.createdAt','DESC')
                        ->setParameter('isEnabled', true)
                        ->setParameter('now', date('Y-m-d H:i:s', time()))
                        ->setMaxResults($limit)
                        ->setFirstResult(($page -1)* $limit)
                        ->getQuery()
                        ->getResult();

        if(isset($offers[$limit - 1]))
        {
            $isNextPage = true;
        } else {
            $isNextPage = false;
        }

        $offersResult = array();
        foreach ($offers as $offer){
            $offersResult[] = $this->buildOfferObject($request, $offer);
        }

        $responseOfferList = new ResponseOfferList($page, $isNextPage);
        $responseOfferList->offers = $offersResult;

        return $apiOperations->getJsonResponseForObject($responseOfferList);
    }

}
