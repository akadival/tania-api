<?php

namespace AppBundle\Controller;

use AppBundle\Controller\DefaultController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Ibtikar\TaniaModelBundle\Entity\Order;
use AppBundle\APIResponse\Item\RequestItem;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

/**
 * Description of MiscController
 *
 */
class MiscController extends DefaultController {

    /**
     * Get orders created at rang from and to date
     * mainly created for the tania's integration
     *
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getOrderWebServiceAction(Request $request) {
        $token = $request->get('token', null);
        $apiOperations = $this->get('api_operations');

        if (empty($token)) { /* not provided */
            return new JsonResponse(array("message" => "Token not provided"), 403);
        }

        if ($token != $this->getParameter('tania-intgration-webservice-token')) { /* not matched */
            return new JsonResponse(array("message" => "Invalid token", "token" =>$this->getParameter('tania-intgration-webservice-token')), 401);
        }

        $date_from = $request->get('date_from');
        $date_to = $request->get('date_to');

        $datefrom = new \DateTime(date('r', $date_from));
        $dateto = new \DateTime(date('r', $date_to));

        $em = $this->getDoctrine()->getManager();
        $orders = $em->getRepository('IbtikarTaniaModelBundle:Order')->getOrdersCreatedAt($datefrom, $dateto);

        if (count($orders) == 0) {
            return $apiOperations->getSuccessJsonResponse($this->get('translator')->trans('No Order is available'));
        }
        $ordersResponse = [];

        try {
            /* @var Order $order */
            foreach ($orders as $order) {
                $newOrder = [];
                $newOrder['OrderDate'] = $order->getCreatedAt() ? $order->getCreatedAt()->format('Y-m-d\TH:i:s') : "";
                $newOrder['OrderNumber'] = $order->getId();
                $newOrder['MobileNo'] = $order->getUser() ? $order->getUser()->getPhone() : $order->getCustomerPhone();
                $newOrder['EmailId'] = $order->getUser() ? $order->getUser()->getEmail() : "";
                $newOrder['FullName'] = $order->getUser() ? $order->getUser()->getFullName() : $order->getCustomerUsername();
                $newOrder['CityId'] = $order->getCity() ? $order->getCity()->getId() : 0;
                $newOrder['DriverId'] = $order->getDriver() ? $order->getDriver()->getId(): '';
                $newOrder['Remarks'] = $order->getNote();
                $newOrder['LatitudeDD'] = $order->getLatitude();
                $newOrder['LongitudeDD'] = $order->getLongitude();
                $newOrder['DataSource'] = $order->getSource() ? $order->getSource() : "";
                $newOrder['ExpectedDate'] = $order->getReceivingDate() ? date('Y-m-d\TH:i:s', $order->getReceivingDate() / 1000) : "";
                $newOrder['Status'] = array_search($order->getStatus(), array_values(Order::$statuses));
                $newOrder['Items'] = [];
                $itemOrders = $order->getOrderItems();
                foreach ($itemOrders as $itemOrder) {
                    $newitem = [];
                    $newitem['ItemCode'] =   $itemOrder->getItem() ? $itemOrder->getItem()->getId(): '';
                    $newitem['Quantity'] = $itemOrder->getCount();
                    $newitem['Price'] = $itemOrder->getPrice();
                    $newitem['RefItemCode'] = $itemOrder->getItem()->getReferenceCode();
                    $newOrder['Items'] [] = $newitem;
                }
                $ordersResponse[] = $newOrder;            }
        } catch (\Exception $ex) {
            // log error related
            $this->get('logger')->critical($ex->getMessage());
        }


        return $this->get('api_operations')->getJsonResponseForObject($ordersResponse);
    }

    /**
     * Get orders created at rang from and to date
     * mainly created for the tania's integration
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getUsersWebSeviceAction(Request $request) {
        $token = $request->get('token', null);
        $apiOperations = $this->get('api_operations');

        if (empty($token)) { /* not provided */
            return new JsonResponse(array("message" => "Token not provided"), 403);
        }

        if ($token != $this->getParameter('tania-intgration-webservice-token')) { /* not matched */
            return new JsonResponse(array("message" => "Invalid token", "token" =>$this->getParameter('tania-intgration-webservice-token')), 401);
        }

        $date_from = $request->get('date_from');
        $date_to = $request->get('date_to');

        $datefrom = new \DateTime(date('r', $date_from));
        $dateto = new \DateTime(date('r', $date_to));

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('IbtikarTaniaModelBundle:User')
            ->getUsersCreatedAt($datefrom, $dateto);
        $userResponse = [];
        foreach ($users as $user) {
            try {
                $_user = [];
                $_user['RegDate'] = $user->getCreatedAt()->format('c'); // ISO 8601
                $_user['RegNo'] = $user->getId();
                $_user['FullName'] = $user->getFullName();
                $_user['TelephoneNo'] = str_replace("+", "00", $user->getPhone());
                $_user['Address'] = $user->getAddress();
                $_user['Email'] = $user->getEmail();
                $_user['Password'] = $user->getUserPassword();
                $_user['City'] = $user->getCity() == null ? null :$user->getCity()->getId();
                $_user['Neighborhood'] = $user->getNeighborhood();
                $_user['Latitude'] = $user->getLatitude();
                $_user['Longitude'] = $user->getLongitude();
            } catch (Exception $ex) {
                $this->get('logger')->critical($ex->getMessage());
            }
            $userResponse[] = $_user;
        }

        if (!$users) {
            return $this->get('api_operations')->getNotFoundErrorJsonResponse($this->get('translator')->trans('No User is available'));
        }


        return $this->get('api_operations')->getJsonResponseForObject($userResponse);
    }

    /**
     * Get Items
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getItemListAction(Request $request){

        $token = $request->get('token', null);
        $apiOperations = $this->get('api_operations');

        if (empty($token)) { /* not provided */
            return new JsonResponse(array("message" => "Token not provided"), 403);
        }

        if ($token != $this->getParameter('tania-intgration-webservice-token')) { /* not matched */
            return new JsonResponse(array("message" => "Invalid token", "token" =>$this->getParameter('tania-intgration-webservice-token')), 401);
        }

        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $items = $em->getRepository("IbtikarTaniaModelBundle:Item")->findBy(array('shown' => true));

        if(count($items)<=0){
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }

        $itemResponse = [];
        foreach ($items as $item) {
            $prices = $item->getPrices();
            $newItem = [];
            $newItem['ItemCode'] = $item->getId();
            $newItem['ItemName'] = $item->getName();
            $newItem['ItemNameLang'] = $item->getNameEn();
            $newItem['DefaultPrice'] = $item->getDefaultPrice();
            $newItem['RefItemCode'] = $item->getReferenceCode();
            $newItem['Picture'] = $item->getWebPath();
            $newItem['CityItemPrices'] = [];
            foreach ($prices as $price) {
                $newItemPrice = [];
                $newItemPrice['CityId'] = $price->getCity()->getId();
                $newItemPrice['Price'] = $price->getPrice();
                $newItem['CityItemPrices'][] = $newItemPrice;
            }
            $itemResponse[] = $newItem;
        }


        return new JsonResponse($itemResponse);
    }


    /**
     * Get CityArea
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getCityAreaListAction(Request $request){

        $token = $request->get('token', null);
        $apiOperations = $this->get('api_operations');

        if (empty($token)) { /* not provided */
            return new JsonResponse(array("message" => "Token not provided"), 403);
        }

        if ($token != $this->getParameter('tania-intgration-webservice-token')) { /* not matched */
            return new JsonResponse(array("message" => "Invalid token"), 401);
        }

        $em = $this->getDoctrine()->getManager();
        /** @var $cityAreas Collection */
        $cityAreas = $em->getRepository('IbtikarTaniaModelBundle:CityArea')
            ->findAll();

        $response = [];
        foreach ($cityAreas as $area) {
            try {
                $_item = [];
                $_item['Id'] = $area->getId();
                $_item['NameAr'] = $area->getNameAr();
                $_item['NameEn'] = $area->getNameEn();
                $_item['CityId'] = $area->getCity()->getId();
                $_item['createdAt'] = $area->getCreatedAt()->format('c');
            } catch (Exception $ex) {
                $this->get('logger')->critical($ex->getMessage());
            }
            $response[] = $_item;
        }

        if (!$cityAreas) {
            return $this->get('api_operations')->getNotFoundErrorJsonResponse($this->get('translator')->trans('No CityArea is available'));
        }


        return $this->get('api_operations')->getJsonResponseForObject($response);

    }



    /**
     * Get User Suggestions and complains
     *
     *
     */
    public function getContactUsListAction(Request $request){
        $token = $request->get('token', null);
        $apiOperations = $this->get('api_operations');

        if (empty($token)) { /* not provided */
            return new JsonResponse(array("message" => "Token not provided"), 403);
        }

        if ($token != $this->getParameter('tania-intgration-webservice-token')) { /* not matched */
            return new JsonResponse(array("message" => "Invalid token"), 401);
        }

        $date_from = $request->get('date_from');
        $date_to = $request->get('date_to');

        $datefrom = null;
        $dateto = null;
        if ($date_from && $date_to) {
            $datefrom = new \DateTime(date('r', $date_from));
            $dateto = new \DateTime(date('r', $date_to));
        }

        $em = $this->getDoctrine()->getManager();

        $contactUs = $em->getRepository('IbtikarTaniaModelBundle:ContactUs')
            ->getContactUsCreatedAt($datefrom, $dateto);


        $response = [];
        foreach ($contactUs as $item) {
            try {
                $_item = [];
                $_item['Id'] = $item->getId();
                $_item['Title'] = $item->getTitle();
                $_item['Type'] = $item->getType();
                if ($item->getOrder()) {
                    $_item['OrderId'] = $item->getOrder()->getId();
                }
                $_item['UserId'] = $item->getUser()->getId();
                $_item['CreatedAt'] = $item->getCreatedAt()->format('c');

            } catch (Exception $ex) {
                $this->get('logger')->critical($ex->getMessage());
            }

            $response[] = $_item;
        }

        if (!$response) {
            return $this->get('api_operations')->getNotFoundErrorJsonResponse($this->get('translator')->trans('No ContactUs is available'));
        }


        return $this->get('api_operations')->getJsonResponseForObject($response);
    }

    /**
     * Get Offers
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Web Services",
     *  tags={
     *     "user application"="DarkCyan"
     *  },
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getCustomerGroupListCallAction(Request $request){
        $url = "http://185.10.112.195/taniawebservice/TaniaDataService.asmx/Get_CustomerGroup?CustGroupCode=0";

        $groupFieldName = $request->headers->get('accept-language') == "en" ? "//CustGroupName" : "//CustGroupNameLang";

        $xml = simplexml_load_file($url);
        $results = $xml->xpath($groupFieldName);
        $i = 0;
        $response = array();

        foreach ($results as $group){
            $response[$i]['id'] = $i+1;
            $response[$i]['name'] = (string) $group;
            $i++;
        }
        return $this->get('api_operations')->getJsonResponseForObject($response);
    }

}
