<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\DefaultController;

class VanController extends DefaultController
{

    /**
     * refill van
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Van",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function refillAction() {

        $translator = $this->get('translator');

        $apiOperations = $this->get('api_operations');
        $em = $this->getDoctrine()->getManager();
        $driver = $this->getUser();

        if(!count($driver->getVanDrivers()))
        {
            $errorMsg = $translator->trans("no_van_found", array(), 'api_error_message');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        $vanItems = $driver->getVanDrivers()[0]->getVan()->getVanItems();
        foreach($vanItems as $vanItem)
        {
            $vanItem->setCurrentCapacity($vanItem->getCapacity());
        }

        $em->flush();

        return $apiOperations->getSuccessJsonResponse();
    }
}
