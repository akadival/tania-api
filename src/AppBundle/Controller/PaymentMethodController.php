<?php

namespace AppBundle\Controller;

use AppBundle\APIResponse\Payment\RequestPayOneNonRecurring;
use Ibtikar\ShareEconomyPayFortBundle\Service\PayFortIntegration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\APIResponse\NotFound;
use AppBundle\APIResponse\AccessDenied;
use AppBundle\APIResponse\Success;
use Ibtikar\TaniaModelBundle\Entity\Order;
use Ibtikar\ShareEconomyToolsBundle\APIResponse as ToolsBundleAPIResponses;
use Ibtikar\ShareEconomyPayFortBundle\Entity\PfPaymentMethod;

class PaymentMethodController extends Controller
{

    /**
     * set payment method as default
     *
     * @ApiDoc(
     *  resource=true,
     *  section="PayFort",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  parameters={
     *      {"name"="type", "dataType"="integer", "required"=true, "description" = "CASH/SADAD/BALANCE/CREDIT"},
     *      {"name"="credit_card_id", "dataType"="integer", "required"=false, "description" = "required if type=credit"},
     *  },
     *  statusCodes = {
     *      200="Returned on success",
     *      403="Access denied",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @return JsonResponse
     */
    public function setDefaultAction(Request $request)
    {
        $apiOperations = $this->get('api_operations');
        $translator = $this->get('translator');
        $em   = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $type = $request->get('type');

        $paymentMethods = Order::$paymentMethodList;
        $paymentMethods = array_keys($paymentMethods);

        if(!in_array($type, $paymentMethods))
            return $apiOperations->getSingleErrorJsonResponse($translator->trans('invalid_payment_method', array(), 'validators'));

        if (in_array($type, array(Order::CASH, Order::SADAD, Order::BALANCE))) { //if set defautl to cash or balance
            $defaultPaymentMethod = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->findOneBy(['holder' => $user, 'isDefault' => true]);

            if ($defaultPaymentMethod) {
                $defaultPaymentMethod->setIsDefault(false);
            }

            $user->setPaymentMethod($type);
            $em->flush();

            $output = new Success();
        } elseif($type == Order::CREDIT) { //if default to credit
            if(!$request->get('credit_card_id'))
                return $apiOperations->getSingleErrorJsonResponse($translator->trans('fill_mandatory_field', array(), 'validators'));

            $paymentMethod = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->find($request->get('credit_card_id'));

            if (!$paymentMethod) {
                $output = new NotFound($translator->trans('payment_not_exist', array(), 'messages'));
            } elseif ($paymentMethod->getHolder()->getId() !== $user->getId()) {
                $output = new AccessDenied();
            } else {
                $defaultPaymentMethod = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->findOneBy(['holder' => $user, 'isDefault' => true]);

                if ($defaultPaymentMethod) {
                    $defaultPaymentMethod->setIsDefault(false);
                }

                $paymentMethod->setIsDefault(true);
                $user->setPaymentMethod($type);
                $em->flush();

                $output = new Success();
            }
        }

        return new JsonResponse($output);
    }

    /**
     * add credit card
     *
     * @ApiDoc(
     *  resource=true,
     *  section="PayFort",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  parameters={
     *      {"name"="cardNumber", "dataType"="string", "required"=true},
     *      {"name"="expiryDate", "dataType"="string", "required"=true},
     *      {"name"="tokenName", "dataType"="string", "required"=true},
     *      {"name"="paymentOption", "dataType"="string", "required"=false},
     *      {"name"="merchantReference", "dataType"="string", "required"=true},
     *      {"name"="fortId", "dataType"="string", "required"=true}
     *  },
     *  statusCodes = {
     *      200="Returned on success",
     *      403="Access denied",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if (!$user) {
            $output = new ToolsBundleAPIResponses\Fail();
            $output->message = 'Please login first';
        } else {
            $paymentMethod = new PfPaymentMethod();
            $paymentMethod->setHolder($user);
            $paymentMethod->setCardNumber($request->request->get('cardNumber'));
            $paymentMethod->setExpiryDate($request->request->get('expiryDate'));
            $paymentMethod->setTokenName($request->request->get('tokenName'));
            $paymentMethod->setPaymentOption($request->request->get('paymentOption'));
            $paymentMethod->setMerchantReference($request->request->get('merchantReference'));
            $paymentMethod->setFortId($request->request->get('fortId'));
            $paymentMethod->setIsDefault(false);

            $validationMessages = $this->get('api_operations')->validateObject($paymentMethod);

            if (count($validationMessages)) {
                $output = new ToolsBundleAPIResponses\ValidationErrors();
                $output->errors = $validationMessages;
            } else {
                $em->persist($paymentMethod);

                try {
                    $em->flush();

                    $output = $this->getPaymentMethodDetailsResponse($paymentMethod);
                } catch (\Exception $exc) {
                    $output = new ToolsBundleAPIResponses\InternalServerError();

                    $this->get('logger')->critical($exc->getMessage());
                }
            }
        }

        return new JsonResponse($output);
    }

    /**
     *
     * @param PfPaymentMethod $paymentMethod
     * @return \Ibtikar\ShareEconomyPayFortBundle\APIResponse\PaymentMethodDetailsResponse
     */
    private function getPaymentMethodDetailsResponse(PfPaymentMethod $paymentMethod)
    {
        $output = new \Ibtikar\ShareEconomyPayFortBundle\APIResponse\PaymentMethodDetailsResponse();
        $output->id = $paymentMethod->getId();
        $output->fortId = $paymentMethod->getFortId();
        $output->cardNumber = $paymentMethod->getCardNumber();
        $output->expiryDate = $paymentMethod->getExpiryDate();
        $output->merchantReference = $paymentMethod->getMerchantReference();
        $output->tokenName = $paymentMethod->getTokenName();
        $output->paymentOption = $paymentMethod->getPaymentOption();
        $output->isDefault = $paymentMethod->getIsDefault();

        return $output;
    }

    /**
     * delete payment method
     *
     * @ApiDoc(
     *  resource=true,
     *  section="PayFort",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  parameters={
     *      {"name"="id", "dataType"="integer", "required"=true},
     *  },
     *  statusCodes = {
     *      200="Returned on success",
     *      403="Access denied",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200="Ibtikar\ShareEconomyToolsBundle\APIResponse\Success",
     *      403="Ibtikar\ShareEconomyToolsBundle\APIResponse\AccessDenied",
     *      500="Ibtikar\ShareEconomyToolsBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @return JsonResponse
     */
    public function deleteAction(Request $request)
    {
        $em                      = $this->getDoctrine()->getManager();
        $user                    = $this->getUser();
        $paymentMethod           = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->find($request->request->get('id'));
        $preventLastDeletion     = $this->getParameter('ibtikar_share_economy_pay_fort.prevent_last_payment_method_removal');
        $userPaymentMethodsCount = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->countHolderPaymentMethods($user);

        $activePaymentOrders = $em->getRepository('IbtikarTaniaModelBundle:Order')->findBy(['user' => $user, 'status' => Order::$disallowDeletePaymentStatuses]);

        // payment method not found
        if (!$paymentMethod) {
            $output = new ToolsBundleAPIResponses\NotFound();
        }
        // not the owner of the payment method
        elseif ($paymentMethod->getHolder()->getId() !== $user->getId()) {
            $output = new ToolsBundleAPIResponses\AccessDenied();
        }
        // cannot delete the default payment method
        elseif ($paymentMethod->getIsDefault()) {
            $output          = new ToolsBundleAPIResponses\Fail();
            $output->message = $this->get('translator')->trans('cannot_delete_default_payment_method');
        }
        // if deletion of the last payment method prevented and this is the last one then do not remove
        elseif ($preventLastDeletion && $userPaymentMethodsCount == 1) {
            $output          = new ToolsBundleAPIResponses\Fail();
            $output->message = $this->get('translator')->trans('cannot_delete_last_payment_method');
        }
        elseif(count($activePaymentOrders))
        {
            $output          = new ToolsBundleAPIResponses\Fail();
            $output->message = $this->get('translator')->trans('cannot_delete_payment_method_with_active_order');
        }else {
            $paymentMethod->setTokenName(null);
            $paymentMethod->setIsDefault(false);
            $em->flush();

            $em->remove($paymentMethod);
            $em->flush();

            $output = new ToolsBundleAPIResponses\Success();
        }

        return new JsonResponse($output);
    }

    /**
     * get all supported payment methods
     *
     * @ApiDoc(
     *  resource=true,
     *  section="PayFort",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  statusCodes = {
     *      200="Returned on success",
     *      403="Access denied",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        $output = new Success();
        $output->methods = array(Order::CASH, Order::CREDIT);
        if ($this->getUser()) {
            $output->methods [] = Order::BALANCE;
        }
        return new JsonResponse($output);
    }

    public function payOneNonRecurringAction(Request $request)
    {
        $payOneRequest = new RequestPayOneNonRecurring();

        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $apiOperations->bindObjectDataFromJsonRequest($payOneRequest, $request);

        /* @var PayFortIntegration $payfort */
        $payfort = $this->get('ibtikar.shareeconomy.payfort.integration');

        try {
            $output = $payfort->nonRecurringPurchase($payOneRequest->token_name, 1, $payOneRequest->merchant_reference, "appsupport@tania.sa", $payOneRequest->return_url);
            return new JsonResponse($output);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage());
        }
    }
}