<?php

namespace AppBundle\Controller;

use AppBundle\APIResponse\Item\ResponseItem;
use AppBundle\APIResponse\Offer\ResponseBuyItem;
use AppBundle\APIResponse\Offer\ResponseGetItem;
use AppBundle\APIResponse\Offer\ResponseOffer;
use AppBundle\APIResponse\Order\NanaRequestOrder;
use AppBundle\APIResponse\Order\RequestOrderExternal;
use AppBundle\Controller\ResponseItemList;
use AppBundle\Service\APIOperations;
use Doctrine\Common\Util\Debug;
use Ibtikar\ShareEconomyPayFortBundle\Entity\PfTransaction;
use Ibtikar\ShareEconomyPayFortBundle\Entity\PfTransactionStatus;
use Ibtikar\TaniaModelBundle\Entity\Item;
use Ibtikar\TaniaModelBundle\Entity\Offer;
use Ibtikar\TaniaModelBundle\Entity\OrderOffer;
use Ibtikar\TaniaModelBundle\Entity\OrderOfferBuyItem;
use Ibtikar\TaniaModelBundle\Entity\OrderOfferGetItem;
use Ibtikar\TaniaModelBundle\Entity\User;
use Ibtikar\TaniaModelBundle\Service\NotificationCenter;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request as RequestAlias;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\DefaultController;
use AppBundle\APIResponse\Order\RequestOrder;
use AppBundle\APIResponse\Order\RequestEditOrder;
use AppBundle\APIResponse\Shift\ResponseShift;
use AppBundle\APIResponse\Order\ResponseOrderDriver;
use AppBundle\APIResponse\User as ResponseUser;
use AppBundle\APIResponse\Order\ResponseOrder;
use AppBundle\APIResponse\Order\RequestOrderItem;
use AppBundle\APIResponse\Order\PaymentMethod;
use Ibtikar\TaniaModelBundle\Entity\UserAddress;
use AppBundle\APIResponse\Order\Address;
use AppBundle\APIResponse\Order\ResponseOrderList;
use Ibtikar\ShareEconomyPayFortBundle\Entity\PfPaymentMethod;
use AppBundle\APIResponse\Order\ResponseOrderListItem;
use AppBundle\APIResponse\Order\ResponseOrderListData;
use Ibtikar\TaniaModelBundle\Entity\PromoCode;
use Ibtikar\TaniaModelBundle\Entity\OrderItem;
use Ibtikar\TaniaModelBundle\Entity\Order;
use Ibtikar\TaniaModelBundle\Entity\Shift;
use AppBundle\APIResponse\ValidationErrorsResponse;
use AppBundle\APIResponse\Order\Receipt;
use AppBundle\APIResponse\PromoCode\PromoCode as PromoCodeResponse;
use Ibtikar\GoogleServicesBundle\Service\FireBaseRetrievingData;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class OrderController extends DefaultController
{

    /**
     * Get Order List
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\RequestEditOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function editOLDAction(RequestAlias $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestOrder = new RequestEditOrder();
        $requestData = json_decode($request->getContent());
        $user = $this->getUser();
        if (!$user) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("please login"));
        }
        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("you are logged in as driver"));
        }

        $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
            'receipt' => 'AppBundle\APIResponse\Order\Receipt',
            'items' => '\AppBundle\APIResponse\Order\RequestOrderItem',
            'offers' => '\AppBundle\APIResponse\Offer\ResponseOffer',
            'buyItems' => '\AppBundle\APIResponse\Offer\ResponseBuyItem',
            'getItems' => '\AppBundle\APIResponse\Offer\ResponseGetItem',
            'paymentMethod' => '\AppBundle\APIResponse\Order\PaymentMethod',
            'promoCode' => '\AppBundle\APIResponse\PromoCode\PromoCode'
        ));

        if ($requestOrder->paymentMethod->type == 'CREDIT' && $em->getRepository('IbtikarTaniaModelBundle:Order')->findBy(array('user' => $user->getId(), 'status' => Order::$statuses['transaction-pending']))) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("You have orders with pending payment", array(), 'order'));
        }

        if ($requestOrder->receivingDate) {
            $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
        }
//        if($requestOrder->receivingDate && $requestOrder->receivingDate < (int)$this->getDateInTimeStamp(new \DateTime()))
//            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("receiving date should be larger than the current time"));

        /* @var Order $order */
        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(array('id'=> $requestOrder->orderId));
        if (!$order) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No order was found"));
        }
        if (!in_array($order->getStatus(), Order::$statusCategories['current'])) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Order is not editable anymore"));
        }

        $itemIds = array();
        $offerIds = array();

        $totalItemsCount = 0;
        $totalOffersCount = 0;

        $countArray = array();
        $offerCountArray = array();

        if (isset($requestData->items) && count($requestData->items) > 0) {
            foreach ($requestOrder->items as $requestOrderItem) {
                $errorsObjects = $this->get('validator')->validate($requestOrderItem, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }
                $itemIds[] = $requestOrderItem->id;
                $totalItemsCount += (integer) $requestOrderItem->count;
                $countArray[$requestOrderItem->id] = array('price' => $requestOrderItem->price, 'count' => $requestOrderItem->count);
            }
        } else {
            $requestOrder->items = array();
        }

        if (isset($requestData->offers) && count($requestData->offers) > 0) {
            $i = 0;
            /* @var $requestOrderOffer ResponseOffer */
            foreach ($requestOrder->offers as $requestOrderOffer) {
                if (!isset($requestData->offers[$i]->getItems) || (isset($requestData->offers[$i]->getItems) && count($requestData->offers[$i]->getItems) == 0)) {
                    $requestOrderOffer->getItems = array();
                }

                $requestOrderOffer->startTime = $requestOrderOffer->startTime ? $requestOrderOffer->startTime->getTimestamp() : null;
                $requestOrderOffer->expiryTime = $requestOrderOffer->expiryTime ? $requestOrderOffer->expiryTime->getTimestamp() : null;
                $requestOrderOffer->numberOfUsedTimes = 0;
                $errorsObjects = $this->get('validator')->validate($requestOrderOffer, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }

                $i++;
                $offerIds[] = $requestOrderOffer->id;
                $totalOffersCount += (integer)$requestOrderOffer->count;
                $offerCountArray[$requestOrderOffer->id] = array('price' => $requestOrderOffer->buyItemsCost, 'count' => $requestOrderOffer->count);
            }
        } else {
            $requestOrder->offers = array();
        }

        $minimumNumberOfItemsPerOrder = (integer) $this->get('settings_manager')->get('minimum_number_of_items_per_order');
        if ($minimumNumberOfItemsPerOrder && $minimumNumberOfItemsPerOrder > $totalItemsCount && count($offerIds) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You must order a minimum of %items-count% items in any order', array('%items-count%' => $minimumNumberOfItemsPerOrder), 'api_error_message'));
        }
        $requestOrder->paymentMethod->cardNumber = isset($requestOrder->paymentMethod->cardNumber) ? $requestOrder->paymentMethod->cardNumber : $requestOrder->paymentMethod->type;
        $errorsObjects = $this->get('validator')->validate($requestOrder->paymentMethod, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $creditCard = null;
        if ($requestOrder->paymentMethod->type == 'CREDIT') {
            $creditCard = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->findOneBy(array('id'=> $requestOrder->paymentMethod->id));
            if (!$creditCard) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("credit card was not found"));
            }
        } elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
            if ($user->getBalance() < $requestOrder->totalPrice) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No Enough Balance"));
            }
        }
        $errorsObjects = $this->get('validator')->validate($requestOrder, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $errorsObjects = $this->get('validator')->validate($requestOrder->receipt);
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $city = $order->getCity();
        $items = $em->createQueryBuilder()
            ->select('i')
            ->from('IbtikarTaniaModelBundle:Item', 'i')
            ->where('i.id in (:itemId)')
            ->andWhere('i.shown = :shown')
            ->setParameter('itemId', $itemIds)
            ->setParameter('shown', true)
            ->getQuery()
            ->getResult();

        $offers = $em->createQueryBuilder()
            ->select('o')
            ->from('IbtikarTaniaModelBundle:Offer', 'o')
            ->where('o.id in (:offerIds)')
            ->setParameter('offerIds', $offerIds)
            ->getQuery()
            ->getResult();

        if (count($items)<=0 && count($requestData->offers) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $itemNameGetter = 'getName';
        if ($request->getLocale() == 'en') {
            $itemNameGetter = 'getNameEn';
        }
        /* @var $item \Ibtikar\TaniaModelBundle\Entity\Item */
        foreach ($items as $item) {
            $minimumAmountToOrder = $item->getMinimumAmountToOrder();
            if ($minimumAmountToOrder && $minimumAmountToOrder > $countArray[$item->getId()]['count']) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You can only order a minimum of %item-minimum-count% items of %item-name%', array('%item-minimum-count%' => $minimumAmountToOrder, '%item-name%' => $item->$itemNameGetter()), 'api_error_message'));
            }
        }
        $responseItemList = $this->getItemObjectPerCity($request, $items, $city);
        $totalPrice = 0;
        foreach ($responseItemList->items as $item) {
            $totalPrice += ($item->price * $countArray[$item->getId()]['count']);
        }

        $currentOrderOffers = $order->getOrderOffers(); // TO Later exclude these orders from expiry & activation validations
        $currentOrderOfferIds = array();
        /* @var OrderOffer $currentOrderOffer*/
        foreach ($currentOrderOffers as $currentOrderOffer) {
            if ($o = $currentOrderOffer->getOffer()) {
                $currentOrderOfferIds[] = $o->getId();
            }
        }

        /* @var $offer \Ibtikar\TaniaModelBundle\Entity\Offer */
        foreach ($offers as $offer) {
            if (!in_array($offer->getId(), $currentOrderOfferIds) && $offer->getExpiryTime() && $offer->getExpiryTime()->getTimestamp() <= time()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }

            if (!in_array($offer->getId(), $currentOrderOfferIds) && !$offer->getEnabled()) { //Seperated in case we needed to change locale of validation error
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }
            $totalPrice += ($offer->getOfferPrice() * $offerCountArray[$offer->getId()]['count']);
        }

        $promocode = null;
        if ($requestOrder->promoCode && $requestOrder->promoCode->code) {
            $validationResponse = $this->get('order_operations')->getValidationErrorResponseForPromoCodeOrPromoCode($requestOrder->promoCode->code, $this->getUser(), $requestOrder->orderId, $city);
            if ($validationResponse instanceof JsonResponse) {
                return $validationResponse;
            }
            /* @var $promocode PromoCode */
            $promocode = $validationResponse;
        }

        $withoutTaxes = false;
        if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE') {
            $withoutTaxes = true;
        }
        $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true);
        // NEW-ISPL START
        /*if($totalPrice != $receipt->receipt->paidTotalPrice || $receipt->receipt->finalPrice != $requestOrder->receipt->finalPrice) //if cost of any item changed or fees changed
        {
            $receipt = $this->getReceipt($totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);

            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Costs Changed', array(), 'validators'), $receipt);
        }*/ // NEW-ISPL END

        $shift = null;
        if ($requestOrder->receivingDate) {
            if (!$requestOrder->shiftId) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('No shift was found'));
            }
            $shift = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findOneBy(array('id'=> $requestOrder->shiftId));
            if (!$shift) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
        } else {
            $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findAll();
            if (count($shifts) === 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
            $currentTime = new \DateTime();
            foreach ($shifts as $dayShift) {
                $shiftTimeToday = clone $currentTime;
                $shiftTimeToday->modify($dayShift->getTo()->format('H:i'));
                if ($shiftTimeToday > $currentTime) {
                    $shift = $dayShift;
                    $requestOrder->receivingDate = $currentTime->getTimestamp();
                    break;
                }
            }
            if (!$shift) {
                // No shift available today deliver tomorrow
                $requestOrder->receivingDate = new \DateTime('tomorrow');
                $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
                $shift = $shifts[0];
            }
        }
        $firstTimeToday = new \DateTime('midnight');
        $lastTimeToday = new \DateTime('tomorrow');
        if ($requestOrder->receivingDate >= $firstTimeToday->getTimestamp() && $requestOrder->receivingDate < $lastTimeToday->getTimestamp()) {
            $firstTimeToday->modify($shift->getTo()->format('H:i'));
            if (time() > $firstTimeToday->getTimestamp()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('shif_expired', array(), 'api_error_message'));
            }
        }

        $responseOrder = $this->applyEditOrder($user, $order, $requestOrder, $items, $shift, $countArray, $creditCard, $promocode, $withoutTaxes, $offerCountArray, $offers, $request->headers);
        return $apiOperations->getJsonResponseForObject($responseOrder);
    }

    /**
     * @author Moemen Hussein <moemen.hussein@ibtikar.net.sa>
     *
     * @param \Ibtikar\TaniaModelBundle\Entity\User $user
     * @param Order $order
     * @param RequestEditOrder $requestOrder
     * @param type $items
     * @param type $shift
     * @param type $countArray
     * @param PfPaymentMethod $creditCard
     * @param PromoCode $promoCode
     * @param boolean $withoutTaxes
     * @param array $offerCountArray
     * @param array $offers
     * @return ResponseItemList
     * @throws \Exception
     */
    protected function applyEditOrder($user, Order $order, RequestEditOrder $requestOrder, $items, $shift, $countArray, PfPaymentMethod $creditCard = null, PromoCode $promoCode = null, $withoutTaxes, $offerCountArray = array(), $offers = array(), $header = null)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\PromoCodeRepository */
        $promoCodeRepo = $em->getRepository('IbtikarTaniaModelBundle:PromoCode');
        /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\OrderRepository */
        $orderRepo = $em->getRepository('IbtikarTaniaModelBundle:Order');
        $translator = $this->get('translator');
        $totalPrice = 0;
        $oldPromoCode = $order->getPromoCode();
        if ($promoCode) {
            if ($promoCode !== $oldPromoCode) {
                // user changed the used promocode in the order
                if ($oldPromoCode) {
                    $usedByUserBefore = ((integer) $orderRepo->countPromoCodeUsedTimesByUser($user->getId(), $oldPromoCode->getId())) > 1 ? true : false;
                    $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $oldPromoCode, $offers, $offerCountArray);
                    if (isset($receipt->discount)) {
                        $promoCodeRepo->decreasePromoCodeUsage($oldPromoCode->getId(), $receipt->discount, $usedByUserBefore);
                    } else {
                        $promoCodeRepo->decreasePromoCodeUsage($oldPromoCode->getId(), $receipt->receipt->discount, $usedByUserBefore);
                    }
                }
                $order->setPromoCodeMethod($promoCode->getType());
                $order->setPromoCodeName($promoCode->getCode());
                $order->setPromoCodeValue($promoCode->getDiscountAmount());
                $order->setPromoCode($promoCode);
                $usedByUserBefore = ((integer) $orderRepo->countPromoCodeUsedTimesByUser($user->getId(), $promoCode->getId())) > 1 ? true : false;
                $promoCodeRepo->increasePromoCodeUsage($promoCode->getId(), $requestOrder->receipt->discount, $usedByUserBefore);
            }
        } else {
            // user removed the promocode
            if ($oldPromoCode) {
                $order->setPromoCodeMethod(null);
                $order->setPromoCodeName(null);
                $order->setPromoCodeValue(null);
                $order->setPromoCode(null);
                $usedByUserBefore = ((integer) $orderRepo->countPromoCodeUsedTimesByUser($user->getId(), $oldPromoCode->getId())) > 1 ? true : false;
                $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $oldPromoCode, $offers, $offerCountArray);
                if (isset($receipt->discount)) {
                    $promoCodeRepo->decreasePromoCodeUsage($oldPromoCode->getId(), $receipt->discount, $usedByUserBefore);
                } else {
                    $promoCodeRepo->decreasePromoCodeUsage($oldPromoCode->getId(), $receipt->receipt->discount, $usedByUserBefore);
                }
            }
        }
        foreach ($order->getOrderItems() as $orderItems) {
            $em->remove($orderItems);
        }
        if ($order->getPaymentMethod() == 'BALANCE') {
            $user->setBalance(($user->getBalance() + $order->getAmountDue()));
            $user->setUsedBalance($user->getUsedBalance() - $order->getAmountDue());
        }
        $order->setPaymentMethod($requestOrder->paymentMethod->type);
        $order->setCreditCard($creditCard);
        if ($creditCard) {
            $order->setCardNumber($creditCard->getCardNumber());
            $order->setExpiryDate($creditCard->getExpiryDate());
            $order->setFortId($creditCard->getFortId());
            $order->setIsDefault($creditCard->getIsDefault());
            $order->setMerchantReference($creditCard->getMerchantReference());
            $order->setPaymentOption($creditCard->getPaymentOption());
            $order->setTokenName($creditCard->getTokenName());
        } elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
            $order->setPaymentValue($requestOrder->paymentMethod->value);
            $order->setCardNumber($requestOrder->paymentMethod->type);
            $user->setBalance(($user->getBalance() - $requestOrder->receipt->finalPrice));
            $user->setUsedBalance($user->getUsedBalance() + $requestOrder->receipt->finalPrice);
            $em->persist($user);
        } else {
            $order->setCardNumber($requestOrder->paymentMethod->type);
        }
        if ($requestOrder->receivingDate) {
            $order->setReceivingDate($requestOrder->receivingDate);
        } else {
            $order->setReceivingDate($this->getDateInTimeStamp(new \DateTime()));
        }

        $order->setShift($shift);
        $order->setShiftFrom($shift->getFrom());
        $order->setShiftTo($shift->getTo());

        $order->setAmountDue($requestOrder->receipt->finalPrice);
        $order->setPrice($requestOrder->receipt->totalPrice);
        $order->setTaxFees($requestOrder->receipt->taxFees);
        $order->setNote($requestOrder->note);
        $order->setAppVersion($header->get('app-version'));
        $order->setDeviceInformation($header->get('device-information'));
        $em->persist($order);
        foreach ($items as $item) {
            $orderItem = new OrderItem();
            $orderItem->setCity($order->getCity());
            $orderItem->setCount($countArray[$item->getId()]['count']);
            $orderItem->setItem($item);
            $orderItem->setPrice($countArray[$item->getId()]['price']);
            $order->addOrderItem($orderItem);
            $orderItem->setOrder($order);
            $em->persist($orderItem);
        }

        /* @var OrderOffer $orderOffer */
        foreach ($order->getOrderOffers() as $orderOffer) {
            if ($oo = $orderOffer->getOffer()) {
                $oo->setNumberOfUsedTimes(max(0, $oo->getNumberOfUsedTimes() - $offerCountArray[$oo->getId()]['count'])); // DECREMENT Offer Usage
            }
            $em->remove($orderOffer);
        }

        /* @var Offer $offer */
        foreach ($offers as $offer) {
            $orderOffer = new OrderOffer();
            $orderOffer->setType($offer->getType());
            $orderOffer->setPrice($offer->getOfferPrice());
            $orderOffer->setCount($offerCountArray[$offer->getId()]['count']);
            $orderOffer->setOffer($offer);
            $orderOffer->setOrder($order);
            $orderOffer->setCashGetAmount($offer->getCashGetAmount());
            $orderOffer->setPercentageGetAmount($offer->getPercentageGetAmount());
            $orderOffer->setDescriptionPublic($offer->getDescriptionPublic());
            $orderOffer->setDescriptionPublicEn($offer->getDescriptionPublicEn());
            $orderOffer->setTitle($offer->getTitle());
            $orderOffer->setTitleEn($offer->getTitleEn());
            $order->addOrderOffer($orderOffer);

            $em->persist($orderOffer);
            $offer->setNumberOfUsedTimes($offer->getNumberOfUsedTimes() + $offerCountArray[$offer->getId()]['count']); //INCREMENT Offer Usage
        }

        $em->flush();
        $responseOrderListData = $this->getUserOrderObject($order);
        $driver = $order->getDriver();
        if ($driver) {
            $title = $translator->trans('Order Edited', array('%orderId%' => $order->getId()), 'notifications', $driver->getLocale());
            $body = $translator->trans('Order Edited', array(
                '%orderId%' => $order->getId(),
            ), 'notifications', $driver->getLocale());

            $this->get('user_device_notification')->sendNotificationToUser($driver, $title, $body, array(
                'id' => $order->getId(),
                'title' => $title,
                'body' => $body,
                'type' => NotificationCenter::USER_ORDER_EDITED
            ));
        }
        return $responseOrderListData;
    }

    /**
     * Get Order List
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\RequestEditOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function editAction(RequestAlias $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestOrder = new RequestEditOrder();
        $requestData = json_decode($request->getContent());
        $user = $this->getUser();
        if (!$user) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("please login"));
        }
        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("you are logged in as driver"));
        }

        $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
            'receipt' => 'AppBundle\APIResponse\Order\Receipt',
            'items' => '\AppBundle\APIResponse\Order\RequestOrderItem',
            'offers' => '\AppBundle\APIResponse\Offer\ResponseOffer',
            'buyItems' => '\AppBundle\APIResponse\Offer\ResponseBuyItem',
            'getItems' => '\AppBundle\APIResponse\Offer\ResponseGetItem',
            'paymentMethod' => '\AppBundle\APIResponse\Order\PaymentMethod',
            'promoCode' => '\AppBundle\APIResponse\PromoCode\PromoCode'
        ));

        if ($requestOrder->paymentMethod->type == 'CREDIT' && $em->getRepository('IbtikarTaniaModelBundle:Order')->findBy(array('user' => $user->getId(), 'status' => Order::$statuses['transaction-pending']))) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("You have orders with pending payment", array(), 'order'));
        }

        if ($requestOrder->receivingDate) {
            $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
        }
//        if($requestOrder->receivingDate && $requestOrder->receivingDate < (int)$this->getDateInTimeStamp(new \DateTime()))
//            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("receiving date should be larger than the current time"));

        /* @var Order $order */
        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(array('id'=> $requestOrder->orderId));
        if (!$order) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No order was found"));
        }
        if (!in_array($order->getStatus(), Order::$statusCategories['current'])) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Order is not editable anymore"));
        }

        if ($order->getPaymentMethod()=='BALANCE') {
            $userBalance = $user->getBalance() + $order->getFirstPaymentPrice();
        } else {
            $userBalance = $user->getBalance();
        }

        $itemIds = array();
        $offerIds = array();

        $totalItemsCount = 0;
        $totalOffersCount = 0;

        $countArray = array();
        $offerCountArray = array();

        if (isset($requestData->items) && count($requestData->items) > 0) {
            foreach ($requestOrder->items as $requestOrderItem) {
                $errorsObjects = $this->get('validator')->validate($requestOrderItem, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }
                $itemIds[] = $requestOrderItem->id;
                $totalItemsCount += (integer) $requestOrderItem->count;
                $countArray[$requestOrderItem->id] = array('price' => $requestOrderItem->price, 'count' => $requestOrderItem->count);
            }
        } else {
            $requestOrder->items = array();
        }

        if (isset($requestData->offers) && count($requestData->offers) > 0) {
            $i = 0;
            /* @var $requestOrderOffer ResponseOffer */
            foreach ($requestOrder->offers as $requestOrderOffer) {
                if (!isset($requestData->offers[$i]->getItems) || (isset($requestData->offers[$i]->getItems) && count($requestData->offers[$i]->getItems) == 0)) {
                    $requestOrderOffer->getItems = array();
                }

                $requestOrderOffer->startTime = $requestOrderOffer->startTime ? $requestOrderOffer->startTime->getTimestamp() : null;
                $requestOrderOffer->expiryTime = $requestOrderOffer->expiryTime ? $requestOrderOffer->expiryTime->getTimestamp() : null;
                $requestOrderOffer->numberOfUsedTimes = 0;
                $errorsObjects = $this->get('validator')->validate($requestOrderOffer, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }

                $i++;
                $offerIds[] = $requestOrderOffer->id;
                $totalOffersCount += (integer)$requestOrderOffer->count;
                $offerCountArray[$requestOrderOffer->id] = array('price' => $requestOrderOffer->buyItemsCost, 'count' => $requestOrderOffer->count);
            }
        } else {
            $requestOrder->offers = array();
        }

        $minimumNumberOfItemsPerOrder = (integer) $this->get('settings_manager')->get('minimum_number_of_items_per_order');
        if ($minimumNumberOfItemsPerOrder && $minimumNumberOfItemsPerOrder > $totalItemsCount && count($offerIds) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You must order a minimum of %items-count% items in any order', array('%items-count%' => $minimumNumberOfItemsPerOrder), 'api_error_message'));
        }
        $requestOrder->paymentMethod->cardNumber = isset($requestOrder->paymentMethod->cardNumber) ? $requestOrder->paymentMethod->cardNumber : $requestOrder->paymentMethod->type;
        $errorsObjects = $this->get('validator')->validate($requestOrder->paymentMethod, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $creditCard = null;
        if ($requestOrder->paymentMethod->type == 'CREDIT') {
            $creditCard = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->findOneBy(array('id'=> $requestOrder->paymentMethod->id));
            if (!$creditCard) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("credit card was not found"));
            }
        } elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
            //if($user->getBalance() < $requestOrder->totalPrice){
            if ($userBalance==0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No Enough Balance"));
            }
        }
        $errorsObjects = $this->get('validator')->validate($requestOrder, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
    
        // NEW-ISPL on 21/01/2019
        if ($requestOrder->addressId) {
            $address = $em->getRepository('IbtikarTaniaModelBundle:UserAddress')->findOneBy(array('id'=> $requestOrder->addressId));
            if (!$address) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No address was found"));
            } else {
                $cityArea = $this->getCityAreaPerLatLong($address->getLatitude(), $address->getLongitude());
            }
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        } elseif ($requestOrder->lat && $requestOrder->long) {
            $address = null;
            $cityArea = $this->getCityAreaPerLatLong($requestOrder->lat, $requestOrder->long);
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        } else {
            $address = null;
            $cityArea = $this->getCityAreaPerLatLong($this->getUser()->getLatitude(), $this->getUser()->getLongitude());
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        }
        // NEW-ISPL on 21/01/2019
        $errorsObjects = $this->get('validator')->validate($requestOrder->receipt);
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }

        $city = $order->getCity();
        $items = $em->createQueryBuilder()
            ->select('i')
            ->from('IbtikarTaniaModelBundle:Item', 'i')
            ->where('i.id in (:itemId)')
            ->andWhere('i.shown = :shown')
            ->setParameter('itemId', $itemIds)
            ->setParameter('shown', true)
            ->getQuery()
            ->getResult();

        $offers = $em->createQueryBuilder()
            ->select('o')
            ->from('IbtikarTaniaModelBundle:Offer', 'o')
            ->where('o.id in (:offerIds)')
            ->setParameter('offerIds', $offerIds)
            ->getQuery()
            ->getResult();

        if (count($items)<=0 && count($requestData->offers) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $itemNameGetter = 'getName';
        if ($request->getLocale() == 'en') {
            $itemNameGetter = 'getNameEn';
        }
        /* @var $item \Ibtikar\TaniaModelBundle\Entity\Item */
        foreach ($items as $item) {
            $minimumAmountToOrder = $item->getMinimumAmountToOrder();
            if ($minimumAmountToOrder && $minimumAmountToOrder > $countArray[$item->getId()]['count']) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You can only order a minimum of %item-minimum-count% items of %item-name%', array('%item-minimum-count%' => $minimumAmountToOrder, '%item-name%' => $item->$itemNameGetter()), 'api_error_message'));
            }
        }
        $responseItemList = $this->getItemObjectPerCity($request, $items, $city);
        $totalPrice = 0;
        foreach ($responseItemList->items as $item) {
            $totalPrice += ($item->price * $countArray[$item->id]['count']);
        }

        $currentOrderOffers = $order->getOrderOffers(); // TO Later exclude these orders from expiry & activation validations
        $currentOrderOfferIds = array();
        /* @var OrderOffer $currentOrderOffer*/
        foreach ($currentOrderOffers as $currentOrderOffer) {
            if ($o = $currentOrderOffer->getOffer()) {
                $currentOrderOfferIds[] = $o->getId();
            }
        }

        /* @var $offer \Ibtikar\TaniaModelBundle\Entity\Offer */
        foreach ($offers as $offer) {
            if (!in_array($offer->getId(), $currentOrderOfferIds) && $offer->getExpiryTime() && $offer->getExpiryTime()->getTimestamp() <= time()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }

            if (!in_array($offer->getId(), $currentOrderOfferIds) && !$offer->getEnabled()) { //Seperated in case we needed to change locale of validation error
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }
            $totalPrice += ($offer->getOfferPrice() * $offerCountArray[$offer->getId()]['count']);
        }

        $promocode = null;
        if ($requestOrder->promoCode && $requestOrder->promoCode->code) {
            $validationResponse = $this->get('order_operations')->getValidationErrorResponseForPromoCodeOrPromoCode($requestOrder->promoCode->code, $this->getUser(), $requestOrder->orderId, $city);
            if ($validationResponse instanceof JsonResponse) {
                return $validationResponse;
            }
            /* @var $promocode PromoCode */
            $promocode = $validationResponse;
        }

        $withoutTaxes = false;
        if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $userBalance >= $requestOrder->receipt->finalPrice) {
            $withoutTaxes = true;
        }
        /* $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, TRUE);
        */
       
        // NEW-ISPL START on 31/12/2018
        $secondPaymentPrice = 0;
        $secondPaymentMethod= null;
        if($requestOrder->paymentMethod->type == 'CASH')
        {
            $firstPaymentPrice = $requestOrder->receipt->finalPrice;
            $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true);
        } elseif ($requestOrder->paymentMethod->type == 'CREDIT') {
            $firstPaymentPrice = $requestOrder->receipt->finalPrice;
            //$requestOrder->paymentMethod->type = 'CASH';
            $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true);
        } else {
            if ($userBalance < $requestOrder->receipt->finalPrice) {
                $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];

                $requestOrder->receipt->taxFees = ((($requestOrder->totalPrice - $userBalance) * $fees->getTaxPercent()) / 100);
                $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);

                $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));

                $secondPaymentMethod = 'CASH';
                $firstPaymentPrice = $userBalance;
                $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $userBalance);
                $receipt = $this->getReceipt(
                    $requestOrder->receipt->totalPrice,
                    $withoutTaxes,
                    $promocode,
                    $offers,
                    $offerCountArray,
                    true,
                    ($requestOrder->receipt->totalPrice-$firstPaymentPrice)
                );

                $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];
                $requestOrder->receipt->taxFees = ($fees->getTaxPercent() * ($requestOrder->receipt->totalPrice-$firstPaymentPrice) / 100);
                $requestOrder->receipt->finalPrice =
                ($requestOrder->receipt->totalPrice - $receipt->receipt->offersDiscount) + ($requestOrder->receipt->taxFees) - $requestOrder->receipt->discount;
                //$secondPaymentPrice = ($receipt->receipt->finalPrice - $firstPaymentPrice);
                $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $firstPaymentPrice);
            } else {
                $requestOrder->receipt->taxFees = 0;
                $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);
                $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));
                $firstPaymentPrice = $requestOrder->receipt->finalPrice;
                $receipt = $this->getReceipt(
                    $requestOrder->receipt->totalPrice,
                    $withoutTaxes,
                    $promocode,
                    $offers,
                    $offerCountArray,
                    true,
                    ($requestOrder->receipt->totalPrice-$firstPaymentPrice)
                );
            }
        }
        // NEW-ISPL END on 31/12/2018
        // NEW-ISPL START
        /*if($totalPrice != $receipt->receipt->paidTotalPrice || $receipt->receipt->finalPrice != $requestOrder->receipt->finalPrice) //if cost of any item changed or fees changed
        {
            $receipt = $this->getReceipt($totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);

            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Costs Changed', array(), 'validators'), $receipt);
        }*/ // NEW-ISPL END

        $shift = null;
        if ($requestOrder->receivingDate) {
            if (!$requestOrder->shiftId) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('No shift was found'));
            }
            $shift = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findOneBy(array('id'=> $requestOrder->shiftId));
            if (!$shift) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
        } else {
            $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findAll();
            if (count($shifts) === 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
            $currentTime = new \DateTime();
            foreach ($shifts as $dayShift) {
                $shiftTimeToday = clone $currentTime;
                $shiftTimeToday->modify($dayShift->getTo()->format('H:i'));
                if ($shiftTimeToday > $currentTime) {
                    $shift = $dayShift;
                    $requestOrder->receivingDate = $currentTime->getTimestamp();
                    break;
                }
            }
            if (!$shift) {
                // No shift available today deliver tomorrow
                $requestOrder->receivingDate = new \DateTime('tomorrow');
                $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
                $shift = $shifts[0];
            }
        }
        $firstTimeToday = new \DateTime('midnight');
        $lastTimeToday = new \DateTime('tomorrow');
        if ($requestOrder->receivingDate >= $firstTimeToday->getTimestamp() && $requestOrder->receivingDate < $lastTimeToday->getTimestamp()) {
            $firstTimeToday->modify($shift->getTo()->format('H:i'));
            if (time() > $firstTimeToday->getTimestamp()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('shif_expired', array(), 'api_error_message'));
            }
        }

        $responseOrder = $this->applyEditOrderV2($user, $order, $requestOrder, $items, $city, $cityArea, $shift, $address, $countArray, $creditCard, $promocode, $withoutTaxes, $offerCountArray, $offers, $firstPaymentPrice, $secondPaymentPrice, $secondPaymentMethod, $request->headers);

        return $apiOperations->getJsonResponseForObject($responseOrder);
    }

    /**
     * @author Moemen Hussein <moemen.hussein@ibtikar.net.sa>
     *
     * @param \Ibtikar\TaniaModelBundle\Entity\User $user
     * @param Order $order
     * @param RequestEditOrder $requestOrder
     * @param type $items
     * @param $city
     * @param $cityArea
     * @param type $shift
     * @param $address
     * @param type $countArray
     * @param PfPaymentMethod $creditCard
     * @param PromoCode $promoCode
     * @param boolean $withoutTaxes
     * @param array $offerCountArray
     * @param array $offers
     * @param null $firstPaymentPrice
     * @param null $secondPaymentPrice
     * @param null $secondPaymentMethod
     * @return ResponseItemList
     * @throws \Exception
     */
    protected function applyEditOrderV2($user, Order $order, RequestEditOrder $requestOrder, $items, $city, $cityArea, $shift, $address, $countArray, PfPaymentMethod $creditCard = null, PromoCode $promoCode = null, $withoutTaxes, $offerCountArray = array(), $offers = array(), $firstPaymentPrice = null, $secondPaymentPrice = null, $secondPaymentMethod = null, $header)
    {

        // NEW-ISPL
        $isPaymentChanged = (isset($requestOrder->isPaymentChanged)?$requestOrder->isPaymentChanged:true);
        // NEW-ISPL
        $em = $this->getDoctrine()->getManager();
        /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\PromoCodeRepository */
        $promoCodeRepo = $em->getRepository('IbtikarTaniaModelBundle:PromoCode');
        /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\OrderRepository */
        $orderRepo = $em->getRepository('IbtikarTaniaModelBundle:Order');
        $translator = $this->get('translator');
        $totalPrice = 0;
        if ($isPaymentChanged) {
            $oldPromoCode = $order->getPromoCode();
            if ($promoCode) {
                if ($promoCode !== $oldPromoCode) {
                    // user changed the used promocode in the order
                    if ($oldPromoCode) {
                        $usedByUserBefore = ((integer) $orderRepo->countPromoCodeUsedTimesByUser($user->getId(), $oldPromoCode->getId())) > 1 ? true : false;
                        $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $oldPromoCode, $offers, $offerCountArray);
                        if (isset($receipt->discount)) {
                            $promoCodeRepo->decreasePromoCodeUsage($oldPromoCode->getId(), $receipt->discount, $usedByUserBefore);
                        } else {
                            $promoCodeRepo->decreasePromoCodeUsage($oldPromoCode->getId(), $receipt->receipt->discount, $usedByUserBefore);
                        }
                    }
                    $order->setPromoCodeMethod($promoCode->getType());
                    $order->setPromoCodeName($promoCode->getCode());
                    $order->setPromoCodeValue($promoCode->getDiscountAmount());
                    $order->setPromoCode($promoCode);
                    $usedByUserBefore = ((integer) $orderRepo->countPromoCodeUsedTimesByUser($user->getId(), $promoCode->getId())) > 1 ? true : false;
                    $promoCodeRepo->increasePromoCodeUsage($promoCode->getId(), $requestOrder->receipt->discount, $usedByUserBefore);
                }
            } else {
                // user removed the promocode
                if ($oldPromoCode) {
                    $order->setPromoCodeMethod(null);
                    $order->setPromoCodeName(null);
                    $order->setPromoCodeValue(null);
                    $order->setPromoCode(null);
                    $usedByUserBefore = ((integer) $orderRepo->countPromoCodeUsedTimesByUser($user->getId(), $oldPromoCode->getId())) > 1 ? true : false;
                    $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $oldPromoCode, $offers, $offerCountArray);
                    if (isset($receipt->discount)) {
                        $promoCodeRepo->decreasePromoCodeUsage($oldPromoCode->getId(), $receipt->discount, $usedByUserBefore);
                    } else {
                        $promoCodeRepo->decreasePromoCodeUsage($oldPromoCode->getId(), $receipt->receipt->discount, $usedByUserBefore);
                    }
                }
            }
            foreach ($order->getOrderItems() as $orderItems) {
                $em->remove($orderItems);
            }
            $userBalance = $user->getBalance();
            if ($order->getPaymentMethod() == 'BALANCE') {
                //$user->setBalance(($user->getBalance() + $order->getAmountDue()));
                //$user->setUsedBalance($user->getUsedBalance() - $order->getAmountDue());
                $userBalance += $order->getFirstPaymentPrice();
                $usedBalance = $user->getUsedBalance() - $order->getFirstPaymentPrice();
                $user->setBalance($userBalance);
                $user->setUsedBalance($usedBalance);
                //$em->persist($user);
            }
            $order->setPaymentMethod($requestOrder->paymentMethod->type);
            $order->setCreditCard($creditCard);
            if ($creditCard) {
                $order->setCardNumber($creditCard->getCardNumber());
                $order->setExpiryDate($creditCard->getExpiryDate());
                $order->setFortId($creditCard->getFortId());
                $order->setIsDefault($creditCard->getIsDefault());
                $order->setMerchantReference($creditCard->getMerchantReference());
                $order->setPaymentOption($creditCard->getPaymentOption());
                $order->setTokenName($creditCard->getTokenName());
            } elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
                $cartPrice = $requestOrder->receipt->totalPrice - ($requestOrder->receipt->offersDiscount + $requestOrder->receipt->taxFees);
                //$order->setPaymentValue($requestOrder->paymentMethod->value);
                $order->setPaymentValue($cartPrice);
                $order->setCardNumber($requestOrder->paymentMethod->type);
                if ($userBalance > $requestOrder->receipt->finalPrice) {
                    $user->setBalance($userBalance - $requestOrder->receipt->finalPrice);
                    $user->setUsedBalance(($user->getUsedBalance() - $userBalance) + $requestOrder->receipt->finalPrice);
                }
                // NEW-ISPL START on 30/12/2018
                else {
                    $user->setUsedBalance($user->getUsedBalance() + $user->getBalance());
                    $user->setBalance(0);
                }
                // NEW-ISPL START on 30/12/2018
                //$em->persist($user);
            } else {
                $order->setCardNumber($requestOrder->paymentMethod->type);
            }
            // NEW-ISPL START on 30/12/2018
            $em->persist($user);
            if ($requestOrder->receivingDate) {
                $order->setReceivingDate($requestOrder->receivingDate);
            } else {
                $order->setReceivingDate($this->getDateInTimeStamp(new \DateTime()));
            }
        }

        $order->setShift($shift);
        $order->setShiftFrom($shift->getFrom());
        $order->setShiftTo($shift->getTo());
        // NEW-ISPL START on 21/01/2019
        $order->setCity($city);
        $order->setCityArea($cityArea);
        $order->setCityAreaNameEn($cityArea->getNameEn());
        $order->setCityAreaNameAr($cityArea->getNameAr());
        $order->setUser($this->getUser());
        /* @var Order $order */
        if ($address) {
            $order->setTitle($address->getTitle());
            $order->setAddress($address->getAddress());
            $order->setLatitude($address->getLatitude());
            $order->setLongitude($address->getLongitude());
            $order->setUserAddress($address);
            $order->setAddressType($address->getType());
        } else {
            $order->setAddress($this->getUser()->getAddress());
            $order->setLatitude($this->getUser()->getLatitude());
            $order->setLongitude($this->getUser()->getLongitude());
        }
        // NEW-ISPL END on 21/01/2019
        //NEW-ISPL START on 30/12/2018
        if ($isPaymentChanged) {
            //$order->setFirstPaymentMethod($requestOrder->firstPaymentMethod);
            $order->setSecondPaymentMethod($secondPaymentMethod);
            $order->setFirstPaymentPrice($firstPaymentPrice);
            $order->setSecondPaymentPrice($secondPaymentPrice);
                
            $order->setAmountDue($requestOrder->receipt->finalPrice);
            $order->setPrice($requestOrder->receipt->totalPrice);
            $order->setTaxFees($requestOrder->receipt->taxFees);
            //NEW-ISPL END on 30/12/2018
        }
        $order->setNote($requestOrder->note);
        $order->setAppVersion($header->get('app-version'));
        $order->setDeviceInformation($header->get('device-information'));
        $em->persist($order);
        if ($isPaymentChanged) {
            foreach ($items as $item) {
                $orderItem = new OrderItem();
                $orderItem->setCity($order->getCity());
                $orderItem->setCount($countArray[$item->getId()]['count']);
                $orderItem->setItem($item);
                $orderItem->setPrice($countArray[$item->getId()]['price']);
                $order->addOrderItem($orderItem);
                $orderItem->setOrder($order);
                $em->persist($orderItem);
            }

            /* @var OrderOffer $orderOffer */
            foreach ($order->getOrderOffers() as $orderOffer) {
                if ($oo = $orderOffer->getOffer()) {
                    $oo->setNumberOfUsedTimes(max(0, $oo->getNumberOfUsedTimes() - $offerCountArray[$oo->getId()]['count'])); // DECREMENT Offer Usage
                }
                $em->remove($orderOffer);
            }

            /* @var Offer $offer */
            foreach ($offers as $offer) {
                $orderOffer = new OrderOffer();
                $orderOffer->setType($offer->getType());
                $orderOffer->setPrice($offer->getOfferPrice());
                $orderOffer->setCount($offerCountArray[$offer->getId()]['count']);
                $orderOffer->setOffer($offer);
                $orderOffer->setOrder($order);
                $orderOffer->setCashGetAmount($offer->getCashGetAmount());
                $orderOffer->setPercentageGetAmount($offer->getPercentageGetAmount());
                $orderOffer->setDescriptionPublic($offer->getDescriptionPublic());
                $orderOffer->setDescriptionPublicEn($offer->getDescriptionPublicEn());
                $orderOffer->setTitle($offer->getTitle());
                $orderOffer->setTitleEn($offer->getTitleEn());
                $order->addOrderOffer($orderOffer);

                $em->persist($orderOffer);
                $offer->setNumberOfUsedTimes($offer->getNumberOfUsedTimes() + $offerCountArray[$offer->getId()]['count']); //INCREMENT Offer Usage
            }
        }
        $em->flush();
        $responseOrderListData = $this->getUserOrderObject($order);
        $driver = $order->getDriver();
        if ($driver) {
            $title = $translator->trans('Order Edited', array('%orderId%' => $order->getId()), 'notifications', $driver->getLocale());
            $body = $translator->trans('Order Edited', array(
                '%orderId%' => $order->getId(),
            ), 'notifications', $driver->getLocale());

            $this->get('user_device_notification')->sendNotificationToUser($driver, $title, $body, array(
                'id' => $order->getId(),
                'title' => $title,
                'body' => $body,
                'type' => NotificationCenter::USER_ORDER_EDITED
            ));
        }
        return $responseOrderListData;
    }


    /**
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\RequestOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function addAction(RequestAlias $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestData = json_decode($request->getContent());
        $requestOrder = new RequestOrder();
        $user = $this->getUser();
        if (!$user) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("please login"));
        }
        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("you are logged in as driver"));
        }

        $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
            'receipt' => 'AppBundle\APIResponse\Order\Receipt',
            'items' => '\AppBundle\APIResponse\Order\RequestOrderItem',
            'offers' => '\AppBundle\APIResponse\Offer\ResponseOffer',
            'buyItems' => '\AppBundle\APIResponse\Offer\ResponseBuyItem',
            'getItems' => '\AppBundle\APIResponse\Offer\ResponseGetItem',
            'paymentMethod' => '\AppBundle\APIResponse\Order\PaymentMethod',
            'promoCode' => '\AppBundle\APIResponse\PromoCode\PromoCode'
        ));

        if ($requestOrder->paymentMethod->type == 'CREDIT' && $em->getRepository('IbtikarTaniaModelBundle:Order')->findBy(array('user' => $user->getId(), 'status' => Order::$statuses['transaction-pending']))) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("You have orders with pending payment", array(), 'order'));
        }

        if ($requestOrder->receivingDate) {
            $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
        }

        date_default_timezone_set('UTC');
        $receivingdateTime=new \DateTime();
        $receivingdateTime->setTimestamp($requestOrder->receivingDate);
        $receivingdateTime->modify("today");
        if (
           $requestOrder->receivingDate &&
           (int)$this->getDateInTimeStamp($receivingdateTime) < (int)$this->getDateInTimeStamp(new \DateTime("today"))
        ) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Invalid Data"));
        }
        date_default_timezone_set('Asia/Riyadh');
        
        $itemIds = array();

        $offerIds = array();
        $totalItemsCount = 0;

        $totalOffersCount = 0;
        $countArray = array();
        $offerCountArray = array();
        if (isset($requestData->items) && count($requestData->items) > 0) {
            foreach ($requestOrder->items as $requestOrderItem) {
                $errorsObjects = $this->get('validator')->validate($requestOrderItem, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }
                $itemIds[] = $requestOrderItem->id;
                $totalItemsCount += (integer)$requestOrderItem->count;
                $countArray[$requestOrderItem->id] = array('price' => $requestOrderItem->price, 'count' => $requestOrderItem->count);
            }
        } else {
            $requestOrder->items = array();
        }

        if (isset($requestData->offers) && count($requestData->offers) > 0) {
            $i = 0;
            /* @var $requestOrderOffer ResponseOffer */
            foreach ($requestOrder->offers as $requestOrderOffer) {
                if (!isset($requestData->offers[$i]->getItems) || (isset($requestData->offers[$i]->getItems) && count($requestData->offers[$i]->getItems) == 0)) {
                    $requestOrderOffer->getItems = array();
                }

                $requestOrderOffer->startTime = $requestOrderOffer->startTime ? $requestOrderOffer->startTime->getTimestamp() : null;
                $requestOrderOffer->expiryTime = $requestOrderOffer->expiryTime ? $requestOrderOffer->expiryTime->getTimestamp() : null;
                $requestOrderOffer->numberOfUsedTimes = 0;
                $errorsObjects = $this->get('validator')->validate($requestOrderOffer, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }

                $i++;
                $offerIds[] = $requestOrderOffer->id;
                $totalOffersCount += (integer)$requestOrderOffer->count;
                $offerCountArray[$requestOrderOffer->id] = array('price' => $requestOrderOffer->buyItemsCost, 'count' => $requestOrderOffer->count);
            }
        } else {
            $requestOrder->offers = array();
        }

        $minimumNumberOfItemsPerOrder = (integer) $this->get('settings_manager')->get('minimum_number_of_items_per_order');
        if ($minimumNumberOfItemsPerOrder && $minimumNumberOfItemsPerOrder > $totalItemsCount && count($offerIds) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You must order a minimum of %items-count% items in any order', array('%items-count%' => $minimumNumberOfItemsPerOrder), 'api_error_message'));
        }
        $requestOrder->paymentMethod->cardNumber = isset($requestOrder->paymentMethod->cardNumber) ? $requestOrder->paymentMethod->cardNumber : $requestOrder->paymentMethod->type;
        $errorsObjects = $this->get('validator')->validate($requestOrder->paymentMethod, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $creditCard = null;
        if ($requestOrder->paymentMethod->type == 'CREDIT') {
            $creditCard = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->findOneBy(array('id'=> $requestOrder->paymentMethod->id));
            if (!$creditCard) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("credit card was not found"));
            }
        } elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
            if ($user->getBalance() < $requestOrder->totalPrice) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No Enough Balance"));
            }
        }
        $errorsObjects = $this->get('validator')->validate($requestOrder, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        if ($requestOrder->addressId) {
            $address = $em->getRepository('IbtikarTaniaModelBundle:UserAddress')->findOneBy(array('id'=> $requestOrder->addressId));
            if (!$address) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No address was found"));
            } else {
                $cityArea = $this->getCityAreaPerLatLong($address->getLatitude(), $address->getLongitude());
            }
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        } elseif ($requestOrder->lat && $requestOrder->long) {
            $address = null;
            $cityArea = $this->getCityAreaPerLatLong($requestOrder->lat, $requestOrder->long);
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        } else {
            $address = null;
            $cityArea = $this->getCityAreaPerLatLong($this->getUser()->getLatitude(), $this->getUser()->getLongitude());
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        }
        $errorsObjects = $this->get('validator')->validate($requestOrder->receipt);
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $city = $cityArea->getCity();
        $em = $this->getDoctrine()->getManager();
        $items = $em->createQueryBuilder()
            ->select('i')
            ->from('IbtikarTaniaModelBundle:Item', 'i')
            ->where('i.id in (:itemId)')
            ->andWhere('i.shown = :shown')
            ->setParameter('itemId', $itemIds)
            ->setParameter('shown', true)
            ->getQuery()
            ->getResult();

        $offers = $em->createQueryBuilder()
            ->select('o')
            ->from('IbtikarTaniaModelBundle:Offer', 'o')
            ->where('o.id in (:offerIds)')
            ->setParameter('offerIds', $offerIds)
            ->getQuery()
            ->getResult();

        if (count($items)<=0 && count($requestData->offers) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $itemNameGetter = 'getName';
        if ($request->getLocale() == 'en') {
            $itemNameGetter = 'getNameEn';
        }
        /* @var $item \Ibtikar\TaniaModelBundle\Entity\Item */
        foreach ($items as $item) {
            $minimumAmountToOrder = $item->getMinimumAmountToOrder();
            if ($minimumAmountToOrder && $minimumAmountToOrder > $countArray[$item->getId()]['count'] && count($requestData->offers) == 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You can only order a minimum of %item-minimum-count% items of %item-name%', array('%item-minimum-count%' => $minimumAmountToOrder, '%item-name%' => $item->$itemNameGetter()), 'api_error_message'));
            }
        }
        $responseItemList = $this->getItemObjectPerCity($request, $items, $city);
        $totalPrice = 0;
        foreach ($responseItemList->items as $item) {
            $totalPrice += ($item->price * $countArray[$item->id]['count']);
        }
        /* @var $offer \Ibtikar\TaniaModelBundle\Entity\Offer */
        foreach ($offers as $offer) {
            if ($offer->getExpiryTime() && $offer->getExpiryTime()->getTimestamp() <= time()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }

            if (!$offer->getEnabled()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }
            $totalPrice += ($offer->getOfferPrice() * $offerCountArray[$offer->getId()]['count']);
        }
        $promocode = null;
        if ($requestOrder->promoCode && $requestOrder->promoCode->code) {
            $validationResponse = $this->get('order_operations')->getValidationErrorResponseForPromoCodeOrPromoCode($requestOrder->promoCode->code, $this->getUser(), null, $city);
            if ($validationResponse instanceof JsonResponse) {
                return $validationResponse;
            }
            /* @var $promocode PromoCode */
            $promocode = $validationResponse;
        }

        $withoutTaxes = false;
        if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE') {
            $withoutTaxes = true;
        }

      
        $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true);
       
        if ($requestOrder->paymentMethod->type == 'CREDIT') {
            $requestOrder->paymentMethod->type = 'CASH';
        }
        // NEW-ISPL START
        /*if($totalPrice != $receipt->receipt->paidTotalPrice || $receipt->receipt->finalPrice != $requestOrder->receipt->finalPrice) //if cost of any item changed or fees changed
        {
            $receipt = $this->getReceipt($totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Costs Changed', array(), 'validators'), $receipt);
        }*/ // NEW-ISPL

        $shift = null;
        if ($requestOrder->receivingDate) {
            if (!$requestOrder->shiftId) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('No shift was found'));
            }
            $shift = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findOneBy(array('id'=> $requestOrder->shiftId));
            if (!$shift) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
        } else {
            $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findAll();
            if (count($shifts) === 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
            $currentTime = new \DateTime();
            foreach ($shifts as $dayShift) {
                $shiftTimeToday = clone $currentTime;
                $shiftTimeToday->modify($dayShift->getTo()->format('H:i'));
                if ($shiftTimeToday > $currentTime) {
                    $shift = $dayShift;
                    $requestOrder->receivingDate = $currentTime->getTimestamp();
                    break;
                }
            }
            if (!$shift) {
                // No shift available today deliver tomorrow
                $requestOrder->receivingDate = new \DateTime('tomorrow');
                $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
                $shift = $shifts[0];
            }
        }
        $firstTimeToday = new \DateTime('midnight');
        $lastTimeToday = new \DateTime('tomorrow');
        if ($requestOrder->receivingDate >= $firstTimeToday->getTimestamp() && $requestOrder->receivingDate < $lastTimeToday->getTimestamp()) {
            $firstTimeToday->modify($shift->getTo()->format('H:i'));
            if (time() > $firstTimeToday->getTimestamp()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('shif_expired', array(), 'api_error_message'));
            }
        }

        $responseOrder = $this->applySubmitOrder($requestOrder, $items, $city, $cityArea, $shift, $address, $countArray, $creditCard, $promocode, $receipt, $offerCountArray, $offers, $request->headers);

        return $apiOperations->getJsonResponseForObject($responseOrder);
    }

    protected function applySubmitOrder(RequestOrder $requestOrder, $items, $city, $cityArea, Shift $shift, $address, $countArray, PfPaymentMethod $creditCard = null, PromoCode $promoCode = null, $receipt, $offerCountArray = array(), $offers = array(), $header)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\PromoCodeRepository */
        $promoCodeRepo = $em->getRepository('IbtikarTaniaModelBundle:PromoCode');
        /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\OrderRepository */
        $orderRepo = $em->getRepository('IbtikarTaniaModelBundle:Order');
        $totalPrice = 0;
        $order = new Order();
        $order->setPaymentMethod($requestOrder->paymentMethod->type);
        $order->setCreditCard($creditCard);
        $user = $this->getUser();
        if ($promoCode) {
            $usedByUserBefore = ((integer) $orderRepo->countPromoCodeUsedTimesByUser($user->getId(), $promoCode->getId())) > 0 ? true : false;
            if (isset($receipt->discount)) {
                $promoCodeRepo->increasePromoCodeUsage($promoCode->getId(), $receipt->discount, $usedByUserBefore);
            } else {
                $promoCodeRepo->increasePromoCodeUsage($promoCode->getId(), $receipt->receipt->discount, $usedByUserBefore);
            }
            $order->setPromoCodeMethod($promoCode->getType());
            $order->setPromoCodeName($promoCode->getCode());
            $order->setPromoCodeValue($promoCode->getDiscountAmount());
            $order->setPromoCode($promoCode);
        }
        if ($creditCard) {
            $order->setStatus(Order::$statuses['cancelled']);
            $order->setCancelReason('Transaction Failure | Invalid payment info.');
            $order->setCardNumber($creditCard->getCardNumber());
            $order->setExpiryDate($creditCard->getExpiryDate());
            $order->setFortId($creditCard->getFortId());
            $order->setIsDefault($creditCard->getIsDefault());
            $order->setMerchantReference($creditCard->getMerchantReference());
            $order->setPaymentOption($creditCard->getPaymentOption());
            $order->setTokenName($creditCard->getTokenName());
        } elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
            $order->setPaymentValue($requestOrder->paymentMethod->value);
            $order->setCardNumber($requestOrder->paymentMethod->type);
            $user->setBalance(($user->getBalance() - $requestOrder->receipt->finalPrice));
            $user->setUsedBalance($user->getUsedBalance() + $requestOrder->receipt->finalPrice);
            $em->persist($user);
        } else {
            $order->setCardNumber($requestOrder->paymentMethod->type);
        }
        $order->setSource($this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom'));
        $order->setShift($shift);
        $order->setShiftFrom($shift->getFrom());
        $order->setShiftTo($shift->getTo());
        $order->setCity($city);
        $order->setCityArea($cityArea);
        $order->setCityAreaNameEn($cityArea->getNameEn());
        $order->setCityAreaNameAr($cityArea->getNameAr());
        $order->setUser($this->getUser());
        /* @var Order $order */
        if ($address) {
            $order->setTitle($address->getTitle());
            $order->setAddress($address->getAddress());
            $order->setLatitude($address->getLatitude());
            $order->setLongitude($address->getLongitude());
            $order->setUserAddress($address);
            $order->setAddressType($address->getType());
        } else {
            $order->setAddress($this->getUser()->getAddress());
            $order->setLatitude($this->getUser()->getLatitude());
            $order->setLongitude($this->getUser()->getLongitude());
        }
        if ($requestOrder->receivingDate) {
            $order->setReceivingDate($requestOrder->receivingDate);
        } else {
            $order->setReceivingDate($this->getDateInTimeStamp(new \DateTime()));
        }
        $order->setAmountDue($requestOrder->receipt->finalPrice);
        $order->setFirstPaymentPrice($requestOrder->receipt->finalPrice);
        $order->setPrice($requestOrder->receipt->totalPrice);
        $order->setTaxFees($requestOrder->receipt->taxFees);
        if ($requestOrder->receivingDate && $requestOrder->receivingDate != '') {
            $order->setReceivingDate($requestOrder->receivingDate);
        }
        $order->setNote($requestOrder->note);
        $order->setAppVersion($header->get('app-version'));
        $order->setDeviceInformation($header->get('device-information'));

        $em->persist($order);
        foreach ($items as $item) {
            $orderItem = new OrderItem();
            $orderItem->setCity($city);
            $orderItem->setCount($countArray[$item->getId()]['count']);
            $orderItem->setItem($item);
            $orderItem->setPrice($countArray[$item->getId()]['price']);
            $order->addOrderItem($orderItem);
            $orderItem->setOrder($order);
            $em->persist($orderItem);
        }

        /* @var Offer $offer */
        foreach ($offers as $offer) {
            $orderOffer = new OrderOffer();
            $orderOffer->setType($offer->getType());
            $orderOffer->setPrice($offer->getOfferPrice());
            $orderOffer->setCount($offerCountArray[$offer->getId()]['count']);
            $orderOffer->setOffer($offer);
            $orderOffer->setOrder($order);
            $orderOffer->setCashGetAmount($offer->getCashGetAmount());
            $orderOffer->setPercentageGetAmount($offer->getPercentageGetAmount());
            $orderOffer->setDescriptionPublic($offer->getDescriptionPublic());
            $orderOffer->setDescriptionPublicEn($offer->getDescriptionPublicEn());
            $orderOffer->setTitle($offer->getTitle());
            $orderOffer->setTitleEn($offer->getTitleEn());
            $order->addOrderOffer($orderOffer);

            $em->persist($orderOffer);
            $offer->setNumberOfUsedTimes(max(0, $offer->getNumberOfUsedTimes() + $offerCountArray[$offer->getId()]['count'])); // DECREMENT Offer Usage
        }
        $em->flush();
       
        //NEW-ISPL START
        if ($creditCard) {
            $orderOperations = $this->get('order_operations');
            if (isset($requestOrder->paymentMethod->cardSecurityCode)) { // in case of MADA
                $orderOperations->payCard($order);
            } else {
                $orderOperations->payCard($order);
            }
            /*$order = $em->getRepository('IbtikarTaniaModelBundle:Order')->find($order->getId());
            $order->setStatus(Order::$statuses['new']);
            $order->setCancelReason(NULL);
            $em->persist($order);
            $em->flush();*/
        }
        //NEW-ISPL END
        $em->getRepository('IbtikarTaniaModelBundle:User')->increamentNumberOfOrders($user);
        $responseOrderListData = $this->getUserOrderObject($order);
        return $responseOrderListData;
    }

    /**
     * Get Order List
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\RequestOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function addV2Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestData = json_decode($request->getContent());
        $requestOrder = new RequestOrder();
        $requestOrder->orderId = null;
        $user = $this->getUser();
       $user = $em->getRepository("IbtikarTaniaModelBundle:User")->find(1);

        $secondPaymentPrice = null;
        $secondPaymentMethod = null;


        if (!$user) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("please login"));
        }
        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("you are logged in as driver"));
        }

        $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
            'receipt' => 'AppBundle\APIResponse\Order\Receipt',
            'items' => '\AppBundle\APIResponse\Order\RequestOrderItem',
            'offers' => '\AppBundle\APIResponse\Offer\ResponseOffer',
            'buyItems' => '\AppBundle\APIResponse\Offer\ResponseBuyItem',
            'getItems' => '\AppBundle\APIResponse\Offer\ResponseGetItem',
            'paymentMethod' => '\AppBundle\APIResponse\Order\PaymentMethod',
            'promoCode' => '\AppBundle\APIResponse\PromoCode\PromoCode'
        ));

        if (($requestOrder->paymentMethod->type == 'CREDIT')
            && $em->getRepository('IbtikarTaniaModelBundle:Order')->findBy(array('user' => $user->getId(), 'status' => Order::$statuses['transaction-pending']))) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("You have orders with pending payment", array(), 'order'));
        }

        if ($requestOrder->receivingDate) {
            $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
        }

        date_default_timezone_set('UTC');
        $receivingdateTime=new \DateTime();
        $receivingdateTime->setTimestamp($requestOrder->receivingDate);
        $receivingdateTime->modify("today");
        if (
            $requestOrder->receivingDate &&
            (int)$this->getDateInTimeStamp($receivingdateTime) < (int)$this->getDateInTimeStamp(new \DateTime("today"))
        ) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Invalid Data"));
        }
        date_default_timezone_set('Asia/Riyadh');
        
        $itemIds = array();

        $offerIds = array();
        $totalItemsCount = 0;

        $totalOffersCount = 0;
        $countArray = array();
        $offerCountArray = array();
        if (isset($requestData->items) && count($requestData->items) > 0) {
            foreach ($requestOrder->items as $requestOrderItem) {
                $errorsObjects = $this->get('validator')->validate($requestOrderItem, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }
                $itemIds[] = $requestOrderItem->id;
                $totalItemsCount += (integer)$requestOrderItem->count;
                $countArray[$requestOrderItem->id] = array('price' => $requestOrderItem->price, 'count' => $requestOrderItem->count);
            }
        } else {
            $requestOrder->items = array();
        }

        if (isset($requestData->offers) && count($requestData->offers) > 0) {
            $i = 0;
            /* @var $requestOrderOffer ResponseOffer */
            foreach ($requestOrder->offers as $requestOrderOffer) {
                if (!isset($requestData->offers[$i]->getItems) || (isset($requestData->offers[$i]->getItems) && count($requestData->offers[$i]->getItems) == 0)) {
                    $requestOrderOffer->getItems = array();
                }

                $requestOrderOffer->startTime = $requestOrderOffer->startTime ? $requestOrderOffer->startTime->getTimestamp() : null;
                $requestOrderOffer->expiryTime = $requestOrderOffer->expiryTime ? $requestOrderOffer->expiryTime->getTimestamp() : null;
                $requestOrderOffer->numberOfUsedTimes = 0;
                $errorsObjects = $this->get('validator')->validate($requestOrderOffer, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }

                $i++;
                $offerIds[] = $requestOrderOffer->id;
                $totalOffersCount += (integer)$requestOrderOffer->count;
                $offerCountArray[$requestOrderOffer->id] = array('price' => $requestOrderOffer->buyItemsCost, 'count' => $requestOrderOffer->count);
            }
        } else {
            $requestOrder->offers = array();
        }

        $minimumNumberOfItemsPerOrder = (integer) $this->get('settings_manager')->get('minimum_number_of_items_per_order');
        if ($minimumNumberOfItemsPerOrder && $minimumNumberOfItemsPerOrder > $totalItemsCount && count($offerIds) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You must order a minimum of %items-count% items in any order', array('%items-count%' => $minimumNumberOfItemsPerOrder), 'api_error_message'));
        }
        $requestOrder->paymentMethod->cardNumber = isset($requestOrder->paymentMethod->cardNumber) ? $requestOrder->paymentMethod->cardNumber : $requestOrder->paymentMethod->type;
        $errorsObjects = $this->get('validator')->validate($requestOrder->paymentMethod, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $creditCard = null;
        if ($requestOrder->paymentMethod->type == 'CREDIT') {
            $creditCard = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->findOneBy(array('id'=> $requestOrder->paymentMethod->id));
            if (!$creditCard) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("credit card was not found"));
            }
        }
        // NEW-ISPL START on 30-12-2018
        elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
            if ($user->getBalance()==0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No Enough Balance"));
            }
        }
        // NEW-ISPL END on 30-12-2018

        $errorsObjects = $this->get('validator')->validate($requestOrder, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        if ($requestOrder->addressId) {
            $address = $em->getRepository('IbtikarTaniaModelBundle:UserAddress')->findOneBy(array('id'=> $requestOrder->addressId));
            if (!$address) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No address was found"));
            } else {
                $cityArea = $this->getCityAreaPerLatLong($address->getLatitude(), $address->getLongitude());
            }
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        } elseif ($requestOrder->lat && $requestOrder->long) {
            $address = null;
            $cityArea = $this->getCityAreaPerLatLong($requestOrder->lat, $requestOrder->long);
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        } else {
            $address = null;
            $cityArea = $this->getCityAreaPerLatLong($this->getUser()->getLatitude(), $this->getUser()->getLongitude());
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        }
        $errorsObjects = $this->get('validator')->validate($requestOrder->receipt);
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $city = $cityArea->getCity();
        $em = $this->getDoctrine()->getManager();
        $items = $em->createQueryBuilder()
            ->select('i')
            ->from('IbtikarTaniaModelBundle:Item', 'i')
            ->where('i.id in (:itemId)')
            ->andWhere('i.shown = :shown')
            ->setParameter('itemId', $itemIds)
            ->setParameter('shown', true)
            ->getQuery()
            ->getResult();

        $offers = $em->createQueryBuilder()
            ->select('o')
            ->from('IbtikarTaniaModelBundle:Offer', 'o')
            ->where('o.id in (:offerIds)')
            ->setParameter('offerIds', $offerIds)
            ->getQuery()
            ->getResult();

        if (count($items)<=0 && count($requestData->offers) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $itemNameGetter = 'getName';
        if ($request->getLocale() == 'en') {
            $itemNameGetter = 'getNameEn';
        }
        /* @var $item \Ibtikar\TaniaModelBundle\Entity\Item */
        foreach ($items as $item) {
            $minimumAmountToOrder = $item->getMinimumAmountToOrder();
            if ($minimumAmountToOrder && $minimumAmountToOrder > $countArray[$item->getId()]['count'] && count($requestData->offers) == 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You can only order a minimum of %item-minimum-count% items of %item-name%', array('%item-minimum-count%' => $minimumAmountToOrder, '%item-name%' => $item->$itemNameGetter()), 'api_error_message'));
            }
            $itemPrice = $item->getDefaultPrice();
            if ($itemPrice != $countArray[$item->getId()]['price']) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Price of %item-name% has been changed to %item-price%. Please refill cart and try again ', array('%item-name%' => $item->$itemNameGetter(),'%item-price%' => $itemPrice), 'api_error_message'));
            }
        }
        $responseItemList = $this->getItemObjectPerCity($request, $items, $city);
        $totalPrice = 0;
        foreach ($responseItemList->items as $item) {
            $totalPrice += ($item->price * $countArray[$item->id]['count']);
        }
        /* @var $offer \Ibtikar\TaniaModelBundle\Entity\Offer */
        foreach ($offers as $offer) {
            if ($offer->getExpiryTime() && $offer->getExpiryTime()->getTimestamp() <= time()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }

            if (!$offer->getEnabled()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }
            $totalPrice += ($offer->getOfferPrice() * $offerCountArray[$offer->getId()]['count']);

            $offerPrice = $offer->getOfferPrice();
            if ($offerPrice != $offerCountArray[$offer->getId()]['price']) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Price of %item-name% has been changed to %item-price%. Please refill cart and try again ', array('%item-name%' => $offer->getTitle(),'%item-price%' => $offerPrice), 'api_error_message'));
            }
        }
        $promocode = null;
        if ($requestOrder->promoCode && $requestOrder->promoCode->code) {
            $validationResponse = $this->get('order_operations')->getValidationErrorResponseForPromoCodeOrPromoCode($requestOrder->promoCode->code, $this->getUser(), null, $city);
            if ($validationResponse instanceof JsonResponse) {
                return $validationResponse;
            }
            /* @var $promocode PromoCode */
            $promocode = $validationResponse;
        }

        $withoutTaxes = false;
        if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $user->getBalance() >= $requestOrder->receipt->finalPrice) {
            $withoutTaxes = true;
        }
        /*   $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, TRUE);
         */

        // NEW-ISPL START on 31/12/2018
        if ($requestOrder->paymentMethod->type == 'CASH') {
            $firstPaymentPrice = $requestOrder->receipt->finalPrice;
            $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true);
        } elseif ($requestOrder->paymentMethod->type == 'CREDIT') {
            $firstPaymentPrice = $requestOrder->receipt->finalPrice;
            //$requestOrder->paymentMethod->type = 'CASH';
            $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true);
        } else {
            if ($user->getBalance() < $requestOrder->receipt->finalPrice) {
                $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];

                $requestOrder->receipt->taxFees = ((($requestOrder->totalPrice - $user->getBalance()) * $fees->getTaxPercent()) / 100);
                $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);

                $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));

                $secondPaymentMethod = 'CASH';
                $firstPaymentPrice = $user->getBalance();
                $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $user->getBalance());
                $receipt = $this->getReceipt(
                    $requestOrder->receipt->totalPrice,
                    $withoutTaxes,
                    $promocode,
                    $offers,
                    $offerCountArray,
                    true,
                    ($requestOrder->totalPrice-$firstPaymentPrice)
                );

                $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];
                $requestOrder->receipt->taxFees = ($fees->getTaxPercent() * ($requestOrder->receipt->totalPrice-$firstPaymentPrice) / 100);
                $requestOrder->receipt->finalPrice =
                    ($requestOrder->receipt->totalPrice - $receipt->receipt->offersDiscount) + ($requestOrder->receipt->taxFees) - $requestOrder->receipt->discount;
                $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $firstPaymentPrice);
            } else {
                $requestOrder->receipt->taxFees = 0;
                $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);
                $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));
                $firstPaymentPrice = $requestOrder->receipt->finalPrice;
                $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true, ($requestOrder->receipt->totalPrice-$firstPaymentPrice));
            }
        }
        // NEW-ISPL END on 31/12/2018

        // NEW-ISPL START
        /*if($totalPrice != $receipt->receipt->paidTotalPrice || $receipt->receipt->finalPrice != $requestOrder->receipt->finalPrice) //if cost of any item changed or fees changed
        {
            $receipt = $this->getReceipt($totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Costs Changed', array(), 'validators'), $receipt);
        }*/ // NEW-ISPL END

        $shift = null;
        if ($requestOrder->receivingDate) {
            if (!$requestOrder->shiftId) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('No shift was found'));
            }
            $shift = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findOneBy(array('id'=> $requestOrder->shiftId));
            if (!$shift) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
        } else {
            $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findAll();
            if (count($shifts) === 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
            $currentTime = new \DateTime();
            foreach ($shifts as $dayShift) {
                $shiftTimeToday = clone $currentTime;
                $shiftTimeToday->modify($dayShift->getTo()->format('H:i'));
                if ($shiftTimeToday > $currentTime) {
                    $shift = $dayShift;
                    $requestOrder->receivingDate = $currentTime->getTimestamp();
                    break;
                }
            }
            if (!$shift) {
                // No shift available today deliver tomorrow
                $requestOrder->receivingDate = new \DateTime('tomorrow');
                $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
                $shift = $shifts[0];
            }
        }
        $firstTimeToday = new \DateTime('midnight');
        $lastTimeToday = new \DateTime('tomorrow');
        if ($requestOrder->receivingDate >= $firstTimeToday->getTimestamp() && $requestOrder->receivingDate < $lastTimeToday->getTimestamp()) {
            $firstTimeToday->modify($shift->getTo()->format('H:i'));
            if (time() > $firstTimeToday->getTimestamp()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('shif_expired', array(), 'api_error_message'));
            }
        }

        $responseOrder = $this->applySubmitOrderV2($requestOrder, $items, $city, $cityArea, $shift, $address, $countArray, $creditCard, $promocode, $receipt, $offerCountArray, $offers, $firstPaymentPrice, $secondPaymentPrice, $secondPaymentMethod, $request);
        return $apiOperations->getJsonResponseForObject($responseOrder);
    }

    /**
     * Add Order after payment
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\RequestOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function addV3Action(RequestAlias $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestData = json_decode($request->getContent());
        $requestOrder = new RequestOrder();
        $requestOrder->orderId = null;
        $user = $this->getUser();


        if (!$user) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("please login"));
        }
        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("you are logged in as driver"));
        }

        $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
            'receipt' => 'AppBundle\APIResponse\Order\Receipt',
            'items' => '\AppBundle\APIResponse\Order\RequestOrderItem',
            'offers' => '\AppBundle\APIResponse\Offer\ResponseOffer',
            'buyItems' => '\AppBundle\APIResponse\Offer\ResponseBuyItem',
            'getItems' => '\AppBundle\APIResponse\Offer\ResponseGetItem',
            'paymentMethod' => '\AppBundle\APIResponse\Order\PaymentMethod',
            'promoCode' => '\AppBundle\APIResponse\PromoCode\PromoCode'
        ));

        if (($requestOrder->paymentMethod->type == 'CREDIT')
            && $em->getRepository('IbtikarTaniaModelBundle:Order')->findBy(array('user' => $user->getId(), 'status' => Order::$statuses['transaction-pending']))) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("You have orders with pending payment", array(), 'order'));
        }

        if ($requestOrder->receivingDate) {
            $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
        }

        date_default_timezone_set('UTC');
        $receivingdateTime=new \DateTime();
        $receivingdateTime->setTimestamp($requestOrder->receivingDate);
        $receivingdateTime->modify("today");
        if (
            $requestOrder->receivingDate &&
            (int)$this->getDateInTimeStamp($receivingdateTime) < (int)$this->getDateInTimeStamp(new \DateTime("today"))
        ) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Invalid Data"));
        }
        date_default_timezone_set('Asia/Riyadh');

        $itemIds = array();

        $offerIds = array();
        $totalItemsCount = 0;

        $totalOffersCount = 0;
        $countArray = array();
        $offerCountArray = array();
        if (isset($requestData->items) && count($requestData->items) > 0) {
            foreach ($requestOrder->items as $requestOrderItem) {
                $errorsObjects = $this->get('validator')->validate($requestOrderItem, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }
                $itemIds[] = $requestOrderItem->id;
                $totalItemsCount += (integer)$requestOrderItem->count;
                $countArray[$requestOrderItem->id] = array('price' => $requestOrderItem->price, 'count' => $requestOrderItem->count);
            }
        } else {
            $requestOrder->items = array();
        }

        if (isset($requestData->offers) && count($requestData->offers) > 0) {
            $i = 0;
            /* @var $requestOrderOffer ResponseOffer */
            foreach ($requestOrder->offers as $requestOrderOffer) {
                if (!isset($requestData->offers[$i]->getItems) || (isset($requestData->offers[$i]->getItems) && count($requestData->offers[$i]->getItems) == 0)) {
                    $requestOrderOffer->getItems = array();
                }

                $requestOrderOffer->startTime = $requestOrderOffer->startTime ? $requestOrderOffer->startTime->getTimestamp() : null;
                $requestOrderOffer->expiryTime = $requestOrderOffer->expiryTime ? $requestOrderOffer->expiryTime->getTimestamp() : null;
                $requestOrderOffer->numberOfUsedTimes = 0;
                $errorsObjects = $this->get('validator')->validate($requestOrderOffer, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }

                $i++;
                $offerIds[] = $requestOrderOffer->id;
                $totalOffersCount += (integer)$requestOrderOffer->count;
                $offerCountArray[$requestOrderOffer->id] = array('price' => $requestOrderOffer->buyItemsCost, 'count' => $requestOrderOffer->count);
            }
        } else {
            $requestOrder->offers = array();
        }

        $minimumNumberOfItemsPerOrder = (integer) $this->get('settings_manager')->get('minimum_number_of_items_per_order');
        if ($minimumNumberOfItemsPerOrder && $minimumNumberOfItemsPerOrder > $totalItemsCount && count($offerIds) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You must order a minimum of %items-count% items in any order', array('%items-count%' => $minimumNumberOfItemsPerOrder), 'api_error_message'));
        }
        $requestOrder->paymentMethod->cardNumber = isset($requestOrder->paymentMethod->cardNumber) ? $requestOrder->paymentMethod->cardNumber : $requestOrder->paymentMethod->type;
        $errorsObjects = $this->get('validator')->validate($requestOrder->paymentMethod, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $creditCard = null;
        if ($requestOrder->paymentMethod->type == 'CREDIT') {
            $creditCard = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->findOneBy(array('id'=> $requestOrder->paymentMethod->id));
            if (!$creditCard) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("credit card was not found"));
            }
        }
        // NEW-ISPL START on 30-12-2018
        elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
            if ($user->getBalance()==0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No Enough Balance"));
            }
        }
        // NEW-ISPL END on 30-12-2018

        $errorsObjects = $this->get('validator')->validate($requestOrder, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        if ($requestOrder->addressId) {
            $address = $em->getRepository('IbtikarTaniaModelBundle:UserAddress')->findOneBy(array('id'=> $requestOrder->addressId));
            if (!$address) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No address was found"));
            } else {
                $cityArea = $this->getCityAreaPerLatLong($address->getLatitude(), $address->getLongitude());
            }
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        } elseif ($requestOrder->lat && $requestOrder->long) {
            $address = null;
            $cityArea = $this->getCityAreaPerLatLong($requestOrder->lat, $requestOrder->long);
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        } else {
            $address = null;
            $cityArea = $this->getCityAreaPerLatLong($this->getUser()->getLatitude(), $this->getUser()->getLongitude());
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        }
        $errorsObjects = $this->get('validator')->validate($requestOrder->receipt);
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $city = $cityArea->getCity();
        $em = $this->getDoctrine()->getManager();
        $items = $em->createQueryBuilder()
            ->select('i')
            ->from('IbtikarTaniaModelBundle:Item', 'i')
            ->where('i.id in (:itemId)')
            ->andWhere('i.shown = :shown')
            ->setParameter('itemId', $itemIds)
            ->setParameter('shown', true)
            ->getQuery()
            ->getResult();

        $offers = $em->createQueryBuilder()
            ->select('o')
            ->from('IbtikarTaniaModelBundle:Offer', 'o')
            ->where('o.id in (:offerIds)')
            ->setParameter('offerIds', $offerIds)
            ->getQuery()
            ->getResult();

        if (count($items)<=0 && count($requestData->offers) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $itemNameGetter = 'getName';
        if ($request->getLocale() == 'en') {
            $itemNameGetter = 'getNameEn';
        }
        /* @var $item \Ibtikar\TaniaModelBundle\Entity\Item */
        foreach ($items as $item) {
            $minimumAmountToOrder = $item->getMinimumAmountToOrder();
            if ($minimumAmountToOrder && $minimumAmountToOrder > $countArray[$item->getId()]['count'] && count($requestData->offers) == 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You can only order a minimum of %item-minimum-count% items of %item-name%', array('%item-minimum-count%' => $minimumAmountToOrder, '%item-name%' => $item->$itemNameGetter()), 'api_error_message'));
            }
            $itemPrice = $item->getDefaultPrice();
            if ($itemPrice != $countArray[$item->getId()]['price']) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Price of %item-name% has been changed to %item-price%. Please refill cart and try again ', array('%item-name%' => $item->$itemNameGetter(),'%item-price%' => $itemPrice), 'api_error_message'));
            }
        }
        $responseItemList = $this->getItemObjectPerCity($request, $items, $city);
        $totalPrice = 0;
        foreach ($responseItemList->items as $item) {
            $totalPrice += ($item->price * $countArray[$item->id]['count']);
        }
        /* @var $offer \Ibtikar\TaniaModelBundle\Entity\Offer */
        foreach ($offers as $offer) {
            if ($offer->getExpiryTime() && $offer->getExpiryTime()->getTimestamp() <= time()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }

            if (!$offer->getEnabled()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }
            $totalPrice += ($offer->getOfferPrice() * $offerCountArray[$offer->getId()]['count']);

            $offerPrice = $offer->getOfferPrice();
            if ($offerPrice != $offerCountArray[$offer->getId()]['price']) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Price of %item-name% has been changed to %item-price%. Please refill cart and try again ', array('%item-name%' => $offer->$getTitle(),'%item-price%' => $offerPrice), 'api_error_message'));
            }
        }
        $promocode = null;
        if ($requestOrder->promoCode && $requestOrder->promoCode->code) {
            $validationResponse = $this->get('order_operations')->getValidationErrorResponseForPromoCodeOrPromoCode($requestOrder->promoCode->code, $this->getUser(), null, $city);
            if ($validationResponse instanceof JsonResponse) {
                return $validationResponse;
            }
            /* @var $promocode PromoCode */
            $promocode = $validationResponse;
        }

        $withoutTaxes = false;
        if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $user->getBalance() >= $requestOrder->receipt->finalPrice) {
            $withoutTaxes = true;
        }
        /*   $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, TRUE);
         */

        // NEW-ISPL START on 31/12/2018
        if ($requestOrder->paymentMethod->type == 'CASH') {
            $firstPaymentPrice = $requestOrder->receipt->finalPrice;
            $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true);
        } elseif ($requestOrder->paymentMethod->type == 'CREDIT') {
            $firstPaymentPrice = $requestOrder->receipt->finalPrice;
            //$requestOrder->paymentMethod->type = 'CASH';
            $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true);
        } else {
            if ($user->getBalance() < $requestOrder->receipt->finalPrice) {
                $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];

                $requestOrder->receipt->taxFees = ((($requestOrder->totalPrice - $user->getBalance()) * $fees->getTaxPercent()) / 100);
                $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);

                $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));

                $secondPaymentMethod = 'CASH';
                $firstPaymentPrice = $user->getBalance();
                $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $user->getBalance());
                $receipt = $this->getReceipt(
                    $requestOrder->receipt->totalPrice,
                    $withoutTaxes,
                    $promocode,
                    $offers,
                    $offerCountArray,
                    true,
                    ($requestOrder->totalPrice-$firstPaymentPrice)
                );

                $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];
                $requestOrder->receipt->taxFees = ($fees->getTaxPercent() * ($requestOrder->receipt->totalPrice-$firstPaymentPrice) / 100);
                $requestOrder->receipt->finalPrice =
                    ($requestOrder->receipt->totalPrice - $receipt->receipt->offersDiscount) + ($requestOrder->receipt->taxFees) - $requestOrder->receipt->discount;
                $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $firstPaymentPrice);
            } else {
                $requestOrder->receipt->taxFees = 0;
                $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);
                $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));
                $firstPaymentPrice = $requestOrder->receipt->finalPrice;
                $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true, ($requestOrder->receipt->totalPrice-$firstPaymentPrice));
            }
        }
        // NEW-ISPL END on 31/12/2018

        // NEW-ISPL START
        /*if($totalPrice != $receipt->receipt->paidTotalPrice || $receipt->receipt->finalPrice != $requestOrder->receipt->finalPrice) //if cost of any item changed or fees changed
        {
            $receipt = $this->getReceipt($totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Costs Changed', array(), 'validators'), $receipt);
        }*/ // NEW-ISPL END

        $shift = null;
        if ($requestOrder->receivingDate) {
            if (!$requestOrder->shiftId) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('No shift was found'));
            }
            $shift = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findOneBy(array('id'=> $requestOrder->shiftId));
            if (!$shift) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
        } else {
            $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findAll();
            if (count($shifts) === 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
            $currentTime = new \DateTime();
            foreach ($shifts as $dayShift) {
                $shiftTimeToday = clone $currentTime;
                $shiftTimeToday->modify($dayShift->getTo()->format('H:i'));
                if ($shiftTimeToday > $currentTime) {
                    $shift = $dayShift;
                    $requestOrder->receivingDate = $currentTime->getTimestamp();
                    break;
                }
            }
            if (!$shift) {
                // No shift available today deliver tomorrow
                $requestOrder->receivingDate = new \DateTime('tomorrow');
                $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
                $shift = $shifts[0];
            }
        }
        $firstTimeToday = new \DateTime('midnight');
        $lastTimeToday = new \DateTime('tomorrow');
        if ($requestOrder->receivingDate >= $firstTimeToday->getTimestamp() && $requestOrder->receivingDate < $lastTimeToday->getTimestamp()) {
            $firstTimeToday->modify($shift->getTo()->format('H:i'));
            if (time() > $firstTimeToday->getTimestamp()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('shif_expired', array(), 'api_error_message'));
            }
        }

        $responseOrder = $this->applySubmitOrderV2($requestOrder, $items, $city, $cityArea, $shift, $address, $countArray, $creditCard, $promocode, $receipt, $offerCountArray, $offers, $firstPaymentPrice, $secondPaymentPrice, $secondPaymentMethod, $request);


        if (isset($requestData->transactiondetails)) {
            $cardData = $requestData->transactiondetails;
            $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->find($responseOrder->id);
            $this->addPfPaymentMethod($user, $cardData, $order);
        }

        return $apiOperations->getJsonResponseForObject($responseOrder);
    }

    /**
     * For save Mada payment details
     *
     * @param User $user
     * @param $cardData
     * @param Order $order
     * @return PfPaymentMethod
     */
    private function addPfPaymentMethod(User $user, $cardData, Order $order)
    {
        $em = $this->getDoctrine()->getManager();
        $pfPaymentMethod = new PfPaymentMethod();
        $pfPaymentMethod->setCardNumber($cardData->card_number);
        $pfPaymentMethod->setExpiryDate($cardData->expiry_date);
        $pfPaymentMethod->setFortId($cardData->fort_id);
        $pfPaymentMethod->setHolder($user);
        $pfPaymentMethod->setPaymentOption($cardData->payment_option);
        $pfPaymentMethod->setMerchantReference($cardData->merchant_reference);
        $pfPaymentMethod->setTokenName($cardData->token_name);
        $pfPaymentMethod->setMerchantReference($cardData->merchant_reference);
        $em->persist($pfPaymentMethod);

        $pfTransaction = new PfTransaction();
        $pfTransaction->setPaymentMethod($pfPaymentMethod);
        $pfTransaction->setInvoice($order);
        $pfTransaction->setFortId($cardData->fort_id);
        $pfTransaction->setCurrency($cardData->currency);
        $pfTransaction->setAmount($cardData->amount);
        $pfTransaction->setMerchantReference($cardData->merchant_reference);
        $pfTransaction->setAuthorizationCode($cardData->authorization_code);
        $pfTransaction->setCurrentStatus(3);
        $em->persist($pfTransaction);

        $PfTransactionStatus = new PfTransactionStatus();
        $PfTransactionStatus->setResponse($cardData->response);
        $PfTransactionStatus->setResponseCode($cardData->response_code);
        $PfTransactionStatus->setResponseMessage($cardData->response_message);
        $PfTransactionStatus->setTransaction($pfTransaction);
        $PfTransactionStatus->setStatus($cardData->status);
        $em->persist($PfTransactionStatus);
        $em->flush();

        return $pfPaymentMethod;
    }


    /**
     * @author Moemen Hussein <moemen.hussein@ibtikar.net.sa>
     * @param RequestOrder $requestOrder
     * @param type $items
     * @param type $city
     * @param $cityArea
     * @param Shift $shift
     * @param $address
     * @param $countArray
     * @param PfPaymentMethod|null $creditCard
     * @param PromoCode|null $promoCode
     * @param $receipt
     * @param array $offerCountArray
     * @param array $offers
     * @param null $firstPaymentPrice
     * @param null $secondPaymentPrice
     * @param null $secondPaymentMethod
     * @param Request $request
     * @return ResponseItemList
     * @throws \Exception
     */
    protected function applySubmitOrderV2(RequestOrder $requestOrder, $items, $city, $cityArea, Shift $shift, $address, $countArray, PfPaymentMethod $creditCard = null, PromoCode $promoCode = null, $receipt, $offerCountArray = array(), $offers = array(), $firstPaymentPrice = null, $secondPaymentPrice = null, $secondPaymentMethod = null, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $order = new Order();
        /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\PromoCodeRepository */
        $promoCodeRepo = $em->getRepository('IbtikarTaniaModelBundle:PromoCode');
        /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\OrderRepository */
        $orderRepo = $em->getRepository('IbtikarTaniaModelBundle:Order');
        $totalPrice = 0;

        if ($requestOrder->paymentMethod->type!='' && $requestOrder->paymentMethod->type!=null) {
            $order->setPaymentMethod($requestOrder->paymentMethod->type);
        }
        //else
        // $order->setPaymentMethod($requestOrder->paymentMethod->type2);
            
        $order->setCreditCard($creditCard);
        $user = $this->getUser();
        $user = $em->getRepository("IbtikarTaniaModelBundle:User")->find(1);
        if ($promoCode) {
            $usedByUserBefore = ((integer) $orderRepo->countPromoCodeUsedTimesByUser($user->getId(), $promoCode->getId())) > 0 ? true : false;
            if (isset($receipt->discount)) {
                $promoCodeRepo->increasePromoCodeUsage($promoCode->getId(), $receipt->discount, $usedByUserBefore);
            } else {
                $promoCodeRepo->increasePromoCodeUsage($promoCode->getId(), $receipt->receipt->discount, $usedByUserBefore);
            }
            $order->setPromoCodeMethod($promoCode->getType());
            $order->setPromoCodeName($promoCode->getCode());
            $order->setPromoCodeValue($promoCode->getDiscountAmount());
            $order->setPromoCode($promoCode);
        }

        if ($creditCard) {
            $order->setStatus(Order::$statuses['cancelled']);
            $order->setCancelReason('Transaction Failure | Invalid payment info.');
            $order->setCardNumber($creditCard->getCardNumber());
            $order->setExpiryDate($creditCard->getExpiryDate());
            $order->setFortId($creditCard->getFortId());
            $order->setIsDefault($creditCard->getIsDefault());
            $order->setMerchantReference($creditCard->getMerchantReference());
            $order->setPaymentOption($creditCard->getPaymentOption());
            $order->setTokenName($creditCard->getTokenName());
        } elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
            $cartPrice = $requestOrder->receipt->totalPrice - ($requestOrder->receipt->offersDiscount + $requestOrder->receipt->taxFees);
            //$order->setPaymentValue($requestOrder->paymentMethod->value);
            $order->setPaymentValue($cartPrice);
            $order->setCardNumber($requestOrder->paymentMethod->type);
            if ($user->getBalance() > $requestOrder->receipt->finalPrice) {
                $user->setBalance(($user->getBalance() - $requestOrder->receipt->finalPrice));
                $user->setUsedBalance($user->getUsedBalance() + $requestOrder->receipt->finalPrice);
            }
            // NEW-ISPL START on 30/12/2018
            else {
                $user->setUsedBalance($user->getUsedBalance() + $user->getBalance());
                $user->setBalance(0);
            }
            // NEW-ISPL START on 30/12/2018
            $em->persist($user);
        } else {
            $order->setCardNumber($requestOrder->paymentMethod->type);
        }

        if (isset($requestOrder->source) && $requestOrder->source!='') {
            $order->setSource($requestOrder->source);
        } else {
            $order->setSource($this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom'));
        }

        if ($requestOrder->lat && $requestOrder->long
            && ($requestOrder->source=='Landing-Page'
                || $requestOrder->source=='cc-agent')) {
            $order->setAddressVerified('0');
        } else {
            $order->setAddressVerified('1');
        }

        $order->setShift($shift);
        $order->setShiftFrom($shift->getFrom());
        $order->setShiftTo($shift->getTo());
        $order->setCity($city);
        $order->setCityArea($cityArea);
        $order->setCityAreaNameEn($cityArea->getNameEn());
        $order->setCityAreaNameAr($cityArea->getNameAr());
        $order->setUser($this->getUser());
        if ($requestOrder->createdBy > 0) {
            $order->setCreatedBy($requestOrder->createdBy);
            $order->setCreatedByFullName($requestOrder->created_by_fullName);
        } else {
            $order->setCreatedBy($user->getId());
            $order->setCreatedByFullName($user->getFullName());
        }
        /* @var Order $order */
        if ($address) {
            $order->setTitle($address->getTitle());
            $order->setAddress($address->getAddress());
            $order->setLatitude($address->getLatitude());
            $order->setLongitude($address->getLongitude());
            $order->setUserAddress($address);
            $order->setAddressType($address->getType());
        } else {
            $order->setAddress($this->getUser()->getAddress());
            $order->setLatitude($this->getUser()->getLatitude());
            $order->setLongitude($this->getUser()->getLongitude());
        }
        if ($requestOrder->receivingDate) {
            $order->setReceivingDate($requestOrder->receivingDate);
        } else {
            $order->setReceivingDate($this->getDateInTimeStamp(new \DateTime()));
        }
        //NEW-ISPL START on 30/12/2018
        $order->setSecondPaymentMethod($secondPaymentMethod);
        $order->setFirstPaymentPrice($firstPaymentPrice);
        $order->setSecondPaymentPrice($secondPaymentPrice);
        
        $order->setAmountDue($requestOrder->receipt->finalPrice);
        $order->setPrice($requestOrder->receipt->totalPrice);
        $order->setTaxFees($requestOrder->receipt->taxFees);
        //NEW-ISPL END on 30/12/2018
        
        if ($requestOrder->receivingDate && $requestOrder->receivingDate != '') {
            $order->setReceivingDate($requestOrder->receivingDate);
        }
        $order->setNote($requestOrder->note);
        $order->setAppVersion($request->headers->get('app-version'));
        $order->setDeviceInformation($request->headers->get('device-information'));

        $em->persist($order);
        foreach ($items as $item) {
            $orderItem = new OrderItem();
            $orderItem->setCity($city);
            $orderItem->setCount($countArray[$item->getId()]['count']);
            $orderItem->setItem($item);
            $orderItem->setPrice($countArray[$item->getId()]['price']);
            $order->addOrderItem($orderItem);
            $orderItem->setOrder($order);
            $em->persist($orderItem);
        }

        /* @var Offer $offer */
        foreach ($offers as $offer) {
            $orderOffer = new OrderOffer();
            $orderOffer->setType($offer->getType());
            $orderOffer->setPrice($offer->getOfferPrice());
            $orderOffer->setCount($offerCountArray[$offer->getId()]['count']);
            $orderOffer->setOffer($offer);
            $orderOffer->setOrder($order);
            $orderOffer->setCashGetAmount($offer->getCashGetAmount());
            $orderOffer->setPercentageGetAmount($offer->getPercentageGetAmount());
            $orderOffer->setDescriptionPublic($offer->getDescriptionPublic());
            $orderOffer->setDescriptionPublicEn($offer->getDescriptionPublicEn());
            $orderOffer->setTitle($offer->getTitle());
            $orderOffer->setTitleEn($offer->getTitleEn());
            $order->addOrderOffer($orderOffer);

            $em->persist($orderOffer);
            $offer->setNumberOfUsedTimes(max(0, $offer->getNumberOfUsedTimes() + $offerCountArray[$offer->getId()]['count'])); // DECREMENT Offer Usage
        }
        $em->flush();

        //NEW-ISPL START
        if ($creditCard) {
            $orderOperations = $this->get('order_operations');
            $orderOperations->payCard($order);
            /*$order = $em->getRepository('IbtikarTaniaModelBundle:Order')->find($order->getId());
            $order->setStatus(Order::$statuses['new']);
            $order->setCancelReason(NULL);
            $em->persist($order);
            $em->flush();*/
        }
        //NEW-ISPL END
        $em->getRepository('IbtikarTaniaModelBundle:User')->increamentNumberOfOrders($user);
        $responseOrderListData = $this->getUserOrderObject($order);
        return $responseOrderListData;
    }


    /**
    * @ApiDoc(
    *  resource=true,
    *  input="AppBundle\APIResponse\Order\RequestOrder",
    *  authentication=true,
    *  tags={
    *     "user application"="DarkCyan",
    *     "stable"="green"
    *  },
    *  section="Order",
    *  statusCodes={
    *      200="Returned on success",
    *      401="Returned if the authorization header is missing or expired",
    *      403="Returned if the api key is not valid",
    *      404="Returned if the user was not found",
    *      500="Returned if there is an internal server error"
    *  }
    * )
    *
    *
    * @param RequestAlias $request
    *
    * @return JsonResponse
    */
    public function addByDriverV1Action(RequestAlias $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestData = json_decode($request->getContent());
        $requestOrder = new RequestOrder();
        if(isset($requestData->driverId) && !empty($requestData->driverId)){
            $requestOrder->driverId = $requestData->driverId;
        }else{
            $requestOrder->driverId = $requestOrder->driverId;
        }

        $user = $this->getUser();

        
        if (!$user) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("please login"));
        }
        /*if($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver){
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("you are logged in as driver"));
        }*/

        $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
            'receipt' => 'AppBundle\APIResponse\Order\Receipt',
            'items' => '\AppBundle\APIResponse\Order\RequestOrderItem',
            'offers' => '\AppBundle\APIResponse\Offer\ResponseOffer',
            'buyItems' => '\AppBundle\APIResponse\Offer\ResponseBuyItem',
            'getItems' => '\AppBundle\APIResponse\Offer\ResponseGetItem',
            'paymentMethod' => '\AppBundle\APIResponse\Order\PaymentMethod',
            'promoCode' => '\AppBundle\APIResponse\PromoCode\PromoCode'
        ));

        if (($requestOrder->paymentMethod->type == 'CREDIT')
            && $em->getRepository('IbtikarTaniaModelBundle:Order')->findBy(array('user' => $user->getId(), 'status' => Order::$statuses['transaction-pending']))) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("You have orders with pending payment", array(), 'order'));
        }

        if ($requestOrder->receivingDate) {
            $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
        }

        date_default_timezone_set('UTC');
        $receivingdateTime=new \DateTime();
        $receivingdateTime->setTimestamp($requestOrder->receivingDate);
        $receivingdateTime->modify("today");
        if (
           $requestOrder->receivingDate &&
           (int)$this->getDateInTimeStamp($receivingdateTime) < (int)$this->getDateInTimeStamp(new \DateTime("today"))
        ) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Invalid Data"));
        }
        date_default_timezone_set('Asia/Riyadh');
        
        $itemIds = array();

        $offerIds = array();
        $totalItemsCount = 0;

        $totalOffersCount = 0;
        $countArray = array();
        $offerCountArray = array();
        if (isset($requestData->items) && count($requestData->items) > 0) {
            foreach ($requestOrder->items as $requestOrderItem) {
                $errorsObjects = $this->get('validator')->validate($requestOrderItem, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }
                $itemIds[] = $requestOrderItem->id;
                $totalItemsCount += (integer)$requestOrderItem->count;
                $countArray[$requestOrderItem->id] = array('price' => $requestOrderItem->price, 'count' => $requestOrderItem->count);
            }
        } else {
            $requestOrder->items = array();
        }

        if (isset($requestData->offers) && count($requestData->offers) > 0) {
            $i = 0;
            /* @var $requestOrderOffer ResponseOffer */
            foreach ($requestOrder->offers as $requestOrderOffer) {
                if (!isset($requestData->offers[$i]->getItems) || (isset($requestData->offers[$i]->getItems) && count($requestData->offers[$i]->getItems) == 0)) {
                    $requestOrderOffer->getItems = array();
                }

                $requestOrderOffer->startTime = $requestOrderOffer->startTime ? $requestOrderOffer->startTime->getTimestamp() : null;
                $requestOrderOffer->expiryTime = $requestOrderOffer->expiryTime ? $requestOrderOffer->expiryTime->getTimestamp() : null;
                $requestOrderOffer->numberOfUsedTimes = 0;
                $errorsObjects = $this->get('validator')->validate($requestOrderOffer, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }

                $i++;
                $offerIds[] = $requestOrderOffer->id;
                $totalOffersCount += (integer)$requestOrderOffer->count;
                $offerCountArray[$requestOrderOffer->id] = array('price' => $requestOrderOffer->buyItemsCost, 'count' => $requestOrderOffer->count);
            }
        } else {
            $requestOrder->offers = array();
        }

        $minimumNumberOfItemsPerOrder = (integer) $this->get('settings_manager')->get('minimum_number_of_items_per_order');
        if ($minimumNumberOfItemsPerOrder && $minimumNumberOfItemsPerOrder > $totalItemsCount && count($offerIds) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You must order a minimum of %items-count% items in any order', array('%items-count%' => $minimumNumberOfItemsPerOrder), 'api_error_message'));
        }
        $requestOrder->paymentMethod->cardNumber = isset($requestOrder->paymentMethod->cardNumber) ? $requestOrder->paymentMethod->cardNumber : $requestOrder->paymentMethod->type;
        $errorsObjects = $this->get('validator')->validate($requestOrder->paymentMethod, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $creditCard = null;
        if ($requestOrder->paymentMethod->type == 'CREDIT') {
            $creditCard = $em->getRepository('IbtikarShareEconomyPayFortBundle:PfPaymentMethod')->findOneBy(array('id'=> $requestOrder->paymentMethod->id));
            if (!$creditCard) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("credit card was not found"));
            }
        }
        // NEW-ISPL START on 30-12-2018
        elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
            if ($user->getBalance()==0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No Enough Balance"));
            }
        }
        // NEW-ISPL END on 30-12-2018

        $errorsObjects = $this->get('validator')->validate($requestOrder, null, array('Default'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        if ($requestOrder->addressId) {
            $address = $em->getRepository('IbtikarTaniaModelBundle:UserAddress')->findOneBy(array('id'=> $requestOrder->addressId));
            if (!$address) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No address was found"));
            } else {
                $cityArea = $this->getCityAreaPerLatLong($address->getLatitude(), $address->getLongitude());
            }
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        } elseif ($requestOrder->lat && $requestOrder->long) {
            $address = null;
            $cityArea = $this->getCityAreaPerLatLong($requestOrder->lat, $requestOrder->long);
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        } else {
            $address = null;
            $cityArea = $this->getCityAreaPerLatLong($this->getUser()->getLatitude(), $this->getUser()->getLongitude());
            if (!$cityArea) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
            }
        }
        $errorsObjects = $this->get('validator')->validate($requestOrder->receipt);
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }
        $city = $cityArea->getCity();
        $em = $this->getDoctrine()->getManager();
        $items = $em->createQueryBuilder()
            ->select('i')
            ->from('IbtikarTaniaModelBundle:Item', 'i')
            ->where('i.id in (:itemId)')
            ->andWhere('i.shown = :shown')
            ->setParameter('itemId', $itemIds)
            ->setParameter('shown', true)
            ->getQuery()
            ->getResult();

        $offers = $em->createQueryBuilder()
            ->select('o')
            ->from('IbtikarTaniaModelBundle:Offer', 'o')
            ->where('o.id in (:offerIds)')
            ->setParameter('offerIds', $offerIds)
            ->getQuery()
            ->getResult();

        if (count($items)<=0 && count($requestData->offers) == 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $itemNameGetter = 'getName';
        if ($request->getLocale() == 'en') {
            $itemNameGetter = 'getNameEn';
        }
        /* @var $item \Ibtikar\TaniaModelBundle\Entity\Item */
        foreach ($items as $item) {
            $minimumAmountToOrder = $item->getMinimumAmountToOrder();
            if ($minimumAmountToOrder && $minimumAmountToOrder > $countArray[$item->getId()]['count'] && count($requestData->offers) == 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You can only order a minimum of %item-minimum-count% items of %item-name%', array('%item-minimum-count%' => $minimumAmountToOrder, '%item-name%' => $item->$itemNameGetter()), 'api_error_message'));
            }
        }
        $responseItemList = $this->getItemObjectPerCity($request, $items, $city);
        $totalPrice = 0;
        foreach ($responseItemList->items as $item) {
            $totalPrice += ($item->price * $countArray[$item->getId()]['count']);
        }
        /* @var $offer \Ibtikar\TaniaModelBundle\Entity\Offer */
        foreach ($offers as $offer) {
            if ($offer->getExpiryTime() && $offer->getExpiryTime()->getTimestamp() <= time()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }

            if (!$offer->getEnabled()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Offer: %offer-title% is expired', array('%offer-title%' => $request->getLocale() == 'en' ? $offer->getTitleEn() : $offer->getTitle()), 'api_error_message'));
            }
            $totalPrice += ($offer->getOfferPrice() * $offerCountArray[$offer->getId()]['count']);
        }
        $promocode = null;
        if ($requestOrder->promoCode && $requestOrder->promoCode->code) {
            $validationResponse = $this->get('order_operations')->getValidationErrorResponseForPromoCodeOrPromoCode($requestOrder->promoCode->code, $this->getUser(), null, $city);
            if ($validationResponse instanceof JsonResponse) {
                return $validationResponse;
            }
            /* @var $promocode PromoCode */
            $promocode = $validationResponse;
        }

        $withoutTaxes = false;
        if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $user->getBalance() >= $requestOrder->receipt->finalPrice) {
            $withoutTaxes = true;
        }
        /*   $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, TRUE);
         */
       
        // NEW-ISPL START on 31/12/2018
        $secondPaymentPrice = 0;
        $secondPaymentMethod= null;
        $firstPaymentPrice= null;
        if ($requestOrder->paymentMethod->type == 'CASH') {
            $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true);
        } elseif ($requestOrder->paymentMethod->type == 'CREDIT') {
            $requestOrder->paymentMethod->type = 'CASH';
            $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true);
        } else {
            if ($user->getBalance() < $requestOrder->receipt->finalPrice) {
                $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];

                $requestOrder->receipt->taxFees = ((($requestOrder->totalPrice - $user->getBalance()) * $fees->getTaxPercent()) / 100);
                $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);

                $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));

                $secondPaymentMethod = 'CASH';
                $firstPaymentPrice = $user->getBalance();
                $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $user->getBalance());
                $receipt = $this->getReceipt(
                    $requestOrder->receipt->totalPrice,
                    $withoutTaxes,
                    $promocode,
                    $offers,
                    $offerCountArray,
                    true,
                    ($requestOrder->totalPrice-$firstPaymentPrice)
                );

                $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];
                $requestOrder->receipt->taxFees = ($fees->getTaxPercent() * ($requestOrder->receipt->totalPrice-$firstPaymentPrice) / 100);
                $requestOrder->receipt->finalPrice =
                ($requestOrder->receipt->totalPrice - $receipt->receipt->offersDiscount) + ($requestOrder->receipt->taxFees) - $requestOrder->receipt->discount;

                $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $firstPaymentPrice);
            } else {
                $requestOrder->receipt->taxFees = 0;
                $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);
                $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));
                $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray, true, ($requestOrder->receipt->totalPrice->firstPaymentPrice));
            }
        }
        // NEW-ISPL END on 31/12/2018
      
        // NEW-ISPL START
        /*if($totalPrice != $receipt->receipt->paidTotalPrice || $receipt->receipt->finalPrice != $requestOrder->receipt->finalPrice) //if cost of any item changed or fees changed
        {
            $receipt = $this->getReceipt($totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Costs Changed', array(), 'validators'), $receipt);
        }*/ // NEW-ISPL END

        $shift = null;
        if ($requestOrder->receivingDate) {
            if (!$requestOrder->shiftId) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('No shift was found'));
            }
            $shift = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findOneBy(array('id'=> $requestOrder->shiftId));
            if (!$shift) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
        } else {
            $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findAll();
            if (count($shifts) === 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
            $currentTime = new \DateTime();
            foreach ($shifts as $dayShift) {
                $shiftTimeToday = clone $currentTime;
                $shiftTimeToday->modify($dayShift->getTo()->format('H:i'));
                if ($shiftTimeToday > $currentTime) {
                    $shift = $dayShift;
                    $requestOrder->receivingDate = $currentTime->getTimestamp();
                    break;
                }
            }
            if (!$shift) {
                // No shift available today deliver tomorrow
                $requestOrder->receivingDate = new \DateTime('tomorrow');
                $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
                $shift = $shifts[0];
            }
        }
        $firstTimeToday = new \DateTime('midnight');
        $lastTimeToday = new \DateTime('tomorrow');
        if ($requestOrder->receivingDate >= $firstTimeToday->getTimestamp() && $requestOrder->receivingDate < $lastTimeToday->getTimestamp()) {
            $firstTimeToday->modify($shift->getTo()->format('H:i'));
            if (time() > $firstTimeToday->getTimestamp()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('shif_expired', array(), 'api_error_message'));
            }
        }
       
        $responseOrder = $this->applySubmitOrderByDriverV1($requestOrder, $items, $city, $cityArea, $shift, $address, $countArray, $creditCard, $promocode, $receipt, $offerCountArray, $offers, $firstPaymentPrice, $secondPaymentPrice, $secondPaymentMethod);
        return $apiOperations->getJsonResponseForObject($responseOrder);
    }

    /**
     * @author Moemen Hussein <moemen.hussein@ibtikar.net.sa>
     * @param type $items
     * @param type $city
     * @return ResponseItemList
     * @throws \Exception
     */
    protected function applySubmitOrderByDriverV1(RequestOrder $requestOrder, $items, $city, $cityArea, Shift $shift, $address, $countArray, PfPaymentMethod $creditCard = null, PromoCode $promoCode = null, $receipt, $offerCountArray = array(), $offers = array(), $firstPaymentPrice = null, $secondPaymentPrice = null, $secondPaymentMethod = null)
    {
        $em = $this->getDoctrine()->getManager();
        $order = new Order();
        // ** NEW-ISPL START on 05/01/2019 **//
        // ** Auto Assign Driver When Order is from Driver App **//
        $orderSource = 'driver-app';
        $createdBy = $requestOrder->driverId; //NEED TO DO
        if ($requestOrder->driverId >0) {
            /* @var $driver \Ibtikar\TaniaModelBundle\Entity\Driver */
            $driver = $em->getRepository('IbtikarTaniaModelBundle:Driver')->find($requestOrder->driverId);
            if (! $driver) {
                return $apiOperations->getErrorJsonResponse($translator->trans('Driver not found.', array(), 'order'));
            } else {
                /* $driverHasOrderCityArea = false;
                 foreach ($driver->getDriverCityAreas() as $driverCityArea)
                 {
                     if ($driverCityArea->getCityArea() && $order->getCityAreaId() == $driverCityArea->getCityArea()->getId()) {
                         $driverHasOrderCityArea = true;
                         break;
                     }
                 }
                 if (! $driverHasOrderCityArea) {
                    return $apiOperations->getErrorJsonResponse($translator->trans('Driver not in order area.', array(), 'order'));
                 }
                 else
                 {*/
                /* if (! $order->isOrderAssignableToOfflineDrivers() && ! $driver->getStatus()) {
                     return $apiOperations->getErrorJsonResponse($translator->trans('Order is now and Driver is offline.', array(), 'order'));
                 }
                 else
                 {*/
                /*     $driverAssignedToVan = false;
                     foreach ($driver->getVanDrivers() as $vanDriver) {
                         if ($vanDriver->getVan()) {
                             $driverAssignedToVan = true;
                             break;
                         }
                     }
                     if (! $driverAssignedToVan) {
                         return $apiOperations->getErrorJsonResponse($translator->trans('Please assign a van to this driver first', array(), 'order'));
                     }
                     else
                     {
                         $order->setAssignedBy($this->getUser());
                         $previousDriver = $order->getDriver();
                         // notify the previous driver that the order assigned to another driver
                         $driverChanged = false;
                         if ($order->getStatus() == Order::$statuses['verified'] && $driver != $previousDriver) {
                             $msgLocale = ($previousDriver->getLocale() == 'ar') ? 'ar' : 'en';
                             $notificationService->sendNotificationToUser($previousDriver, $translator->trans('Order %order-id% is reassigned to another driver', array('%order-id%' => $order->getId()), 'order', $msgLocale), $translator->trans('Order %order-id% is reassigned to another driver', array('%order-id%' => $order->getId()), 'order', $msgLocale), array('type' => NotificationCenter::DRIVER_ORDER_REASSIGNED, 'id' => $order->getId(), 'oldStatus' => Order::$statuses['verified'], 'newStatus' => Order::$statuses['verified']));
                             $driverChanged = true;
                         }*/
                $driverChanged = true;
                $order->setDriver($driver);
                $oldStatus = $order->getStatus();
                $dashboardUser = $this->getUser();
                //$dashboardUser = "0";
                                        if ($driverChanged) { // to force logging if the status is not changed & the driver changed
                                          $order->setStatusAndLogStatusHistory(Order::$statuses['verified'], $dashboardUser);
                                        } else { // this will log if status changed only
                                            $order->setStatus(Order::$statuses['verified'], $dashboardUser);
                                        }
                // $order->setIsSynced(false);
                                        //$em->persist($order);
                                       // $em->flush();
                                       // $this->firebaseDbAction($order);
                                        /*$msgLocale = ($driver->getLocale() == 'ar') ? 'ar' : 'en';
                                        $notificationService->sendNotificationToUser($driver, $translator->trans('New request', array(), 'order', $msgLocale), $translator->trans('You have new request', array(), 'order', $msgLocale), array(
                                            'id' => $order->getId(),
                                            'oldStatus' => $oldStatus,
                                            'newStatus' => $order->getStatus()
                                        ));
                                        $user = $order->getUser();*/
                                        // $userNotificationDriverName = $user->getLocale() === 'en' ? $driver->getFullName() : $driver->getFullNameAr();
                                        /*return $apiOperations->getErrorJsonResponse($translator->trans('Your order is assigned to %driver-name%', array(
                                            '%driver-name%' => $driver->getFullNameAr()
                                        ), 'order', 'ar'));
                                       return $apiOperations->getErrorJsonResponse($translator->trans('Your order is assigned to %driver-name%', array(
                                            '%driver-name%' => $driver->getFullNameAr()
                                        ), 'order', 'ar'));
                                        return $apiOperations->getErrorJsonResponse($translator->trans('Your order is assigned to %driver-name%', array(
                                            '%driver-name%' => $driver->getFullName()
                                        ), 'order', 'en'));
                                       return $apiOperations->getErrorJsonResponse($translator->trans('Your order is assigned to %driver-name%', array(
                                            '%driver-name%' => $driver->getFullName()
                                        ), 'order', 'en'));
                                        $notifData = array(
                                            'id' => $order->getId(),
                                            'oldStatus' => $oldStatus,
                                            'newStatus' => $order->getStatus()
                                        );*/
                                        // $notificationService->sendNotificationToUser($user, $translator->trans('Your order is assigned to %driver-name%', array('%driver-name%' => $userNotificationDriverName), 'order', $user->getLocale()), $translator->trans('Your order is assigned to %driver-name%', array('%driver-name%' => $userNotificationDriverName), 'order', $user->getLocale()), $notifData);
                                       /* if ($user) {
                                            $this->get('notification_center')->sendNotificationToUser($user, NotificationCenter::USER_ORDER_ASSIGNED, $titleAr, $titleEn, $bodyAr, $bodyEn, $user->getLocale(), $notifData);
                                            $this->firebaseDbAction($order);
                                        }
                                        return $apiOperations->getErrorJsonResponse($translator->trans('Done', array(), 'order'));
                                        return $apiOperations->getErrorJsonResponse($translator->trans('Order %order-id% successfully assigned to driver %driver-name%', array(
                                            '%order-id%' => $orderId,
                                            '%driver-name%' => $driver->getUsername()
                                        ), 'order'));
                                        $notificationStatus = 'success';*/

                                        // NANACALL2
                                       /* if ($order->getSource() == 'nana') {
                                            $syncResult = $this->nanaApiUpdateOrderStatus($orderId, Order::$statuses['verified']);
                                            if ($syncResult && isset($syncResult->status) && $syncResult->status) {
                                                $order->setIsNanaSynced(TRUE);
                                            }
                                            if ($syncResult) {
                                                $order->setNanaSyncData($syncResult);
                                            } else {
                                                $order->setNanaSyncData(array());
                                            }
                                            $em->flush();
                                         }*/
                                 //   }
                              //  }
                           // }
            }
        }
        // ** NEW-ISPL END on 05/01/2019 **//
        // ** Auto Assign Driver When Order is from Driver App **//
        /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\PromoCodeRepository */
        $promoCodeRepo = $em->getRepository('IbtikarTaniaModelBundle:PromoCode');
        /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\OrderRepository */
        $orderRepo = $em->getRepository('IbtikarTaniaModelBundle:Order');
        $totalPrice = 0;
        
        if ($requestOrder->paymentMethod->type!='' && $requestOrder->paymentMethod->type!=null) {
            $order->setPaymentMethod($requestOrder->paymentMethod->type);
        }
        //else
        // $order->setPaymentMethod($requestOrder->paymentMethod->type2);
            
        $order->setCreditCard($creditCard);
        $user = $this->getUser();
        if ($promoCode) {
            $usedByUserBefore = ((integer) $orderRepo->countPromoCodeUsedTimesByUser($user->getId(), $promoCode->getId())) > 0 ? true : false;
            if (isset($receipt->discount)) {
                $promoCodeRepo->increasePromoCodeUsage($promoCode->getId(), $receipt->discount, $usedByUserBefore);
            } else {
                $promoCodeRepo->increasePromoCodeUsage($promoCode->getId(), $receipt->receipt->discount, $usedByUserBefore);
            }
            $order->setPromoCodeMethod($promoCode->getType());
            $order->setPromoCodeName($promoCode->getCode());
            $order->setPromoCodeValue($promoCode->getDiscountAmount());
            $order->setPromoCode($promoCode);
        }
        if ($creditCard) {
            $order->setCardNumber($creditCard->getCardNumber());
            $order->setExpiryDate($creditCard->getExpiryDate());
            $order->setFortId($creditCard->getFortId());
            $order->setIsDefault($creditCard->getIsDefault());
            $order->setMerchantReference($creditCard->getMerchantReference());
            $order->setPaymentOption($creditCard->getPaymentOption());
            $order->setTokenName($creditCard->getTokenName());
        } elseif ($requestOrder->paymentMethod->type == 'BALANCE') {
            $cartPrice = $requestOrder->receipt->totalPrice - ($requestOrder->receipt->offersDiscount + $requestOrder->receipt->taxFees);
            //$order->setPaymentValue($requestOrder->paymentMethod->value);
            $order->setPaymentValue($cartPrice);
            $order->setCardNumber($requestOrder->paymentMethod->type);
            if ($user->getBalance() > $requestOrder->receipt->finalPrice) {
                $user->setBalance(($user->getBalance() - $requestOrder->receipt->finalPrice));
                $user->setUsedBalance($user->getUsedBalance() + $requestOrder->receipt->finalPrice);
            }
            // NEW-ISPL START on 30/12/2018
            else {
                $user->setUsedBalance($user->getUsedBalance() + $user->getBalance());
                $user->setBalance(0);
            }
            // NEW-ISPL START on 30/12/2018
            $em->persist($user);
        } else {
            $order->setCardNumber($requestOrder->paymentMethod->type);
        }
        //$order->setSource($this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom'));
        $order->setSource($orderSource);
        $order->setShift($shift);
        $order->setShiftFrom($shift->getFrom());
        $order->setShiftTo($shift->getTo());
        $order->setCity($city);
        $order->setCityArea($cityArea);
        $order->setCityAreaNameEn($cityArea->getNameEn());
        $order->setCityAreaNameAr($cityArea->getNameAr());
        $order->setUser($this->getUser());
        $createdByUser = $em->getRepository('IbtikarTaniaModelBundle:User')->findOneBy(array('id'=> $createdBy));
        $order->setCreatedBy($createdBy);
        $order->setCreatedByFullName($createdByUser->getFullName());
        
        /* @var Order $order */
        if ($address) {
            $order->setTitle($address->getTitle());
            $order->setAddress($address->getAddress());
            $order->setLatitude($address->getLatitude());
            $order->setLongitude($address->getLongitude());
            $order->setUserAddress($address);
            $order->setAddressType($address->getType());
        } else {
            $order->setAddress($this->getUser()->getAddress());
            $order->setLatitude($this->getUser()->getLatitude());
            $order->setLongitude($this->getUser()->getLongitude());
        }
        if ($requestOrder->receivingDate) {
            $order->setReceivingDate($requestOrder->receivingDate);
        } else {
            $order->setReceivingDate($this->getDateInTimeStamp(new \DateTime()));
        }
        //NEW-ISPL START on 30/12/2018
        $order->setSecondPaymentMethod($secondPaymentMethod);
        $order->setFirstPaymentPrice($firstPaymentPrice);
        $order->setSecondPaymentPrice($secondPaymentPrice);
        
        $order->setAmountDue($requestOrder->receipt->finalPrice);
        $order->setPrice($requestOrder->receipt->totalPrice);
        $order->setTaxFees($requestOrder->receipt->taxFees);
        //NEW-ISPL END on 30/12/2018
        
        if ($requestOrder->receivingDate && $requestOrder->receivingDate != '') {
            $order->setReceivingDate($requestOrder->receivingDate);
        }
        $order->setNote($requestOrder->note);
        $em->persist($order);
        foreach ($items as $item) {
            $orderItem = new OrderItem();
            $orderItem->setCity($city);
            $orderItem->setCount($countArray[$item->getId()]['count']);
            $orderItem->setItem($item);
            $orderItem->setPrice($countArray[$item->getId()]['price']);
            $order->addOrderItem($orderItem);
            $orderItem->setOrder($order);
            $em->persist($orderItem);
        }

        /* @var Offer $offer */
        foreach ($offers as $offer) {
            $orderOffer = new OrderOffer();
            $orderOffer->setType($offer->getType());
            $orderOffer->setPrice($offer->getOfferPrice());
            $orderOffer->setCount($offerCountArray[$offer->getId()]['count']);
            $orderOffer->setOffer($offer);
            $orderOffer->setOrder($order);
            $orderOffer->setCashGetAmount($offer->getCashGetAmount());
            $orderOffer->setPercentageGetAmount($offer->getPercentageGetAmount());
            $orderOffer->setDescriptionPublic($offer->getDescriptionPublic());
            $orderOffer->setDescriptionPublicEn($offer->getDescriptionPublicEn());
            $orderOffer->setTitle($offer->getTitle());
            $orderOffer->setTitleEn($offer->getTitleEn());
            $order->addOrderOffer($orderOffer);

            $em->persist($orderOffer);
            $offer->setNumberOfUsedTimes(max(0, $offer->getNumberOfUsedTimes() + $offerCountArray[$offer->getId()]['count'])); // DECREMENT Offer Usage
        }
        $em->flush();
        $em->getRepository('IbtikarTaniaModelBundle:User')->increamentNumberOfOrders($user);
        $responseOrderListData = $this->getUserOrderObject($order);
        return $responseOrderListData;
    }


    /**
     * Rate order
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="order", "dataType"="integer", "required"=true},
     *      {"name"="rate", "dataType"="integer", "required"=true},
     *      {"name"="rateComment", "dataType"="string", "required"=false, "description"= "required if rate 1->3"},
     *      {"name"="tags", "dataType"="json", "required"=false, "description"= "json array of tags"},
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     *
     * @return JsonResponse
     */
    public function rateAction(RequestAlias $request)
    {
        /* @var $translator \Symfony\Component\Translation\DataCollectorTranslator */
        $translator = $this->get('translator');
        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */

        $userOperations = $this->get('user_operations');
        $apiOperations = $this->get('api_operations');

        $em = $this->getDoctrine()->getManager();
        $orderId = trim($request->get('order'));
        $rate = trim($request->get('rate'));
        $comment = trim($request->get('rateComment'));
        $tags = json_decode(urldecode($request->get('tags')));

        $validationMessages = array();
        /* @var $order \Ibtikar\TaniaModelBundle\Entity\Order */
        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->find($orderId);
        if (!$order) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Order Not Found"));
        }
        $orderUser = $order->getUser();

        if ($orderUser && (!$order || $orderUser->getId() != $this->getUser()->getId())) {
            $validationMessages['order'] = $translator->trans('invalid_order_id', array(), 'order');
        } else {
            $order->setOldRate(null);
            if ($order->getRate()) { // order is already rated
                $order->setOldRate($order->getRate());
            }
            if ($order->getStatus() != Order::$statuses['delivered']) {
                return $apiOperations->getSingleErrorJsonResponse($translator->trans('incomplete_order', array(), 'order'));
            }

            $order->setRate($rate);
            $order->setRateComment($comment);
            $validationMessages = $apiOperations->validateObject($order, ['rate']);

            // 3 stars or less rating comment mandatory no longer required
            /*
            if ($rate && $rate <= 3 && !$comment)
                $validationMessages['rateComment'] = $translator->trans('fill_mandatory_field', array(), 'validators');
            */
        }

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        $oldOrderRatingTags = $order->getOrderRatingTags();
        foreach ($oldOrderRatingTags as $oldOrderRatingTag) {
            $em->remove($oldOrderRatingTag);
        }

        if ($tags) {
            foreach ($tags as $tag) {
                $ratingTag = $em->getRepository('IbtikarTaniaModelBundle:RatingTag')->find($tag->id);
                if (!$ratingTag) {
                    continue;
                }
                $orderRatingTag = new \Ibtikar\TaniaModelBundle\Entity\OrderRatingTag();
                $orderRatingTag->setRatingTag($ratingTag);
                $orderRatingTag->setOrder($order);
                $em->persist($orderRatingTag);
            }
        }

        // recalculate driver average rate
        $em->getFilters()->disable('softdeleteable');
        $driver = $order->getDriver();
        if ($driver && $driver->getdeletedAt() == false) {
            $average = $em->getRepository('IbtikarTaniaModelBundle:Order')->getDriverRate($driver->getId());
            $driver->setDriverRate($average);
            $em->getFilters()->enable('softdeleteable');
        }
        $em->flush();

        $errorMsg = $translator->trans("Order rated successfully", array(), 'order');
        return $apiOperations->getSuccessJsonResponse($errorMsg);
    }

    /**
     * Get order details
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "user application"="DarkCyan",
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     *
     * @return JsonResponse
     */
    public function detailsAction(RequestAlias $request, $id)
    {
        $apiOperations = $this->get('api_operations');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $id, 'driver' => $user->getId()]);
        } elseif ($user instanceof \Ibtikar\TaniaModelBundle\Entity\User) {
            $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $id, 'user' => $user->getId()]);
        }

        $translator = $this->get('translator');

        if (!$order) {
            return $apiOperations->getSingleErrorJsonResponse($translator->trans('order_not_found', array(), 'order'));
        }

        $this->getDoctrine()->getManager()->getFilters()->disable('softdeleteable');

        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            $responseOrderListData = $this->getDriverOrderObject($order);
        } elseif ($user instanceof \Ibtikar\TaniaModelBundle\Entity\User) {
            $responseOrderListData = $this->getUserOrderObject($order);
        }

        return $apiOperations->getJsonResponseForObject($responseOrderListData);
    }

    /**
     * List user orders
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "user application"="DarkCyan",
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="category", "dataType"="string", "required"=true},
     *      {"name"="page", "dataType"="integer", "required"=true}
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     *
     * @return JsonResponse
     */
    public function listAction(RequestAlias $request)
    {
        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $category = $request->get('category');
        $page = $request->get('page');
        if ($page !== 0 && !$page) {
            $page = 1;
        }
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            $orders = $em->getRepository('IbtikarTaniaModelBundle:Order')->getDriverListOrders($user->getId(), strtolower($category), $page);
        } elseif ($user instanceof \Ibtikar\TaniaModelBundle\Entity\User) {
            $orders = $em->getRepository('IbtikarTaniaModelBundle:Order')->getListOrders($user->getId(), strtolower($category), $page);
        }
        if (count($orders) == 0) {
            return $apiOperations->getSuccessJsonResponse($this->get('translator')->trans('No Order is available'));
        }
        $responseOrderList = new ResponseOrderList();
        $this->getDoctrine()->getManager()->getFilters()->disable('softdeleteable');
        foreach ($orders as $order) {
            if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
                $responseOrderList->orders[] = $this->getDriverOrderObject($order);
            } elseif ($user instanceof \Ibtikar\TaniaModelBundle\Entity\User) {
                $responseOrderList->orders[] = $this->getUserOrderObject($order);
            }
        }
        return $apiOperations->getJsonResponseForObject($responseOrderList);
    }

    public function getUserOrderObject($order)
    {
        /* @var $order Order */
        $request = $this->get('request_stack')->getCurrentRequest();
        $responseOrderListData = $this->getOrderObject($order);
        if ($order->getDriverUsername()) {
            $responseDriver = new ResponseOrderDriver();
            $responseDriver->id = $order->getDriver()?$order->getDriver()->getId():null;
            $responseDriver->fullName = $order->getDriverFullName();
            $responseDriver->fullNameAr = $order->getDriverFullNameAr();
            $responseDriver->phone = $order->getDriverPhone();
            $responseDriver->username = $order->getDriverUsername();
            $responseDriver->image = $order->getDriverImage() ? $request->getSchemeAndHttpHost() . '/' . $order->getDriverImage() : null;
            $responseDriver->rate = (double)$order->getDriverRate();
            $responseDriver->vanNumber = (string)$order->getDriver() ? $order->getVanNumber() : null;
        } else {
            $responseDriver = null;
        }
        $responseOrderListData->driver = $responseDriver;
        unset($responseOrderListData->user);
        return $responseOrderListData;
    }

    public function getDriverOrderObject($order)
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $responseOrderListData = $this->getOrderObject($order);

        $responseUser = new \AppBundle\APIResponse\Driver\User();

        /* @var Order $order */
        $responseUser->id = $order->getUser() ? $order->getUser()->getId() : 0;
        $responseUser->fullName = $order->getUser() ? $order->getUser()->getFullName() : $order->getCustomerUsername();
        $responseUser->phone = $order->getUser() ? $order->getUser()->getPhone() : $order->getCustomerPhone();

        $responseOrderListData->user = $responseUser;
        unset($responseOrderListData->driver);
        return $responseOrderListData;
    }

    private function getOrderRatingTagsForOrderObject($order, $locale)
    {
        $orderRatingTags = $order->getOrderRatingTags();
        $ratingTags=[];
        if ($orderRatingTags) {
            foreach ($orderRatingTags as $orderRatingTag) {
                $ratingTag = $orderRatingTag->getRatingTag();
                if ($ratingTag) {
                    $ratingTagName = $locale == "Ar" ? $ratingTag->getName() : $ratingTag->getNameEn();
                    $ratingTags[] = ['id' => $ratingTag->getId(), 'name'=> $ratingTagName];
                }
            }
        }
        return $ratingTags;
    }
    
    public function getOrderObject($order)
    {
        /* @var $order \Ibtikar\TaniaModelBundle\Entity\Order */
        $request = $this->get('request_stack')->getCurrentRequest();
        $locale = ucfirst($request->headers->get('accept-language'));
        $apiOperations = $this->get('api_operations');
        $responseOrderListData = new ResponseOrderListData();
        $responseOrderListData->id = $order->getId();
        $responseOrderListData->totalPrice = (double)$order->getPrice();
        $responseOrderListData->createdDate = $order->getCreatedAt()->getTimestamp();
        $responseOrderListData->status = $order->getStatus();
        $responseOrderListData->closeReason = $order->getCloseReason();
        $responseOrderListData->receivingDate = (int)$order->getReceivingDate();
        $responseOrderListData->rate = (double)$order->getRate();
        $responseOrderListData->rateComment = $order->getRateComment();
        $responseOrderListData->ratingTags = $this->getOrderRatingTagsForOrderObject($order, $locale);
        $responseOrderListData->note = $order->getNote();
        $responseOrderListData->type = 'order';
        if ($order->getSource()=='driver-app') {
            $responseOrderListData->canEditByDriver = true;
        } //can change in future
        else {
            $responseOrderListData->canEditByDriver = false;
        }
        if ($order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest) {
            $responseOrderListData->type = 'balance';
            $responseOrderListData->balance = $this->get('order_operations')->getBalanceRequest($order);
        }

        $promoCode = $order->getPromoCode();
        if ($promoCode) {
            $promoCodeResponseData = new PromoCodeResponse();
            $promoCodeResponseData->id = $promoCode->getId();
            $promoCodeResponseData->code = $promoCode->getCode();
            $promoCodeResponseData->title = $promoCode->getTitle();
            $promoCodeResponseData->type = $promoCode->getType();
            $promoCodeResponseData->discountAmount = (double) $promoCode->getDiscountAmount();
            $responseOrderListData->promoCode = $promoCodeResponseData;
        }

        $address = new Address();
        $address->id = $order->getUserAddress() ? $order->getUserAddress()->getId() : 0;
        $address->title = $order->getTitle();
        $address->address = $order->getAddress();
        $address->lat = (double)$order->getLatitude();
        $address->long = (double)$order->getLongitude();
        $address->type = $order->getUserAddress() ? $order->getUserAddress()->getType() : null;
        $address->capacity = $order->getUserAddress() ? $order->getUserAddress()->getCapacity() : null;
        $address->distance = 0;

        $responseOrderListData->addressId = $order->getUserAddress() ? $order->getUserAddress()->getId() : 0;
        $responseOrderListData->address = $address;

        $receipt = new Receipt();
        $receipt->totalPrice = (double)$order->getPrice();
        //$receipt->finalPrice = (double)$order->getAmountDue();
        if ($order->getSecondPaymentMethod()!='') { // CASH OR BALANCE
        $receipt->finalPrice = (double)$order->getSecondPaymentPrice();
        } else {
            $receipt->finalPrice = (double)$order->getAmountDue();
        }
        $receipt->taxFees = (double)$order->getTaxFees();
        $receipt->paidTotalPrice = (double)$order->getAmountDue();
        if ($order->getSecondPaymentMethod()!='') {
            $receipt->firstPaymentPrice = (double)$order->getFirstPaymentPrice();
            $receipt->secondPaymentPrice = (double)$order->getSecondPaymentPrice() - $receipt->taxFees;
        } else {
            $receipt->firstPaymentPrice = (double)$order->getFirstPaymentPrice() - $receipt->taxFees;
            $receipt->secondPaymentPrice = (double)$order->getSecondPaymentPrice();
        }

        $creditId = null;
        if ($order->getCreditCard()) {
            $creditId = $order->getCreditCard()->getId();
        }
        $paymentMethod = new PaymentMethod();
        $paymentMethod->id = $creditId;
        $paymentMethod->cardNumber = $order->getCardNumber();
        $paymentMethod->expiryDate = (double)$order->getExpiryDate();
        $paymentMethod->fortId = $order->getFortId();
        $paymentMethod->isDefault = $order->getIsDefault();
        $paymentMethod->merchantReference = $order->getMerchantReference();
        $paymentMethod->paymentOption = $order->getPaymentOption();
        $paymentMethod->tokenName = $order->getTokenName();
        //if($order->getSecondPaymentMethod()!=null)
        //$paymentMethod->type = $order->getPaymentMethod().','.$order->getSecondPaymentMethod();
        // else
        $paymentMethod->type = $order->getPaymentMethod();
        if ($order->getPaymentMethod() == Order::BALANCE) {
            $paymentMethod->value = $order->getAmountDue();
        } else {
            $paymentMethod->value = $order->getPaymentValue();
        }
        $responseOrderListData->paymentMethod = $paymentMethod;

        $shift = new ResponseShift();

        $shift->id = $order->getShift()->getId();
        if (strtolower($request->headers->get('accept-language')) == 'en') {
            $shift->shift = $order->getShift()->getShift();
        } else {
            $shift->shift = $order->getShift()->getShiftAr();
        }
        $shift->from = strtotime($order->getShiftFrom()->format('H:i'));
        $shift->to = strtotime($order->getShiftTo()->format('H:i'));

        $responseOrderListData->shift = $shift;

        $offersDiscount = 0;
        $orderDiscount = 0;

        if (!$order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest) {
            foreach ($order->getOrderItems() as $orderItem) {
                /* @var $orderItem \Ibtikar\TaniaModelBundle\Entity\OrderItem */
                $responseOrderListItem = new ResponseOrderListItem();
                $responseOrderListItem->id = $orderItem->getItem()->getId();
                $responseOrderListItem->name =  $request->headers->get('accept-language') == 'en' ? $orderItem->getItem()->getNameEn(): $orderItem->getItem()->getName();
                $responseOrderListItem->image = $request->getSchemeAndHttpHost().'/'.$orderItem->getItem()->getWebPath();
                $responseOrderListItem->count = $orderItem->getCount();
                $responseOrderListItem->price = (double)$orderItem->getPrice();
                $responseOrderListItem->minimumAmountToOrder = $orderItem->getItem() ? (double)$orderItem->getItem()->getMinimumAmountToOrder() : 0;
                $responseOrderListData->items[] = $responseOrderListItem;
            }

            /* @var OrderOffer $orderOffer */
            foreach ($order->getOrderOffers() as $orderOffer) {
                /* @var $orderItem \Ibtikar\TaniaModelBundle\Entity\OrderItem */
                $responseOrderListOffer = new ResponseOffer();
                /* @var Item $item */
                $responseOrderListOffer->id = $orderOffer->getOffer()->getId();
                $responseOrderListOffer->type = $orderOffer->getType();
                $responseOrderListOffer->title =  $request->headers->get('accept-language') == 'en' ?  $orderOffer->getTitleEn(): $orderOffer->getTitle();
                $responseOrderListOffer->description =  $request->headers->get('accept-language') == 'en' ?  $orderOffer->getDescriptionPublicEn(): $orderOffer->getDescriptionPublic();
                $responseOrderListOffer->startTime = $orderOffer->getOffer()->getStartTime() ? $orderOffer->getOffer()->getStartTime()->getTimestamp() : 0;
                $responseOrderListOffer->expiryTime = $orderOffer->getOffer()->getExpiryTime() ? $orderOffer->getOffer()->getExpiryTime()->getTimestamp() : 0;
                $responseOrderListOffer->numberOfUsedTimes = $orderOffer->getOffer()->getNumberOfUsedTimes();
                $responseOrderListOffer->count = $orderOffer->getCount();

                $discountPercent = 0;
                if ($orderOffer->getType() == Offer::TYPE_CASH_PERCENTAGE) {
                    $responseOrderListOffer->getAmount = (double) $orderOffer->getPercentageGetAmount() * 100;
                    $discountPercent = (double) $orderOffer->getPercentageGetAmount();
                } elseif ($orderOffer->getType() == Offer::TYPE_CASH_AMOUNT) {
                    $responseOrderListOffer->getAmount = (double) $orderOffer->getCashGetAmount();
                } else {
                    $responseOrderListOffer->getAmount = 0;
                }

                $offerBuyItems = array();
                $offerGetItems = array();

                $buyItems = $orderOffer->getOrderOfferBuyItems();
                $getItems = $orderOffer->getOrderOfferGetItems();

                if (count($buyItems) > 0) {
                    $responseOrderListOffer->image = $buyItems[0]->getItem() ? $request->getSchemeAndHttpHost().'/'.$buyItems[0]->getItem()->getWebPath() : null;
                }

                $buyItemsCost = 0;
                /* @var OrderOfferBuyItem $buyItem*/
                foreach ($buyItems as $buyItem) {
                    if ($buyItem->getItem()) {
                        $responseBuyItem = new ResponseBuyItem();
                        $responseBuyItem->id = $buyItem->getItem() ? $buyItem->getItem()->getId() : 0;
                        $responseBuyItem->name = $request->headers->get('accept-language') == 'en' ? $buyItem->getNameEn() : $buyItem->getName();
                        $responseBuyItem->count = $buyItem->getCount();
                        $responseBuyItem->price = (double)$buyItem->getPrice();
                        $responseBuyItem->image = $buyItem->getItem() ? $request->getSchemeAndHttpHost() . '/' . $buyItem->getItem()->getWebPath() : "";

                        $offerBuyItems[] = $responseBuyItem;
                        $buyItemsCost += (double)$buyItem->getPrice() * $buyItem->getCount();
                    }
                }

                /* @var OrderOfferGetItem $getItem*/
                foreach ($getItems as $getItem) {
                    if ($getItem->getItem()) {
                        $responseGetItem = new ResponseGetItem();
                        $responseGetItem->id = $getItem->getItem() ? $getItem->getItem()->getId() : 0;
                        $responseGetItem->name = $request->headers->get('accept-language') == 'en' ? $getItem->getNameEn() : $getItem->getName();
                        $responseGetItem->count = $getItem->getCount();
                        $responseGetItem->price = (double)$getItem->getPrice();
                        $responseGetItem->image = $getItem->getItem() ? $request->getSchemeAndHttpHost() . '/' . $getItem->getItem()->getWebPath() : "";

                        $offerGetItems[] = $responseGetItem;
                    }
                }
                $responseOrderListOffer->buyItemsCost = round(max(0, $buyItemsCost - $buyItemsCost * $discountPercent), 2);
                $responseOrderListOffer->buyItems = $offerBuyItems;
                $responseOrderListOffer->getItems = $offerGetItems;

                $responseOrderListData->offers[] = $responseOrderListOffer;
                $offersDiscount += $orderOffer->getOfferDiscount() * $orderOffer->getCount();
            }

            if ($promoCode) {
                if ($order->getPromoCodeMethod() == PromoCode::PROMOCODE_TYPE_FIXED_VALUE) {
                    $orderDiscount = (double)$order->getPromoCodeValue();
                } elseif ($order->getPromoCodeMethod() == PromoCode::PROMOCODE_TYPE_PERCENTAGE) {
                    $orderDiscount = (double)round(((double)$order->getPrice() + $receipt->taxFees) * ($order->getPromoCodeValue() / 100), 2);
                }
            }
        }

        $receipt->discount = $orderDiscount;
        $receipt->offerDiscount = $offersDiscount;

        $responseOrderListData->receipt = $receipt;

        return $responseOrderListData;
    }


    /**
     * deliver order
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="order", "dataType"="integer", "required"=true},
     *      {"name"="code", "dataType"="string", "required"=true}
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function deliverAction(RequestAlias $request)
    {
        $orderId = trim($request->get('order'));
        $code = trim($request->get('code'));

        $translator = $this->get('translator');
        $apiOperations = $this->get('api_operations');
        $orderOperations = $this->get('order_operations');
        $em = $this->getDoctrine()->getManager();
        $driver = $this->getUser();

        $validationMessages = array();

        if (!$orderId) {
            $validationMessages['order'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        if (!$code) {
            $validationMessages['code'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $orderId, 'driver' => $driver->getId()]);

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (!$order) {
            $errorMsg = $translator->trans("order_not_found", array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        if ($order->getStatus() !== Order::$statuses['delivering']) {
            $errorMsg = $translator->trans('order_not_delivering', array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }
        if ($order->getDestinationVerificationCode() != $code) {
            $errorMsg = $translator->trans("invalid_verification_code", array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        try {
            $orderOperations->finishOrderV2($order);
            // TEMPORARY COMMENTED FOR ONLY DEV BY ISPL on 10/10/2019
            $this->firebaseDbAction($order);
        } catch (\Exception $exception) {
            $errorMsg = $translator->trans($exception->getMessage(), array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        return $apiOperations->getSuccessJsonResponse();
    }


    /**
     * deliver order
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="order", "dataType"="integer", "required"=true},
     *      {"name"="latitude", "dataType"="double", "required"=true},
     *      {"name"="longitude", "dataType"="double", "required"=true}
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function deliveringAction(RequestAlias $request)
    {
        $orderId = trim($request->get('order'));
        $latitude = trim($request->get('latitude'));
        $longitude = trim($request->get('longitude'));

        $translator = $this->get('translator');
        $apiOperations = $this->get('api_operations');
        $orderOperations = $this->get('order_operations');
        $em = $this->getDoctrine()->getManager();
        $driver = $this->getUser();

        $validationMessages = array();

        if (!$orderId || !$longitude || !$latitude) {
            $validationMessages['order'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $orderId, 'driver' => $driver->getId()]);

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (!$order) {
            $errorMsg = $translator->trans("order_not_found", array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        if ($order->getStatus() !== Order::$statuses['verified']) {
            $errorMsg = $translator->trans('order_not_verified', array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }
        $order->setStatus(Order::$statuses['delivering']);
        $order->setStartingLatitude($latitude);
        $order->setStartingLongitude($longitude);
        $em->flush();


        $user = $order->getUser();

        if ($user) {
            $title = $translator->trans('Order on the way', array(), 'notifications', $user->getLocale());
            $body = $translator->trans('Your order %orderId% is on the way', array(
                '%orderId%' => $order->getId(),
            ), 'notifications', $user->getLocale());
            $titleAr = $translator->trans('Order on the way', array(), 'notifications', 'ar');
            $bodyAr = $translator->trans('Your order %orderId% is on the way', array('%orderId%' => $order->getId()), 'notifications', 'ar');
            $titleEn = $translator->trans('Order on the way', array(), 'notifications', 'en');
            $bodyEn = $translator->trans('Your order %orderId% is on the way', array('%orderId%' => $order->getId()), 'notifications', 'en');
            $notifData = array(
                'id' => $order->getId(),
                'oldStatus' => Order::$statuses['verified'],
                'newStatus' => $order->getStatus(),
                'title' => $title,
                'body' => $body,
            );

            $this->get('notification_center')->sendNotificationToUser($user, NotificationCenter::USER_ORDER_DELIVERING, $titleAr, $titleEn, $bodyAr, $bodyEn, $user->getLocale(), $notifData);
            // TEMPORARY COMMENTED FOR ONLY DEV BY ISPL on 10/10/2019
            $this->firebaseDbAction($order);
        }

        //NANACALL2
        if ($order->getSource() == 'nana') {
            $syncResult = $this->nanaApiUpdateOrderStatus($orderId, Order::$statuses['delivering']);
            if ($syncResult && isset($syncResult->status) && $syncResult->status) {
                $order->setIsNanaSynced(true);
            }
            if ($syncResult) {
                $order->setNanaSyncData($syncResult);
            } else {
                $order->setNanaSyncData(array());
            }
            $em->flush();
        }
        //        $this->get('user_device_notification')->sendNotificationToUser($user, $title, $body, array(
//            'id' => $order->getId(),
//            'oldStatus' => Order::$statuses['verified'],
//            'newStatus' => $order->getStatus(),
//            'title' => $title,
//            'body' => $body,
//                ));

        return $apiOperations->getSuccessJsonResponse();
    }

    /**
     * deliver order
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="order", "dataType"="integer", "required"=true},
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function resendVerificationCodeAction(RequestAlias $request)
    {

        /* @var $order \Ibtikar\TaniaModelBundle\Entity\Order */
        $orderId = trim($request->get('order'));

        $translator = $this->get('translator');
        $apiOperations = $this->get('api_operations');
        $userOperations = $this->get('user_operations');
        $orderOperations = $this->get('order_operations');
        $em = $this->getDoctrine()->getManager();
        $driver = $this->getUser();

        $validationMessages = array();

        if (!$orderId) {
            $validationMessages['order'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $orderId, 'driver' => $driver->getId()]);

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (!$order) {
            $errorMsg = $translator->trans("order_not_found", array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }
        if (time() - $order->getDestinationVerificationCodeDate() >= (5 * 60)) {
            if ($order->getStatus() !== Order::$statuses['delivering']) {
                $errorMsg = $translator->trans('order_not_delivering', array(), 'order');
                return $apiOperations->getSingleErrorJsonResponse($errorMsg);
            }

            if ($order->getDestinationVerificationCodeCounter() >= 5) {
                if (time() - $order->getDestinationVerificationCodeDate() < (24 * 60 * 60)) {
                    $errorMsg = $translator->trans('Only 5 codes are allowed per day', array(), 'order');
                    return $apiOperations->getSingleErrorJsonResponse($errorMsg);
                }
                $order->setDestinationVerificationCodeCounter(1);
            } else {
                $order->setDestinationVerificationCodeCounter(($order->getDestinationVerificationCodeCounter()+1));
            }
            $code = rand(pow(10, 3), pow(10, 4)-1);
            $order->setDestinationVerificationCode($code);
            $order->setDestinationVerificationCodeDate($this->getDateInTimeStamp(new \DateTime()));
            $em->flush();
            $message = str_replace(array('%project%' , '%code%'), array($this->getParameter('nexmo_from_name'), $code), $translator->trans('Verification code for Tania', array(), 'order'));
            $user = $order->getUser();
//            $this->get('jhg_nexmo_sms')->sendText($user->getPhone(), $message, null, 0, $user->getLocale() === 'ar' ? 'unicode' : 'text');
            if ($user) {
                $this->get('jawaly_sms')->sendText($user->getPhone(), $message, $user->getLocale() === 'ar' ? true : false);
            }
            return $apiOperations->getSuccessJsonResponse();
        }
        $errorMsg = $translator->trans('Cannot resend within 5 Mins', array(), 'order');
        return $apiOperations->getSingleErrorJsonResponse($errorMsg);
    }


    /**
     * return order
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="order", "dataType"="integer", "required"=true},
     *      {"name"="message", "dataType"="string", "required"=true},
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function returnAction(RequestAlias $request)
    {
        $orderId = trim($request->get('order'));
        $reason = trim($request->get('message'));

        $translator = $this->get('translator');
        $apiOperations = $this->get('api_operations');

        $em = $this->getDoctrine()->getManager();
        $driver = $this->getUser();

        $validationMessages = array();

        if (!$orderId) {
            $validationMessages['order'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        /* @var $order \Ibtikar\TaniaModelBundle\Entity\Order */
        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $orderId, 'driver' => $driver->getId()]);

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (!$order) {
            $errorMsg = $translator->trans("order_not_found", array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        if (!$reason) {
            $validationMessages['reason'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (!in_array($order->getStatus(), [ Order::$statuses['delivering'], Order::$statuses['verified'] ])) {
            $errorMsg = $translator->trans('cant_return_order', array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        $order->setReturnReason($reason);
        $order->setStatus(Order::$statuses['returned']);
        $order->setEndDate(new \DateTime());
        $order->setDriver(null);

        $em->flush();

        if ($order->getUser()) {
            $this->get('user_device_notification')->sendDataNotificationToUsers(array($order->getUser()->getId()), array('id' => $orderId, 'type' => 'returned'));
            $this->firebaseDbAction($order); // BY ISPL
        }

        //NANACALL2
        if ($order->getSource() == 'nana') {
            $syncResult = $this->nanaApiUpdateOrderStatus($orderId, Order::$statuses['returned']);
            if ($syncResult && isset($syncResult->status) && $syncResult->status) {
                $order->setIsNanaSynced(true);
            }
            if ($syncResult) {
                $order->setNanaSyncData($syncResult);
            } else {
                $order->setNanaSyncData(array());
            }
            $em->flush();
        }
        

        return $apiOperations->getSuccessJsonResponse();
    }

    /**
     * return reponse
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="order", "dataType"="integer", "required"=true},
     *      {"name"="message", "dataType"="string", "required"=true},
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function SkipAction(RequestAlias $request)
    {
        $orderId = trim($request->get('order'));
        $reason = trim($request->get('message'));
        $translator = $this->get('translator');
        $apiOperations = $this->get('api_operations');
        $em = $this->getDoctrine()->getManager();
        $driver = $this->getUser();

        $validationMessages = array();

        if (!$orderId) {
            $validationMessages['order'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        if (!$reason) {
            $validationMessages['reason'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        /** Check driver is assiged to the order */
        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $orderId, 'driver' => $driver->getId()]);

        if (!$order) {
            $errorMsg = $translator->trans("order_not_found", array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        if (!in_array($order->getStatus(), [ Order::$statuses['delivering'] ])) {
            $errorMsg = $translator->trans('cant_return_order', array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        $order->setSkipReason($reason);

        $orderOperations = $this->get('order_operations');

        try {
            $orderOperations->finishOrderV2($order); // NEW-ISPL
            // TEMPORARY COMMENTED FOR ONLY DEV BY ISPL on 10/10/2019
            $this->firebaseDbAction($order);
        } catch (\Exception $exception) {
            $errorMsg = $translator->trans($exception->getMessage(), array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        $em->flush();

        return $apiOperations->getSuccessJsonResponse();
    }

    /**
     * testtt
     *
     * @ApiDoc(
     *  resource=true,
     *  section="PayFort",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  parameters={
     *      {"name"="amount", "dataType"="integer", "required"=true},
     *      {"name"="tokenName", "dataType"="string", "required"=true},
     *  },
     *  statusCodes = {
     *      200="Returned on success",
     *      403="Access denied",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @return JsonResponse
     */
    public function testPayfortAction(RequestAlias $request)
    {
        $translator = $this->get('translator');
        $validationMessages = array();
        if (!$request->get('tokenName')) {
            $validationMessages['tokenName'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        if (!$request->get('amount')) {
            $validationMessages['amount'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        $apiOperations = $this->get('api_operations');
        $pf = $this->get('ibtikar.shareeconomy.payfort.integration');
        $response = $pf->purchase(
            $request->get('tokenName'),
            $request->get('amount'),
            time(),
            'sarah.mostafa@ibtikar.net.sa'
        );

        return $apiOperations->getSuccessJsonResponse($response['response_message']);
    }


    /**
     * cancel order
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="order", "dataType"="integer", "required"=true},
     *      {"name"="message", "dataType"="string", "required"=true},
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function cancelAction(RequestAlias $request)
    {
        $orderId = trim($request->get('order'));
        $reason = trim($request->get('message'));

        $translator = $this->get('translator');
        $apiOperations = $this->get('api_operations');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $validationMessages = array();

        if (!$orderId) {
            $validationMessages['order'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        /* @var $order Order */
        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $orderId, 'user' => $user->getId()]);

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (!$order || ($order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest)) {
            $errorMsg = $translator->trans("order_not_found", array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (!in_array($order->getStatus(), [ Order::$statuses['delivering'], Order::$statuses['verified'], Order::$statuses['new'], Order::$statuses['returned'] ])) {
            $errorMsg = $translator->trans('cant_cancel_order', array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        $order->setCancelReason($reason);
        $order->setStatus(Order::$statuses['cancelled']);
        $currentTime = new \DateTime();
        $order->setEndDate($currentTime);
        $order->setCancelDate($currentTime);
 
        if ($order->getPaymentMethod() == Order::BALANCE) {
            // $user->setBalance(($user->getBalance() + $order->getAmountDue()));
            //  $user->setUsedBalance($user->getUsedBalance() - $order->getAmountDue());
            $user->setBalance(($user->getBalance() + $order->getFirstPaymentPrice()));
            $user->setUsedBalance($user->getUsedBalance() - $order->getFirstPaymentPrice());
        }

        $em->flush();

        $orderOperations = $this->get('order_operations');


        $user = $order->getUser();

        if ($user) {
            $title = $translator->trans('Order cancelled', array(), 'notifications', $user->getLocale());
            $body = $translator->trans('Order %orderId% has been cancelled', array(
                '%orderId%' => $order->getId(),
            ), 'notifications', $user->getLocale());
            $titleAr = $translator->trans('Order cancelled', array(), 'notifications', 'ar');
            $bodyAr = $translator->trans('Order %orderId% has been cancelled', array('%orderId%' => $order->getId()), 'notifications', 'ar');
            $titleEn = $translator->trans('Order cancelled', array(), 'notifications', 'en');
            $bodyEn = $translator->trans('Order %orderId% has been cancelled', array('%orderId%' => $order->getId()), 'notifications', 'en');

            $notifData = array(
                'id' => $order->getId(),
                'oldStatus' => Order::$statuses['delivering'],
                'newStatus' => $order->getStatus(),
                'title' => $title,
                'body' => $body,
            );

            $this->get('notification_center')->sendNotificationToUser($user, NotificationCenter::USER_ORDER_CANCEL, $titleAr, $titleEn, $bodyAr, $bodyEn, $user->getLocale(), $notifData);
            $this->firebaseDbAction($order);
        }

//        $this->get('user_device_notification')->sendNotificationToUser($user, $title, $body, array(
//            'id' => $order->getId(),
//            'oldStatus' => Order::$statuses['delivering'],
//            'newStatus' => $order->getStatus(),
//            'title' => $title,
//            'body' => $body,
//                ));


        $driver = $order->getDriver();
        if ($driver) {
            $title = $translator->trans('Order cancelled', array(), 'notifications', $driver->getLocale());
            $body = $translator->trans('Order %orderId% has been cancelled by the customer', array(
                '%orderId%' => $order->getId(),
            ), 'notifications', $driver->getLocale());

            $this->get('user_device_notification')->sendNotificationToUser($driver, $title, $body, array(
                'id' => $order->getId(),
                'oldStatus' => Order::$statuses['delivering'],
                'newStatus' => $order->getStatus(),
                'title' => $title,
                'body' => $body,
            ));
        }

        //NANACALL2
        if ($order->getSource() == 'nana') {
            $syncResult = $this->nanaApiUpdateOrderStatus($orderId, Order::$statuses['cancelled']);
            if ($syncResult && isset($syncResult->status) && $syncResult->status) {
                $order->setIsNanaSynced(true);
            }
            if ($syncResult) {
                $order->setNanaSyncData($syncResult);
            } else {
                $order->setNanaSyncData(array());
            }
            $em->flush();
        }
//        if(count($order->getOrderOffers()) > 0){
//            $orderOffers = $order->getOrderOffers();
//            foreach ($orderOffers as $orderOffer) {
//                /* @var $o Offer  */
//                if($o = $orderOffer->getOffer()){
//                    Debug::dump($o->getOrderOffers());die;
//                    $o->setNumberOfUsedTimes($o->getNumberOfUsedTimes() + $o->getOrderOffers()); //INCREMENT Offer Usage
//                }
//            }
//        }

        return $apiOperations->getSuccessJsonResponse();
    }

    /**
     * Calculate receipt
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\RequestOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     */
    public function calculateReceiptAction(RequestAlias $request)
    {
        $em = $this->getDoctrine()->getManager();
        $requestData = json_decode($request->getContent());
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestOrder = new RequestOrder();
        $user = $this->getUser();
        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("please login"));
        } elseif (!$user) {
            // annonymous user
        }
        
        $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
            'paymentMethod' => '\AppBundle\APIResponse\Order\PaymentMethod',
            'promoCode' => '\AppBundle\APIResponse\PromoCode\PromoCode',
            'offers' => '\AppBundle\APIResponse\Offer\ResponseOffer',
            'buyItems' => '\AppBundle\APIResponse\Offer\ResponseBuyItem',
            'getItems' => '\AppBundle\APIResponse\Offer\ResponseGetItem',
        ));


        if (!isset($requestOrder->totalPrice)) {
            $translator = $this->get('translator');
            $validationMessages['totalPrice'] = $translator->trans('fill_mandatory_field', array(), 'validators');
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        $promocode = null;
        if ($user && $requestOrder->promoCode && $requestOrder->promoCode->code) {
            $validationResponse = $this->get('order_operations')->getValidationErrorResponseForPromoCodeOrPromoCode($requestOrder->promoCode->code, $user);
            if ($validationResponse instanceof JsonResponse) {
                return $validationResponse;
            }
            /* @var $promocode PromoCode */
            $promocode = $validationResponse;
        }


        $offerIds = array();
        $offerCountArray = array();
        if (isset($requestData->offers) && count($requestData->offers) > 0) {
            $i = 0;
            /* @var $requestOrderOffer ResponseOffer */
            foreach ($requestOrder->offers as $requestOrderOffer) {
                if (!isset($requestData->offers[$i]->getItems) || (isset($requestData->offers[$i]->getItems) && count($requestData->offers[$i]->getItems) == 0)) {
                    $requestOrderOffer->getItems = array();
                }

                $requestOrderOffer->startTime = $requestOrderOffer->startTime ? $requestOrderOffer->startTime->getTimestamp() : null;
                $requestOrderOffer->expiryTime = $requestOrderOffer->expiryTime ? $requestOrderOffer->expiryTime->getTimestamp() : null;
                $requestOrderOffer->numberOfUsedTimes = 0;

                $errorsObjects = $this->get('validator')->validate($requestOrderOffer, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }

                $i++;
                $offerIds[] = $requestOrderOffer->id;
                $offerCountArray[$requestOrderOffer->id] = array('count' => $requestOrderOffer->count);
            }
        }

        $offers = $em->createQueryBuilder()
            ->select('o')
            ->from('IbtikarTaniaModelBundle:Offer', 'o')
            ->where('o.id in (:offerIds)')
            ->setParameter('offerIds', $offerIds)
            ->getQuery()
            ->getResult();

        //$requestOrder->orderId = 87852 ;
       
        if (isset($requestOrder->orderId) && $requestOrder->orderId >'0') {
            $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(array('id'=> $requestOrder->orderId));
            if (!$order) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No order was found"));
            }
          
            $withoutTaxes = false;
            if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $user->getBalance() >= $requestOrder->totalPrice) {
                $withoutTaxes = true;
            }

            // NEW-ISPL START on 17/01/2019
            if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $order->getPaymentMethod() == 'BALANCE' && ($user->getBalance() + $order->getFirstPaymentPrice())>= $requestOrder->totalPrice) {
                $withoutTaxes = true;
            }
            // NEW-ISPL END on 17/01/2019

            // NEW-ISPL START on 31/12/2018
            if ($requestOrder->paymentMethod->type == 'CASH') {
                $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                $receipt->receipt->secondPaymentPrice = 0;
            } elseif ($requestOrder->paymentMethod->type == 'CREDIT') {
                $requestOrder->paymentMethod->type = 'CASH';
                $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                $receipt->receipt->secondPaymentPrice = 0;
            } else {
                $isPaymentChanged = $requestOrder->isPaymentChanged;
                if (!isset($isPaymentChanged) || $isPaymentChanged) {
                    if ($order->getPaymentMethod()=='BALANCE') {
                        $userBalance = $user->getBalance() + $order->getFirstPaymentPrice();
                    } else {
                        $userBalance = $user->getBalance();
                    }
                } elseif ($requestOrder->totalPrice > $order->getPaymentValue()) {
                    if ($order->getPaymentMethod()=='BALANCE') {
                        $userBalance = $user->getBalance() + $order->getFirstPaymentPrice();
                    } else {
                        $userBalance = $user->getBalance();
                    }
                } elseif ($requestOrder->totalPrice <= $order->getPaymentValue()) {
                    if ($order->getPaymentMethod()=='BALANCE') {
                        $userBalance = $order->getFirstPaymentPrice();
                    } else {
                        $userBalance = $user->getBalance();
                    }
                } else {
                    if ($order->getPaymentMethod()=='BALANCE') {
                        $userBalance = $order->getFirstPaymentPrice();
                    } else {
                        $userBalance = $user->getBalance();
                    }
                }

                if ($userBalance < $requestOrder->totalPrice) {
                    $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];

                    $requestOrder->receipt->taxFees = ((($requestOrder->totalPrice - $userBalance) * $fees->getTaxPercent()) / 100);
                    $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);

                    $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));

                    $firstPaymentPrice = $userBalance;
                    $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $userBalance);
                 
                    $receipt = $this->getReceipt(
                        $requestOrder->totalPrice,
                        $withoutTaxes,
                        $promocode,
                        $offers,
                        $offerCountArray,
                        false,
                        ($requestOrder->totalPrice-$firstPaymentPrice)
                    );
                    $receipt->receipt->firstPaymentPrice = $userBalance;
                    $receipt->receipt->secondPaymentPrice = ($receipt->receipt->finalPrice) - ($userBalance + $receipt->receipt->taxFees);
                } else {
                    $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                    $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                    $receipt->receipt->secondPaymentPrice = 0;
                }
            }
        } else {
            $withoutTaxes = false;
            if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $user->getBalance() >= $requestOrder->totalPrice) {
                $withoutTaxes = true;
            }
            // NEW-ISPL START on 31/12/2018
            if ($requestOrder->paymentMethod->type == 'CASH') {
                $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                $receipt->receipt->secondPaymentPrice = 0;
            } elseif ($requestOrder->paymentMethod->type == 'CREDIT') {
                $requestOrder->paymentMethod->type = 'CASH';
                $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                $receipt->receipt->secondPaymentPrice = 0;
            } else {
                $userBalance = $user->getBalance();

                if ($userBalance < $requestOrder->totalPrice) {
                    $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];

                    $requestOrder->receipt->taxFees = ((($requestOrder->totalPrice - $userBalance) * $fees->getTaxPercent()) / 100);
                    $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);

                    $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));

                    $firstPaymentPrice = $userBalance;
                    $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $userBalance);
                 

                    $receipt = $this->getReceipt(
                        $requestOrder->totalPrice,
                        $withoutTaxes,
                        $promocode,
                        $offers,
                        $offerCountArray,
                        false,
                        ($requestOrder->totalPrice-$firstPaymentPrice)
                    );

                    $receipt->receipt->firstPaymentPrice = $userBalance;
                    $receipt->receipt->secondPaymentPrice = ($receipt->receipt->finalPrice) - ($userBalance + $receipt->receipt->taxFees);
                } else {
                    $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                    $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                    $receipt->receipt->secondPaymentPrice = 0;
                }
            }
        }
        return $apiOperations->getJsonResponseForObject($receipt);
    }

    /**
     * Calculate receipt
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\RequestOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     */
    public function calculateReceiptByDriverAction(RequestAlias $request)
    {
        $em = $this->getDoctrine()->getManager();
        $requestData = json_decode($request->getContent());
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestOrder = new RequestOrder();
        $user = $this->getUser();
        /* if($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver){
             return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("please login"));
         }else if(!$user){
             // annonymous user
         }*/
        
        $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
            'paymentMethod' => '\AppBundle\APIResponse\Order\PaymentMethod',
            'promoCode' => '\AppBundle\APIResponse\PromoCode\PromoCode',
            'offers' => '\AppBundle\APIResponse\Offer\ResponseOffer',
            'buyItems' => '\AppBundle\APIResponse\Offer\ResponseBuyItem',
            'getItems' => '\AppBundle\APIResponse\Offer\ResponseGetItem',
        ));


        if (!isset($requestOrder->totalPrice)) {
            $translator = $this->get('translator');
            $validationMessages['totalPrice'] = $translator->trans('fill_mandatory_field', array(), 'validators');
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        $promocode = null;
        if ($user && $requestOrder->promoCode && $requestOrder->promoCode->code) {
            $validationResponse = $this->get('order_operations')->getValidationErrorResponseForPromoCodeOrPromoCode($requestOrder->promoCode->code, $user);
            if ($validationResponse instanceof JsonResponse) {
                return $validationResponse;
            }
            /* @var $promocode PromoCode */
            $promocode = $validationResponse;
        }

        $withoutTaxes = false;
        if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $user->getBalance() >= $requestOrder->totalPrice) {
            $withoutTaxes = true;
        }

        $offerIds = array();
        $offerCountArray = array();
        if (isset($requestData->offers) && count($requestData->offers) > 0) {
            $i = 0;
            /* @var $requestOrderOffer ResponseOffer */
            foreach ($requestOrder->offers as $requestOrderOffer) {
                if (!isset($requestData->offers[$i]->getItems) || (isset($requestData->offers[$i]->getItems) && count($requestData->offers[$i]->getItems) == 0)) {
                    $requestOrderOffer->getItems = array();
                }

                $requestOrderOffer->startTime = $requestOrderOffer->startTime ? $requestOrderOffer->startTime->getTimestamp() : null;
                $requestOrderOffer->expiryTime = $requestOrderOffer->expiryTime ? $requestOrderOffer->expiryTime->getTimestamp() : null;
                $requestOrderOffer->numberOfUsedTimes = 0;

                $errorsObjects = $this->get('validator')->validate($requestOrderOffer, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }

                $i++;
                $offerIds[] = $requestOrderOffer->id;
                $offerCountArray[$requestOrderOffer->id] = array('count' => $requestOrderOffer->count);
            }
        }

        $offers = $em->createQueryBuilder()
            ->select('o')
            ->from('IbtikarTaniaModelBundle:Offer', 'o')
            ->where('o.id in (:offerIds)')
            ->setParameter('offerIds', $offerIds)
            ->getQuery()
            ->getResult();

        // NEW-ISPL START on 31/12/2018
        if (isset($requestOrder->orderId) && $requestOrder->orderId >'0') {
            $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(array('id'=> $requestOrder->orderId));
            if (!$order) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No order was found"));
            }
          
            $withoutTaxes = false;
            if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $user->getBalance() >= $requestOrder->totalPrice) {
                $withoutTaxes = true;
            }

            // NEW-ISPL START on 17/01/2019
            if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $order->getPaymentMethod() == 'BALANCE' && ($user->getBalance() + $order->getFirstPaymentPrice())>= $requestOrder->totalPrice) {
                $withoutTaxes = true;
            }
            // NEW-ISPL END on 17/01/2019

            // NEW-ISPL START on 31/12/2018
            if ($requestOrder->paymentMethod->type == 'CASH') {
                $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                $receipt->receipt->secondPaymentPrice = 0;
            } elseif ($requestOrder->paymentMethod->type == 'CREDIT') {
                $requestOrder->paymentMethod->type = 'CASH';
                $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                $receipt->receipt->secondPaymentPrice = 0;
            } else {
                $isPaymentChanged = $requestOrder->isPaymentChanged;
                if (!isset($isPaymentChanged) || $isPaymentChanged) {
                    if ($order->getPaymentMethod()=='BALANCE') {
                        $userBalance = $user->getBalance() + $order->getFirstPaymentPrice();
                    } else {
                        $userBalance = $user->getBalance();
                    }
                } elseif ($requestOrder->totalPrice > $order->getPaymentValue()) {
                    if ($order->getPaymentMethod()=='BALANCE') {
                        $userBalance = $user->getBalance() + $order->getFirstPaymentPrice();
                    } else {
                        $userBalance = $user->getBalance();
                    }
                } elseif ($requestOrder->totalPrice <= $order->getPaymentValue()) {
                    if ($order->getPaymentMethod()=='BALANCE') {
                        $userBalance = $order->getFirstPaymentPrice();
                    } else {
                        $userBalance = $user->getBalance();
                    }
                } else {
                    if ($order->getPaymentMethod()=='BALANCE') {
                        $userBalance = $order->getFirstPaymentPrice();
                    } else {
                        $userBalance = $user->getBalance();
                    }
                }

                if ($userBalance < $requestOrder->totalPrice) {
                    $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];

                    $requestOrder->receipt->taxFees = ((($requestOrder->totalPrice - $userBalance) * $fees->getTaxPercent()) / 100);
                    $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);

                    $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));

                    $firstPaymentPrice = $userBalance;
                    $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $userBalance);
                 
                    $receipt = $this->getReceipt(
                        $requestOrder->totalPrice,
                        $withoutTaxes,
                        $promocode,
                        $offers,
                        $offerCountArray,
                        false,
                        ($requestOrder->totalPrice-$firstPaymentPrice)
                    );
                    $receipt->receipt->firstPaymentPrice = $userBalance;
                    $receipt->receipt->secondPaymentPrice = ($receipt->receipt->finalPrice) - ($userBalance + $receipt->receipt->taxFees);
                } else {
                    $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                    $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                    $receipt->receipt->secondPaymentPrice = 0;
                }
            }
        } else {
            $withoutTaxes = false;
            if (is_object($requestOrder->paymentMethod) && $requestOrder->paymentMethod->type && $requestOrder->paymentMethod->type === 'BALANCE' && $user->getBalance() >= $requestOrder->totalPrice) {
                $withoutTaxes = true;
            }
            // NEW-ISPL START on 31/12/2018
            if ($requestOrder->paymentMethod->type == 'CASH') {
                $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                $receipt->receipt->secondPaymentPrice = 0;
            } elseif ($requestOrder->paymentMethod->type == 'CREDIT') {
                $requestOrder->paymentMethod->type = 'CASH';
                $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                $receipt->receipt->secondPaymentPrice = 0;
            } else {
                $userBalance = $user->getBalance();

                if ($userBalance < $requestOrder->totalPrice) {
                    $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];

                    $requestOrder->receipt->taxFees = ((($requestOrder->totalPrice - $userBalance) * $fees->getTaxPercent()) / 100);
                    $requestOrder->receipt->totalPaidPrice = ($requestOrder->totalPrice - $requestOrder->receipt->offersDiscount);

                    $requestOrder->receipt->finalPrice = (($requestOrder->receipt->totalPaidPrice + $requestOrder->receipt->taxFees) - ($requestOrder->receipt->discount));

                    $firstPaymentPrice = $userBalance;
                    $secondPaymentPrice = ($requestOrder->receipt->finalPrice - $userBalance);
                 

                    $receipt = $this->getReceipt(
                        $requestOrder->totalPrice,
                        $withoutTaxes,
                        $promocode,
                        $offers,
                        $offerCountArray,
                        false,
                        ($requestOrder->totalPrice-$firstPaymentPrice)
                    );

                    $receipt->receipt->firstPaymentPrice = $userBalance;
                    $receipt->receipt->secondPaymentPrice = ($receipt->receipt->finalPrice) - ($userBalance + $receipt->receipt->taxFees);
                } else {
                    $receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);
                    $receipt->receipt->firstPaymentPrice = ($receipt->receipt->finalPrice - $receipt->receipt->taxFees);
                    $receipt->receipt->secondPaymentPrice = 0;
                }
            }
        }
        // NEW-ISPL END on 31/12/2018
        //$receipt = $this->getReceipt($requestOrder->totalPrice, $withoutTaxes, $promocode, $offers, $offerCountArray);

        return $apiOperations->getJsonResponseForObject($receipt);
    }

    /**
     * Get Order List (3rd Part Calling)
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\NanaRequestOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     */
    public function nanaAddAction(RequestAlias $request)
    {
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        try {
            $em = $this->getDoctrine()->getManager();
//            $requestOrder = new NanaRequestOrder();

            //        $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
            //            'items' => '\AppBundle\APIResponse\Order\NanaRequestOrderItem',
            //            'customer' => 'AppBundle\APIResponse\Order\Customer',
            ////            'address' => '\AppBundle\APIResponse\User\CustomerAddress',
            ////            'gps' => '\AppBundle\APIResponse\Address\GPS',
            //        ));
            $requestOrder = json_decode($request->getContent());

            //        if($requestOrder->receivingDate) {
            //            $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
            //        }
            //        if($requestOrder->receivingDate && $requestOrder->receivingDate < (int)$this->getDateInTimeStamp(new \DateTime()))
            //            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Invalid Data"));
            $itemIds = array();
            $totalItemsCount = 0;
            $countArray = array();
            foreach ($requestOrder->items as $requestOrderItem) {
                $errorsObjects = $this->get('validator')->validate($requestOrderItem, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }
                $itemIds[] = $requestOrderItem->sku;
                $totalItemsCount += (integer)$requestOrderItem->quantity;
                $countArray[$requestOrderItem->sku] = array('price' => $requestOrderItem->price, 'count' => $requestOrderItem->quantity);
            }
//            if(!array_key_exists("ignore_validations", $requestOrder)) {
//                $minimumNumberOfItemsPerOrder = (integer)$this->get('settings_manager')->get('minimum_number_of_items_per_order');
//                if ($minimumNumberOfItemsPerOrder && $minimumNumberOfItemsPerOrder > $totalItemsCount) {
//                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You must order a minimum of %items-count% items in any order', array('%items-count%' => $minimumNumberOfItemsPerOrder), 'api_error_message'));
//                }
//            }
            $address = null;

            $cityArea = null;
            $sendMail = false;
            if ($requestOrder->customer->address->gps->latitude && $requestOrder->customer->address->gps->longitude) {
                $address = null;
                $cityArea = $this->getCityAreaPerLatLong($requestOrder->customer->address->gps->latitude, $requestOrder->customer->address->gps->longitude);
//                if (!array_key_exists("ignore_validations", $requestOrder) && !$cityArea) {
//                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
//                }
                if (!$cityArea) {
                    $sendMail = true;
                }
            }
//            else {
//                $address = null;
//                $cityArea = $this->getCityAreaPerLatLong($this->getUser()->getLatitude(), $this->getUser()->getLongitude());
//                if (!$cityArea) {
//                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
//                }
//            }
//            $errorsObjects = $this->get('validator')->validate($requestOrder->receipt);
//            if (count($errorsObjects) > 0) {
//                return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
//            }
            $city = $cityArea ? $cityArea->getCity() : null;
            $em = $this->getDoctrine()->getManager();
            $items = $em->createQueryBuilder()
                ->select('i')
                ->from('IbtikarTaniaModelBundle:Item', 'i')
                ->where('i.id in (:itemId)')
                ->andWhere('i.shown = :shown')
                ->setParameter('itemId', $itemIds)
                ->setParameter('shown', true)
                ->getQuery()
                ->getResult();
            if (count($items) <= 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
            }
            $itemNameGetter = 'getName';
            if ($request->getLocale() == 'en') {
                $itemNameGetter = 'getNameEn';
            }
//            if(!array_key_exists("ignore_validations", $requestOrder)) {
//                /* @var $item \Ibtikar\TaniaModelBundle\Entity\Item */
//                foreach ($items as $item) {
//                    $minimumAmountToOrder = $item->getMinimumAmountToOrder();
//                    if ($minimumAmountToOrder && $minimumAmountToOrder > $countArray[$item->getId()]['count']) {
//                        return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You can only order a minimum of %item-minimum-count% items of %item-name%', array('%item-minimum-count%' => $minimumAmountToOrder, '%item-name%' => $item->$itemNameGetter()), 'api_error_message'));
//                    }
//                }
//            }
            $responseItemList = $this->getItemObjectPerCity($request, $items, $city);
            $totalPrice = 0;
            foreach ($responseItemList->items as $item) {
                $totalPrice += ($item->price * $countArray[$item->id]['count']);
            }


            $withoutTaxes = false;

//            $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes);
//            if ($totalPrice != $receipt->receipt->paidTotalPrice || $receipt->receipt->finalPrice != $requestOrder->receipt->finalPrice) //if cost of any item changed or fees changed
//            {
//                if ($totalPrice != $receipt->receipt->paidTotalPrice)
//                    $receipt = $this->getReceipt($totalPrice, $withoutTaxes);
//
//                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Costs Changed', array(), 'validators'), $receipt);
//            }

            $receipt = $this->getReceipt($totalPrice, $withoutTaxes);

            $shift = null;
//            if ($requestOrder->receivingDate) {
//                if (!$requestOrder->shiftId) {
//                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('No shift was found'));
//                }
//                $shift = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findOneBy(array('id' => $requestOrder->shiftId));
//                if (!$shift) {
//                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
//                }
//            } else {
            $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findAll();
            if (count($shifts) === 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
            $currentTime = new \DateTime();
            foreach ($shifts as $dayShift) {
                $shiftTimeToday = clone $currentTime;
                $shiftTimeToday->modify($dayShift->getTo()->format('H:i'));
                if ($shiftTimeToday > $currentTime) {
                    $shift = $dayShift;
                    $requestOrder->receivingDate = $currentTime->getTimestamp();
                    break;
                }
            }
            if (!$shift) {
                // No shift available today deliver tomorrow
                $requestOrder->receivingDate = new \DateTime('tomorrow');
                $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
                $shift = $shifts[0];
            }
//            }

            $receivingDate = 0;
            if (array_key_exists("receiving_date", $requestOrder) && $requestOrder->receiving_date) {
                $receivingDate = round($requestOrder->receiving_date);

                $firstTimeToday = new \DateTime('midnight');
                $lastTimeToday = new \DateTime('tomorrow');
                if ($receivingDate >= $firstTimeToday->getTimestamp() && $receivingDate < $lastTimeToday->getTimestamp()) {
                    $firstTimeToday->modify($shift->getTo()->format('H:i'));
                    if (time() > $firstTimeToday->getTimestamp()) {
                        return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('shif_expired', array(), 'api_error_message'));
                    }
                }
            }

            /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\OrderRepository */
//            $orderRepo = $em->getRepository('IbtikarTaniaModelBundle:Order');
//            $totalPrice = 0;
            $order = new Order();
            $order->setPaymentMethod(Order::CASH);
            $order->setCreditCard(null);

            $order->setCustomerUsername($requestOrder->customer->name);
            $order->setCustomerPhone($requestOrder->customer->phone);
            $order->setSource($this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom'));
            $order->setShift($shift);
            $order->setShiftFrom($shift->getFrom());
            $order->setShiftTo($shift->getTo());
            if ($city) {
                $order->setCity($city);
            }

            if ($cityArea) {
                $order->setCityArea($cityArea);
                $order->setCityAreaNameEn($cityArea ? $cityArea->getNameEn() : null);
                $order->setCityAreaNameAr($cityArea ? $cityArea->getNameAr() : null);
            }
            $order->setLatitude($requestOrder->customer->address->gps->latitude);
            $order->setLongitude($requestOrder->customer->address->gps->longitude);

            if (array_key_exists("receiving_date", $requestOrder) && $requestOrder->receiving_date) {
                $order->setReceivingDate($receivingDate);
            } else {
                $order->setReceivingDate($this->getDateInTimeStamp(new \DateTime()));
            }
            $order->setAmountDue($receipt->receipt->finalPrice);
            $order->setPrice($totalPrice);
            $order->setTaxFees($receipt->receipt->taxFees);

            if (array_key_exists("notes", $requestOrder) && $requestOrder->notes) {
                $order->setNote($requestOrder->notes);
            }

            $em->persist($order);
            foreach ($items as $item) {
                $orderItemPrice = (!$countArray[$item->getId()]['price'] || $countArray[$item->getId()]['price'] == 0) ? $item->getDefaultPrice() : $countArray[$item->getId()]['price'];
                $orderItem = new OrderItem();
                $orderItem->setCity($city);
                $orderItem->setCount($countArray[$item->getId()]['count']);
                $orderItem->setItem($item);
                $orderItem->setPrice($orderItemPrice);
                $order->addOrderItem($orderItem);
                $orderItem->setOrder($order);
                $em->persist($orderItem);
            }
            $em->flush();

            $response = $this->getUserOrderObject($order);
            $response->sendMail = $sendMail;

            return $apiOperations->getSuccessDataJsonResponse($response);
        } catch (Exception $e) {
            return $apiOperations->getErrorJsonResponse($e->getMessage());
        }
    }

    /**
     * edit nana Order
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\RequestEditOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     */
    public function nanaEditAction(RequestAlias $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestOrder = json_decode($request->getContent());
        $translator = $this->get('translator');

        if (isset($requestOrder->receiving_date)) {
            $requestOrder->receivingDate = round($requestOrder->receiving_date);
        }
//        if($requestOrder->receivingDate && $requestOrder->receivingDate < (int)$this->getDateInTimeStamp(new \DateTime()))
//            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("receiving date should be larger than the current time"));

        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(array('id'=> $requestOrder->id));
        if (!$order) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No order was found"));
        }

        if ($this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom') != $order->getSource()) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Order Not Found"));
        }


        if (!in_array($order->getStatus(), Order::$statusCategories['current'])) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Order is not editable anymore"));
        }
        $itemIds = array();
        $totalItemsCount = 0;
        $countArray = array();
        foreach ($requestOrder->items as $requestOrderItem) {
//            $errorsObjects = $this->get('validator')->validate($requestOrderItem, null, array('Default'));
//            if (count($errorsObjects) > 0) {
//                return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
//            }
            $itemIds[] = $requestOrderItem->sku;
            $totalItemsCount += (integer) $requestOrderItem->quantity;
            $countArray[$requestOrderItem->sku] = array('price' => $requestOrderItem->price, 'count' => $requestOrderItem->quantity);
        }
        $minimumNumberOfItemsPerOrder = (integer) $this->get('settings_manager')->get('minimum_number_of_items_per_order');
        if ($minimumNumberOfItemsPerOrder && $minimumNumberOfItemsPerOrder > $totalItemsCount) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You must order a minimum of %items-count% items in any order', array('%items-count%' => $minimumNumberOfItemsPerOrder), 'api_error_message'));
        }

        $city = $order->getCity();
        $items = $em->createQueryBuilder()
            ->select('i')
            ->from('IbtikarTaniaModelBundle:Item', 'i')
            ->where('i.id in (:itemId)')
            ->andWhere('i.shown = :shown')
            ->setParameter('itemId', $itemIds)
            ->setParameter('shown', true)
            ->getQuery()
            ->getResult();
        if (count($items)<=0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $itemNameGetter = 'getName';
        if ($request->getLocale() == 'en') {
            $itemNameGetter = 'getNameEn';
        }
        /* @var $item \Ibtikar\TaniaModelBundle\Entity\Item */
        foreach ($items as $item) {
            $minimumAmountToOrder = $item->getMinimumAmountToOrder();
            if ($minimumAmountToOrder && $minimumAmountToOrder > $countArray[$item->getId()]['count']) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You can only order a minimum of %item-minimum-count% items of %item-name%', array('%item-minimum-count%' => $minimumAmountToOrder, '%item-name%' => $item->$itemNameGetter()), 'api_error_message'));
            }
        }
        $responseItemList = $this->getItemObjectPerCity($request, $items, $city);
        $totalPrice = 0;
        foreach ($responseItemList->items as $item) {
            $totalPrice += ($item->price * $countArray[$item->id]['count']);
        }

        $withoutTaxes = false;

        $receipt = $this->getReceipt($totalPrice, $withoutTaxes);

        $shift = null;

        $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findAll();
        if (count($shifts) === 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
        }
        $currentTime = new \DateTime();
        foreach ($shifts as $dayShift) {
            $shiftTimeToday = clone $currentTime;
            $shiftTimeToday->modify($dayShift->getTo()->format('H:i'));
            if ($shiftTimeToday > $currentTime) {
                $shift = $dayShift;
                $requestOrder->receivingDate = $currentTime->getTimestamp();
                break;
            }
        }

        if (!$shift) {
            // No shift available today deliver tomorrow
            $requestOrder->receivingDate = new \DateTime('tomorrow');
            $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
            $shift = $shifts[0];
        }

        $firstTimeToday = new \DateTime('midnight');
        $lastTimeToday = new \DateTime('tomorrow');
        if ($requestOrder->receivingDate >= $firstTimeToday->getTimestamp() && $requestOrder->receivingDate < $lastTimeToday->getTimestamp()) {
            $firstTimeToday->modify($shift->getTo()->format('H:i'));
            if (time() > $firstTimeToday->getTimestamp()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('shif_expired', array(), 'api_error_message'));
            }
        }

        $totalPrice = 0;
        $oldPromoCode = $order->getPromoCode();

        foreach ($order->getOrderItems() as $orderItems) {
            $em->remove($orderItems);
        }

        if ($requestOrder->receivingDate) {
            $order->setReceivingDate($requestOrder->receivingDate);
        } else {
            $order->setReceivingDate($this->getDateInTimeStamp(new \DateTime()));
        }

        $order->setShift($shift);
        $order->setShiftFrom($shift->getFrom());
        $order->setShiftTo($shift->getTo());

        $order->setAmountDue($receipt->receipt->finalPrice);
        $order->setPrice($receipt->receipt->totalPrice);
        $order->setTaxFees($receipt->receipt->taxFees);
        $order->setCustomerUsername($requestOrder->customer->name);
        $order->setCustomerPhone($requestOrder->customer->phone);

        if (array_key_exists("notes", $requestOrder) && $requestOrder->notes) {
            $order->setNote($requestOrder->notes);
        }

        $em->persist($order);
        foreach ($items as $item) {
            $orderItem = new OrderItem();
            $orderItem->setCity($order->getCity());
            $orderItem->setCount($countArray[$item->getId()]['count']);
            $orderItem->setItem($item);
            $orderItem->setPrice($countArray[$item->getId()]['price']);
            $order->addOrderItem($orderItem);
            $orderItem->setOrder($order);
            $em->persist($orderItem);
        }
        $em->flush();

        $driver = $order->getDriver();
        if ($driver) {
            $title = $translator->trans('Order Edited', array('%orderId%' => $order->getId()), 'notifications', $driver->getLocale());
            $body = $translator->trans('Order Edited', array(
                '%orderId%' => $order->getId(),
            ), 'notifications', $driver->getLocale());

            $this->get('user_device_notification')->sendNotificationToUser($driver, $title, $body, array(
                'id' => $order->getId(),
                'title' => $title,
                'body' => $body,
                'type' => NotificationCenter::USER_ORDER_EDITED
            ));
        }

        return $apiOperations->getSuccessDataJsonResponse($this->getUserOrderObject($order));
    }

    /**
     * nana cancel order
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="order", "dataType"="integer", "required"=true},
     *      {"name"="message", "dataType"="string", "required"=true},
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function nanaCancelAction(RequestAlias $request)
    {
        $requestData = json_decode($request->getContent());

        $orderId = array_key_exists('order', $requestData) ? trim($requestData->order) : null;
        $reason = array_key_exists('message', $requestData) ? trim($requestData->message) : '';

        $translator = $this->get('translator');
        $apiOperations = $this->get('api_operations');

        $em = $this->getDoctrine()->getManager();

        $validationMessages = array();

        if (!$orderId) {
            $validationMessages['order'] = $translator->trans('fill_mandatory_field', array(), 'validators');
        }

        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $orderId]);

        if (!$order || ($order instanceof \Ibtikar\TaniaModelBundle\Entity\BalanceRequest)) {
            $errorMsg = $translator->trans("order_not_found", array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        if ($this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom') != $order->getSource()) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Order Not Found"));
        }

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (count($validationMessages)) {
            $output = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (!in_array($order->getStatus(), [ Order::$statuses['delivering'], Order::$statuses['verified'], Order::$statuses['new'], Order::$statuses['returned'] ])) {
            $errorMsg = $translator->trans('cant_cancel_order', array(), 'order');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        $order->setCancelReason($reason);
        $order->setStatus(Order::$statuses['cancelled']);
        $currentTime = new \DateTime();
        $order->setEndDate($currentTime);
        $order->setCancelDate($currentTime);

        $em->flush();

        $driver = $order->getDriver();
        if ($driver) {
            $title = $translator->trans('Order cancelled', array(), 'notifications', $driver->getLocale());
            $body = $translator->trans('Order %orderId% has been cancelled by the customer', array(
                '%orderId%' => $order->getId(),
            ), 'notifications', $driver->getLocale());

            $this->get('user_device_notification')->sendNotificationToUser($driver, $title, $body, array(
                'id' => $order->getId(),
                'oldStatus' => Order::$statuses['delivering'],
                'newStatus' => $order->getStatus(),
                'title' => $title,
                'body' => $body,
            ));
        }


        return $apiOperations->getSuccessJsonResponse();
    }

    /**
     * get maximum number of items per order
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Order",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function limitsOfItemsPerOrderAction(RequestAlias $request)
    {
        $translator = $this->get('translator');
        $apiOperations = $this->get('api_operations');
        $em = $this->getDoctrine()->getManager();
        $RAW_QUERY = 'SELECT value,name FROM dmishh_settings where dmishh_settings.name = :name or dmishh_settings.name = :name1 LIMIT 2;';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters
        $statement->bindValue('name', 'maximum_number_of_items_per_order');
        $statement->bindValue('name1', 'minimum_number_of_items_per_order');
        $statement->execute();
        
        $result = $statement->fetchAll();
        
        $returnValues=array();
        $returnValues["message"]="Success.";
        for ($i=0;$i<count($result);$i++) {
            foreach ($result[$i] as $key=>$value) {
                if ($result[$i][$key]=="maximum_number_of_items_per_order") {
//                     $returnValues["maxNum"]= preg_replace('/[:;a-zA-Z]/', '', $result[$i]["value"]);
                } else {
                    $returnValues["minNum"]= preg_replace('/[:;a-zA-Z]/', '', $result[$i]["value"]);
                }
            }
        }
        
        
        return $apiOperations->getJsonResponseForObject((Object)$returnValues);
    }

    /**
     * Submit Order API For External use
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\RequestOrderExternal",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     */
    public function externalAddAction(RequestAlias $request)
    {
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        try {
            $requestOrder = new RequestOrderExternal();

            $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
                'items' => '\AppBundle\APIResponse\Order\RequestOrderItem',
                'user' => '\AppBundle\APIResponse\User\User'
            ));

            if ($requestOrder->receivingDate) {
                $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
            }
            if ($requestOrder->receivingDate && $requestOrder->receivingDate < (int)$this->getDateInTimeStamp(new \DateTime())) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Invalid Data"));
            }

            $itemIds = array();
            $totalItemsCount = 0;
            $countArray = array();

            /* @var $requestOrderItem RequestOrderItem */
            foreach ($requestOrder->items as $requestOrderItem) {
                $errorsObjects = $this->get('validator')->validate($requestOrderItem, null, array('Default'));
                if (count($errorsObjects) > 0) {
                    return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
                }
                $itemIds[] = $requestOrderItem->id;
                $totalItemsCount += (integer)$requestOrderItem->count;
                $countArray[$requestOrderItem->id] = array('price' => $requestOrderItem->price, 'count' => $requestOrderItem->count);
            }
//            if(!array_key_exists("ignore_validations", $requestOrder)) {
//                $minimumNumberOfItemsPerOrder = (integer)$this->get('settings_manager')->get('minimum_number_of_items_per_order');
//                if ($minimumNumberOfItemsPerOrder && $minimumNumberOfItemsPerOrder > $totalItemsCount) {
//                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You must order a minimum of %items-count% items in any order', array('%items-count%' => $minimumNumberOfItemsPerOrder), 'api_error_message'));
//                }
//            }
            $address = null;

            $cityArea = null;
//            $sendMail = false;
            if ($requestOrder->latitude && $requestOrder->longitude) {
                $address = null;
                $cityArea = $this->getCityAreaPerLatLong($requestOrder->latitude, $requestOrder->longitude);
//                if (!array_key_exists("ignore_validations", $requestOrder) && !$cityArea) {
//                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
//                }
//                if (!$cityArea) {
//                    $sendMail = true;
//                }
            }
//            else {
//                $address = null;
//                $cityArea = $this->getCityAreaPerLatLong($this->getUser()->getLatitude(), $this->getUser()->getLongitude());
//                if (!$cityArea) {
//                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
//                }
//            }
//            $errorsObjects = $this->get('validator')->validate($requestOrder->receipt);
//            if (count($errorsObjects) > 0) {
//                return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
//            }
            $city = $cityArea ? $cityArea->getCity() : null;
            $em = $this->getDoctrine()->getManager();
            $items = $em->createQueryBuilder()
                ->select('i')
                ->from('IbtikarTaniaModelBundle:Item', 'i')
                ->where('i.id in (:itemId)')
                ->andWhere('i.shown = :shown')
                ->setParameter('itemId', $itemIds)
                ->setParameter('shown', true)
                ->getQuery()
                ->getResult();
            if (count($items) <= 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
            }
            $itemNameGetter = 'getName';
            if ($request->getLocale() == 'en') {
                $itemNameGetter = 'getNameEn';
            }
//            if(!array_key_exists("ignore_validations", $requestOrder)) {
//                /* @var $item \Ibtikar\TaniaModelBundle\Entity\Item */
//                foreach ($items as $item) {
//                    $minimumAmountToOrder = $item->getMinimumAmountToOrder();
//                    if ($minimumAmountToOrder && $minimumAmountToOrder > $countArray[$item->getId()]['count']) {
//                        return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You can only order a minimum of %item-minimum-count% items of %item-name%', array('%item-minimum-count%' => $minimumAmountToOrder, '%item-name%' => $item->$itemNameGetter()), 'api_error_message'));
//                    }
//                }
//            }
            $responseItemList = $this->getItemObjectPerCity($request, $items, $city);
            $totalPrice = 0;
            foreach ($responseItemList->items as $item) {
                $totalPrice += ($item->price * $countArray[$item->id]['count']);
            }

            $withoutTaxes = false;

//            $receipt = $this->getReceipt($requestOrder->receipt->totalPrice, $withoutTaxes);
//            if ($totalPrice != $receipt->receipt->paidTotalPrice || $receipt->receipt->finalPrice != $requestOrder->receipt->finalPrice) //if cost of any item changed or fees changed
//            {
//                if ($totalPrice != $receipt->receipt->paidTotalPrice)
//                    $receipt = $this->getReceipt($totalPrice, $withoutTaxes);
//
//                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Costs Changed', array(), 'validators'), $receipt);
//            }

            $receipt = $this->getReceipt($totalPrice, $withoutTaxes);

            $shift = null;
//            if ($requestOrder->receivingDate) {
//                if (!$requestOrder->shiftId) {
//                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('No shift was found'));
//                }
//                $shift = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findOneBy(array('id' => $requestOrder->shiftId));
//                if (!$shift) {
//                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
//                }
//            } else {
            $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findAll();
            if (count($shifts) === 0) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
            }
            $currentTime = new \DateTime();
            foreach ($shifts as $dayShift) {
                $shiftTimeToday = clone $currentTime;
                $shiftTimeToday->modify($dayShift->getTo()->format('H:i'));
                if ($shiftTimeToday > $currentTime) {
                    $shift = $dayShift;
                    $requestOrder->receivingDate = $currentTime->getTimestamp();
                    break;
                }
            }
            if (!$shift) {
                // No shift available today deliver tomorrow
                $requestOrder->receivingDate = new \DateTime('tomorrow');
                $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
                $shift = $shifts[0];
            }
//            }

            $receivingDate = 0;
            if ($requestOrder->receivingDate) {
                $receivingDate = $requestOrder->receivingDate;

                $firstTimeToday = new \DateTime('midnight');
                $lastTimeToday = new \DateTime('tomorrow');
                if ($receivingDate >= $firstTimeToday->getTimestamp() && $receivingDate < $lastTimeToday->getTimestamp()) {
                    $firstTimeToday->modify($shift->getTo()->format('H:i'));
                    if (time() > $firstTimeToday->getTimestamp()) {
                        return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('shif_expired', array(), 'api_error_message'));
                    }
                }
            }

            /* @var $promoCodeRepo \Ibtikar\TaniaModelBundle\Repository\OrderRepository */
//            $orderRepo = $em->getRepository('IbtikarTaniaModelBundle:Order');
//            $totalPrice = 0;
            $order = new Order();
            $order->setPaymentMethod(Order::CASH);
            $order->setCreditCard(null);

            $order->setCustomerUsername($requestOrder->user->fullName);
            $order->setCustomerPhone($requestOrder->user->phone);
            $order->setSource($this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom'));
            $order->setShift($shift);
            $order->setShiftFrom($shift->getFrom());
            $order->setShiftTo($shift->getTo());
            if ($city) {
                $order->setCity($city);
            }

            if ($cityArea) {
                $order->setCityArea($cityArea);
                $order->setCityAreaNameEn($cityArea ? $cityArea->getNameEn() : null);
                $order->setCityAreaNameAr($cityArea ? $cityArea->getNameAr() : null);
            }
            $order->setLatitude($requestOrder->latitude);
            $order->setLongitude($requestOrder->longitude);

            if ($requestOrder->receivingDate) {
                $order->setReceivingDate($receivingDate);
            } else {
                $order->setReceivingDate($this->getDateInTimeStamp(new \DateTime()));
            }
            $order->setAmountDue($receipt->receipt->finalPrice);
            $order->setPrice($totalPrice);
            $order->setTaxFees($receipt->receipt->taxFees);
            $order->setNote($requestOrder->note);
            $order->setAppVersion($request->headers->get('app-version'));
            $order->setDeviceInformation($request->headers->get('device-information'));
            $em->persist($order);
            foreach ($items as $item) {
                $orderItem = new OrderItem();
                $orderItem->setCity($city);
                $orderItem->setCount($countArray[$item->getId()]['count']);
                $orderItem->setItem($item);
                $orderItem->setPrice($countArray[$item->getId()]['price']);
                $order->addOrderItem($orderItem);
                $orderItem->setOrder($order);
                $em->persist($orderItem);
            }
            $em->flush();

            $response = $this->getUserOrderObject($order);
//            $response->sendMail = $sendMail;

            return $apiOperations->getSuccessDataJsonResponse($response);
        } catch (Exception $e) {
            return $apiOperations->getErrorJsonResponse($e->getMessage());
        }
    }


    /**
     * edit nana Order
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\RequestEditOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function externalEditAction(RequestAlias $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $requestOrder = new RequestOrderExternal();

        $apiOperations->bindObjectDataFromJsonRequest($requestOrder, $request, array(
            'items' => '\AppBundle\APIResponse\Order\RequestOrderItem',
            'user' => '\AppBundle\APIResponse\User\User'
        ));

        $translator = $this->get('translator');

        if ($requestOrder->receivingDate) {
            $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
        }
        if ($requestOrder->receivingDate && $requestOrder->receivingDate < (int)$this->getDateInTimeStamp(new \DateTime())) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("receiving date should be larger than the current time"));
        }

        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(array('id'=> $requestOrder->id));
        if (!$order) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No order was found"));
        }

        if ($this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom') != $order->getSource()) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Order Not Found"));
        }

        if (!in_array($order->getStatus(), Order::$statusCategories['current'])) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Order is not editable anymore"));
        }
        $itemIds = array();
        $totalItemsCount = 0;
        $countArray = array();
        /* @var RequestOrderItem $requestOrderItem */
        foreach ($requestOrder->items as $requestOrderItem) {
            $errorsObjects = $this->get('validator')->validate($requestOrderItem, null, array('Default'));
            if (count($errorsObjects) > 0) {
                return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
            }
            $itemIds[] = $requestOrderItem->id;
            $totalItemsCount += (integer) $requestOrderItem->count;
            $countArray[$requestOrderItem->id] = array('price' => $requestOrderItem->price, 'count' => $requestOrderItem->count);
        }
        $minimumNumberOfItemsPerOrder = (integer) $this->get('settings_manager')->get('minimum_number_of_items_per_order');
        if ($minimumNumberOfItemsPerOrder && $minimumNumberOfItemsPerOrder > $totalItemsCount) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You must order a minimum of %items-count% items in any order', array('%items-count%' => $minimumNumberOfItemsPerOrder), 'api_error_message'));
        }

        $city = $order->getCity();
        $items = $em->createQueryBuilder()
            ->select('i')
            ->from('IbtikarTaniaModelBundle:Item', 'i')
            ->where('i.id in (:itemId)')
            ->andWhere('i.shown = :shown')
            ->setParameter('itemId', $itemIds)
            ->setParameter('shown', true)
            ->getQuery()
            ->getResult();
        if (count($items)<=0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No items found"));
        }
        $itemNameGetter = 'getName';
        if ($request->getLocale() == 'en') {
            $itemNameGetter = 'getNameEn';
        }
        /* @var $item \Ibtikar\TaniaModelBundle\Entity\Item */
        foreach ($items as $item) {
            $minimumAmountToOrder = $item->getMinimumAmountToOrder();
            if ($minimumAmountToOrder && $minimumAmountToOrder > $countArray[$item->getId()]['count']) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('You can only order a minimum of %item-minimum-count% items of %item-name%', array('%item-minimum-count%' => $minimumAmountToOrder, '%item-name%' => $item->$itemNameGetter()), 'api_error_message'));
            }
        }
        $responseItemList = $this->getItemObjectPerCity($request, $items, $city);
        $totalPrice = 0;
        foreach ($responseItemList->items as $item) {
            $totalPrice += ($item->price * $countArray[$item->id]['count']);
        }

        $withoutTaxes = false;

        $receipt = $this->getReceipt($totalPrice, $withoutTaxes);

        $shift = null;

        $shifts = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findAll();
        if (count($shifts) === 0) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("No shift was found"));
        }
        $currentTime = new \DateTime();
        foreach ($shifts as $dayShift) {
            $shiftTimeToday = clone $currentTime;
            $shiftTimeToday->modify($dayShift->getTo()->format('H:i'));
            if ($shiftTimeToday > $currentTime) {
                $shift = $dayShift;
                $requestOrder->receivingDate = $currentTime->getTimestamp();
                break;
            }
        }

        if (!$shift) {
            // No shift available today deliver tomorrow
            $requestOrder->receivingDate = new \DateTime('tomorrow');
            $requestOrder->receivingDate = $requestOrder->receivingDate->getTimestamp();
            $shift = $shifts[0];
        }

        $firstTimeToday = new \DateTime('midnight');
        $lastTimeToday = new \DateTime('tomorrow');
        if ($requestOrder->receivingDate >= $firstTimeToday->getTimestamp() && $requestOrder->receivingDate < $lastTimeToday->getTimestamp()) {
            $firstTimeToday->modify($shift->getTo()->format('H:i'));
            if (time() > $firstTimeToday->getTimestamp()) {
                return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('shif_expired', array(), 'api_error_message'));
            }
        }

        $totalPrice = 0;
        $oldPromoCode = $order->getPromoCode();

        foreach ($order->getOrderItems() as $orderItems) {
            $em->remove($orderItems);
        }

        if ($requestOrder->receivingDate) {
            $order->setReceivingDate($requestOrder->receivingDate);
        } else {
            $order->setReceivingDate($this->getDateInTimeStamp(new \DateTime()));
        }

        $order->setShift($shift);
        $order->setShiftFrom($shift->getFrom());
        $order->setShiftTo($shift->getTo());

        $order->setAmountDue($receipt->receipt->finalPrice);
        $order->setPrice($receipt->receipt->totalPrice);
        $order->setTaxFees($receipt->receipt->taxFees);
        $order->setCustomerUsername($requestOrder->user->fullName);
        $order->setCustomerPhone($requestOrder->user->phone);
        $order->setNote($requestOrder->note);
        $order->setAppVersion($request->headers->get('app-version'));
        $order->setDeviceInformation($request->headers->get('device-information'));

        $em->persist($order);
        foreach ($items as $item) {
            $orderItem = new OrderItem();
            $orderItem->setCity($order->getCity());
            $orderItem->setCount($countArray[$item->getId()]['count']);
            $orderItem->setItem($item);
            $orderItem->setPrice($countArray[$item->getId()]['price']);
            $order->addOrderItem($orderItem);
            $orderItem->setOrder($order);
            $em->persist($orderItem);
        }
        $em->flush();

        $driver = $order->getDriver();
        if ($driver) {
            $title = $translator->trans('Order Edited', array('%orderId%' => $order->getId()), 'notifications', $driver->getLocale());
            $body = $translator->trans('Order Edited', array(
                '%orderId%' => $order->getId(),
            ), 'notifications', $driver->getLocale());

            $this->get('user_device_notification')->sendNotificationToUser($driver, $title, $body, array(
                'id' => $order->getId(),
                'title' => $title,
                'body' => $body,
                'type' => NotificationCenter::USER_ORDER_EDITED
            ));
        }

        return $apiOperations->getSuccessDataJsonResponse($this->getUserOrderObject($order));
    }


    /**
     * get Order Details by id
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     */
    public function getByIdAction(RequestAlias $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $translator = $this->get('translator');

        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(array('id'=> $id));
        if (!$order) {
            return $apiOperations->getErrorJsonResponse($translator->trans("No order was found"));
        }

        if ($this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom') != $order->getSource()) {
            return $apiOperations->getErrorJsonResponse($translator->trans("No order was found"));
        }

        return $apiOperations->getSuccessDataJsonResponse($this->getUserOrderObject($order));
    }

    public function firebaseDbAction($order)
    {
        $fbrd = new FireBaseRetrievingData($this->getParameter("firebase_credentials_path"));
        $db = $fbrd->getDatabaseObject();
        $orderId = $order->getId();
        $newStatus = $order->getStatus();
        $ref = $db->getReference("Orders/".$orderId);
        $result = $ref->update(['status' => $newStatus]);
        return $result;
    }

    /**
     * 3rd party | get Order Rate by id
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     */
    public function externalGetRateByIdAction(RequestAlias $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $translator = $this->get('translator');

        $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(array('id'=> $id));
        if (!$order) {
            return $apiOperations->getErrorJsonResponse($translator->trans("No order was found"));
        }

        $orderResponse = new \stdClass();
        $orderResponse->rate = (double)$order->getRate();
        $orderResponse->rateComment = $order->getRateComment() ? $order->getRateComment() : "";

        return $apiOperations->getSuccessDataJsonResponse($orderResponse);
    }

    /**
     * 3rd party | get Order Rate by id
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="rate", "dataType"="integer", "required"=true},
     *      {"name"="fromTime", "dataType"="integer", "required"=true},
     *      {"name"="toTime", "dataType"="integer", "required"=true}
     *  },
     *  section="Order",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param RequestAlias $request
     *
     * @return JsonResponse
     */
    public function externalGetRateFromIntervalAction(RequestAlias $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $translator = $this->get('translator');

        $start = $request->get('fromTime') ? date('Y-m-d H:i:s', $request->get('fromTime')) : date('1970-01-01 00:00:00');
        $end = $request->get('toTime') ? date('Y-m-d H:i:s', $request->get('toTime')) : date('Y-m-d 23:59:59');
        $rate = $request->get('rate') ? $request->get('rate') : -1;

        $orders = $em->createQueryBuilder()
            ->select('o')
            ->from('IbtikarTaniaModelBundle:Order', 'o')
            ->where('o.createdAt >= :start')
            ->andWhere('o.createdAt <= :end')
            ->andWhere('o.rate <= :rate')
            ->andWhere('o.rate != :noRate')
            ->andWhere("o INSTANCE OF Ibtikar\TaniaModelBundle\Entity\Order")
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('rate', $rate)
            ->setParameter('noRate', 0)
            ->getQuery()
            ->getResult();

        $ordersResponseArr = array();

        foreach ($orders as $order) {
            $orderResponse = new \stdClass();
            $orderResponse->id = (double)$order->getId();
            $orderResponse->rate = (double)$order->getRate();
            $orderResponse->rateComment = $order->getRateComment() ? $order->getRateComment() : "";
            $ordersResponseArr[] = $orderResponse;
        }

        return $apiOperations->getSuccessDataJsonResponse($ordersResponseArr);
    }


    public function detailsV2Action(RequestAlias $request, $id)
    {
        //echo 'Hiii ';exit;
        $apiOperations = $this->get('api_operations');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $id, 'driver' => $user->getId()]);
        } elseif ($user instanceof \Ibtikar\TaniaModelBundle\Entity\User) {
            $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->findOneBy(['id' => $id, 'user' => $user->getId()]);
        }

        $translator = $this->get('translator');

        if (!$order) {
            return $apiOperations->getSingleErrorJsonResponse($translator->trans('order_not_found', array(), 'order'));
        }

        $this->getDoctrine()->getManager()->getFilters()->disable('softdeleteable');

        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            $responseOrderListData = $this->getDriverOrderObject($order);
        } elseif ($user instanceof \Ibtikar\TaniaModelBundle\Entity\User) {
            $responseOrderListData = $this->getUserOrderObject($order);
        }

        return $apiOperations->getJsonResponseForObject($responseOrderListData);
    }
}
