<?php

namespace AppBundle\Controller;

use AppBundle\APIResponse\DeviceNotification\DeviceNotificationList;
use AppBundle\APIResponse\DeviceNotification\DeviceNotification;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class DeviceNotificationController extends DefaultController
{

    /**
     * List Device notifications
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Device notification",
     *  output="AppBundle\APIResponse\DeviceNotification\DeviceNotificationList",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=true}
     *  },
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200="AppBundle\APIResponse\DeviceNotification\DeviceNotificationList",
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        /* @var $em \Doctrine\ORM\EntityManager */

        $page = $request->get('page');
        if($page !== 0 && !$page)
            $page = 1;
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $notifications = $em->getRepository('IbtikarTaniaModelBundle:DeviceNotification')->findBy(['user' => $user->getId()], ['id' => 'desc'], 10, ($page-1)*10);

        $responseList = new DeviceNotificationList();
        foreach ($notifications as $notification) {
            $notificationResponse = new DeviceNotification();
            $apiOperations->bindObjectDataFromObject($notificationResponse, $notification);
            $notificationResponse->createdDate = $notification->getCreatedAt()->getTimestamp();
            $responseList->notifications[] = $notificationResponse;
        }
        return $apiOperations->getJsonResponseForObject($responseList);
    }
}
