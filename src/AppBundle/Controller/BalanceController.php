<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\DefaultController;
use AppBundle\APIResponse\Order\BalanceOrder;
use AppBundle\APIResponse\Order\PaymentMethod;
use AppBundle\APIResponse\Order\ResponseOrderList;
use AppBundle\APIResponse\Balance\ResponseBalance;
use AppBundle\APIResponse\Order\ResponseOrderDriver;
use AppBundle\APIResponse\Balance\ResponseBalanceList;
use Ibtikar\TaniaModelBundle\Entity\BalanceRequest;
use Ibtikar\TaniaModelBundle\Entity\Order;
use AppBundle\APIResponse\Order\Address;
use AppBundle\APIResponse\Shift\ResponseShift;
use AppBundle\APIResponse\Order\ResponseOrderListItem;
use AppBundle\APIResponse\Order\ResponseOrderListData;
use AppBundle\APIResponse\Order\Receipt;
use AppBundle\APIResponse\ValidationErrorsResponse;

class BalanceController extends DefaultController
{
    /**
     * List balances
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Balance",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $balances = $em->getRepository('IbtikarTaniaModelBundle:Balance')->findAll();

        $fee = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];
        $taxPercent = $fee->getTaxPercent();
        $responseBalanceList = new ResponseBalanceList();

        foreach ($balances as $balance) {
            $responseBalance = new ResponseBalance();

            $responseBalance->id = $balance->getId();
            $responseBalance->balanceName = $balance->getName();
            $responseBalance->price = (double)$balance->getPrice();
            $responseBalance->taxFees = $taxPercent*$responseBalance->price/100;
            $responseBalance->finalPrice = $responseBalance->price + $responseBalance->taxFees;
            $responseBalance->points = (double)$balance->getPoints();

            $responseBalanceList->balances[] = $responseBalance;
        }
        return $apiOperations->getJsonResponseForObject($responseBalanceList);
    }

    /**
     * List balance requests
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Balance",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function balanceRequestsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $balances = $em->getRepository('IbtikarTaniaModelBundle:BalanceRequest')->findBy(['user' => $this->getUser()]);

        $responseBalanceList = new ResponseOrderList();
        foreach ($balances as $balance) {
            $responseBalanceList->orders[] = $this->getUserOrderObject($balance);
        }
        return $apiOperations->getJsonResponseForObject($responseBalanceList);
    }

    /**
     * Add new balance order
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\BalanceOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="Balance",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $balanceOrder = new BalanceOrder();
        $balanceOrder->paymentMethod = new PaymentMethod();
        $user = $this->getUser();
        if ($user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('Drivers can not add balace orders.'));
        }
        $apiOperations->bindObjectDataFromJsonRequest($balanceOrder, $request, array(
            'receipt' => 'AppBundle\APIResponse\Order\Receipt',
            'items' => '\AppBundle\APIResponse\Order\RequestOrderItem',
            'paymentMethod' => '\AppBundle\APIResponse\Order\PaymentMethod'
        ));
//        //FIX: ISSUE WITH SADAD ANDROID 05/06/2018

//        $apiKey = $request->headers->get('X-Api-Key');
//        if($apiKey != '456' && $balanceOrder->paymentMethod->type == Order::SADAD){
//            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Payment with SADAD will be available soon, please choose other payment method"));
//        }

        if ($balanceOrder->receivingDate) {
            $balanceOrder->receivingDate = $balanceOrder->receivingDate->getTimestamp();
        }
        if ($balanceOrder->receivingDate && $balanceOrder->receivingDate < time()) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('Receiving date should be larger than current time'));
        }
        $errorsObjects = $this->get('validator')->validate($balanceOrder, null, array('Default', 'balance'));
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }

        $errorsObjects = $this->get('validator')->validate($balanceOrder->receipt);
        if (count($errorsObjects) > 0) {
            return $apiOperations->getValidationErrorsJsonResponse($errorsObjects);
        }

        $em = $this->getDoctrine()->getManager();
        $cityArea = null;
        if ($balanceOrder->paymentMethod->type == Order::CASH) {
            if ($balanceOrder->addressId) {
                /* @var $balance \Ibtikar\TaniaModelBundle\Entity\UserAddress */
                $address = $em->getRepository('IbtikarTaniaModelBundle:UserAddress')->findOneBy(array('id' => $balanceOrder->addressId));
                if (!$address) {
                    return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('Address not found.'));
                } else {
                    $cityArea = $this->getCityAreaPerLatLong($address->getLatitude(), $address->getLongitude());
                }
                if (!$cityArea) {
                    return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
                }
            } elseif ($balanceOrder->lat && $balanceOrder->long && $balanceOrder->paymentMethod->type == Order::CASH) {
                $address = null;
                $cityArea = $this->getCityAreaPerLatLong($balanceOrder->lat, $balanceOrder->long);
                if (!$cityArea) {
                    return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
                }
            } else {
                $address = null;
                if ($balanceOrder->addressId == 0) {
                    $cityArea = $this->getCityAreaPerLatLong($this->getUser()->getLatitude(), $this->getUser()->getLongitude());
                    if (!$cityArea) {
                        return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
                    }
                }
                return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans("select_address_first"));
            }
        }
        $city = $cityArea ? $cityArea->getCity() : null;
        /* @var $balance \Ibtikar\TaniaModelBundle\Entity\Shift */
        $shift = $em->getRepository('IbtikarTaniaModelBundle:Shift')->findOneBy(array('id' => $balanceOrder->shiftId));
        if (!$shift) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('Shift not found'));
        }
        /* @var $balance \Ibtikar\TaniaModelBundle\Entity\Balance */
        $balance = $em->getRepository('IbtikarTaniaModelBundle:Balance')->findOneBy(array('id' => $balanceOrder->balanceId));
        if (!$balance) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('Balance not found'));
        }

        $receipt = $this->getReceipt($balanceOrder->receipt->totalPrice);
        if ($balance->getPrice() != $balanceOrder->receipt->totalPrice || $receipt->receipt->finalPrice != $balanceOrder->receipt->finalPrice) { //if cost of any item changed or fees changed
            if ($balance->getPrice() != $balanceOrder->receipt->totalPrice) {
                $receipt = $this->getReceipt($balance->getPrice());
            }

            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans('Costs Changed', array(), 'validators'), $receipt);
        }
        $balanceOrderObject = new BalanceRequest();
        $balanceOrderObject->setPoints($balance->getPoints());
        $balanceOrderObject->setPrice($balance->getPrice());
        $balanceOrderObject->setBalanceName($balance->getName());
        $balanceOrderObject->setPaymentMethod($balanceOrder->paymentMethod->type);
        $balanceOrderObject->setShift($shift);
        $balanceOrderObject->setShiftFrom($shift->getFrom());
        $balanceOrderObject->setShiftTo($shift->getTo());
        $balanceOrderObject->setCity($city);
        $balanceOrderObject->setCityArea($cityArea);
        $balanceOrderObject->setUser($user);
        if ($address) {
            $balanceOrderObject->setTitle($address->getTitle());
            $balanceOrderObject->setAddress($address->getAddress());
            $balanceOrderObject->setLatitude($address->getLatitude());
            $balanceOrderObject->setLongitude($address->getLongitude());
        } else {
            $balanceOrderObject->setAddress($user->getAddress());
            $balanceOrderObject->setLatitude($user->getLatitude());
            $balanceOrderObject->setLongitude($user->getLongitude());
        }
        if ($balanceOrder->receivingDate) {
            $balanceOrderObject->setReceivingDate($balanceOrder->receivingDate);
        } else {
            $balanceOrderObject->setReceivingDate(time());
        }
        $balanceOrderObject->setAmountDue($balanceOrder->receipt->finalPrice);
        $balanceOrderObject->setTaxFees($balanceOrder->receipt->taxFees);
        $balanceOrderObject->setNote($balanceOrder->note);
        $balanceOrderObject->setSource($this->get('request_stack')->getCurrentRequest()->attributes->get('requestFrom'));
        $em->persist($balanceOrderObject);
        $em->flush();
        return $apiOperations->getJsonResponseForObject($this->getUserOrderObject($balanceOrderObject));
    }

    public function getUserOrderObject($order)
    {
        /* @var $order Order */
        $request = $this->get('request_stack')->getCurrentRequest();
        $responseOrderListData = $this->getOrderObject($order);
        if ($order->getDriverUsername()) {
            $responseDriver = new ResponseOrderDriver();
            $responseDriver->id = $order->getDriver() ? $order->getDriver()->getId() : null;
            $responseDriver->fullName = $order->getDriverFullName();
            $responseDriver->fullNameAr = $order->getDriverFullNameAr();
            $responseDriver->phone = $order->getDriverPhone();
            $responseDriver->username = $order->getDriverUsername();
            $responseDriver->image = $order->getDriverImage() ? $request->getSchemeAndHttpHost() . '/' . $order->getDriverImage() : null;
            $responseDriver->rate = (double)$order->getDriverRate();
            $responseDriver->vanNumber = (string)$order->getDriver() ? $order->getVanNumber() : null;
        } else {
            $responseDriver = null;
        }
        $responseOrderListData->driver = $responseDriver;
        unset($responseOrderListData->user);
        return $responseOrderListData;
    }

    public function getOrderObject($order)
    {
        /* @var $order \Ibtikar\TaniaModelBundle\Entity\Order */
        $request = $this->get('request_stack')->getCurrentRequest();

        $responseOrderListData = new ResponseOrderListData();
        $responseOrderListData->id = $order->getId();
        $responseOrderListData->totalPrice = (double)$order->getPrice();
        $responseOrderListData->createdDate = strtotime($order->getCreatedAt()->format('H:i'));
        $responseOrderListData->status = $order->getStatus();
        $responseOrderListData->closeReason = $order->getCloseReason();
        $responseOrderListData->receivingDate = (int) $order->getReceivingDate();
        $responseOrderListData->rate = (double)$order->getRate();
        $responseOrderListData->rateComment = $order->getRateComment();
        $responseOrderListData->note = $order->getNote();

        $address = new Address();
        $address->title = $order->getTitle();
        $address->address = $order->getAddress();
        $address->lat = (double) $order->getLatitude();
        $address->long = (double) $order->getLongitude();

        $responseOrderListData->address = $address;

        $receipt = new Receipt();
        $receipt->totalPrice = (double)$order->getPrice();
        $receipt->finalPrice = (double)$order->getAmountDue();
        $receipt->taxFees = (double)$order->getTaxFees();

        $responseOrderListData->receipt = $receipt;

        $creditId = null;
        if ($order->getCreditCard()) {
            $creditId = $order->getCreditCard()->getId();
        }
        $paymentMethod = new PaymentMethod();
        $paymentMethod->id = $creditId;
        $paymentMethod->cardNumber = $order->getCardNumber();
        $paymentMethod->expiryDate = (double) $order->getExpiryDate();
        $paymentMethod->fortId = $order->getFortId();
        $paymentMethod->isDefault = $order->getIsDefault();
        $paymentMethod->merchantReference = $order->getMerchantReference();
        $paymentMethod->paymentOption = $order->getPaymentOption();
        $paymentMethod->tokenName = $order->getTokenName();
        $paymentMethod->type = $order->getPaymentMethod();
        $paymentMethod->value = $order->getPaymentValue();
        $responseOrderListData->paymentMethod = $paymentMethod;

        $shift = new ResponseShift();

        $shift->id = $order->getShift()->getId();
        if (strtolower($request->headers->get('accept-language')) == 'en') {
            $shift->shift = $order->getShift()->getShift();
        } else {
            $shift->shift = $order->getShift()->getShiftAr();
        }
        $shift->from = strtotime($order->getShiftFrom()->format('H:i'));
        $shift->to = strtotime($order->getShiftTo()->format('H:i'));

        $responseOrderListData->shift = $shift;

//        foreach ($order->getOrderItems() as $orderItem) {
//            /* @var $orderItem \Ibtikar\TaniaModelBundle\Entity\OrderItem */
//            $responseOrderListItem = new ResponseOrderListItem();
//            $responseOrderListItem->id = $orderItem->getItem()->getId();
//            $responseOrderListItem->name =  $request->headers->get('accept-language') == 'en' ?  $orderItem->getNameEn():$orderItem->getName();
//            $responseOrderListItem->image = $request->getSchemeAndHttpHost() . '/' . $orderItem->getItem()->getWebPath();
//            $responseOrderListItem->count = $orderItem->getCount();
//            $responseOrderListItem->price = $orderItem->getPrice();
//            $responseOrderListData->items[] = $responseOrderListItem;
//        }

        $responseOrderListData->balance = $this->get('order_operations')->getBalanceRequest($order);
        unset($responseOrderListData->items);
        return $responseOrderListData;
    }


    /**
     * Calculate receipt
     *
     * @ApiDoc(
     *  resource=true,
     *  input="AppBundle\APIResponse\Order\BalanceOrder",
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *  },
     *  section="Balance",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function calculateReceiptAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        $balanceOrder = new BalanceOrder();
        $user = $this->getUser();
        if (!$user || $user instanceof \Ibtikar\TaniaModelBundle\Entity\Driver) {
            return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("please login"));
        }
        $apiOperations->bindObjectDataFromJsonRequest($balanceOrder, $request);
        if (!isset($balanceOrder->balanceId)) {
            $validationMessages['balanceId'] = $this->get('translator')->trans('fill_mandatory_field', array(), 'validators');

            if (count($validationMessages)) {
                $output = new ValidationErrorsResponse();
                $output->errors = $validationMessages;
                return new JsonResponse($output);
            }
        }

        $balance = $em->getRepository('IbtikarTaniaModelBundle:Balance')->findOneBy(array('id' => $balanceOrder->balanceId));
        if (!$balance) {
            return $apiOperations->getSingleErrorJsonResponse($this->get('translator')->trans('Balance not found'));
        }

        $receipt = $this->getReceipt($balance->getPrice());

        return $apiOperations->getJsonResponseForObject($receipt);
    }
}
