<?php

namespace AppBundle\Controller;

use AppBundle\APIResponse\Package\ResponsePackage;
use AppBundle\APIResponse\Package\ResponseBuyItem as PackageResponseBuyItem;
use AppBundle\APIResponse\Offer\ResponseBuyItem;
use AppBundle\APIResponse\Offer\ResponseGetItem;
use AppBundle\APIResponse\Offer\ResponseOffer;
use Doctrine\Common\Util\Debug;
use Ibtikar\TaniaModelBundle\Entity\Item;
use Ibtikar\TaniaModelBundle\Entity\Offer;
use Ibtikar\TaniaModelBundle\Entity\OfferBuyItem;
use Ibtikar\TaniaModelBundle\Entity\OfferGetItem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\APIResponse\Item\ResponseItem;
use AppBundle\APIResponse\Item\ResponseItemList;
use AppBundle\APIResponse\City\City as cityitem;
use AppBundle\APIResponse\City\CityList;
use Ibtikar\TaniaModelBundle\Entity\UserAddress;
use AppBundle\APIResponse\Order\Address;
use AppBundle\APIResponse\Success;
use Ibtikar\TaniaModelBundle\Entity\PromoCode;
use Location\Polygon;
use Location\Coordinate;
use GuzzleHttp\Client;

class DefaultController extends Controller
{
    public function getDateInTimeStamp($date){
        if(is_object($date)){
            return strtotime($date->format('Y-m-d H:i:s'));
        }
        else if(is_string($date)){
            return strtotime($date);
        }
    }

    /**
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     * @param string $polygonPointsString
     * @return array
     */
    private function getPointsArrayFromString($polygonPointsString)
    {
        $polygonPointsStringsArray = unserialize($polygonPointsString);
        $points = array();
        foreach ($polygonPointsStringsArray as $polygonPointString) {
            $coordinates = explode(',', $polygonPointString);
            $points[] = array('longitude' => trim($coordinates[0]), 'latitude' => trim($coordinates[1]));
        }
        return $points;
    }

    /**
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     * @param string $polygonPointsString
     * @return array
     */
    private function getPointsArrayFromJson($polygonPointsString)
    {
        $polygonPointsStringsArray = json_decode($polygonPointsString);
        $points = array();
        foreach ($polygonPointsStringsArray as $polygonPointString) {
            $points[] = array('longitude' => $polygonPointString->lng, 'latitude' => $polygonPointString->lat);
        }
        return $points;
    }

    /**
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     * @param array $pointArray
     * @param array $polygonArray
     * @return boolean
     */
    private function checkPointInPolygon($pointArray, $polygonArray)
    {
        $geofence = new Polygon();
        foreach ($polygonArray as $polygonPoint) {
            $geofence->addPoint(new Coordinate($polygonPoint['latitude'], $polygonPoint['longitude']));
        }
        return $geofence->contains(new Coordinate($pointArray['latitude'], $pointArray['longitude']));
    }

    /**
     * @author Moemen Hussein <moemen.hussein@ibtikar.net.sa>
     * @param type $latitude
     * @param type $longitude
     * @return boolean
     */
    protected function getCityPerLatLong($latitude, $longitude)
    {
        $em = $this->getDoctrine()->getManager();
        $cities = $em->getRepository("IbtikarTaniaModelBundle:City")->findAll();
        foreach ($cities as $city) {
            if ($this->checkPointInPolygon(array('latitude' => $latitude, 'longitude' => $longitude), $this->getPointsArrayFromString($city->getCityPolygon()))) {
                return $city;
            }
        }
        return false;
    }

    /**
     * @author Moemen Hussein <moemen.hussein@ibtikar.net.sa>
     * @param type $latitude
     * @param type $longitude
     * @return boolean
     */
    protected function getCityAreaPerLatLong($latitude, $longitude)
    {
        $em = $this->getDoctrine()->getManager();
        $cityAreas = $em->getRepository("IbtikarTaniaModelBundle:CityArea")->findAll();//By(array('city' => $city->getId()));
        foreach ($cityAreas as $cityArea) {
            if ($this->checkPointInPolygon(array('latitude' => $latitude, 'longitude' => $longitude), $this->getPointsArrayFromJson($cityArea->getPolygon()))) {
                return $cityArea;
            }
        }
        return false;
    }

    public function getSubObject($subEntity, $propertiesToGet){
        if ($subEntity == null) {
            return null;
        }
        $subObject = new \stdClass();
        foreach ($propertiesToGet as $key => $value) {
            $subObject->$key = $subEntity->{$value}();
        }
        return $subObject;
    }
    /**
     * @author Moemen Hussein <moemen.hussein@ibtikar.net.sa>
     * @param type $items
     * @param type $city
     * @return \AppBundle\Controller\ResponseItemList
     */
    protected function getItemObjectPerCity($request, $items, $city){
        $locale = ucfirst($request->headers->get('accept-language'));
        $source = ucfirst($request->headers->get('source'));

        if (strlen($locale) > 2 || !$locale) {
            $locale = "En";
        }
        $responseItemList = new ResponseItemList();
        /* @var Item $item */
        foreach ($items as $item) {

            if(($source=='Erwaa' && $item->getBrand()->getNameEn()=='Erwaa')
            || ($source!='Erwaa' && $item->getBrand()->getNameEn()!='Erwaa'))
            {    
                $responseItem = new ResponseItem();
                $responseItem->id = $item->getId();
                $responseItem->minimumAmountToOrder = $item->getMinimumAmountToOrder();
                $responseItem->name =  $request->headers->get('accept-language') == 'en' ?  $item->getNameEn(): $item->getName();
                $responseItem->nameEn =  $item->getNameEn();
                $responseItem->nameAr =  $item->getName();
                $responseItem->image = $request->getSchemeAndHttpHost().'/'.$item->getWebPath();
                $responseItem->price = (double)$item->getDefaultPrice();
                $responseItem->oldPrice = (double)$item->getOldDefaultPrice();
                $responseItem->cityId = $city ? $city->getId() : null;

                $responseItem->attribute = $this->getSubObject($item->getAttribute(), array('id' => "getId", 'name' => "getName" . $locale));
                $responseItem->brand = $this->getSubObject($item->getBrand(), array('id' => "getId", 'name' => "getName" . $locale));
                $responseItem->package = $this->getSubObject($item->getPackage(), array('id' => "getId", 'name' => "getName" . $locale));
                $responseItem->packageSize = $this->getSubObject($item->getPackageSize(), array('id' => "getId", 'name' => "getName" . $locale));
                $responseItem->type = $this->getSubObject($item->getType(), array('id' => "getId", 'name' => "getName" . $locale));

                $prices = $item->getPrices();
                if(!$city){
                    $responseItem->price = (double)$item->getDefaultPrice();
                    $responseItem->oldPrice = (double)$item->getOldDefaultPrice();
                }else {
                    foreach ($prices as $price) {
                        if ($price->getCity()->getId() == $city->getId()) {
                            $responseItem->price = (double)$price->getPrice();
                            $responseItem->oldPrice = (double)$price->getOldPrice();
                            break;
                        }
                    }
                }
                $responseItemList->items[] = $responseItem;
            }
        }
        return $responseItemList;
    }

    /**
     * Get the cities list
     *
     * @ApiDoc(
     *  resource=true,
     *  section="City",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="Ibtikar\ShareEconomyUMSBundle\APIResponse\InvalidCredentials",
     *      403="Ibtikar\ShareEconomyToolsBundle\APIResponse\InvalidAPIKey",
     *      500="Ibtikar\ShareEconomyToolsBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     * @param Request $request
     * @return JsonResponse
     */
    public function cityListAction(Request $request) {
        $apiOperations = $this->get('api_operations');
        $em = $this->getDoctrine()->getManager();
        $sortyBy = $request->getLocale() === 'ar' ? 'nameAr' : 'nameEn';
        $cities = $em->getRepository("IbtikarTaniaModelBundle:City")->findBy(array(), array($sortyBy => 'asc'));
        $cityList = new CityList();
        foreach ($cities as $city) {
            $cityItem = new cityitem();
            $cityItem->id = $city->getId();
            $cityItem->name = $city->getNameEn();
            $cityItem->nameAr = $city->getNameAr();
            $cityList->cities[] = $cityItem;
        }
        return $apiOperations->getJsonResponseForObject($cityList);
    }

    /**
     * Get the settings list
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Settings",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="Ibtikar\ShareEconomyUMSBundle\APIResponse\InvalidCredentials",
     *      403="Ibtikar\ShareEconomyToolsBundle\APIResponse\InvalidAPIKey",
     *      500="Ibtikar\ShareEconomyToolsBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     * @return JsonResponse
     */
    public function settingsListAction(Request $request) {
        $output = new Success();
        $setToZero = $request->headers->get('X-Api-Key') == $this->container->getParameter('ios_api_key') ? true : false;
        $output->settings = array();
        $output->settings['minimumNumberOfItemsPerOrder'] = $setToZero ? 0 : $this->get('settings_manager')->get('minimum_number_of_items_per_order');
        return new JsonResponse($output);
    }

    protected function getDriverItemObject($request, $items){
        $responseItemList = new ResponseItemList();
        foreach ($items as $item) {
            $responseItem = new ResponseItem();
            $responseItem->id = $item["item"]->getId();
            $responseItem->minimumAmountToOrder = $item["item"]->getMinimumAmountToOrder();
            $responseItem->name =  $request->headers->get('accept-language') == 'en' ?  $item["item"]->getNameEn():$item["item"]->getName();
            $responseItem->image = $request->getSchemeAndHttpHost().'/'.$item["item"]->getWebPath();
            $responseItem->price = (double)$item["item"]->getDefaultPrice();
            $responseItem->currentCapacity = $item["currentCapacity"];
            $responseItem->totalCapacity = $item["capacity"];
            unset($responseItem->cityId);
            $responseItemList->items[] = $responseItem;
        }
        return $responseItemList;
    }

    public function getReceipt($totalPrice, $withoutTaxes = false, $promoCode = null, $offers = array(), $offerCountArray = array(), $costWithOffers = FALSE , $taxableAmt = 0)
    {
        $receipt = new \stdClass();
        $em = $this->getDoctrine()->getManager();
        $fees = $em->getRepository('IbtikarTaniaModelBundle:Fee')->findAll()[0];

        $offersDiscount = 0;
        $offersPrice = 0;
        /* @var Offer $offer */
        foreach ($offers as $offer) {
            $offersDiscount += $offer->getOfferDiscount() * $offerCountArray[$offer->getId()]['count'];
            $offersPrice += $offer->getOfferPrice() * $offerCountArray[$offer->getId()]['count'];
        }

        $totalPrice1 = $totalPrice; // NEW-ISPL
        if($costWithOffers) {
            $totalPrice = $totalPrice - $offersDiscount;
        }

        $receipt->finalPrice = $receipt->totalPrice = (double) $totalPrice;

        if($taxableAmt > 0)  // NEW-ISPL
        $receipt->taxFees = (double) ($withoutTaxes ? 0 : ($fees->getTaxPercent() * ($taxableAmt + $offersDiscount) / 100));
        else    
        $receipt->taxFees = (double) ($withoutTaxes ? 0 : ($fees->getTaxPercent() * ($receipt->totalPrice + $offersDiscount) / 100));    
        
        //if($taxableAmt > 0)      // NEW-ISPL
        //$receipt->finalPrice = $totalPrice + $receipt->taxFees;  // NEW-ISPL   
        //else                                                     // NEW-ISPL
        $receipt->finalPrice = $receipt->totalPrice + $receipt->taxFees;

        $finalPriceWithoutOffers = $totalPrice1 - $offersPrice ; // NEW-ISPL
        $receipt->discount = 0;
        if ($promoCode) {
            if ($promoCode->getType() == PromoCode::PROMOCODE_TYPE_FIXED_VALUE) {
                $receipt->discount = (double) $promoCode->getDiscountAmount();
            } else if ($promoCode->getType() == PromoCode::PROMOCODE_TYPE_PERCENTAGE) {
                //$receipt->discount = (double) ($receipt->finalPrice * $promoCode->getDiscountAmount()) / 100; // NEW-ISPL
                $receipt->discount = (double) ($finalPriceWithoutOffers * $promoCode->getDiscountAmount()) / 100; // NEW-ISPL
            }
        }

        $receipt->finalPrice = max(0, $receipt->finalPrice - $receipt->discount);
        if ($receipt->finalPrice < 0) {
            $receipt->finalPrice = 0;
        }
        $receipt->offersDiscount = round($offersDiscount, 2);

        $receipt->paidTotalPrice = $receipt->totalPrice;
        $receipt->totalPrice += $offersDiscount;

        $response = new \stdClass();
        $response->receipt = $receipt;
        return $response;
    }

    /**
     * @param Request $request
     * @param Offer $offer
     * @return ResponseOffer
     */
    protected function buildOfferObject($request, $offer){
        $responseOffer = new ResponseOffer();
        /* @var Item $item */
        $responseOffer->id = $offer->getId();
        $responseOffer->type = $offer->getType();
        $responseOffer->title =  $request->headers->get('accept-language') == 'en' ?  $offer->getTitleEn(): $offer->getTitle();
        $responseOffer->titleEn =  $offer->getTitleEn();
        $responseOffer->titleAr =  $offer->getTitle();
        $responseOffer->description =  $request->headers->get('accept-language') == 'en' ?  $offer->getDescriptionPublicEn(): $offer->getDescriptionPublic();
        $responseOffer->descriptionEn =  $offer->getDescriptionPublicEn();
        $responseOffer->descriptionAr =  $offer->getDescriptionPublic();
        $responseOffer->startTime = $offer->getStartTime() ? $offer->getStartTime()->getTimestamp() : 0;
        $responseOffer->expiryTime = $offer->getExpiryTime() ? $offer->getExpiryTime()->getTimestamp() : 0;
        $responseOffer->numberOfUsedTimes = $offer->getNumberOfUsedTimes();

        $discountPercent = 0;
        if($offer->getType() == Offer::TYPE_CASH_PERCENTAGE){
            $responseOffer->getAmount = (double) $offer->getPercentageGetAmount() * 100;
            $discountPercent = (double) $offer->getPercentageGetAmount();
        }elseif ($offer->getType() == Offer::TYPE_CASH_AMOUNT){
            $responseOffer->getAmount = (double) $offer->getCashGetAmount();
        }else{
            $responseOffer->getAmount = 0;
        }

        $offerBuyItems = array();
        $offerGetItems = array();

        $buyItems = $offer->getOfferBuyItems();
        $getItems = $offer->getOfferGetItems();

        if(count($buyItems) > 0) {
            $responseOffer->image = $buyItems[0]->getItem() ? $request->getSchemeAndHttpHost().'/'.$buyItems[0]->getItem()->getWebPath() : null;
        }

        $buyItemsCost = 0;
        /* @var OfferBuyItem $buyItem*/
        foreach ($buyItems as $buyItem) {
            if($buyItem->getItem()) {
                $responseBuyItem = new ResponseBuyItem();
                $responseBuyItem->id = $buyItem->getItem() ? $buyItem->getItem()->getId() : 0;
                $responseBuyItem->name = $request->headers->get('accept-language') == 'en' ? $buyItem->getNameEn() : $buyItem->getName();
                $responseBuyItem->nameEn = $buyItem->getNameEn();
                $responseBuyItem->nameAr = $buyItem->getName();
                $responseBuyItem->count = $buyItem->getCount();
                $responseBuyItem->price = (double)$buyItem->getPrice();
                $responseBuyItem->image = $buyItem->getItem() ? $request->getSchemeAndHttpHost() . '/' . $buyItem->getItem()->getWebPath() : "";

                $offerBuyItems[] = $responseBuyItem;
                $buyItemsCost += (double)$buyItem->getPrice() * $buyItem->getCount();
            }
        }

        /* @var OfferGetItem $getItem*/
        foreach ($getItems as $getItem) {
            if($getItem->getItem()) {
                $responseGetItem = new ResponseGetItem();
                $responseGetItem->id = $getItem->getItem() ? $getItem->getItem()->getId() : 0;
                $responseGetItem->name = $request->headers->get('accept-language') == 'en' ? $getItem->getNameEn() : $getItem->getName();
                $responseGetItem->count = $getItem->getCount();
                $responseGetItem->price = (double)$getItem->getPrice();
                $responseGetItem->image = $getItem->getItem() ? $request->getSchemeAndHttpHost() . '/' . $getItem->getItem()->getWebPath() : "";

                $offerGetItems[] = $responseGetItem;
            }
        }

        $responseOffer->buyItemsCost = round(max(0,$buyItemsCost - $buyItemsCost * $discountPercent), 2);
        $responseOffer->buyItems = $offerBuyItems;
        $responseOffer->getItems = $offerGetItems;

        return $responseOffer;
    }

    /**
     * @param Request $request
     * @param Package $package
     * @return ResponsePackage
     */
    protected function buildPackageObject($request, $package){
        $responsePackage = new ResponsePackage();
        /* @var Item $item */
        $responsePackage->id = $package->getId();
        $responsePackage->title =  $request->headers->get('accept-language') == 'en' ?  $package->getTitleEn(): $package->getTitle();
        $responsePackage->description =  $request->headers->get('accept-language') == 'en' ?  $package->getDescriptionPublicEn(): $package->getDescriptionPublic();
        $responsePackage->startTime = $package->getStartTime() ? $package->getStartTime()->getTimestamp() : 0;
        $responsePackage->expiryTime = $package->getExpiryTime() ? $package->getExpiryTime()->getTimestamp() : 0;
        $responsePackage->numberOfUsedTimes = $package->getNumberOfUsedTimes();
        $responsePackage->getAmount = $package->getGetAmount();

        $packageBuyItems = array();

        $buyItems = $package->getPackageBuyItems();

        if(count($buyItems) > 0) {
            $responsePackage->image = $buyItems[0]->getItem() ? $request->getSchemeAndHttpHost().'/'.$buyItems[0]->getItem()->getWebPath() : null;
        }

        $buyItemsCost = 0;
        /* @var PackageBuyItem $buyItem*/
        foreach ($buyItems as $buyItem) {
            if($buyItem->getItem()) {
                $responseBuyItem = new PackageResponseBuyItem();
                $responseBuyItem->id = $buyItem->getItem() ? $buyItem->getItem()->getId() : 0;
                $responseBuyItem->name = $request->headers->get('accept-language') == 'en' ? $buyItem->getNameEn() : $buyItem->getName();
                $responseBuyItem->count = $buyItem->getCount();
                $responseBuyItem->price = (double)$buyItem->getPrice();
                $responseBuyItem->image = $buyItem->getItem() ? $request->getSchemeAndHttpHost() . '/' . $buyItem->getItem()->getWebPath() : "";

                $packageBuyItems[] = $responseBuyItem;
                $buyItemsCost += (double)$buyItem->getPrice() * $buyItem->getCount();
            }
        }
        $responsePackage->buyItemsCost = round(max(0, $buyItemsCost), 2);
        $responsePackage->buyItems = $packageBuyItems;
        return $responsePackage;
    }

    protected function nanaApiUpdateOrderStatus($orderId, $status){
        $url = "https://admin.nana.sa/api/v1/orders/$orderId/status";
        $now = date("Y-m-d")."T".date("H:i:s")."Z";

        // set post fields
        $payLoad = new \stdClass();
        $payLoad->status = $status;
        $payLoad->updated = $now;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsYW5nIjoiZW4iLCJwbGF0Zm9ybV92ZXJzaW9uIjoiNi4wNiIsInBsYXRmb3JtIjoiQU5EUk9JRCIsImFnZW50X2lkIjoiREVNMDA',
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payLoad));

        $response = curl_exec($ch);

        curl_close($ch);

        return json_decode($response);
    }

    /**
     * @param type $item
     * @return \AppBundle\Controller\ResponseItem
     */
    protected function getItemObject($request, $item){
        $locale = ucfirst($request->headers->get('accept-language'));
        if (strlen($locale) > 2) {
            $locale = "En";
        }

        /* @var Item $item */
        $responseItem = new ResponseItem();
        $responseItem->id = $item->getId();
        $responseItem->minimumAmountToOrder = $item->getMinimumAmountToOrder();
        $responseItem->name =  $request->headers->get('accept-language') == 'en' ?  $item->getNameEn(): $item->getName();
        $responseItem->nameEn =  $item->getNameEn();
        $responseItem->nameAr =  $item->getName();
        $responseItem->image = $request->getSchemeAndHttpHost().'/'.$item->getWebPath();
        $responseItem->price = (double)$item->getDefaultPrice();
        $responseItem->oldPrice = (double)$item->getOldDefaultPrice();

        $responseItem->attribute = $this->getSubObject($item->getAttribute(), array('id' => "getId", 'name' => "getName" . $locale));
        $responseItem->brand = $this->getSubObject($item->getBrand(), array('id' => "getId", 'name' => "getName" . $locale));
        $responseItem->package = $this->getSubObject($item->getPackage(), array('id' => "getId", 'name' => "getName" . $locale));
        $responseItem->packageSize = $this->getSubObject($item->getPackageSize(), array('id' => "getId", 'name' => "getName" . $locale));
        $responseItem->type = $this->getSubObject($item->getType(), array('id' => "getId", 'name' => "getName" . $locale));
        

        return $responseItem;
    }   

    /**
     * Get the test 
     *
     * @ApiDoc(
     *  resource=true,
     *  section="test",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="Ibtikar\ShareEconomyUMSBundle\APIResponse\InvalidCredentials",
     *      403="Ibtikar\ShareEconomyToolsBundle\APIResponse\InvalidAPIKey",
     *      500="Ibtikar\ShareEconomyToolsBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     * @return JsonResponse
     */

    public function addressVerifyAction(Request $request) {

        $requestData = json_decode($request->getContent());
        $id = $requestData->orderId;
        $output = array();
      
        $em1 = $this->get('doctrine')->getManager();
        $orderObj = $em1->getRepository('IbtikarTaniaModelBundle:Order')->find($id);
        if(!empty($id) ){ 
            $addressId = $orderObj->getUserAddressId();
            if(!empty($addressId)){ 
                $em1 = $this->get('doctrine')->getManager();

                $cityArea = $this->getCityAreaPerLatLong($requestData->latitude, $requestData->longitude);
                if(!$cityArea){
                    //return $apiOperations->getErrorJsonResponse($this->get('translator')->trans("Not Supported Area"));
                    $output['status'] = false;
                    $output['message'] = 'Your area is not supported';
                }
                else
                {
                    $city = $cityArea->getCity();
                    $orderObj = $em1->getRepository('IbtikarTaniaModelBundle:Order')->find($id);
                    if($requestData->address!='DEFAULT')
                    $orderObj->setAddress($requestData->address);
                    $orderObj->setLatitude($requestData->latitude);
                    $orderObj->setLongitude($requestData->longitude);
                    $orderObj->setCity($city);
                    $orderObj->setCityArea($cityArea);
                    $orderObj->setCityAreaNameEn($cityArea->getNameEn());
                    $orderObj->setCityAreaNameAr($cityArea->getNameAr());
                    //$orderObj->setType('USER');
                    $orderObj->setAddressVerified('1');
                    $em1->flush();  
                    $em2 = $this->get('doctrine')->getManager();
                    $userAddres = $em2->getRepository('IbtikarTaniaModelBundle:UserAddress')->find($addressId);
                    
                    $userAddres->setTitle('Default Address');
                    if($requestData->address!='DEFAULT')
                    $userAddres->setAddress($requestData->address);
                    $userAddres->setLatitude($requestData->latitude);
                    $userAddres->setLongitude($requestData->longitude);
                    $userAddres->setType('USER');
                    $res = $em2->flush();
                    $output = new Success();
                }
            }else{
                $output['status'] = false;
                $output['message'] = '1';
            }
        }else{
            $output['status'] = false;
            $output['message'] = '2';
        }
        return new JsonResponse($output);
    }

    public function unverifiedAddressListAction(Request $request) {

        $apiOperations = $this->get('api_operations');
        $em = $this->getDoctrine()->getManager();
        //$sortyBy = $request->getLocale() === 'ar' ? 'nameAr' : 'nameEn';
        $orders = $em->getRepository("IbtikarTaniaModelBundle:Order")->findBy(array('address_verified'=> '0','status'=> 'new'));
       
        return $apiOperations->getJsonResponseForObject($orders);
        //return new JsonResponse($output);
    }
}
