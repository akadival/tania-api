<?php

namespace AppBundle\Controller;

use AppBundle\Controller\DefaultController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Description of StaticPageController
 *
 */
class StaticPageController extends DefaultController {

    /**
     * get Static Page by name
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="page", "dataType"="string", "required"=true}
     *  },
     *
     *  section="Static Pages",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      404="Returned if the user was not found",
     *      500="Returned if there is an internal server error"
     *  }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getByNameAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        /* @var $apiOperations \AppBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');

        $translator = $this->get('translator');

        $page = $em->getRepository('IbtikarTaniaModelBundle:StaticPage')->findOneBy(array('name'=> $request->get('page')));
        if(!$page){
            return $apiOperations->getErrorJsonResponse($translator->trans("No page was found"));
        }

        $response = new \stdClass();
        $response->page_id = $request->headers->get('accept-language') == "en" ? $page->getIdentifierEn() : $page->getIdentifierAr();
        $response->page_id_ar = $page->getIdentifierAr();
        $response->page_id_en = $page->getIdentifierEn();

        return $apiOperations->getSuccessDataJsonResponse($response);
    }

}
