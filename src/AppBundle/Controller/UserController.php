<?php

namespace AppBundle\Controller;

use AppBundle\Service\UserOperations;
use Doctrine\Common\Util\Debug;
use AppBundle\Service\APIOperations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\Extension\Core\Type as formInputsTypes;
use AppBundle\APIResponse\RegisterUserFail;
use AppBundle\APIResponse\RegisterUserSuccess;
use Symfony\Component\HttpFoundation\Request;
use Ibtikar\TaniaModelBundle\Entity\User;
use AppBundle\APIResponse\Fail;
use AppBundle\APIResponse\UserToken;
use Ibtikar\TaniaModelBundle\Entity\ContactUs;
use AppBundle\APIResponse\ValidationErrorsResponse;
use AppBundle\APIResponse\MainResponse;
use AppBundle\APIResponse\User\Address;
use Ibtikar\TaniaModelBundle\Entity\UserAddress;
use AppBundle\APIResponse\NotFound;
use AppBundle\APIResponse\AccessDenied;
use AppBundle\APIResponse\Success;
use AppBundle\APIResponse\ContactTania\ResponseSuggestionList;
use AppBundle\APIResponse\ContactTania\ResponseSuggestion;
use AppBundle\APIResponse\ContactTania\ResponseComplain;
use AppBundle\APIResponse\ContactTania\ResponseComplainList;
use Ibtikar\TaniaModelBundle\Entity\Order;
use AppBundle\APIResponse\RemainingTime;
use AppBundle\Event\UserCreatedEvent;
use Symfony\Component\Process\Process;
use Ibtikar\ShareEconomyToolsBundle\APIResponse as ToolsBundleAPIResponses;

class UserController extends Controller
{

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
    }

    /**
     * Login with an existing user
     *
     * @ApiDoc(
     *  resource=true,
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="phone", "dataType"="string", "required"=true},
     *      {"name"="password", "dataType"="string", "required"=true}
     *  },
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the login information was not correct",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200="AppBundle\APIResponse\SuccessLoggedInUser",
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @return JsonResponse
     */
    public function loginAction()
    {
        // The security layer will intercept this request it should never reach here
        return new JsonResponse(array('code' => 401, 'message' => 'Bad credentials'));
    }

    /**
     * Register a customer to the system
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Register a customer to the system",
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="fullName", "dataType"="string", "required"=true},
     *      {"name"="email", "dataType"="string", "required"=true},
     *      {"name"="phone", "dataType"="string", "required"=true},
     *      {"name"="userPassword", "dataType"="string", "required"=true},
     *      {"name"="city", "dataType"="integer", "required"=true},
     *      {"name"="address", "dataType"="integer", "required"=true},
     *      {"name"="neighborhood", "dataType"="string", "required"=false},
     *      {"name"="longitude", "dataType"="integer", "required"=true},
     *      {"name"="latitude", "dataType"="integer", "required"=true},
     *      {"name"= "locale", "dataType"="string", "required"=true, "format"="ar|en"}
     *  },
     *  statusCodes = {
     *      200 = "Returned on success",
     *      400 = "Validation failed.",
     *      500 = "Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200 = "AppBundle\APIResponse\RegisterUserSuccess",
     *      400 = "AppBundle\APIResponse\RegisterUserFail",
     *      500 = "AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function registerCustomerAction(Request $request)
    {
        /* @var $userOperations AppBundle\Service\UserOperations */
        $userOperations = $this->get('user_operations');
        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        /* @var $translator \Symfony\Component\Translation\DataCollectorTranslator */
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();

        $password = $request->get('userPassword');
        if (trim($request->get('userPassword')) == "") {
            $password = "";

        }


        $city = '';
        if($request->get('city'))
        {
            $city = $em->getRepository('IbtikarTaniaModelBundle:City')->findOneById($request->get('city'));

            if (!$city) {
                $errorMsg = $translator->trans("city_not_found", array(), 'api_error_message');
                return $apiOperations->getSingleErrorJsonResponse($errorMsg);
            }
        }
        $phone = trim($request->get('phone'));
        if (substr($phone, 0, 3) == "966") {
            $phone = '+966'. substr($phone, 3);
        }

        $user = new User();
        $user->setFullName(trim($request->get('fullName')));
        $user->setEmail(trim($request->get('email')));
        $user->setPhone($phone);
        $user->setNeighborhood(trim($request->get('neighborhood')));
        $user->setAddress(trim($request->get('address')));
        $user->setLongitude(trim($request->get('longitude')));
        $user->setLatitude(trim($request->get('latitude')));
        $user->setUserPassword($password);
        $user->setRoles([User::ROLE_CUSTOMER]);
        $user->setSystemUser(false);
        $user->setLocale(trim($request->get('locale')));

        if($city)
        {
            $user->setCity($city);
            $user->setCountry($city->getCountry());
        }
        else
            $user->setCity(NULL);


        $validationMessages = $apiOperations->validateObject($user, ['signup']);

        if (count($validationMessages)) {
            $output = new RegisterUserFail();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (!$this->get('google.phone_number')->validatePhoneNumber($phone)) {
            $errorMsg = $translator->trans('Please enter a valid phone number', array(), 'validators');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg . '`' . $phone . '`');
        }

        $phoneVerificationCode = $userOperations->addNewVerificationCode($user);
        $userOperations->generateNewEmailVerificationToken($user);

        $apiKey = $request->headers->get('X-Api-Key');

        if($apiKey == $this->getParameter('ios_api_key'))
            $user->setDeviceType(User::IOS_USER_TYPE);
        else if($apiKey == $this->getParameter('android_api_key'))
            $user->setDeviceType(User::ANDROID_USER_TYPE);

        $user->setIsSynced(false);
        $em->persist($user);

        try {
            $em->flush();
            $output = new RegisterUserSuccess();
            $output->user = $userOperations->getUserData($user);

        } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $exc) {
            return $this->registerCustomerAction($request);
        } catch (\Exception $exc) {
            $output = new Fail();
            $output->message = $this->get('translator')->trans("something_went_wrong", array()) . 'execption: '. $exc->getMessage();
            $this->get('logger')->critical($exc->getMessage());
        }

        if ($output->status) {
            try {
                // send phone verification code
                $userOperations->sendVerificationCodeMessage($user, $phoneVerificationCode);

                // send verification email
                $this->get('tania.email_sender')->sendEmailVerification($user, $this->getParameter('project_name'));
                $message = str_replace(array('%project%', '%code%', '%validationTimeInMinutes%'), array($this->getParameter('nexmo_from_name'), $phoneVerificationCode->getCode(), $this->getParameter('verification_code_expiry_minutes')), 'Verification code for %project% is (%code%), valid for %validationTimeInMinutes% minutes');
                $this->get('tania.email_sender')->sendVerificationCodeEmail($user, $this->getParameter('project_name'), $message);
            } catch (\Exception $exc) {
                echo $exc->getMessage();die;
                $this->get('logger')->critical($exc->getMessage());
            }
        }



        return new JsonResponse($output);
    }

    /**
     * Register a customer to the system
     *
     * @ApiDoc(
     *  resource=true,
     *  description="NEW | Register a customer to the system",
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan"
     *  },
     *  input="AppBundle\APIResponse\User\RequestUser",
     *  statusCodes = {
     *      200 = "Returned on success",
     *      400 = "Validation failed.",
     *      500 = "Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200 = "AppBundle\APIResponse\RegisterUserSuccess",
     *      400 = "AppBundle\APIResponse\RegisterUserFail",
     *      500 = "AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function registerCustomerV2Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $userOperations UserOperations */
        $userOperations = $this->get('user_operations');
        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        /* @var $translator \Symfony\Component\Translation\DataCollectorTranslator */
        $translator = $this->get('translator');
        $requestUser = new \AppBundle\APIResponse\User\RequestUser();

        $apiOperations->bindObjectDataFromJsonRequest($requestUser, $request);

        if($city = $requestUser->city)
        {
            $city = $em->getRepository('IbtikarTaniaModelBundle:City')->findOneById($request->get('city'));

            if (!$city) {
                $errorMsg = $translator->trans("city_not_found", array(), 'api_error_message');
                return $apiOperations->getSingleErrorJsonResponse($errorMsg);
            }
        }

        $apiKey = $request->headers->get('X-Api-Key');
        if(!$requestUser->password){ // Not a website user
            // Random Password
            $requestUser->password = $userOperations->generateRandomStringRegister(6);
        }

        $user = new User();
        $user->setFullName(trim($requestUser->fullName));
        $user->setEmail(trim($requestUser->email));
        $user->setPhone(trim($requestUser->phone));
        $user->setNeighborhood(trim($requestUser->neighborhood));
        $user->setAddress(trim($requestUser->address));
        $user->setLongitude(trim($requestUser->longitude));
        $user->setLatitude(trim($requestUser->latitude));
        $user->setUserPassword($requestUser->password);
        $user->setRoles([User::ROLE_CUSTOMER]);
        $user->setSystemUser(false);
        $user->setLocale(trim($requestUser->locale));

        if($city){
            $user->setCity($city);
            $user->setCountry($city->getCountry());
        } else {
            $user->setCity(NULL);
        }

        $validationMessages = $apiOperations->validateObject($user, ['easy-signup']);

        if (count($validationMessages)) {
            $output = new RegisterUserFail();
            $output->errors = $validationMessages;
            return new JsonResponse($output);
        }

        if (!$this->get('google.phone_number')->validatePhoneNumber($requestUser->phone)) {
            $errorMsg = $translator->trans('Please enter a valid phone number', array(), 'validators');
            return $apiOperations->getSingleErrorJsonResponse($errorMsg);
        }

        $phoneVerificationCode = $userOperations->addNewVerificationCode($user);
        if($requestUser->email) {
            $userOperations->generateNewEmailVerificationToken($user);
        }

        $apiKey = $request->headers->get('X-Api-Key');

        if($apiKey == $this->getParameter('ios_api_key'))
            $user->setDeviceType(User::IOS_USER_TYPE);
        else if($apiKey == $this->getParameter('android_api_key'))
            $user->setDeviceType(User::ANDROID_USER_TYPE);

        $user->setIsSynced(false);
        $em->persist($user);

        try {
            $em->flush();
            $output = new RegisterUserSuccess();
            $output->user = $userOperations->getUserData($user);

        } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $exc) {
            return $this->registerCustomerAction($request);
        } catch (\Exception $exc) {
            $output = new Fail();
            $output->message = $this->get('translator')->trans("something_went_wrong", array()) . 'execption: '. $exc->getMessage();
            $this->get('logger')->critical($exc->getMessage());
        }

//        $output->verCode = $phoneVerificationCode->getCode();
        if ($output->status) {
            try {
                // send phone verification code
                $userOperations->sendVerificationCodeMessage($user, $phoneVerificationCode);

                if($requestUser->email) {
                    // send verification email
                    $this->get('tania.email_sender')->sendEmailVerification($user, $this->getParameter('project_name'));
                    $message = str_replace(array('%project%', '%code%', '%validationTimeInMinutes%'), array($this->getParameter('nexmo_from_name'), $phoneVerificationCode->getCode(), $this->getParameter('verification_code_expiry_minutes')), 'Verification code for %project% is (%code%), valid for %validationTimeInMinutes% minutes');
                    $this->get('tania.email_sender')->sendVerificationCodeEmail($user, $this->getParameter('project_name'), $message);
                }
            } catch (\Exception $exc) {
                echo $exc->getMessage();die;
                $this->get('logger')->critical($exc->getMessage());
            }
        }

        return new JsonResponse($output);
    }

    /**
     * This actually generates a new password and sends it in an sms to the provided phone
     *
     * @ApiDoc(
     *  resource=true,
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan"
     *  },
     *  input="AppBundle\APIResponse\User\RequestLoginPhone",
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the login information was not correct",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200="AppBundle\APIResponse\SuccessLoggedInUser",
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     * @return JsonResponse
     */
    public function sendOneTimePasswordAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $userOperations UserOperations */
        $userOperations = $this->get('user_operations');
        /* @var $apiOperations \Ibtikar\ShareEconomyToolsBundle\Service\APIOperations */
        $apiOperations = $this->get('api_operations');
        /* @var $translator \Symfony\Component\Translation\DataCollectorTranslator */
        $translator = $this->get('translator');
        $requestLoginPhone = new \AppBundle\APIResponse\User\RequestLoginPhone();

        $apiOperations->bindObjectDataFromJsonRequest($requestLoginPhone, $request);

        $errorMessage = "";

        $requestPhone = $requestLoginPhone->phone;
        if(!$requestPhone){
            $errorMessage = $translator->trans('Please fill the mandatory field first.', array(), 'security');
        }

        /* @var User $user */
        $user = $em->getRepository('IbtikarTaniaModelBundle:User')->getUserByPhone($requestPhone);

        if(!$user) {
            $errorMessage = $translator->trans('The entered phone is not registered, please enter again.', array(), 'security');
        }

        if($errorMessage != ""){
            return new JsonResponse(array('code' => 401, 'message' => $errorMessage));
        }

        $now = new \DateTime();

        if(!$userOperations->canRequestOneTimePassword($user)){
            return new JsonResponse(array('code' => 422, 'message' => $translator->trans('max_login_password_requests_error')));
        }

        if (null !== $user->getlastLoginPasswordRequestDate() && $user->getlastLoginPasswordRequestDate()->format('Ymd') == $now->format('Ymd')) {
            $user->setLoginPasswordRequests($user->getLoginPasswordRequests() + 1); //FIXME
        } else {
            $user->setLoginPasswordRequests(1);
        }
        $em->persist($user);
        $em->flush();

        $user->setlastLoginPasswordRequestDate($now);

        $password = $userOperations->generateRandomString();
        //$password = "4231";
        $user->setUserPassword($password);
        $user->setValidPassword();

        $em->persist($user);

        $em->flush();

        //FIXME uncomment below line before going live
        $userOperations->sendPasswordMessage($user, $password);

        // The security layer will intercept this request it should never reach here
        return new JsonResponse(array('code' => 200, 'message' => 'Success', 'otp' => $password));
    }

    /**
     *
     * @param Request $request
     */
    public function verifyEmailAction(Request $request)
    {
        $em   = $this->getDoctrine()->getManager();
        $user = $em->getRepository('IbtikarTaniaModelBundle:User')->findOneBy(['email' => $request->get('email'), 'emailVerificationToken' => $request->get('token')]);

        if (!$user) {
            throw $this->createNotFoundException();
        }

        $now = new \DateTime();
        $translator = $this->get('translator');

        if ($user->getEmailVerificationTokenExpiryTime() < $now) {
            $this->addFlash('error', $translator->trans('The link expired please request a new one'));
        } else {
            $this->get('user_operations')->verifyUserEmail($user);
            $this->addFlash('success', $translator->trans('Email verified successfully'));
        }

        return $this->render('IbtikarShareEconomyDashboardDesignBundle:Layout:not_loggedin.html.twig');
    }

    /**
     * reset password
     *
     * @ApiDoc(
     *  resource=true,
     *  description="send forgot password email",
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="email", "dataType"="string", "required"=true}
     *  },
     *  statusCodes={
     *      200="Returned on success",
     *      403="Returned if the api key is not valid",
     *      422="Returned if there is a validation error in the sent data",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200="AppBundle\APIResponse\Success",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      422="AppBundle\APIResponse\Fail",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendResetPasswordEmailAction(Request $request)
    {
        $userOperations = $this->get('user_operations');
        $message = $userOperations->sendResetPasswordEmail($request->get('email'));

        if ($message === 'success') {
            return $userOperations->getSuccessJsonResponse();
        }
        return $userOperations->getSingleErrorJsonResponse($message);
    }

    /**
     * @param Request $request
     * @param string $email
     * @param string $token
     * @return Response
     */
    public function resetPasswordAction(Request $request, $email, $token)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('IbtikarTaniaModelBundle:User')->findOneBy(array('email' => $email, 'changePasswordToken' => $token));
        if (!$user) {
            throw $this->createNotFoundException();
        }
        $currentTime = new \DateTime();
        $translator = $this->get('translator');
        if ($user->getChangePasswordTokenExpiryTime() < $currentTime) {
            $this->addFlash('error', $translator->trans('The change password link expired please request a new one.'));
            return $this->render('IbtikarShareEconomyDashboardDesignBundle:Layout:not_loggedin.html.twig');
        }
        if (!$user->getEmailVerified()) {
            $this->get('user_operations')->verifyUserEmail($user);
        }
        $formBuilder = $this->createFormBuilder($user, array(
                'validation_groups' => 'resetPassword',
            ))
            ->setMethod('POST')
            ->add('userPassword', formInputsTypes\RepeatedType::class, array(
                'type' => formInputsTypes\PasswordType::class,
                'invalid_message' => 'The password fields must match',
                'required' => true,
                'first_options' => array('label' => 'Password', 'attr' => array('autocomplete' => 'off')),
                'second_options' => array('label' => 'Repeat Password', 'attr' => array('autocomplete' => 'off')),
            ))
            ->add('Reset your password', formInputsTypes\SubmitType::class);
        $form = $formBuilder->getForm();
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $user->setValidPassword();
                $user->setChangePasswordToken(null);
                $user->setForgetPasswordRequests(0);
                $user->setChangePasswordTokenExpiryTime(null);
                $em->flush($user);
                $this->addFlash('success', $translator->trans('Password changed sucessfully'));
                return $this->render('IbtikarShareEconomyDashboardDesignBundle:Layout:not_loggedin.html.twig');
            }
        }
        return $this->render('IbtikarShareEconomyDashboardDesignBundle:Layout:not_loggedin_form.html.twig', array(
                'form' => $form->createView(),
                'title' => $translator->trans('Reset your password'),
        ));
    }

    /**
     * Send Complain or suggestion to tania
     *
     * @ApiDoc(
     *  resource=true,
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan",
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  authentication=true,
     *  parameters={
     *      {"name"="title", "dataType"="string", "required"=true},
     *      {"name"="description", "dataType"="string", "required"=true},
     *      {"name"="order", "dataType"="integer", "required"=false, "description" = "required if complain"},
     *      {"name"="type", "dataType"="string", "required"=true, "description" = "driver-contact/complain/suggestion"}
     *  },
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     *
     * @return JsonResponse
     */
    public function contactTaniaAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        /* @var $translator \Symfony\Component\Translation\DataCollectorTranslator */
        $translator = $this->get('translator');

        $type = trim($request->get('type'));

        $contact = new ContactUs();
        $contact->setTitle(trim($request->get('title')));
        $contact->setDescription(trim($request->get('description')));
        $contact->setType($type);
        $contact->setClosed(0); // 0 default value instead of null
        $user = $this->getUser();

        $contact->setUser($user);


        $validationMessages = [];
        $errors             = $this->get('validator')->validate($contact);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $validationMessages[$error->getPropertyPath()] = $error->getMessage();
            }
        }

        if ($type && !in_array($type, ContactUs::$contactTypes)) {
            $validationMessages['type'] = $translator->trans('type_not_exist', array(), 'contactus');
        }
        else
        {
            if($type == ContactUs::$contactTypes['complain'])
            {
                if(!$request->get('order'))
                    $validationMessages['order'] = $translator->trans('fill_mandatory_field', array(), 'validators');
                else
                {
                    $order = $em->getRepository('IbtikarTaniaModelBundle:Order')->find(trim($request->get('order')));
                    if(!$order)
                        $validationMessages['order'] = $translator->trans('order_not_exist', array(), 'contactus');
                    else
                        $contact->setOrder($order);
                }
            }
        }

        if (count($validationMessages)) {
            $output         = new ValidationErrorsResponse();
            $output->errors = $validationMessages;
        } else {

            $em->persist($contact);

            try {
                $em->flush();
                $output = new MainResponse();
                $output->message = $translator->trans('Success.');
            } catch (\Exception $exc) {
                $this->get('logger')->critical($exc->getMessage());
                $output          = new MainResponse();
                $output->status  = false;
                $output->code    = 500;
                $output->message = $this->get('translator')->trans("something_went_wrong");
            }
        }

        return new JsonResponse($output);
    }

    /**
     * Add address to the user addresses list
     *
     * @ApiDoc(
     *  resource=true,
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="title", "dataType"="string", "required"=false},
     *      {"name"="address", "dataType"="string", "required"=true},
     *      {"name"="longitude", "dataType"="integer", "required"=true},
     *      {"name"="latitude", "dataType"="integer", "required"=true},
     *      {"name"="customerGroup", "dataType"="string", "required"=false}
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     *
     * @return JsonResponse
     */
    public function addressAddAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $userOperations = $this->get('user_operations');
        $apiOperations = $this->get('api_operations');

        $user = $this->getUser();

        $address = new Address();

        $validationErrorsResponse = $apiOperations->bindAndValidateObjectDataFromRequest($address, $request);
        if ($validationErrorsResponse) {
            return $validationErrorsResponse;
        }

        $userAddress = new UserAddress();
        $userAddress->setAddress(trim($address->address));
        $userAddress->setLongitude(trim($address->longitude));
        $userAddress->setLatitude(trim($address->latitude));
        $userAddress->setTitle(trim($address->title));
        $userAddress->setCustomerGroup(trim($address->customerGroup));

        $user->addAddress($userAddress);
        $em->persist($user);
        $em->flush();
        $address->id = $userAddress->getId();
        $output = new Success();
        $output->address = $address;
        return new JsonResponse($output);
    }

    /**
     * Delete address from the user addresses list
     *
     * @ApiDoc(
     *  resource=true,
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="address", "dataType"="integer", "required"=true},
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @author Mahmoud Mostafa <mahmoud.mostafa@ibtikar.net.sa>
     *
     * @return JsonResponse
     */
    public function addressDeleteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $userOperations = $this->get('user_operations');

        $address = $em->getRepository('IbtikarTaniaModelBundle:UserAddress')->findOneBy(array('user' => $this->getUser()->getId(), 'id' => trim($request->get('address'))));


        if (!$address) {
            $output = new NotFound();
            return new JsonResponse($output);
        }

        $em->remove($address);
        $em->flush();
        $translator = $this->get('translator');
        return $userOperations->getSuccessJsonResponse($translator->trans('Successfully deleted'));
    }


    /**
     * Update user information
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  description="Update user information",
     *  section="User",
     *  parameters={
     *      {"name"="fullName", "dataType"="string", "required"=true},
     *      {"name"="email", "dataType"="string", "required"=false},
     *      {"name"="phone", "dataType"="string", "required"=true},
     *      {"name"="city", "dataType"="integer", "required"=true},
     *      {"name"="address", "dataType"="integer", "required"=true},
     *      {"name"="neighborhood", "dataType"="string", "required"=false},
     *      {"name"="locale", "dataType"="string", "required"=false, "format"="ar|en"},
     *      {"name"="longitude", "dataType"="integer", "required"=true},
     *      {"name"="latitude", "dataType"="integer", "required"=true}
     *  },
     *  statusCodes = {
     *      200 = "Returned on success",
     *      400 = "Validation failed.",
     *      500 = "Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200 = "AppBundle\APIResponse\RegisterUserSuccess",
     *      400 = "AppBundle\APIResponse\RegisterUserFail",
     *      500 = "AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function editProfileAction(Request $request)
    {
        $userOperations = $this->get('user_operations');

        $user           = $this->getUser();
        $oldEmail       = $user->getEmail();
        $oldPhone       = $user->getPhone();

        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $apiOperations = $this->get('api_operations');

        $city = '';
        if($request->get('city') && $request->get('city') != "null")
        {
            $city = $em->getRepository('IbtikarTaniaModelBundle:City')->findOneById($request->get('city'));

            if (!$city) {
                $errorMsg = $translator->trans("city_not_found", array(), 'api_error_message');
                return $apiOperations->getSingleErrorJsonResponse($errorMsg);
            }
        }

        if($city)
        {
            $user->setCity($city);
            $user->setCountry($city->getCountry());
        }
        else
            $user->setCity(NULL);

        $user->setFullName(trim($request->get('fullName')));
        if ($request->get('email') !== null) {
            $user->setEmail(trim($request->get('email')));
        }
        $user->setPhone(trim($request->get('phone')));
        $user->setNeighborhood(trim($request->get('neighborhood')));
        $user->setAddress(trim($request->get('address')));
        if($request->get('longitude') && $request->get('latitude')) {
            $user->setLongitude(trim($request->get('longitude')));
            $user->setLatitude(trim($request->get('latitude')));
        }
        $userLocale = trim($request->get('locale'));
        if ($userLocale && in_array($userLocale, array('ar', 'en'))) {
            $user->setLocale($userLocale);
        }

        $validationMessages = $this->get('user_operations')->validateObject($user, ['easy-edit']);

        if (!$this->get('google.phone_number')->validatePhoneNumber($request->get('phone'))) {
            $errorMsg = $translator->trans('Please enter a valid phone number', array(), 'validators');
            $validationMessages['phone'] = $errorMsg;
        }

        if (count($validationMessages)) {
            $output         = new RegisterUserFail();
            $output->errors = $validationMessages;
        } else {
            $userOperations->updateUserInformation($user, $oldEmail, $oldPhone);
            $output       = new RegisterUserSuccess();
            $output->user = $userOperations->getUserData($user);
            $output->user['token'] = $this->get('lexik_jwt_authentication.encoder')->encode(['phone' => $user->getPhone()]);
        }

        return new JsonResponse($output);
    }

    /**
     * change user password
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  description="change user password",
     *  tags={
     *     "user application"="DarkCyan",
     *     "driver application"="Chocolate",
     *     "stable"="green"
     *  },
     *  section="User",
     *  parameters={
     *      {"name"="oldPassword", "dataType"="string", "required"=true},
     *      {"name"="userPassword", "dataType"="string", "required"=true}
     *  },
     *  statusCodes = {
     *      200 = "Returned on success",
     *      400 = "Validation failed.",
     *      500 = "Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200 = "AppBundle\APIResponse\RegisterUserSuccess",
     *      400 = "AppBundle\APIResponse\RegisterUserFail",
     *      500 = "AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function changePasswordAction(Request $request)
    {
        $userOperations = $this->get('user_operations');

        $em   = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $user->setOldPassword($request->get('oldPassword'));
        $user->setUserPassword($request->get('userPassword'));

        $validationMessages = $userOperations->validateObject($user, ['changePassword']);

        if (count($validationMessages)) {
            $output         = new RegisterUserFail();
            $output->errors = $validationMessages;
        } else {
            if ($request->get('oldPassword') == $request->get('userPassword')) {
                $output = new RegisterUserFail();
                $output->errors = ['oldPassword' => $this->get('translator')->trans('Old Password is same as new', array(), 'messages')];
                return new JsonResponse($output);
            }
            $user->setValidPassword();
            $em->flush();

            return $userOperations->getSuccessJsonResponse();
        }

        return new JsonResponse($output);
    }

    /**
     * resend phone verification code
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Resend phone verification code validity",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="User",
     *  statusCodes = {
     *      200 = "Returned on success",
     *      400 = "Validation failed.",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200 = "AppBundle\APIResponse\Success",
     *      400 = "AppBundle\APIResponse\Fail",
     *      500 = "AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function resendVerificationCodeAction(Request $request, $id)
    {
        $userOperations = $this->get('user_operations');
        $em   = $this->getDoctrine()->getManager();
        $user = $em->getRepository('IbtikarTaniaModelBundle:User')->find($id);

        if ($user) {
            if ($user->getIsPhoneVerified()){
                $output          = new Fail();
                $output->message = $this->get('translator')->trans('phone_already_verified');
            } elseif (!$userOperations->canRequestPhoneVerificationCode($user)){
                $output          = new Fail();
                $output->message = $this->get('translator')->trans('reach_max_phone_verification_requests_error');
            } else {
                $phoneVerificationCode = $userOperations->addNewVerificationCode($user);
                $em->persist($user);

                if ($userOperations->sendVerificationCodeMessage($user, $phoneVerificationCode)) {
                    $em->flush();
                    $output = new Success();
                } else {
                    $output          = new Fail();
                    $output->message = $this->get('translator')->trans('verification_message_not_sent');
                }
            }
        } else {
            $output          = new Fail();
            $output->message = $this->get('translator')->trans('user_not_found');
        }

        return $this->get('api_operations')->getJsonResponseForObject($output);
    }

    /**
     * List suggestions and complains
     *
     * @ApiDoc(
     *  resource=true,
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  parameters={
     *      {"name"="type", "dataType"="string", "required"=true, "description" = "complain/suggestion"}
     *  },
     *  authentication=true,
     *  statusCodes={
     *      200="Returned on success",
     *      401="Returned if the authorization header is missing or expired",
     *      403="Returned if the api key is not valid",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      401="AppBundle\APIResponse\InvalidCredentials",
     *      403="AppBundle\APIResponse\InvalidAPIKey",
     *      500="AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     *
     * @return JsonResponse
     */
    public function contactTaniaListAction(Request $request) {

        $apiOperations = $this->get('api_operations');
        $translator = $this->get('translator');

        $type = trim($request->get('type'));

        if (!$type || ($type && !in_array($type, ContactUs::$contactTypes))) {
            return $apiOperations->getSingleErrorJsonResponse($translator->trans('type_not_exist', array(), 'contactus'));
        }
        else
        {
            if($type == ContactUs::$contactTypes['complain'])
                $responseList = $this->getComplainList();
            elseif($type == ContactUs::$contactTypes['suggestion'])
                $responseList = $this->getSuggestionList();
        }

        return $apiOperations->getJsonResponseForObject($responseList);
    }

    function getSuggestionList()
    {
        $em = $this->getDoctrine()->getManager();

        $suggestions = $em->getRepository('IbtikarTaniaModelBundle:ContactUs')->findBy(['user' => $this->getUser()->getId(), 'type' => ContactUs::$contactTypes['suggestion']]);

        $suggestionList = new ResponseSuggestionList();

        foreach ($suggestions as $suggestion) {

            $responseSuggestion = new ResponseSuggestion();
            $responseSuggestion->id = $suggestion->getId();
            $responseSuggestion->title = $suggestion->getTitle();
            $responseSuggestion->description = $suggestion->getDescription();


            $suggestionList->suggestions[] = $responseSuggestion;
        }

        return $suggestionList;
    }

    function getComplainList()
    {
        $em = $this->getDoctrine()->getManager();

        $complains = $em->getRepository('IbtikarTaniaModelBundle:ContactUs')->findBy(['user' => $this->getUser()->getId(), 'type' => ContactUs::$contactTypes['complain']]);

        $complainList = new ResponseComplainList();

        foreach ($complains as $complain) {

            $responseComplain = new ResponseComplain();
            $responseComplain->id = $complain->getId();
            $responseComplain->title = $complain->getTitle();
            $responseComplain->description = $complain->getDescription();
            $responseComplain->order = $complain->getOrder()->getId();


            $complainList->complains[] = $responseComplain;
        }

        return $complainList;
    }


    /**
     * check phone verification code validity
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Check phone verification code validity",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  section="User",
     *  statusCodes = {
     *      200 = "Returned on success",
     *      400 = "Validation failed.",
     *      500="Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200 = "Ibtikar\ShareEconomyUMSBundle\APIResponse\UserToken",
     *      400 = "Ibtikar\ShareEconomyUMSBundle\APIResponse\Fail",
     *      500 = "Ibtikar\ShareEconomyToolsBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function checkVerificationCodeAction(Request $request, $id, $code)
    {
        $em               = $this->getDoctrine()->getManager();
        $user             = $em->getRepository('IbtikarTaniaModelBundle:User')->find($id);
        $verificationCode = $em->getRepository('IbtikarTaniaModelBundle:User')->getPhoneVerificationCode($id, $code);

        if ($verificationCode) {
            if ($this->get('phone_verification_code_business')->isValidCode($verificationCode)) {
                $verificationCode->setIsVerified(true);
                $user->setIsPhoneVerified(true);

                $em->flush();

                $output        = new UserToken();
                $output->token = $this->get('lexik_jwt_authentication.encoder')->encode(['phone' => $user->getPhone()]);
            } else {
                $output          = new Fail();
                $output->message = $this->get('translator')->trans('expired_verification_code');
            }
        } else {
            $output          = new Fail();
            $output->message = $this->get('translator')->trans('wrong_verification_code');
        }

        return $this->get('api_operations')->getJsonResponseForObject($output);
    }

    /**
     * get verification code remaining validity time in seconds
     *
     * @ApiDoc(
     *  resource=true,
     *  description="get verification code remaining validity time in seconds",
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  statusCodes = {
     *      200 = "Returned on success",
     *      404 = "Returned if the page was not found",
     *      500 = "Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200 = "AppBundle\APIResponse\RemainingTime",
     *      404 = "AppBundle\APIResponse\NotFound",
     *      500 = "AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getVerificationRemainingTimeAction(Request $request, $id)
    {
        /* @var $user \Ibtikar\ShareEconomyUMSBundle\Entity\BaseUser */
        $user = $this->getDoctrine()->getManager()->getRepository('IbtikarTaniaModelBundle:User')->find($id);
        if (!$user) {
            return $this->get('api_operations')->getNotFoundErrorJsonResponse($this->get('translator')->trans('user_not_found'));
        }
        $code = $user->getPhoneVerificationCodes()->first();
        if (!$code) {
            return $this->get('api_operations')->getNotFoundErrorJsonResponse($this->get('translator')->trans('code_not_found'));
        }
        $output          = new RemainingTime();
        $output->seconds = $this->get('phone_verification_code_business')->getValidityRemainingSeconds($code);
        return $this->get('api_operations')->getJsonResponseForObject($output);
    }

    /**
     * Get user information
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  tags={
     *     "user application"="DarkCyan",
     *     "stable"="green"
     *  },
     *  description="Update user information",
     *  section="User",
     *  statusCodes = {
     *      200 = "Returned on success",
     *      500 = "Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      403 = "Unauthorized",
     *      500 = "AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserDataAction(Request $request, $id)
    {
        $userOperations = $this->get('user_operations');
        /* @var APIOperations $apiOperations */
        $apiOperations = $this->get('user_operations');

        $user           = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        /* @var User $userObj */
        $userObj = $em->getRepository('IbtikarTaniaModelBundle:User')->findOneById($id);

        if($user->getId() != $userObj->getId()){
            return $apiOperations->getAccessDeniedJsonResponse();
        }

        $output       = new RegisterUserSuccess();
        $output->user = $userOperations->getUserData($user);

        return new JsonResponse($output);
    }

        /**
     * get password remaining validity time in seconds
     *
     * @ApiDoc(
     *  resource=true,
     *  description="get password remaining validity time in seconds",
     *  section="User",
     *  tags={
     *     "user application"="DarkCyan"
     *  },
     *  parameters={
     *      {"name"="phone", "dataType"="string", "required"=true}
     *  },
     *  statusCodes = {
     *      200 = "Returned on success",
     *      404 = "Returned if the page was not found",
     *      500 = "Returned if there is an internal server error"
     *  },
     *  responseMap = {
     *      200 = "AppBundle\APIResponse\RemainingTime",
     *      404 = "AppBundle\APIResponse\NotFound",
     *      500 = "AppBundle\APIResponse\InternalServerError"
     *  }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getPasswordValidityRemainingTimeAction(Request $request)
    {
        $phone = "+" . $request->get('phone');

        /* @var $user User */
        $user = $this->getDoctrine()->getManager()->getRepository('IbtikarTaniaModelBundle:User')->findOneBy(['phone' => $phone]);
        if (!$user) {
            return $this->get('api_operations')->getNotFoundErrorJsonResponse($this->get('translator')->trans('user_not_found'));
        }
        $code = $user->getPhoneVerificationCodes()->first();
        if (!$code) {
            return $this->get('api_operations')->getNotFoundErrorJsonResponse($this->get('translator')->trans('code_not_found'));
        }
        $output          = new RemainingTime();

        $output->seconds = $user->getValidityRemainingSeconds($this->getParameter('verification_code_expiry_minutes'));
        return $this->get('api_operations')->getJsonResponseForObject($output);
    }
}