<?php

namespace AppBundle\Security\Http\Authentication;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use AppBundle\Service\UserOperations;

/**
 * AuthenticationFailureHandler.
 *
 * @author Dev Lexik <dev@lexik.fr>
 */
class DriverAuthenticationFailureHandler implements AuthenticationFailureHandlerInterface
{
    /* @var $tranlator \Symfony\Component\Translation\TranslatorInterface */
    private $translator;

    /* @var $userOperations  */
    private $userOperations;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher, TranslatorInterface $translator, UserOperations $userOperations)
    {
        $this->dispatcher = $dispatcher;
        $this->translator = $translator;
        $this->userOperations = $userOperations;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $event = new AuthenticationFailureEvent($exception, new JWTAuthenticationFailureResponse());

        $this->dispatcher->dispatch(Events::AUTHENTICATION_FAILURE, $event);

        $errorMessage = $exception->getMessage();
        if (!$request->get('password')) {
            $errorMessage = $this->translator->trans('Please fill the mandatory field first.', array(), 'security');
        } else {
//            var_dump($exception->getMessage());die;
            if ($exception instanceof UsernameNotFoundException) {
                $errorMessage = $this->translator->trans('The entered username is not registered, please enter again.', array(), 'security');
                if ($exception->getUsername() === AuthenticationProviderInterface::USERNAME_NONE_PROVIDED) {
                    $errorMessage = $this->translator->trans('Please fill the mandatory field first.', array(), 'security');
                }
            }
            if ($exception instanceof BadCredentialsException) {
                $errorMessage = $this->translator->trans('The presented password is invalid.', array(), 'security');
            }
        }
        $event->setResponse($this->userOperations->getInvalidCredentialsJsonResponse($errorMessage));


        return $event->getResponse();
    }
}
