<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Ibtikar\TaniaModelBundle\Entity\User;
/**
 * Description of UserCreatedEvent
 *
 * @author abdelrahman.allam@ibtikar.net.sa
 */
class UserCreatedEvent extends Event{

    protected $user;

    const NAME = 'user.created';

    function __construct(User $user) {
        $this->user = $user;
    }

    public function getUser() {
        return $this->user;
    }

}
