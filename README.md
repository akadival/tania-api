Tania-API
=========

Project created on May 14, 2017, 1:41 pm.

***************************************************************************

## Installation instructions:

### 1. Setup git post merge hook

```
ln -s /var/www/html/Tania-API/var/scripts/post-merge /var/www/html/Tania-API/.git/hooks/post-merge
```

### 2. Run the post merge

```
.git/hooks/post-merge
```

### 3. Setup cron jobs found in /var/www/html/Tania-API/var/scripts/crontab